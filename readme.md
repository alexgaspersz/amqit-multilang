## AMQIT Laravel Boilerplate (Current: Laravel 7.x) 

Laravel Boilerplate for develop multi language website/application that provides you with a massive head start on any size web application.
It comes with a full featured access control system out of the box with an easy to learn and is built on a Twitter Bootstrap foundation with a front and backend architecture. 
We have put a lot of work into it and we hope it serves you well and saves you time!

### Demo Credentials
**Demo:** [AMQIT Laravel Boilerplate](http://dev.amqit.id/multilang)\
**User:** admin  
**Password:** admin123

### Donations

If you would like to help the continued efforts of this project, any size [donations](https://paypal.me/alexgaspersz?locale.x=en_US) are welcomed and highly appreciated.

## License
MIT License: [amqit.mit-license.org](https://amqit.mit-license.org/)\
Developed by [AMQIT Consultant](http://www.amqit.id)\
&copy; Copyright 2020
