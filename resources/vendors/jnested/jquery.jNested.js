/*
 * jQuery Nested Selection Plugin
 * Alex Gaspersz
 * amqit.consultant@gmail.com
 * Latest Release: Aug 2019
 */
(function (a) {
    a.fn.jNested = function (b, d) {
        function h(b, d, e, f, g, m) {
            a.ajax({
                type: "GET",
                dataType: "JSON",
                url: d + e,
                success: function (a) {
                    var d = "";
                    if (a.length == 0) {
                        d += '<option value=""></option>';
                        b.html(d)
                    } else {
                        if (f != "" && f != null) {
                            d += '<option value="">' + f + "</option>";
                        }
                        for (var e = 0; e < a.length; e++) {
                            c = a[e];
                            if (c['child'] && c['child'].length > 0) {
                                //check if option group if true or false
                                //false: parent option can be selected
                                //true: default option group will be implemented
                                if (m == false) {
                                    if ($.isArray(g) && $.inArray(c[0], g) !== -1) {
                                        selected = ' selected="selected"';
                                    } else {
                                        selected = c[0] == g ? ' selected="selected"' : "";
                                    }
                                    d += '<option value="' + c[0] + '"' + selected + ' style="font-weight: bold">' + c[1] + '</option>';
                                    for (var j = 0; j < c['child'].length; j++) {
                                        k = c['child'][j];
                                        if ($.isArray(g) && $.inArray(k[0], g) !== -1) {
                                            selectedk = ' selected="selected"';
                                        } else {
                                            selectedk = k[0] == g ? ' selected="selected"' : '';
                                        }
                                        d += '<option value="' + k[0] + '"' + selectedk + ">&raquo; " + k[1] + "</option>";
                                    }
                                } else {
                                    d += '<optgroup label="' + c[1] + '">';
                                    for (var j = 0; j < c['child'].length; j++) {
                                        k = c['child'][j];
                                        if ($.isArray(g) && $.inArray(k[0], g) !== -1) {
                                            selectedk = ' selected="selected"';
                                        } else {
                                            selectedk = k[0] == g ? ' selected="selected"' : '';
                                        }
                                        d += '<option value="' + k[0] + '"' + selectedk + ">" + k[1] + "</option>";
                                    }
                                    d += '</optgroup>';
                                }
                            } else {
                                if ($.isArray(g) && $.inArray(c[0], g) !== -1) {
                                    selected = ' selected="selected"';
                                } else {
                                    selected = c[0] == g ? ' selected="selected"' : "";
                                }
                                d += '<option value="' + c[0] + '"' + selected + ">" + c[1] + "</option>";
                            }
                        }
                        b.html(d);
                    }
                    b.trigger("liszt:updated")
                }
            })
        }

        var e = {
            parent: "",
            selected_value: "0",
            parent_value: "",
            initial_text: "",
            option_group: true
        };
        var d = a.extend(e, d);
        var f = a(this);
        if (d.parent != "") {
            var g = a(d.parent);
            g.removeAttr("disabled", "disabled");
            g.bind("change", function (c) {
                f.attr("disabled", "disabled");
                if (a(this).val() != "0" && a(this).val() != "") f.removeAttr("disabled");
                h(f, b, a(this).val(), d.initial_text, d.selected_value)
            })
        }
        h(f, b, d.parent_value, d.initial_text, d.selected_value, d.option_group)
    }
})(jQuery);
