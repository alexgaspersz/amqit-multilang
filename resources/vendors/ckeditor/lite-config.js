/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here.
    config.toolbarGroups = [
        {name: 'clipboard', groups: ['undo']},
        {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
        {name: 'document', groups: ['mode', 'document', 'doctools']}
    ];
    config.removePlugins = 'cloudservices,image,easyimage';
    config.removeButtons = 'Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,ShowBlocks,BGColor,Flash,SpecialChar,Iframe,PageBreak,HorizontalRule,Anchor,Language,CreateDiv,Subscript,Save,NewPage,Preview,Print,Templates,Find,Replace,SelectAll,Scayt,CopyFormatting,BidiRtl,BidiLtr,PasteText,PasteFromWord,Strike,Underline,Blockquote,Image,Table,Smiley,Styles,Font,FontSize,Format,Maximize,About,Superscript';
    config.format_tags = 'p;h1;h2;h3;pre';
    config.extraAllowedContent = 'span;ul;li;table;td;style;*[id];*(*);*{*}';
    config.removeDialogTabs = 'image:advanced;link:advanced';
    config.language = 'id';
};
