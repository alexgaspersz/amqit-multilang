/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */
CKEDITOR.editorConfig = function (config) {
    config.toolbarGroups = [
        {name: 'clipboard', groups: ['clipboard', 'undo']},
        {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
        {name: 'colors', groups: ['colors']},
        {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph']},
        {name: 'links', groups: ['links']},
        {name: 'insert', groups: ['insert']},
        {name: 'styles', groups: ['styles']},
        {name: 'document', groups: ['mode', 'document', 'doctools']},
        {name: 'tools', groups: ['tools']},
        {name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing']},
        {name: 'forms', groups: ['forms']},
        {name: 'others', groups: ['others']},
        {name: 'about', groups: ['about']}
    ];

    config.removeButtons = 'Save,NewPage,Preview,Print,Templates,Find,Replace,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,SelectAll,CreateDiv,BidiLtr,BidiRtl,Language,Anchor,Flash,Smiley,SpecialChar,PageBreak,Iframe,Font,ShowBlocks,About';
    config.removePlugins = 'cloudservices,easyimage';
    config.format_tags = 'p;h1;h2;h3;h4;pre';
    config.extraAllowedContent = 'span;ul;li;table;td;style;*[id];*(*);*{*}';
    config.removeDialogTabs = 'image:advanced;link:advanced';
    config.language = 'id';

    config.extraPlugins = 'uploadimage';
    // config.imageUploadUrl = '/uploader/upload.php?type=Images';
    // config.filebrowserUploadMethod = 'form';
    // config.filebrowserBrowseUrl = '/apps/ckfinder/3.4.5/ckfinder.html';
    // config.filebrowserImageBrowseUrl = '/apps/ckfinder/3.4.5/ckfinder.html?type=Images';
    // config.filebrowserUploadUrl = '/service/uploads/single';
    // config.filebrowserImageUploadUrl = '/service/uploads/single';
    //
    // {
    //     "uploaded": 1,
    //     "fileName": "foo(2).jpg",
    //     "url": "/files/foo(2).jpg",
    //     "error": {
    //         "message": "A file with the same name already exists. The uploaded file was renamed to \"foo(2).jpg\"."
    //     }
    // }
};
