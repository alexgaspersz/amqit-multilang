/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// Loaded before app.js
import '../bootstrap';
import 'moment/moment';
import 'moment/locale/id';
import 'icheck/icheck';
import 'select2';
import '@fancyapps/fancybox';
import '@fortawesome/fontawesome-free/js/all';
import 'jquery-validation/dist/jquery.validate';
import 'jquery-validation/dist/localization/messages_id';
import 'bootstrap-datepicker/js/bootstrap-datepicker';
import 'bootstrap-datepicker/js/locales/bootstrap-datepicker.id';
import 'jquery-timepicker/jquery.timepicker';
import '../../vendors/bootstrap-table/bootstrap-table';
import '../../vendors/bootstrap-table/locale/bootstrap-table-id-ID';
import '../../vendors/bootstrap-table/extensions/mobile/bootstrap-table-mobile';
import '../../vendors/jnested/jquery.jNested';
import 'bootstrap-fileinput';
