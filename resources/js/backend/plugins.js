/**
 * Place any jQuery/helper plugins in here.
 */
(function ($) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#appForm").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        errorElement: "span",
        ignore: ".ignore",
        errorClass: "is-invalid",
        validClass: "is-valid",
        highlight: function (element, errorClass, validClass) {
            $(element.form).find("label[for=" + element.id + "]").addClass("error");
            $(element.form).find("[name=" + element.id + "]").removeClass(validClass);
            $(element.form).find("[name=" + element.id + "]").addClass(errorClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element.form).find("label[for=" + element.id + "]").removeClass("error");
            $(element.form).find("[name=" + element.id + "]").removeClass(errorClass);
            $(element.form).find("[name=" + element.id + "]").addClass(validClass);
        }
    });

    if ($('[data-fancybox]').length > 0) {
        $('[data-fancybox]').fancybox({
            image: {
                protect: true
            },
            afterLoad: function (instance, current) {
                let pixelRatio = window.devicePixelRatio || 1;

                if (pixelRatio > 1.5) {
                    current.width = current.width / pixelRatio;
                    current.height = current.height / pixelRatio;
                }
            }
        });
    }

    /**
     * Ladda button plugin options
     */
    Ladda.bind('button[type=submit]', {timeout: 1000});

    /**
     * iCheck plugin options
     */
    $(".icheckbox, .iradio").iCheck({checkboxClass: 'icheckbox_minimal-grey', radioClass: 'iradio_minimal-grey'});

    /**
     * toastr plugin options
     */
    toastr.options = {
        "progressBar": true,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": true,
        "closeButton": true,
        "escapeHtml": false,
        "timeOut": "6000"
    };

    /**
     * Language switcher
     */
    $('.en').hide();
    $('#locale').on('change', function () {
        let lang = this.value;
        if (lang === 'en') {
            $('.en').show();
            $('.id').hide();
        } else {
            $('.en').hide();
            $('.id').show();
        }
    });

    /**
     * Disable all submit buttons once clicked
     */
    let $form = $('form');
    $form.submit(function () {
        $(this).find('input[type="submit"]').attr('disabled', true);
        $(this).find('button[type="submit"]').attr('disabled', true);
        return true;
    });
    $form.attr('role', 'form').attr('data-toggle', 'validator').attr('novalidate', true);
    $('.input-required').prop('required', true);

    /* ---------- Tooltip ---------- */
    $('[rel="tooltip"],[data-rel="tooltip"],[data-toggle="tooltip"]').tooltip();

    /* ---------- Popover ---------- */
    $('[rel="popover"],[data-rel="popover"],[data-toggle="popover"]').popover();

    let scrollTop = $(".scrollTop");
    $(window).scroll(function () {
        let topPos = $(this).scrollTop();

        if (topPos > 100) {
            $(scrollTop).css("opacity", "0.9");
        } else {
            $(scrollTop).css("opacity", "0");
        }
    });

    $(scrollTop).click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
})(jQuery);
