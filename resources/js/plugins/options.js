let AppOptions = {
    dateFormat: 'YYYY-MM-DD',
    dateTimeFormat: 'YYYY-MM-DD HH:mm',
    dateDisplayFormat: 'DD/MM/YYYY',
    dateMonDisplayFormat: 'DD MMMM YYYY',
    dateTimeDisplayFormat: 'DD/MM/YYYY HH:mm',
    decimalPrecision: 3,
    decimalSeparator: '.',
    thousandSeparator: ',',
    gridPageSize: 15,
    locale: 'id'
};

let datePickerOptions = {
    format: AppOptions.dateDisplayFormat,
    locale: AppOptions.locale
};
let timePickerOptions = {
    format: 'HH:mm',
    locale: AppOptions.locale
};
let fancyboxOptions = {
    loop: true, infobar: true, animationDuration: 400,
    transitionEffect: 'slide', transitionDuration: 600
};

function defaultDateFormatter(value) {
    if (!$.isEmpty(value)) {
        return moment(value, [AppOptions.dateFormat, AppOptions.dateDisplayFormat]).format(AppOptions.dateMonDisplayFormat);
    }

    return '';
}

function defaultDateTimeFormatter(value) {
    if (!$.isEmpty(value)) {
        return moment(value, AppOptions.dateTimeFormat).format(AppOptions.dateTimeDisplayFormat);
    }

    return '';
}

function defaultMonthFormatter(value) {
    if (!$.isEmpty(value)) {
        return moment(value, [AppOptions.monthFormat, AppOptions.monthDisplayFormat]).format(AppOptions.monYearDisplayFormat);
    }

    return '';
}

function splitWordFormatter(value) {
    if (!$.isEmpty(value)) {
        return StringUtil.replace("_", " ", value);
    }
    return '';
}

function splitWordTitleCaseFormatter(value) {
    if (!$.isEmpty(value)) {
        return StringUtil.titleCase(StringUtil.replace("_", " ", value.toLowerCase()));
    }
    return '';
}

function nullAsEmptyString(value) {
    return !$.isEmpty(value) ? value : '';
}

function playAudio(file) {
    if (file === 'alert')
        document.getElementById('audio-alert').play();

    if (file === 'fail')
        document.getElementById('audio-fail').play();
}

let rowNumbering = 0;

function rowNumber(value) {
    let options = $('[data-toggle="table"]').bootstrapTable('getOptions');
    let page = options.pageNumber;
    let pageSize = options.pageSize;
    let totalRows = options.totalRows;
    let offset = ((page - 1) * pageSize) + 1;

    if (rowNumbering > totalRows) {
        rowNumbering = 1;
    }
    if (rowNumbering < offset || rowNumbering > (offset + pageSize)) rowNumbering = offset;

    return rowNumbering++;
}

function clickButton(url, tab) {
    let target = '_self';
    if (tab) {
        target = tab;
    }
    window.open(url, target);
}

function checkAll(value, row) {
    let checked = '';
    if (!row['core']) {
        checked = '<input type="checkbox" name="ids[]" value="' + row['ids'] + '">';
    }
    return checked;
}

function appModal(url, title, classes) {
    $('.modal-title').html(title);
    $('.modal-dialog').addClass(classes);
    $('#app-modal-content').load(url, function (values) {
    });
    $('#app-modal').modal('show');
}

function slugify(text, separator) {
    let divider = '';
    if (separator != null) {
        divider = separator;
    }
    return text.toString().toLowerCase()
        .replace(/\s+/g, divider)           // Replace spaces with -
        .replace(/&/g, divider)         // Replace & with 'and'
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, divider)         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '');            // Trim - from end of text
}

function togglePassword(el) {
    let input = document.getElementById(el);
    let icon = document.getElementById("icon-append_" + el);

    if (input.type === "password") {
        input.type = "text";
        icon.className = "fas fa-eye-slash";
    } else {
        input.type = "password";
        icon.className = "fas fa-eye";
    }
}
