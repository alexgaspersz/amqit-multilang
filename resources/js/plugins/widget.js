/*
 * Common runner jQuery for Frontend & Backend
 *
 * Copyright (C) 2019
 *
 * @author    Alex
 * @category  jQuery widget plugins
 */
(function ($) {
    /**
     * Global fungsi untuk ajax post
     *
     * @param config
     */
    $.fn.requestApi = function (config) {
        let options = jQuery.extend({
            url: '',
            type: 'GET',
            data: '',
            dataType: 'JSON',
            func: {}
        }, config);

        $.ajax({
            url: options.url,
            type: options.type,
            data: options.data,
            dataType: options.dataType,
            cache: false,
            success: function (response) {
                if ($.isFunction(options.func)) {
                    options.func(response);
                }
            },
            error: function (xhr) {
                $('table').removeClass('loading');
                console.log(xhr.responseText);
            }
        });
    };

    $.apply = function (target, source, defaults) {
        if (defaults) {
            jQuery.extend(target, defaults);
        }

        if (target && source && source !== null && typeof source === 'object') {
            return jQuery.extend(true, target, source);
        }

        return target;
    };

    /**
     * Fancybox Plugin
     *
     * @param config
     * @returns {*}
     */
    $.fn.boxviewer = function (config) {
        let options = jQuery.extend({
            type: 'iframe',
            effect: 'slide',
            file: '',
            loop: true,
            animationDuration: 400,
            transitionDuration: 400
        }, config);

        $(this).fancybox({
            autoSize: true,
            src: options.file,
            type: options.type,
            fitToView: false,
            loop: options.loop,
            infobar: true,
            animationDuration: options.animationDuration,
            transitionEffect: options.effect,
            transitionDuration: options.transitionDuration
        });
    };

    /**
     * JQuery Confirm Plugin
     *
     * @param config
     * @returns {*}
     */
    $.fn.confirmation = function (config) {
        let options = jQuery.extend({
            title: '',
            content: '',
            color: 'blue',
            width: '30%',
            icon: 'fas fa-info-circle',
            bootstrap: false,
            action: {},
            cancel: {},
            autoClose: 'cancel|20000',
            confirmText: 'Ya',
            cancelText: 'Tidak'
        }, config);

        return $.confirm({
            icon: options.icon,
            theme: 'material',
            type: options.color,
            useBootstrap: options.bootstrap,
            boxWidth: options.width,
            title: options.title,
            content: options.content,
            autoClose: options.autoClose,
            buttons: {
                confirm: {
                    text: options.confirmText,
                    btnClass: 'btn-blue',
                    action: options.action
                },
                cancel: {
                    text: options.cancelText,
                    btnClass: 'btn-danger',
                    action: options.cancel
                }
            }
        });
    };

    /**
     * JQuery Alert Plugin
     *
     * @param config
     * @returns {*}
     */
    $.fn.alerts = function (config) {
        let options = jQuery.extend({
            title: '',
            content: '',
            color: 'red',
            width: '30%',
            icon: 'fas fa-exclamation-triangle',
            timeout: '3000',
            autoclose: 'cancel',
            bootstrap: false
        }, config);

        return $.alert({
            icon: options.icon,
            theme: 'material',
            type: options.color,
            useBootstrap: options.bootstrap,
            boxWidth: options.width,
            title: options.title,
            content: options.content,
            autoClose: options.autoclose + '|' + options.timeout,
            buttons: {
                cancel: {
                    text: 'Tutup'
                }
            }
        });
    };

    /**
     * Fungsi untuk menampilkan cascade selection
     * @param url
     * @param val
     * @param parent
     * @param initial_text
     * @param parent_val
     * @param option_group
     */
    $.fn.cascadeCombo = function (url, val, parent, initial_text, parent_val, option_group) {
        $(this).jNested(url, {
            parent: parent,
            selected_value: val,
            parent_value: parent_val,
            initial_text: initial_text,
            option_group: option_group
        });
    };

    /**
     * Limit textbox input keystrokes to only receive number only.
     */
    $.fn.integerOnly = function () {
        $(this).keypress(function (e) {
            let chars = $.possibleChars('Y-m-d');
            let chr = String.fromCharCode(e.charCode == 'undefined' ? e.keyCode : e.charCode);

            return e.ctrlKey || e.metaKey || (chr < ' ' || !chars || chars.indexOf(chr) > -1);
        });
    };

    /**
     * Returns true if the passed value is empty, false otherwise. The value is deemed to be empty if it is either:
     *
     * - `null`
     * - `undefined`
     * - a zero-length array
     * - a zero-length string (Unless the `allowEmptyString` parameter is set to `true`)
     *
     * @param {Object} value The value to test.
     * @param {Boolean} [allowEmptyString=false] `true` to allow empty strings.
     * @return {Boolean}
     */
    $.isEmpty = function (value, allowEmptyString) {
        return (value === null) || (typeof value === 'undefined') || (!allowEmptyString ? value === '' : false) || ($.isArray(value) && value.length === 0);
    };

    /**
     * Menterjemahkan input format dan memberikan daftar karakter berdasarkan input format
     *
     * @param {String} format
     * @returns {String}
     */
    $.possibleChars = function (format) {
        let chars = '';
        let literal = false;
        // Check whether a format character is doubled
        let lookAhead = function (match) {
            let matches = (iFormat + 1 < format.length && format.charAt(iFormat + 1) === match);
            if (matches)
                iFormat++;
            return matches;
        };
        for (let iFormat = 0; iFormat < format.length; iFormat++)
            if (literal)
                if (format.charAt(iFormat) === "'" && !lookAhead("'"))
                    literal = false;
                else
                    chars += format.charAt(iFormat);
            else
                switch (format.charAt(iFormat)) {
                    case 'd':
                    case 'm':
                    case 'y':
                    case '@':
                        chars += '0123456789';
                        break;
                    case 'D':
                    case 'M':
                        return null; // Accept anything
                    case '.':
                        chars += '.';
                        break;
                    case "'":
                        if (lookAhead("'"))
                            chars += "'";
                        else
                            literal = true;
                        break;
                    default:
                        chars += format.charAt(iFormat);
                }
        return chars;
    };

    /**
     * Copies all the properties of `source` to the specified `target`. There are two levels
     * of defaulting supported:
     *
     *      $.apply(obj, { a: 1 }, { a: 2 });
     *      //obj.a === 1
     *
     *      $.apply(obj, {  }, { a: 2 });
     *      //obj.a === 2
     *
     * @param {Object} target The receiver of the properties.
     * @param {Object} source The primary source of the properties.
     * @param {Object} [defaults] An object that will also be applied for default values.
     * @return {Object} returns `object`.
     */
    $.apply = function (target, source, defaults) {
        if (defaults) {
            jQuery.extend(target, defaults);
        }

        if (target && source && source !== null && typeof source === 'object') {
            return jQuery.extend(true, target, source);
        }

        return target;
    };

    /**
     * Toggle checkbox, and set all checkbox to checked
     *
     * @name checkAll
     */
    $.fn.checkAll = function () {
        let checked = false;

        return this.click(function () {
            $(':checkbox').map(function () {
                this.checked = !checked;
                return true;
            });
            checked = this.checked;
        });
    };

    /**
     * Create dropzones programmatically by instantiating the Dropzone class
     * @param config
     * @returns {*}
     */
    $.fn.singleUpload = function (config) {
        let me = $(this);
        let options = jQuery.extend({
            params: {},
            url: '',
            previewUrl: '',
            removeUrl: '',
            method: 'POST',
            acceptedFiles: '.jpg, .jpeg, .png',
            maxFiles: 1,
            addRemoveLinks: true,
            maxFilesize: 2,
            uploadMultiple: false,
            autoProcessQueue: true,
            parallelUploads: 1,
            elementId: 'assetId',
            thumbnailWidth: 133,
            thumbnailHeight: 133,
            dictDefaultMessage: "Drop file disini atau klik untuk pilih...",
            dictRemoveFile: 'Hapus File',
            dictRemoveFileConfirmation: 'Hapus file ini?',
            previewTemplate: '<div class="dz-preview dz-file-preview">\n' +
                '    <div class="dz-image"><img data-dz-thumbnail /></div>\n' +
                '    <div class="dz-details">\n' +
                '        <div class="dz-size"><span data-dz-size></span></div>\n' +
                '        <div class="dz-filename"><span data-dz-name></span></div>\n' +
                '    </div>\n' +
                '    <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>\n' +
                '    <div class="dz-error-message"><span data-dz-errormessage></span></div>\n' +
                '</div>'
        }, config);

        let uploader = new Dropzone(me[0], {
            paramName: 'filename',
            clickable: true,
            url: options.url,
            method: options.method,
            acceptedFiles: options.acceptedFiles,
            maxFiles: options.maxFiles,
            addRemoveLinks: options.addRemoveLinks,
            maxFilesize: options.maxFilesize,
            uploadMultiple: options.uploadMultiple,
            autoProcessQueue: options.autoProcessQueue,
            parallelUploads: options.parallelUploads,
            thumbnailWidth: options.thumbnailWidth,
            thumbnailHeight: options.thumbnailHeight,
            dictDefaultMessage: "<i class=\"fas fa-upload\"></i> " + options.dictDefaultMessage,
            dictRemoveFile: options.dictRemoveFile,
            dictRemoveFileConfirmation: options.dictRemoveFileConfirmation,
            previewTemplate: options.previewTemplate,
            headers: {
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, X-File-Name, X-File-Size, X-File-Type'
            },
            params: options.params,
            init: function () {
                if (!$.isEmpty(options.previewUrl)) {
                    let preview = this;
                    $.get(options.previewUrl, function (data) {
                        $.each(data.images, function (key, value) {
                            let file = {name: value.original, size: value.size};
                            preview.options.addedfile.call(preview, file);
                            preview.options.thumbnail.call(preview, file, value.server);
                            preview.emit("complete", file);
                        });
                    });
                }
            },
            success: function (file, response) {
                $('#' + options.elementId).val(response.success);
            },
            error: function (file, response) {
                console.log(response);
            }
        });

        uploader.on("removedfile", function (file) {
            let imageId = $('#' + options.elementId).val();
            $.ajax({
                type: 'POST',
                url: options.removeUrl,
                data: {assetId: imageId},
                success: function (response) {
                    $('#' + options.elementId).val('');
                    console.log('removed file: ', response);
                }
            });
        });

        uploader.on("maxfilesexceeded", function (file) {
            uploader.removeFile(file);
            console.log('maximum upload exceeded. removed: {}', file);
        });

        return uploader;
    };

    /**
     * Create dropzones programmatically by instantiating the Dropzone class
     * @param config
     * @returns {*}
     */
    $.fn.multipleUpload = function (config) {
        let me = $(this);
        let options = jQuery.extend({
            params: {},
            datas: {},
            url: '',
            removeUrl: '',
            previewUrl: '',
            method: 'POST',
            acceptedFiles: '.jpg, .jpeg, .png',
            maxFiles: 0,
            addRemoveLinks: true,
            maxFilesize: 10,
            galleryAsset: false,
            postGallery: '',
            uploadMultiple: false,
            autoProcessQueue: true,
            parallelUploads: 10,
            elementId: 'imagesId',
            thumbnailWidth: 133,
            thumbnailHeight: 133,
            dictDefaultMessage: "Drop file disini atau klik untuk pilih...",
            dictRemoveFile: 'Hapus File',
            dictRemoveFileConfirmation: 'Hapus file ini?'
        }, config);

        let uploader = new Dropzone(me[0], {
            paramName: 'filename',
            clickable: true,
            url: options.url,
            method: options.method,
            acceptedFiles: options.acceptedFiles,
            maxFiles: options.maxFiles,
            addRemoveLinks: options.addRemoveLinks,
            maxFilesize: options.maxFilesize,
            uploadMultiple: options.uploadMultiple,
            autoProcessQueue: options.autoProcessQueue,
            parallelUploads: options.parallelUploads,
            thumbnailWidth: options.thumbnailWidth,
            thumbnailHeight: options.thumbnailHeight,
            dictDefaultMessage: options.dictDefaultMessage,
            dictRemoveFile: options.dictRemoveFile,
            dictRemoveFileConfirmation: options.dictRemoveFileConfirmation,
            params: options.params,
            headers: {
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, X-File-Name, X-File-Size, X-File-Type'
            },
            init: function () {
                let preview = this;
                if (!$.isEmpty(options.previewUrl)) {
                    $.ajax({
                        type: 'POST',
                        url: options.previewUrl,
                        data: {postData: options.datas},
                        dataType: 'json',
                        success: function (data) {
                            $.each(data.images, function (key, value) {
                                let file = {imageId: value.imageId, name: value.original, size: value.size};
                                preview.options.addedfile.call(preview, file);
                                preview.options.thumbnail.call(preview, file, value.server);
                                preview.emit("complete", file);
                            });
                        }
                    });
                }
            },
            success: function (file, response) {
                let $images = $('#' + options.elementId);
                if ($images.val().length > 0) {
                    let $newImage = JSON.parse(response.success);
                    let $currImages = JSON.parse($images.val());
                    $.each($newImage, function (index, value) {
                        if ($.inArray(value, $currImages) === -1) {
                            $currImages.push(value);
                        }
                    });
                    $images.val(JSON.stringify($currImages));
                } else {
                    $images.val(response.success);
                }
                console.log(response);
            },
            error: function (file, response) {
                console.log(response);
            }
        });

        uploader.on("maxfilesexceeded", function (file) {
            uploader.removeFile(file);
            console.log('maximum upload exceeded. removed: {}', file);
        });

        uploader.on("removedfile", function (file) {
            console.log('removed file: {}', file);
        });

        $('#btnUpload').on('click', function (e) {
            if (uploader.getQueuedFiles().length > 0) {
                e.preventDefault();
                uploader.processQueue();
            }
        });

        return uploader;
    };

    /**
     * Create froala text editor programmatically
     * @param config
     * @returns {*}
     */
    $.fn.froalaText = function (config) {
        let me = $(this);
        let options = jQuery.extend({
            heightMin: 300,
            heightMax: 400,
            toolbarButtons: null,
            quickInsertEnabled: false,
            imageMove: true,
            toolbarSticky: false,
            language: 'id',
            videoInsertButtons: ['videoByURL'],
            videoEditButtons: ['videoReplace', 'videoRemove', 'videoDisplay', 'videoAlign', 'videoSize'],
            videoDefaultWidth: 200,
            videoResponsive: true,
            videoSizeButtons: ['videoBack'],
            videoSplitHTML: true,
            videoUpload: false,
            imageUploadURL: null,
            imageUploadParam: 'file',
            imageUploadParams: {},
            imageMaxSize: 5 * 1024 * 1024, // Set max image size to 5MB.
            imageAllowedTypes: ['jpeg', 'jpg', 'png'],
            imageManagerPreloader: null,
            imageManagerPageSize: 20,
            imageManagerScrollOffset: 10,
            imageManagerToggleTags: true,
            imageManagerLoadURL: null,
            imageManagerLoadParams: {},
            imageManagerDeleteURL: null,
            imageManagerDeleteParams: {},
        }, config);

        new FroalaEditor(me[0], {
            key: "AV:4~?3xROKLJKYHROLDXDR@d2YYGR_Bc1A8@5@4:1B2D2F2F1?1?2A3@1C1",
            toolbarButtons: options.toolbarButtons,
            quickInsertEnabled: options.quickInsertEnabled,
            heightMin: options.heightMin,
            heightMax: options.heightMax,
            imageMove: options.imageMove,
            toolbarSticky: options.toolbarSticky,
            language: options.language,
            videoInsertButtons: options.videoInsertButtons,
            videoEditButtons: options.videoEditButtons,
            videoDefaultWidth: options.videoDefaultWidth,
            videoDefaultDisplay: options.videoDefaultDisplay,
            videoResponsive: options.videoResponsive,
            videoSizeButtons: options.videoSizeButtons,
            videoSplitHTML: options.videoSplitHTML,
            videoUpload: options.videoUpload,
            imageUploadMethod: 'POST',
            imageUploadURL: options.imageUploadURL,
            imageUploadParam: options.imageUploadParam,
            imageUploadParams: options.imageUploadParams,
            imageMaxSize: options.imageMaxSize,
            imageAllowedTypes: options.imageAllowedTypes,
            imageManagerPreloader: options.imageManagerPreloader,
            imageManagerPageSize: options.imageManagerPageSize,
            imageManagerScrollOffset: options.imageManagerScrollOffset,
            imageManagerToggleTags: options.imageManagerToggleTags,
            imageManagerLoadURL: options.imageManagerLoadURL,
            imageManagerLoadMethod: "GET",
            imageManagerLoadParams: options.imageManagerLoadParams,
            imageManagerDeleteURL: options.imageManagerDeleteURL,
            imageManagerDeleteMethod: "POST",
            imageManagerDeleteParams: options.imageManagerDeleteParams,
            events: {
                'image.beforeUpload': function (images) {
                    // Return false if you want to stop the image upload.
                },
                'image.uploaded': function (response) {
                    // Image was uploaded to the server.
                },
                'image.inserted': function ($img, response) {
                    // Image was inserted in the editor.
                },
                'image.replaced': function ($img, response) {
                    // Image was replaced in the editor.
                },
                'image.error': function (error, response) {
                    // Bad link.
                    if (error.code == 1) {
                    }
                    // No link in upload response.
                    else if (error.code == 2) {
                    }
                    // Error during image upload.
                    else if (error.code == 3) {
                    }
                    // Parsing response failed.
                    else if (error.code == 4) {
                    }
                    // Image too text-large.
                    else if (error.code == 5) {
                    }
                    // Invalid image type.
                    else if (error.code == 6) {
                    }
                    // Image can be uploaded only to same domain in IE 8 and IE 9.
                    else if (error.code == 7) {
                    }
                    // Response contains the original server response to the request if available.
                },
                'imageManager.imagesLoaded': function (data) {
                    // Do something when the request finishes with success.
                },
                'imageManager.beforeDeleteImage': function ($img) {
                    // Do something before deleting an image from the image manager.
                    console.log($img.attr('data-id'));
                },
                'imageManager.imageDeleted': function (data) {
                    // Do something after the image was deleted from the image manager.
                },
                'imageManager.error': function (error, response) {
                    // Bad link. One of the returned image links cannot be loaded.
                    if (error.code == 10) {
                    }
                    // Error during request.
                    else if (error.code == 11) {
                    }
                    // Missing imagesLoadURL option.
                    else if (error.code == 12) {
                    }
                    // Parsing response failed.
                    else if (error.code == 13) {
                    }
                }
            }
        });
    };

    /**
     * Create CKEditor editor with standart config
     */
    $.fn.textEditor = function () {
        let me = $(this);
        CKEDITOR.replace(me[0]);
    };

    /**
     * Create CKEditor editor with attachment
     */
    $.fn.textEditorAttachment = function () {
        let me = $(this);
        CKEDITOR.replace(me[0], {
            customConfig: 'attach-config.js'
        });
    };

    /**
     * Create CKEditor lite editor
     */
    $.fn.liteEditor = function () {
        let me = $(this);
        CKEDITOR.replace(me[0], {
            customConfig: 'lite-config.js'
        });
    };

    /**
     * Create inoput file programmatically by instantiating the element class
     * @param config
     * @returns {*}
     */
    $.fn.uploadInput = function (config) {
        let me = $(this);
        let options = $.extend({
            showCaption: true,
            showRemove: true,
            showUpload: false,
            showPreview: false,
            showCancel: false,
            autoReplace: false,
            dropZoneEnabled: false,
            browseLabel: "Upload",
            removeLabel: "Hapus",
            maxFileCount: 1,
            maxFileSize: 5000,
            mainClass: null,
            initialPreview: [],
            initialPreviewAsData: false,
            overwriteInitial: false,
            initialPreviewConfig: [],
            allowedFileTypes: [],
            allowedFileExtensions: [],
            rtl: false
        }, config);

        me.fileinput({
            showCaption: options.showCaption,
            maxFileCount: options.maxFileCount,
            showRemove: options.showRemove,
            showUpload: options.showUpload,
            showPreview: options.showPreview,
            showCancel: options.showCancel,
            autoReplace: options.autoReplace,
            dropZoneEnabled: options.dropZoneEnabled,
            removeClass: "btn btn-danger",
            browseLabel: options.browseLabel,
            browseIcon: "<i class=\"fas fa-folder-open\"></i> ",
            removeLabel: options.removeLabel,
            removeIcon: "<i class=\"fas fa-trash\"></i> ",
            maxFileSize: options.maxFileSize,
            mainClass: options.mainClass,
            initialPreview: options.initialPreview,
            initialPreviewAsData: options.initialPreviewAsData,
            allowedFileTypes: options.allowedFileTypes,
            initialPreviewConfig: options.initialPreviewConfig,
            overwriteInitial: options.overwriteInitial,
            allowedFileExtensions: options.allowedFileExtensions,
            elErrorContainer: "#errorBlock",
            rtl: options.rtl
        });
    };

    /**
     * Create date range picker programmatically by instantiating the element class
     * @param config
     * @returns {*}
     */
    $.fn.rangePicker = function (config) {
        let me = $(this);
        let options = $.extend({
            opens: 'right',
            format: 'YYYY-MM-DD',
            separator: ' s.d ',
            autoApply: true,
            startDate: '',
            endDate: moment().add(6, 'days'),
            wrapper: $('#rangepicker span')
        }, config);

        me.daterangepicker({
            opens: options.opens,
            format: options.format,
            separator: options.separator,
            autoApply: options.autoApply,
            locale: {
                daysOfWeek: ['Ming', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
                monthNames: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember'],
                firstDay: 1
            },
            startDate: options.startDate,
            endDate: options.endDate,
        }, function (start, end) {
            options.wrapper.html(start.format('D MMMM YYYY') + options.format + end.format('D MMMM YYYY'));
        });
    };

    /**
     * Create bootstrap tagsinput programmatically by instantiating the element class
     * @param config
     * @returns {*}
     */
    $.fn.inputTags = function (config) {
        let me = $(this);
        let options = $.extend({
            tagClass: 'big',
            itemValue: 'id',
            itemText: 'label',
            confirmKeys: [13, 44],
            maxTags: 3,
            maxChars: 8,
            trimValue: false,
            allowDuplicates: false,
            freeInput: true,
            cancelConfirmKeysOnEmpty: false,
            typeahead: {}
        }, config);

        me.tagsinput({
            tagClass: options.tagClass,
            itemValue: options.itemValue,
            itemText: options.itemText,
            confirmKeys: options.confirmKeys,
            maxTags: options.maxTags,
            maxChars: options.maxChars,
            trimValue: options.trimValue,
            allowDuplicates: options.allowDuplicates,
            freeInput: options.freeInput,
            cancelConfirmKeysOnEmpty: options.cancelConfirmKeysOnEmpty,
            typeahead: options.typeahead
        });
    };

})(jQuery);
