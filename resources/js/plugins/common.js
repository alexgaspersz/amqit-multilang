/*
 * Common used jQuery for Frontend & Backend
 *
 * Copyright (C) 2020
 *
 * @author    Alex
 * @since     03/10/2018, modified: 03/20/2020 23:30
 * @category  jQuery plugins
 */
function init_select2() {
    if (typeof ($.fn.select2) === 'undefined') {
        return;
    }

    $(".select2").select2({
        width: '100%',
        placeholder: '-- Silahkan Pilih --'
    });

    $(".select2-nosearch").select2({
        minimumResultsForSearch: Infinity,
        width: '100%',
        placeholder: '-- Silahkan Pilih --'
    });

    $(".select2-clear").select2({
        allowClear: true,
        width: '100%',
        placeholder: '-- Silahkan Pilih --'
    });

    $(".select2-multiple").select2({
        maximumSelectionLength: 10,
        placeholder: "Maksimal 10 data",
        width: '100%',
    });
}

function init_number() {
    if ($(".number").length > 0) {
        $('.number').integerOnly();
    }
}

function init_mask() {
    if (typeof ($.fn.mask) === 'undefined') {
        return;
    }

    if ($("input[class^='mask_']").length > 0) {
        $("input.mask_tin").mask('99-9999999');
        $("input.mask_ssn").mask('999-99-9999');
        $("input.mask_date").mask('9999-99-99');
        $("input.mask_product").mask('a*-999-a999');
        $("input.mask_phone").mask('99 (999) 999-99-99');
        $("input.mask_phone_ext").mask('99 (999) 999-9999? x99999');
        $("input.mask_credit").mask('9999-9999-9999-9999');
        $("input.mask_percent").mask('99%');
        $("input.mask_npwp").mask('99.999.999.9-999.999');
    }
}

function init_checkall() {
    if (typeof ($.fn.checkAll) !== 'undefined') {
        $(".checkall").checkAll();
    }
}

function init_CKeditor() {
    if ($(".editor").length > 0) {
        $(".editor").textEditor();
    }
}

function init_attachCKeditor() {
    if ($(".uploadeditor").length > 0) {
        $(".uploadeditor").textEditorAttachment();
    }
}

function init_liteCKeditor() {
    if ($(".lite-editor").length > 0) {
        $(".lite-editor").liteEditor();
    }
}

function init_noteSEditor() {
    if (typeof ($.fn.summernote) === 'undefined') {
        return;
    }
    $(".summernote").noteEditor();
}

function init_timepicker() {
    if ($(".timepicker").length > 0) {
        $('.timepicker').timepicker({
            timeFormat: 'HH:mm',
            interval: 30,
            minTime: new Date(0, 0, 0, 6, 0, 0),
            maxTime: new Date(0, 0, 0, 23, 0, 0),
            startHour: 6,
            startTime: new Date(0, 0, 0, 6, 0, 0),
            scrollbar: true
        });
    }
}

function init_datepicker() {
    if ($(".datepicker").length > 0) {
        $(".datepicker").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            clearBtn: true,
            language: 'id'
        });
    }

    if ($(".datefuture").length > 0) {
        $(".datefuture").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            clearBtn: true,
            startDate: '0d',
            language: 'id'
        });
    }

    if ($(".datepast").length > 0) {
        $(".datepast").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            clearBtn: true,
            endDate: '+0d',
            language: 'id'
        });
    }

    if ($(".monthpickers").length > 0) {
        $('.monthpickers').datepicker({
            format: "yyyy-mm",
            minViewMode: 1,
            maxViewMode: 1,
            autoclose: true,
            clearBtn: true,
            language: 'id'
        });
    }

    if ($(".yearpickers").length > 0) {
        $('.yearpickers').datepicker({
            format: "yyyy",
            minViewMode: 2,
            maxViewMode: 2,
            autoclose: true,
            clearBtn: false,
            language: 'id'
        });
    }
}

function init_tagsinput() {
    if ($(".tagsinput").length > 0) {
        $(".tagsinput").each(function () {
            let dt = 'add a tag';
            if ($(this).data("placeholder") !== '') {
                dt = $(this).data("placeholder");
            }
            $(this).tagsInput({width: '100%', height: 'auto', defaultText: dt});
        });
    }
}

function init_iframe() {
    if ($(".pdfviewer").length > 0) {
        $(".pdfviewer").fancybox({
            autoSize: true,
            fitToView: false,
            loop: true, infobar: true, animationDuration: 400,
            transitionEffect: 'slide', transitionDuration: 600
        });
    }
}

function init_readonly() {
    if ($(".readonly").length > 0) {
        $(document).on("focusin", ".readonly", function () {
            $(this).prop('readonly', true);
        });

        $(document).on("focusout", ".readonly", function () {
            $(this).prop('readonly', false);
        });
    }
}

function init_accounting() {
    if ($(".accounting").length > 0) {
        new Cleave('.accounting', {
            numeral: true,
            // numeralThousandsGroupStyle: 'thousand',
            numeralDecimalMark: ',',
            delimiter: '.'
        });
    }
}

function init_bootstrapTable() {
    if (typeof ($.fn.bootstrapTable) !== 'undefined') {
        $.extend($.fn.bootstrapTable.defaults, $.fn.bootstrapTable.defaults = {
            icons: {
                paginationSwitchDown: 'glyphicon-collapse-down icon-chevron-down',
                paginationSwitchUp: 'glyphicon-collapse-up icon-chevron-up',
                refresh: 'fa-sync icon-refresh',
                fullscreen: 'fa-expand icon-expand',
                toggle: 'glyphicon-list-alt icon-list-alt',
                columns: 'fa-th-list icon-th',
                detailOpen: 'fa-plus-square icon-plus',
                detailClose: 'fa-minus-square icon-minus'
            },
            classes: 'table',
            iconsPrefix: 'fas',
            undefinedText: '',
            pagination: true,
            pageSize: 10,
            pageList: [10, 20, 30, 50, 100],
            paginationHAlign: 'left',
            paginationDetailHAlign: 'right',
            paginationPreText: '<span class="fas fa-chevron-left"></span>',
            paginationNextText: '<span class="fas fa-chevron-right"></span>',
            paginationLoop: false,
            search: true,
            searchOnEnterKey: false,
            showColumns: false,
            showRefresh: true
        });

        $.extend($.fn.bootstrapTable.columnDefaults, $.fn.bootstrapTable.columnDefaults = {
            sortable: false
        });
    }
}

init_bootstrapTable();
$(document).ready(function () {
    init_select2();
    init_checkall();
    init_datepicker();
    init_tagsinput();
    init_noteSEditor();
    init_attachCKeditor();
    init_CKeditor();
    init_liteCKeditor();
    init_iframe();
    init_timepicker();
    init_readonly();
    init_mask();
    init_number();
    init_accounting();
});
