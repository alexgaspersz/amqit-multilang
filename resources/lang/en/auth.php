<?php

return [
    'login' => 'Please login to continue.',
    'failed' => 'Login failed. :attribute do not match with our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'not_found' => 'These credentials not found in our system.',
    'wrong_pass' => 'The password you entered is incorrect, or was mistyped.',
    'blocked' => 'Your account is blocked. Please contact system administrator.',
    'online' => 'Access not allowed. Only one session per account is allowed to login. Please contact the administrator if you believe you are not online',
    'not_active' => 'Your account is not active. Please contact system administrator.',
    'not_allowed' => 'You are not allowed to access this page.',
    'success_update' => 'These credentials has been updated successfully.',
    'failed_update' => 'These credentials failed to update.',
];
