<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Strings Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in strings throughout the system.
    | Regardless where it is placed, a string can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'dashboard' => [
            'welcome' => '<strong>Web</strong> Administration',
            'reset' => 'Reset Password',
            'features' => 'Administration Menus',
            'home' => 'Home',
            'menu' => 'Menu',
            'language' => 'Language',
            'switch_lang' => 'Choose Language',
            'administration' => 'Administration',
            'login_message' => 'Welcome back :name',
            'logout_message' => '<p>Press <strong>No</strong> if you want to continue work. Press <strong>Yes</strong> to logout current user.</p>',
            'logout_success' => 'Logout success! Thank you.',
            'visit' => 'Visit Site',
        ],

        'logs' => [
            'add' => 'New data :val has been added successfully.',
            'edit' => 'Data :val has been updated successfully.',
            'delete' => 'Data :val has been deleted successfully.',
            'batch' => 'Data with id(s) :val has been deleted successfully.',
            'failed' => 'Data :val failed to delete because related with other data(s).',
            'login_success' => 'User :name login successfully',
            'logout_success' => 'User :name logout successfully',
        ]
    ],

    'common' => [
        'user' => [
            'remember' => 'Remember me',
            'forget_pass' => 'Forgot your password?',
            'empty_captcha' => 'Security code is required!',
            'wrong_captcha' => 'Security code don\'t match!',
            'old_pass_wrong' => 'Wrong old password!',
            'username' => 'Username',
            'password' => 'Password',
            'oldpassword' => 'Old Password',
            'newpassword' => 'New Password',
            'password_retype' => 'Retype Password',
            'password_confirm' => 'Password Confirmation',
            'empty_email' => 'Email is required!',
            'empty_username' => 'Username is required!',
            'empty_password' => 'Password is required!',
            'change_password' => 'Change Password',
            'no_account' => 'Don\'t have an account yet?',
            'have_account' => 'Already have an account?',
            'signup_here' => 'Sign up here',
            'login_here' => 'Log in here',
            'restricted' => 'You are not granted to access this page',
            'profile' => 'Profile',
            'hi' => 'Hi, :name',
        ],

        'messages' => [
            'save' => [
                'success' => 'Data has been saved successfully.',
                'failed' => 'Data failed to save. Please try again.',
            ],
            'update' => [
                'success' => 'Data has been updated successfully.',
                'failed' => 'Data failed to update. Please try again.',
            ],
            'delete' => [
                'success' => 'Data(s) has been deleted successfully.',
                'failed' => 'Data(s) failed to delete. Please try again.',
                'related' => 'Data :val failed to delete because related with other data(s).',
                'data' => 'Are you sure want to delete ',
                'confirm_data' => 'Are you sure want to delete ',
                'confirm_all' => 'Are you sure want to delete all selected data?',
                'confirmation' => 'Delete Confirmation',
                'empty' => 'No data selected. Please select data you want to delete.',
                'clear' => 'Are you sure want to clear all data?',
                'file_empty' => 'File not found. Upload process failed.'
            ],
            'logout' => 'Are you sure want to logout?',
            'not_found' => 'Data not found.',
            'empty' => 'No requested data to process. Please try again.',
            'exist' => 'Data :val already exist.',
            'not_translated' => 'Content is not translated yet',
            'whatsapp' => [
                'title' => 'Chat With Us!',
                'text' => 'Welcome to :site website. How can we help you?'
            ]
        ],

        'status' => [
            'label' => 'Status',
            'action' => 'Action',
            'active' => 'Active',
            'inactive' => 'Inactive',
            'blocked' => 'Blocked',
            'published' => 'Published',
            'reviewed' => 'Review',
            'draft' => 'Draft',
        ],

        'notes' => [
            'status' => 'Active / Inactive',
            'yesorno' => 'Yes / No',
            'publish' => 'Publish / Unpublish',
            'show' => 'Show / Hide',
            'share' => 'Social media share',
            'comment' => 'Allow user comment',
            'feature' => 'Headline news',
            'restrict' => 'Logged user only',
            'login' => 'Login <strong>Admin</strong>',
            'forgot' => 'Forgot <strong>Password</strong>',
            'reset' => 'Reset <strong>Password</strong>',
        ],

        'add' => 'Add New',
        'edit' => 'Edit',
        'list' => 'List',
        'detail' => 'Detail',
        'date' => 'Date',
        'select_all' => 'Select All',
        'completed' => 'Completed',
        'login_required' => 'Login Required',
        'login_norequired' => 'Public Access',
        'back' => 'Back to previous page',
        'name' => 'Name',
        'title' => 'Title',
        'intro' => 'Intro',
        'desc' => 'Description',
        'content' => 'Content',
        'metacontent' => 'Meta Data',
        'metadata' => 'Meta Title',
        'metadesc' => 'Meta Description',
        'slug' => 'Alias',
        'order' => 'Sort Order',
        'option' => 'Publish Option',

        'reader' => 'Read',
        'feature_image' => 'Feature Image',
        'gallery_image' => 'Gambar Galeri',
        'header_image' => 'Header Image',
        'noimage' => 'Tidak Ada Gambar',
        'upload' => 'Upload',
        'upload_limit' => 'Max. <strong>:num</strong> file to be upload',
        'upload_size' => 'Max Size <strong>:num</strong>',
        'upload_ext' => 'Allowed files extension <strong>:ext</strong>',

        'share' => 'Share',
        'category' => 'Category',
        'author' => 'Author',
        'nocategory' => 'No Category',

        'dates' => [
            'year' => 'Year',
            'month' => 'Month',
            'day' => 'Date',
            'create' => 'Created Date',
            'publish_start' => 'Publish Date',
            'publish_end' => 'End Date',
        ],
    ],

    'frontpage' => [
        'recent' => 'Recent News',
        'readmore' => 'Read more'
    ],

    'copyright' => '&copy; Copyright :year :app - All Right Reserved',
];
