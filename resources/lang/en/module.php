<?php
/**
 * Description: common.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     10/12/2017, modified: 10/12/2017 01:20
 * @copyright   Copyright (c) 2017.
 */

return [
    'dashboard' => [
        'title' => 'Dashboard',
        'setting' => 'System Settings',
        'content' => 'Content Management',
    ],

    //User language
    'user' => [
        'module' => 'Users',
        'title' => 'Users Management',
        'desc' => 'Application users management',
    ],

    //Group language
    'group' => [
        'module' => 'Group',
        'title' => 'Group Management',
        'desc' => 'User group management',
        'access' => [
            'module' => 'Group Access',
            'title' => 'Group :val Access Configuration',
            'desc' => 'Users group access configuration',
            'empty' => 'No group access selected. Failed to save',
        ]
    ],

    //Menu language
    'menu' => [
        'module' => 'Menu',
        'title' => 'Menu Management',
        'desc' => 'Application menu management',
        'group' => [
            'module' => 'Menu Group',
            'title' => 'Menu Group Management',
            'desc' => 'Application menu group management',
            'empty' => 'No menu navigation added yet. Click here to add a navigation menu',
        ]
    ],

    //Activity language
    'logs' => [
        'title' => 'Log Activity',
        'desc' => 'User admin activity record',
    ],

    //Config
    'config' => [
        'title' => 'Konfigurasi Web'
    ],

    //Pages
    'pages' => [
        'module' => 'Pages',
        'title' => 'Pages Management',
        'desc' => 'Pages content management',
    ],

    //Posts
    'post' => [
        'module' => 'News',
        'title' => 'News Management',
        'desc' => 'News content management',
    ],

    //Tags
    'tags' => [
        'module' => 'Label/Tags',
        'title' => 'Label/Tags Management',
        'desc' => 'News label/tags management',
    ],

    //Category
    'category' => [
        'module' => 'Category',
        'title' => 'Category Management',
        'desc' => 'News category management',
    ],

    //Gallery
    'gallery' => [
        'module' => 'Galeri',
        'photo' => [
            'title' => 'Foto',
            'subtitle' => 'Manajemen Galeri Foto',
            'desc' => 'Manajemen galeri foto',
        ],
        'video' => [
            'title' => 'Video',
            'subtitle' => 'Manajemen Galeri Video',
            'desc' => 'Manajemen galeri video',
        ]
    ],

    //Master Data
    'master' => [
        'module' => 'Master Data'
    ],

    //Extension
    'extension' => [
        'module' => 'Pengaturan Lain'
    ],

    //Slideshow
    'slideshow' => [
        'module' => 'Slideshow',
        'title' => 'Manajemen Slideshow',
        'desc' => 'Manajemen slideshow',
    ],

    //Banner
    'banner' => [
        'module' => 'Banner',
        'title' => 'Manajemen Banner',
        'desc' => 'Manajemen banner',
    ],

    //ACCOUNT
    'account' => [
        'title' => 'Akun Saya',
        'article' => 'Buat Artikel'
    ],
];
