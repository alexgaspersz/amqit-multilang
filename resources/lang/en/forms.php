<?php
/**
 * Description: forms.php PhpStorm.
 * @package     amqit-mutilang
 * @author      alex
 * @created     04/01/2018, modified: 13/07/2020 21:51
 * @copyright   Copyright (c) 2018.
 */

return [
    //Selection Label
    'choose' => [
        'parent_menu' => '-- Choose Parent Menu --',
        'page' => '-- Choose Page --',
        'post' => '-- Choose Post --',
        'user_group' => '-- Choose User Group --',
        'author' => '-- Choose Author --',
        'menu_group' => '-- Choose Group Menu --',
        'data' => '-- Choose :data --',
    ],

    //Menu Form
    'menu' => [
        'add' => 'Add Menu',
        'name' => 'Menu Name',
        'route' => 'Route',
        'type' => 'Menu Type',
        'source' => 'Source',
        'url' => 'URL',
        'link' => 'External URL',
        'position' => 'Menu Position',
        'parent' => 'Parent Menu',
        'icon' => 'Icon',
        'external' => 'Link',
    ],

    //Pages
    'page' => [
        'parent' => 'Parent Page',
        'title' => 'Page Title',
        'restrict' => 'Limited Access',
    ],

    'post' => [
        'feature' => 'Headline',
        'comments' => 'Comments',
        'allow_comment' => 'Allow Comment',
    ],

    //User
    'user' => [
        'first_name' => 'First Name',
        'last_name' => 'Last Name',
        'email' => 'Email',
        'email_other' => 'Secondary Email',
        'fullname' => 'Full Name',
        'display' => 'Display Name',
        'profile' => 'Profile',
        'account' => 'My Account',
        'age' => 'Age',
        'sex' => 'Gender',
        'male' => 'Male',
        'female' => 'Female',
        'birthdate' => 'Birthdate',
        'birthplace' => 'Birth Place',
        'bod' => 'Place/Date of birth',
        'country' => 'Country',
        'city' => 'City',
        'handphone' => 'Handphone',
        'telephone' => 'Phone',
        'company' => 'Working Company / Institution',
        'address' => 'Address',
        'image' => 'Profile Picture',
        'fulladdress' => 'Full Address',
        'zipcode' => 'Zip Code',
        'last_login' => 'Last Login',

        //Group Users
        'group' => [
            'name' => 'Group Name',
            'allow_cms' => 'Allow CMS Access?',
            'cms_role' => 'CMS Access',
        ]
    ],

    'config' => [
        'phone' => 'Contact Phone',
        'whatsapp' => 'Whatsapp',
        'website' => 'Website',
        'email' => 'Contact Email',
        'address' => 'Contact Address',
        'sso' => 'Single Sign-On',
        'maintenance' => 'Maintenance Mode',
        'registration' => 'Enable Registration',
        'nocode' => 'Without Country Code :code'
    ],

    'gallery' => [
        'photo' => [
            'name' => 'Album',
            'total' => 'Photo(s)',
            'cover' => 'Album Cover',
            'images' => 'Photo(s) Gallery',
        ],
        'video' => [
            'code' => 'Youtube Code',
            'title' => 'Video Title',
            'source' => 'Video Source',
            'images' => 'Video Thumbnail',
            'internal' => 'Upload',
            'external' => 'Youtube',
            'youtube' => 'Example: www.youtube.com/watch?v=<strong class="text-danger">[YOUTUBE CODE]</strong>',
            'ycode' => '[YOUTUBE CODE]'
        ],
        'file' => 'File',
        'size' => 'Size',
        'type' => 'Type'
    ],

    'banner' => [
        'name' => 'Banner Title',
        'image' => 'Banner Image',
        'position' => 'Position',
        'link' => 'Link'
    ],
];
