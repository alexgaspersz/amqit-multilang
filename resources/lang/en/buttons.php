<?php

return [

    'backend' => [
        'access' => [
            'users' => [

            ],
        ],
    ],

    'emails' => [
        'auth' => [
            'confirm_account' => 'Confirm Account',
            'reset_password'  => 'Reset Password',
        ],
    ],

    'auth' => [
        'login' => 'Login',
        'logout' => 'Logout',
        'signup' => 'Sign Up',
        'reset' => 'Send Password Reset Link',
    ],

    'general' => [
        'cancel' => 'Cancel',
        'close' => 'Close',
        'continue' => 'Continue',
        'create' => 'Add Data',
        'delete' => 'Delete Data',
        'edit'   => 'Edit Data',
        'update' => 'Update',
        'view'   => 'View Detail',
        'save' => 'Submit',
        'yes' => 'Yes',
        'no' => 'No',
        'reset' => 'Reset',
        'menu_order' => 'Update Menu Order',
        'send' => 'Send',
        'clear' => 'Clear All Data',
        'clear_selected' => 'Clear Selected',
        'back' => 'Back',
        'reload' => 'Reload Data',
        'expand' => 'Expand',
        'collapse' => 'Collapse',
        'find' => 'Search',
        'verify' => 'Verify',
        'download' => 'Download',
    ],
];
