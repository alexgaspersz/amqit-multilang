<?php
/**
 * Description: placeholder.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     21/12/2017, modified: 21/12/2017 01:40
 * @copyright   Copyright (c) 2017.
 */

return [

    'login' => [
        'login_name' => 'type your name...',
        'login_email' => 'type your email...',
        'login_username' => 'type your username...',
        'login_password' => 'type your password...',
        'confirm_password' => 'password confirmation...',
        'captcha' => 'security code...',
        'search' => 'search here...',
        'login_email_reset' => 'type your registered email address...',
        'username_limit' => 'cannot be less than 5 and cannot be more than 10 characters...',
    ],

    'passwords' => [
        'leave_blank' => "leave password blank if dont want to change...",
        'confirm_pass' => "retype password for confirmation...",
        'confirm_newpass' => "retype new password for confirmation...",
        'newpass' => "type your new password...",
        'old_pass' => "type your current password...",
        'old_pass_wrong' => "The old password is incorrect, or was mistyped",
    ],

    'forms' => [
        'birthdate' => 'type your birthdate...',
        'handphone' => 'type your valid phone number...',
        'fulladdress' => 'type your current address...',
        'email' => 'type your valid email...',
        'rules' => ":attribute minimum :min character, max :max character",
        'imagecaption' => 'image caption...'
    ]
];
