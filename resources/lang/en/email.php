<?php
/**
 * Description: email.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     25/02/2018, modified: 25/02/2018 03:51
 * @copyright   Copyright (c) 2018.
 */

return [
    'contact' => [
        'from_name' => '[AMQIT Boilerplate]',
        'auto_info' => 'This is an automatically generated email, please do not reply',
        'thanks' => 'Thank You,',
        'from' => '[Contact Us]',
        'subject' => 'A new :app_name contact form submission!',
        'success' => 'Email sent successfully',
        'failed' => 'Email failed to sent',
        'email_body_title' => 'You have a new contact form request: Below are the details:',
    ],

    'auth' => [
        'account_confirmed'         => 'Your account has been confirmed.',
        'error'                     => 'Whoops!',
        'greeting'                  => 'Hello!',
        'regards'                   => 'Regards,',
        'trouble_clicking_button'   => 'If you’re having trouble clicking the ":action_text" button, copy and paste the URL below into your web browser:',
        'thank_you_for_using_app'   => 'Thank you for using our application!',
        'password_reset_subject'    => 'Reset Password',
        'password_cause_of_email'   => 'You are receiving this email because we received a password reset request for your account.',
        'password_if_not_requested' => 'If you did not request a password reset, no further action is required.',
        'reset_password'            => 'Click here to reset your password',
        'click_to_confirm'          => 'Click here to confirm your account:',
    ],
];