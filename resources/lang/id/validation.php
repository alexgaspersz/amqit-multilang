<?php

return [
    /*
    |---------------------------------------------------------------------------------------
    | Baris Bahasa untuk Validasi
    |---------------------------------------------------------------------------------------
    |
    | Baris bahasa berikut ini berisi standar pesan kesalahan yang digunakan oleh
    | kelas validasi. Beberapa aturan mempunyai banyak versi seperti aturan 'size'.
    | Jangan ragu untuk mengoptimalkan setiap pesan yang ada di sini.
    |
    */

    'accepted' => ':field harus diterima.',
    'active_url' => ':field bukan URL yang valid.',
    'after' => ':field harus berisi tanggal setelah :date.',
    'after_or_equal' => ':field harus berisi tanggal setelah atau sama dengan :date.',
    'alpha' => ':field hanya boleh berisi huruf.',
    'alpha_dash' => ':field hanya boleh berisi huruf, angka, strip, dan garis bawah.',
    'alpha_num' => ':field hanya boleh berisi huruf dan angka.',
    'array' => ':field harus berisi sebuah array.',
    'before' => ':field harus berisi tanggal sebelum :date.',
    'before_or_equal' => ':field harus berisi tanggal sebelum atau sama dengan :date.',
    'between' => [
        'numeric' => ':field harus bernilai antara :min sampai :max.',
        'file' => ':field harus berukuran antara :min sampai :max kilobita.',
        'string' => ':field harus berisi antara :min sampai :max karakter.',
        'array' => ':field harus memiliki :min sampai :max anggota.',
    ],
    'boolean' => ':field harus bernilai true atau false',
    'confirmed' => 'Konfirmasi :field tidak cocok.',
    'date' => ':field bukan tanggal yang valid.',
    'date_equals' => ':field harus berisi tanggal yang sama dengan :date.',
    'date_format' => ':field tidak cocok dengan format :format.',
    'different' => ':field dan :other harus berbeda.',
    'digits' => ':field harus terdiri dari :digits angka.',
    'digits_between' => ':field harus terdiri dari :min sampai :max angka.',
    'dimensions' => ':field tidak memiliki dimensi gambar yang valid.',
    'distinct' => ':field memiliki nilai yang duplikat.',
    'email' => ':field harus berupa alamat surel yang valid.',
    'ends_with' => ':field harus diakhiri salah satu dari berikut: :values',
    'exists' => ':field yang dipilih tidak valid.',
    'file' => ':field harus berupa sebuah berkas.',
    'filled' => ':field harus memiliki nilai.',
    'gt' => [
        'numeric' => ':field harus bernilai lebih besar dari :value.',
        'file' => ':field harus berukuran lebih besar dari :value kilobita.',
        'string' => ':field harus berisi lebih besar dari :value karakter.',
        'array' => ':field harus memiliki lebih dari :value anggota.',
    ],
    'gte' => [
        'numeric' => ':field harus bernilai lebih besar dari atau sama dengan :value.',
        'file' => ':field harus berukuran lebih besar dari atau sama dengan :value kilobita.',
        'string' => ':field harus berisi lebih besar dari atau sama dengan :value karakter.',
        'array' => ':field harus terdiri dari :value anggota atau lebih.',
    ],
    'image' => ':field harus berupa gambar.',
    'in' => ':field yang dipilih tidak valid.',
    'in_array' => ':field tidak ada di dalam :other.',
    'integer' => ':field harus berupa bilangan bulat.',
    'ip' => ':field harus berupa alamat IP yang valid.',
    'ipv4' => ':field harus berupa alamat IPv4 yang valid.',
    'ipv6' => ':field harus berupa alamat IPv6 yang valid.',
    'json' => ':field harus berupa JSON string yang valid.',
    'lt' => [
        'numeric' => ':field harus bernilai kurang dari :value.',
        'file' => ':field harus berukuran kurang dari :value kilobita.',
        'string' => ':field harus berisi kurang dari :value karakter.',
        'array' => ':field harus memiliki kurang dari :value anggota.',
    ],
    'lte' => [
        'numeric' => ':field harus bernilai kurang dari atau sama dengan :value.',
        'file' => ':field harus berukuran kurang dari atau sama dengan :value kilobita.',
        'string' => ':field harus berisi kurang dari atau sama dengan :value karakter.',
        'array' => ':field harus tidak lebih dari :value anggota.',
    ],
    'max' => [
        'numeric' => ':field maskimal bernilai :max.',
        'file' => ':field maksimal berukuran :max kilobita.',
        'string' => ':field maskimal berisi :max karakter.',
        'array' => ':field maksimal terdiri dari :max anggota.',
    ],
    'mimes' => ':field harus berupa berkas berjenis: :values.',
    'mimetypes' => ':field harus berupa berkas berjenis: :values.',
    'min' => [
        'numeric' => ':field minimal bernilai :min.',
        'file' => ':field minimal berukuran :min kilobita.',
        'string' => ':field minimal berisi :min karakter.',
        'array' => ':field minimal terdiri dari :min anggota.',
    ],
    'not_in' => ':field yang dipilih tidak valid.',
    'not_regex' => 'Format :field tidak valid.',
    'numeric' => ':field harus berupa angka.',
    'password' => 'Kata sandi salah.',
    'present' => ':field wajib ada.',
    'regex' => 'Format :field tidak valid.',
    'required' => ':field wajib diisi.',
    'required_if' => ':field wajib diisi bila :other adalah :value.',
    'required_unless' => ':field wajib diisi kecuali :other memiliki nilai :values.',
    'required_with' => ':field wajib diisi bila terdapat :values.',
    'required_with_all' => ':field wajib diisi bila terdapat :values.',
    'required_without' => ':field wajib diisi bila tidak terdapat :values.',
    'required_without_all' => ':field wajib diisi bila sama sekali tidak terdapat :values.',
    'same' => ':field dan :other harus sama.',
    'size'                 => [
        'numeric' => ':field harus berukuran :size.',
        'file' => ':field harus berukuran :size kilobyte.',
        'string' => ':field harus berukuran :size karakter.',
        'array' => ':field harus mengandung :size anggota.',
    ],
    'starts_with' => ':field harus diawali salah satu dari berikut: :values',
    'string' => ':field harus berupa string.',
    'timezone' => ':field harus berisi zona waktu yang valid.',
    'unique' => ':field sudah ada sebelumnya.',
    'uploaded' => ':field gagal diunggah.',
    'url' => 'Format :field tidak valid.',
    'uuid' => ':field harus merupakan UUID yang valid.',

    /*
    |---------------------------------------------------------------------------------------
    | Baris Bahasa untuk Validasi Kustom
    |---------------------------------------------------------------------------------------
    |
    | Di sini Anda dapat menentukan pesan validasi untuk atribut sesuai keinginan dengan
    | menggunakan konvensi "attribute.rule" dalam penamaan barisnya. Hal ini mempercepat
    | dalam menentukan baris bahasa kustom yang spesifik untuk aturan atribut yang diberikan.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
        'g-recaptcha-response' => [
            'required' => 'Harap verifikasi bahwa Anda bukan robot.',
            'captcha' => 'Kesalahan captcha! coba lagi nanti atau hubungi admin web.',
        ],
    ],

    /*
    |---------------------------------------------------------------------------------------
    | Kustom Validasi Atribut
    |---------------------------------------------------------------------------------------
    |
    | Baris bahasa berikut digunakan untuk menukar 'placeholder' atribut dengan sesuatu
    | yang lebih mudah dimengerti oleh pembaca seperti "Alamat Surel" daripada "surel" saja.
    | Hal ini membantu kita dalam membuat pesan menjadi lebih ekspresif.
    |
    */

    'attributes' => [
    ],
];
