<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Strings Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in strings throughout the system.
    | Regardless where it is placed, a string can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'dashboard' => [
            'welcome' => '<strong>Administasi</strong> Web',
            'reset' => 'Kata Sandi',
            'features' => 'Menu Administrasi',
            'home' => 'Beranda',
            'menu' => 'Menu',
            'language' => 'Bahasa',
            'switch_lang' => 'Pilih Bahasa',
            'administration' => 'Administrasi',
            'login_message' => 'Selamat datang kembali :name',
            'logout_message' => '<p>Tekan <strong>Tidak</strong> jika ingin melanjutkan aktifitas. Tekan <strong>Ya</strong> untuk keluar dari sistem.</p>',
            'logout_success' => 'Terima Kasih!',
            'visit' => 'Halaman Depan',
        ],

        'logs' => [
            'add' => 'Data :val berhasil ditambahkan.',
            'edit' => 'Data :val berhasil diubah.',
            'delete' => 'Data :val berhasil dihapus.',
            'batch' => 'Data dengan id :val berhasil dihapus.',
            'failed' => 'Data :val gagal terhapus karena terhubung dengan data lain.',
            'login_success' => 'Pengguna :name berhasil masuk ke aplikasi.',
            'logout_success' => 'Pengguna :name berhasil keluar dari aplikasi.',
        ]
    ],

    'common' => [
        'user' => [
            'remember' => 'Ingat Saya',
            'forget_pass' => 'Lupa kata sandi?',
            'empty_captcha' => 'Kode keamanan wajib diisi!',
            'wrong_captcha' => 'Kode keamanan tidak sesuai!',
            'old_pass_wrong' => 'Kata sandi lama salah!',
            'username' => 'Nama Pengguna',
            'password' => 'Kata Sandi',
            'oldpassword' => 'Kata Sandi Lama',
            'newpassword' => 'Kata Sandi Baru',
            'password_retype' => 'Ulangi Kata Sandi',
            'password_confirm' => 'Konfirmasi Kata Sandi',
            'empty_email' => 'Email wajib diisi!',
            'empty_username' => 'Pengguna wajib diisi!',
            'empty_password' => 'Kata Sandi wajib diisi!',
            'change_password' => 'Ubah Kata Sandi',
            'no_account' => 'Belum punya akun?',
            'have_account' => 'Sudah punya akun?',
            'signup_here' => 'Daftar disini',
            'login_here' => 'Masuk disini',
            'restricted' => 'Anda tidak diijinkan untuk akses halaman ini',
            'profile' => 'Profil',
            'hi' => 'Hai, :name',
        ],

        'messages' => [
            'save' => [
                'success' => 'Data berhasil ditambahkan.',
                'failed' => 'Data gagal ditambahkan. Silahkan ulangi lagi.',
            ],
            'update' => [
                'success' => 'Data berhasil diubah.',
                'failed' => 'Data gagal diubah. Silahkan ulangi lagi.',
            ],
            'delete' => [
                'success' => 'Data berhasil dihapus dari sistem.',
                'failed' => 'Data gagal dihapus. Silahkan ulangi lagi.',
                'related' => 'Data :val gagal dihapus karena terhubung dengan data lain.',
                'data' => 'Anda yakin akan menghapus ',
                'confirm_data' => 'Apakah anda yakin akan menghapus ',
                'confirm_all' => 'Apakah anda yakin akan menghapus semua data terpilih?',
                'confirmation' => 'Konfirmasi Hapus',
                'empty' => 'Tidak ada data yang dipilih. Silahkan pilih data yang akan anda hapus.',
                'clear' => 'Apakah anda yakin akan menghapus semua data?',
                'file_empty' => 'Berkas tidak ditemukan. Proses tidak berhasil.',
                'dependency' => 'dan semua data terhubung'
            ],
            'logout' => 'Anda yakin ingin keluar?',
            'not_found' => 'Data tidak ditemukan.',
            'empty' => 'Tidak ada permintaan untuk diproses. Silahkan ulangi lagi.',
            'exist' => 'Data :val sudah ada pada sistem.',
            'not_translated' => 'Konten belum diterjemahkan.',
            'whatsapp' => [
                'title' => 'Hubungi Kami!',
                'text' => 'Selamat datang di website :site. Ada yang bisa kami bantu?'
            ]
        ],

        'status' => [
            'label' => 'Status',
            'action' => 'Aksi',
            'active' => 'Aktif',
            'inactive' => 'Tidak Aktif',
            'blocked' => 'Blokir',
            'published' => 'Terbit',
            'reviewed' => 'Tinjau',
            'draft' => 'Draft',
        ],

        'notes' => [
            'status' => 'Aktif / Tidak Aktif',
            'yesorno' => 'Ya / Tidak',
            'publish' => 'Terbit / Tidak Terbit',
            'show' => 'Tampil / Sembunyi',
            'share' => 'Bagi ke media sosial',
            'comment' => 'Ijinkan komentar pengunjung',
            'feature' => 'Berita utama',
            'restrict' => 'Login untuk melihat',
            'login' => 'Silahkan <strong>Login</strong>',
            'forgot' => 'Lupa <strong>Kata Sandi</strong>',
            'reset' => 'Setel Ulang <strong>Kata Sandi</strong>',
        ],

        'add' => 'Tambah',
        'edit' => 'Ubah',
        'list' => 'Daftar',
        'detail' => 'Detil',
        'date' => 'Tanggal',
        'select_all' => 'Pilih Semua',
        'completed' => 'Lengkap',
        'login_required' => 'Wajib Login',
        'login_norequired' => 'Akses Publik',
        'back' => 'Kembali ke halaman sebelumnya',
        'name' => 'Nama',
        'title' => 'Judul',
        'intro' => 'Intro',
        'desc' => 'Deskripsi',
        'content' => 'Konten',
        'metacontent' => 'Meta Data',
        'metadata' => 'Meta Judul',
        'metadesc' => 'Meta Deskripsi',
        'slug' => 'Alias',
        'order' => 'Urutan',
        'option' => 'Opsi Terbit',

        'reader' => 'Baca',
        'feature_image' => 'Gambar Utama',
        'gallery_image' => 'Gambar Galeri',
        'header_image' => 'Gambar Halaman',
        'noimage' => 'Tidak Ada Gambar',
        'upload' => 'Unggah',
        'upload_limit' => 'Maksimal. <strong>:num</strong> file dapat diupload',
        'upload_size' => 'Ukuran maksimal <strong>:num</strong>',
        'upload_ext' => 'File yang dibolehkan <strong>:ext</strong>',

        'share' => 'Bagikan',
        'category' => 'Kategori',
        'author' => 'Penulis',
        'nocategory' => 'Tidak Ada Kategori',

        'dates' => [
            'year' => 'Tahun',
            'month' => 'Bulan',
            'day' => 'Tanggal',
            'create' => 'Tanggal Buat',
            'publish_start' => 'Tanggal Terbit',
            'publish_end' => 'Tanggal Akhir',
        ],
    ],

    'frontpage' => [
        'recent' => 'Berita Terbaru',
        'readmore' => 'Baca Selanjutnya'
    ],

    'copyright' => 'Hak Cipta &copy; :year :app - Dilindungi Undang - Undang',
];
