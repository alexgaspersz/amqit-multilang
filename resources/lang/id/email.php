<?php
/**
 * Description: email.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     25/02/2018, modified: 25/02/2018 03:51
 * @copyright   Copyright (c) 2018.
 */

return [
    'contact' => [
        'from_name' => '[Web]',
        'auto_info' => 'Ini adalah email yang dibuat secara otomatis, tolong jangan balas',
        'thanks' => 'Terima Kasih,',
        'from' => '[Hubungi Kami]',
        'subject' => 'Pesan baru dari kontak :app_name!',
        'success' => 'Email sukses terkirim',
        'failed' => 'Email gagal dikirim',
        'email_body_title' => 'Anda mendapatkan email dari kontak website. Berikut detail pesan email:',
    ],

    'auth' => [
        'account_confirmed'         => 'Akun anda telah terkonfirmasi.',
        'error' => 'Terjadi kesalahan!',
        'greeting'                  => 'Halo!',
        'regards' => 'Terima Kasih,',
        'trouble_clicking_button' => 'Jika anda mengalami kesulitan mengklik tombol ":actionText", copy dan paste link berikut ke browser:',
        'thank_you_for_using_app' => 'Terima Kasih!',
        'password_reset_subject' => 'Pengaturan Ulang Kata Sandi',
        'password_cause_of_email' => 'Anda menerima email ini karena kami menerima permintaan pengaturan ulang kata sandi untuk akun anda.',
        'password_if_not_requested' => 'Harap abaikan email ini jika anda tidak meminta pengaturan ulang kata sandi.',
        'reset_password' => 'Klik di sini untuk mengatur ulang kata sandi anda',
        'click_to_confirm' => 'Klik di sini untuk konfirmasi akun anda:',
    ],
];
