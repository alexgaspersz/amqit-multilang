<?php

return [
    'login' => 'Harap login terlebih dahulu.',
    'failed' => 'Gagal masuk. Pastikan :attribute yang anda masukkan sudah benar.',
    'throttle' => 'Anda tidak bisa masuk sementara. Silahkan coba lagi dalam :seconds detik.',
    'not_found' => 'Data anda tidak ditemukan pada sistem kami.',
    'wrong_pass' => 'Kata sandi yang anda masukkan salah.',
    'blocked' => 'Akun anda terblokir. Silahkan hubungi administrator.',
    'online' => 'Akses tidak diijinkan. Hanya satu sesi per akun yang diperbolehkan untuk masuk. Silahkan hubungi administrator jika anda yakin sedang tidak online',
    'not_active' => 'Akun anda tidak aktif. Silahkan hubungi administrator.',
    'not_allowed' => 'Anda tidak diijinkan untuk mengakses halaman ini.',
    'success_update' => 'Data anda sukses diperbarui.',
    'failed_update' => 'Data anda gagal diperbarui.',
];
