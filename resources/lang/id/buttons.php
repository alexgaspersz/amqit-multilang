<?php

return [

    'backend' => [
        'access' => [
            'users' => [

            ],
        ],
    ],

    'emails' => [
        'auth' => [
            'confirm_account' => 'Konfirmasi Akun',
            'reset_password'  => 'Setel Ulang Kata Sandi',
        ],
    ],

    'auth' => [
        'login' => 'Masuk',
        'logout' => 'Keluar',
        'signup' => 'Daftar',
        'reset' => 'Kirim Tautan Setel Ulang Kata Sandi',
    ],

    'general' => [
        'cancel' => 'Batal',
        'close' => 'Tutup',
        'continue' => 'Lanjutkan',
        'create' => 'Tambah',
        'delete' => 'Hapus',
        'edit' => 'Ubah',
        'update' => 'Perbarui',
        'view'   => 'Lihat Detil',
        'save' => 'Simpan',
        'yes' => 'Ya',
        'no' => 'Tidak',
        'reset' => 'Setel Ulang',
        'menu_order' => 'Perbarui Urutan Menu',
        'send' => 'Kirim',
        'clear' => 'Bersihkan Semua Data',
        'clear_selected' => 'Bersihkan Data',
        'back' => 'Kembali',
        'reload' => 'Muat Ulang',
        'expand' => 'Perbesar',
        'collapse' => 'Perkecil',
        'find' => 'Cari',
        'verify' => 'Verifikasi',
        'download' => 'Unduh',
    ],
];
