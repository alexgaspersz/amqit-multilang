<?php
/**
 * Description: common.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     10/12/2017, modified: 10/12/2017 01:20
 * @copyright   Copyright (c) 2017.
 */

return [
    'dashboard' => [
        'title' => 'Dasbor',
        'setting' => 'Pengaturan Sistem',
        'content' => 'Manajemen Konten',
    ],

    //User language
    'user' => [
        'module' => 'Pengguna',
        'title' => 'Pengaturan Pengguna',
        'desc' => 'Pengaturan pengguna aplikasi',
    ],

    //Group language
    'group' => [
        'module' => 'Kelompok Pengguna',
        'title' => 'Manajemen Kelompok Pengguna',
        'desc' => 'Manajemen kelompok pengguna',
        'access' => [
            'module' => 'Akses Kelompok Pengguna',
            'title' => 'Konfigurasi Akses Kelompok Pengguna',
            'desc' => 'Konfigurasi akses kelompok pengguna',
            'empty' => 'Tidak ada hak akses yang terpilih. Gagal menyimpan data.',
        ]
    ],

    //Menu language
    'menu' => [
        'module' => 'Menu',
        'title' => 'Manajemen Menu',
        'desc' => 'Manajemen menu aplikasi',
        'group' => [
            'module' => 'Grup Menu',
            'title' => 'Manajemen Grup Menu',
            'desc' => 'Manajemen grup menu aplikasi',
            'empty' => 'Belum ada menu navigasi yang ditambahkan. Klik disini untuk menambahkan',
        ]
    ],

    //Activity language
    'logs' => [
        'title' => 'Riwayat Aktifitas',
        'desc' => 'Riwayat aktifitas pengguna admin',
    ],

    //Config
    'config' => [
        'title' => 'Konfigurasi Web'
    ],

    //Pages
    'pages' => [
        'module' => 'Halaman',
        'title' => 'Manajemen Halaman',
        'desc' => 'Manajemen konten halaman',
    ],

    //Posts
    'post' => [
        'module' => 'Berita',
        'title' => 'Manajemen Berita',
        'desc' => 'Manajemen konten berita',
    ],

    //Tags
    'tags' => [
        'module' => 'Label/Tagar',
        'title' => 'Manajemen Label/Tagar',
        'desc' => 'Manajemen label/tagar berita',
    ],

    //Category
    'category' => [
        'module' => 'Kategori',
        'title' => 'Manajemen Kategori',
        'desc' => 'Manajemen kategori berita',
    ],

    //Gallery
    'gallery' => [
        'module' => 'Galeri',
        'photo' => [
            'title' => 'Foto',
            'subtitle' => 'Manajemen Galeri Foto',
            'desc' => 'Manajemen galeri foto',
        ],
        'video' => [
            'title' => 'Video',
            'subtitle' => 'Manajemen Galeri Video',
            'desc' => 'Manajemen galeri video',
        ]
    ],

    //Master Data
    'master' => [
        'module' => 'Master Data'
    ],

    //Extension
    'extension' => [
        'module' => 'Pengaturan Lain'
    ],

    //Slideshow
    'slideshow' => [
        'module' => 'Slideshow',
        'title' => 'Manajemen Slideshow',
        'desc' => 'Manajemen slideshow',
    ],

    //Banner
    'banner' => [
        'module' => 'Banner',
        'title' => 'Manajemen Banner',
        'desc' => 'Manajemen banner',
    ],

    //ACCOUNT
    'account' => [
        'title' => 'Akun Saya',
        'article' => 'Buat Artikel'
    ],
];
