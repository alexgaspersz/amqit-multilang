<?php
/**
 * Description: forms.php PhpStorm.
 * @package     amqit-mutilang
 * @author      alex
 * @created     04/01/2018, modified: 13/07/2020 21:51
 * @copyright   Copyright (c) 2018.
 */

return [
    //Selection Label
    'choose' => [
        'parent_menu' => '-- Pilih Menu Induk --',
        'page' => '-- Pilih Halaman --',
        'post' => '-- Pilih Berita --',
        'user_group' => '-- Pilih Grup Pengguna --',
        'author' => '-- Pilih Penulis --',
        'menu_group' => '-- Pilih Grup Menu --',
        'data' => '-- Pilih :data --',
    ],

    //Menu Form
    'menu' => [
        'add' => 'Tambah Menu',
        'name' => 'Nama Menu',
        'route' => 'Route',
        'type' => 'Tipe Menu',
        'source' => 'Sumber',
        'url' => 'Tautan',
        'link' => 'Tautan Luar',
        'position' => 'Posisi Menu',
        'parent' => 'Menu Induk',
        'icon' => 'Icon',
        'external' => 'Tautan',
    ],

    //Pages
    'page' => [
        'parent' => 'Halaman Induk',
        'title' => 'Judul Halaman',
        'restrict' => 'Akses Terbatas',
    ],

    'post' => [
        'feature' => 'Berita Utama',
        'comments' => 'Komentar',
        'allow_comment' => 'Ijinkan Komentar',
    ],

    //User
    'user' => [
        'first_name' => 'Nama Depan',
        'last_name' => 'Nama Belakang',
        'email' => 'Email',
        'email_other' => 'Email Lainnya',
        'fullname' => 'Nama Lengkap',
        'display' => 'Nama Tampilan',
        'profile' => 'Profil',
        'account' => 'Akun Saya',
        'age' => 'Umur',
        'sex' => 'Jenis Kelamin',
        'male' => 'Laki-Laki',
        'female' => 'Perempuan',
        'birthdate' => 'Tanggal Lahir',
        'birthplace' => 'Tempat Lahir',
        'bod' => 'Tempat/Tanggal Lahir',
        'country' => 'Negara',
        'city' => 'Kota',
        'handphone' => 'Handphone',
        'telephone' => 'Telepon',
        'company' => 'Perusahaan/Institusi Bekerja',
        'address' => 'Alamat',
        'image' => 'Foto Profil',
        'fulladdress' => 'Alamat Lengkap',
        'zipcode' => 'Kode Pos',
        'last_login' => 'Masuk Terakhir',

        //Group Users
        'group' => [
            'name' => 'Nama Grup',
            'allow_cms' => 'Ijinkan Akses CMS?',
            'cms_role' => 'Akses CMS',
        ]
    ],

    'config' => [
        'phone' => 'Telepon Kontak',
        'whatsapp' => 'Nomor Whatsapp',
        'website' => 'Website',
        'email' => 'Email Kontak',
        'address' => 'Alamat Kontak',
        'sso' => 'Single Sign-On',
        'maintenance' => 'Mode Perawatan',
        'registration' => 'Aktifkan Pendaftaran',
        'nocode' => 'Format Tanpa Kode Negara :code'
    ],

    'gallery' => [
        'photo' => [
            'name' => 'Nama Album',
            'total' => 'Jumlah Foto',
            'cover' => 'Gambar Album',
            'images' => 'Foto Galeri',
        ],
        'video' => [
            'code' => 'Kode Youtube',
            'title' => 'Judul Video',
            'source' => 'Sumber Video',
            'images' => 'Gambar Video',
            'internal' => 'Upload',
            'external' => 'Youtube',
            'youtube' => 'Contoh: www.youtube.com/watch?v=<strong class="text-danger">[KODE YOUTUBE]</strong>',
            'ycode' => '[KODE YOUTUBE]'
        ],
        'file' => 'File',
        'size' => 'Ukuran',
        'type' => 'Jenis File'
    ],

    'banner' => [
        'name' => 'Judul Banner',
        'image' => 'Gambar Banner',
        'position' => 'Posisi',
        'link' => 'Tautan'
    ],
];
