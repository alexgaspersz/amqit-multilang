<?php

return [

    'password' => 'Kata sandi minimal 6 karakter, maksimal 15 karakter.',
    'reset' => 'Kata sandi anda berhasil disetel ulang!',
    'sent' => 'Kami telah mengirim e-mail tautan pengaturan ulang kata sandi Anda!',
    'token' => 'Token pengaturan ulang kata sandi tidak valid.',
    'user' => "Alamat email anda tidak ditemukan pada sistem kami.",
    'throttled' => 'Harap tunggu sebelum mencoba lagi.'
];
