<?php
/**
 * Description: placeholder.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     21/12/2017, modified: 21/12/2017 01:40
 * @copyright   Copyright (c) 2017.
 */

return [

    'login' => [
        'login_name' => 'ketik nama anda...',
        'login_email' => 'ketik email anda...',
        'login_username' => 'ketik nama pengguna...',
        'login_password' => 'ketik kata sandi...',
        'confirm_password' => 'konfirmasi kata sandi...',
        'captcha' => 'kode keamanan...',
        'search' => 'cari disini...',
        'login_email_reset' => 'ketik email anda yang terdaftar...',
        'username_limit' => 'tidak boleh kurang 5 dan tidak boleh lebih dari 10 karakter...',
    ],

    'passwords' => [
        'leave_blank' => "kosongkan jika tidak ingin mengubah kata sandi...",
        'confirm_pass' => "konfirmasi kata sandi...",
        'confirm_newpass' => "konfirmasi kata sandi baru...",
        'newpass' => "ketik kata sandi baru...",
        'old_pass' => "ketik kata sandi anda...",
        'old_pass_wrong' => "Kata sandi lama salah, atau salah ketik",
    ],

    'forms' => [
        'birthdate' => 'tanggal lahir anda...',
        'handphone' => 'ketik nomor telepon yang aktif...',
        'fulladdress' => 'ketik alamat domisili anda...',
        'email' => 'ketik email anda yang aktif...',
        'rules' => ":attribute minimal :min karakter, maksimal :max karakter",
        'imagecaption' => 'judul gambar...'
    ]
];
