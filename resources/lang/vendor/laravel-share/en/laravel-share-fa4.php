<?php

return [
    'facebook' => '<li><a href=":url" class="social-button :class" id=":id"><span class="fa fa-facebook-square"></span></a></li>',
    'twitter' => '<li><a href=":url" class="social-button :class" id=":id"><span class="fa fa-twitter-square"></span></a></li>',
    'gplus' => '<li><a href=":url" class="social-button :class" id=":id"><span class="fa fa-google-plus"></span></a></li>',
    'linkedin' => '<li><a href=":url" class="social-button :class" id=":id"><span class="fa fa-linkedin"></span></a></li>',
];
 