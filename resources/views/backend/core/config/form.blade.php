@extends('backend._container.layout')
@section('title', $title)
@section('breadcrumbs')
    {{ Breadcrumbs::render('config') }}
@stop
@section('content')
    <div class="container-fluid">
        <div id="ui-view">
            <div>
                <div class="fade-in">
                    <div class="card card-accent-secondary">
                        <div class="card-header d-flex align-items-center">
                            <h5 class="card-title mb-0"><i class="fas fa-edit"></i> {{$title}}</h5>
                        </div>
                        {!! Form::open(['route' => $route.'.store','id' => 'appForm', 'novalidate' => 'novalidate', 'files' => true]) !!}
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    @include('backend.errors.list')
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            {!! Form::label('params_phone', __('Telepon Kontak')) !!}
                                            {!! Form::text('params[phone]', @$item->params['phone']  ? $item->params['phone'] : old('params[phone]'),['class' => 'form-control']); !!}
                                        </div>
                                        <div class="col-md-6">
                                            {!! Form::label('params_wa', __('Whatsapp Operator')) !!}
                                            {!! Form::text('params[whatsapp]', @$item->params['whatsapp']  ? $item->params['whatsapp'] : old('params[whatsapp]'),['class' => 'form-control']); !!}
                                            <small>Tanpa kode negara. Ex.<strong>0812345678</strong></small>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            {!! Form::label('params_website', __('Website Kontak')) !!}
                                            {!! Form::text('params[website]', @$item->params['website']  ? $item->params['website'] : old('params[website]'),['class' => 'form-control']); !!}
                                        </div>
                                        <div class="col-md-6">
                                            {!! Form::label('params_email', __('Email Kontak')) !!}
                                            {!! Form::text('params[email]', @$item->params['email']  ? $item->params['email'] : old('params[email]'),['class' => 'form-control']); !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('params_address', __('Alamat Kontak')) !!}
                                        {!! Form::textarea('params[address]', @$item->params['address']  ? $item->params['address'] : old('params[address]'),['class' => 'form-control', 'rows' => 3]); !!}
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-row hidden">
                                        {!! Form::label('params_sso', __('Single Sign-On'), ['class' => 'col-md-9 control-label']) !!}
                                        <div class="col-md-3">
                                            <label class="c-switch c-switch-label c-switch-pill c-switch-success">
                                                <input type="checkbox" name="params[sso]" class="c-switch-input"
                                                       @if(@$item->params['sso'] == true) checked @endif>
                                                <span class="c-switch-slider" data-checked="&#x2713"
                                                      data-unchecked="&#x2715"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-row hidden">
                                        {!! Form::label('params_maintenance', __('Maintenance Website'), ['class' => 'col-md-9 control-label']) !!}
                                        <div class="col-md-3">
                                            <label class="c-switch c-switch-label c-switch-pill c-switch-success">
                                                <input type="checkbox" name="params[maintenance]" class="c-switch-input"
                                                       @if(@$item->params['maintenance'] == true) checked @endif>
                                                <span class="c-switch-slider" data-checked="&#x2713"
                                                      data-unchecked="&#x2715"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-row hidden">
                                        {!! Form::label('params_language', __('Multi Language'), ['class' => 'col-md-9 control-label']) !!}
                                        <div class="col-md-3">
                                            <label class="c-switch c-switch-label c-switch-pill c-switch-success">
                                                <input type="checkbox" name="params[language]" class="c-switch-input"
                                                       @if(@$item->params['language'] == true) checked @endif>
                                                <span class="c-switch-slider" data-checked="&#x2713"
                                                      data-unchecked="&#x2715"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            {!! Form::label('params_facebook', __('Facebook')) !!}
                                            {!! Form::text('params[facebook]', @$item->params['facebook'] ? $item->params['facebook'] : old('params[facebook]'),['class' => 'form-control']); !!}
                                        </div>
                                        <div class="col-md-6">
                                            {!! Form::label('params_instagram', __('Instagram')) !!}
                                            {!! Form::text('params[instagram]', @$item->params['instagram'] ? $item->params['instagram'] : old('params[instagram]'),['class' => 'form-control']); !!}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            {!! Form::label('params_twitter', __('Twitter')) !!}
                                            {!! Form::text('params[twitter]', @$item->params['twitter'] ? $item->params['twitter'] : old('params[twitter]'),['class' => 'form-control']); !!}
                                        </div>
                                        <div class="col-md-6">
                                            {!! Form::label('params_youtube', __('YouTube')) !!}
                                            {!! Form::text('params[youtube]', @$item->params['youtube'] ? $item->params['youtube'] : old('params[youtube]'),['class' => 'form-control']); !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('logo', __('Logo Web')); !!}
                                        <div class="profile-image-edit">
                                            {!! Form::file('logo', ['class' => 'file-input', 'accept' => 'image/png, image/x-png, image/jpeg, image/svg+xml']); !!}
                                            {!! Form::hidden('params[logo]', @$item->params['logo']); !!}
                                        </div>
                                        <ul class="list-unstyled text-danger">
                                            <li>
                                                <small>Ukuran logo: <strong>300px x 63px</strong></small>
                                            </li>
                                            <li>
                                                <small>{!! __('strings.common.upload_size',['num' => imageSize()]) !!}
                                                    MB;
                                                </small>
                                            </li>
                                            <li>
                                                <small>{!! __('strings.common.upload_ext',['ext' => '.svg, .jpg, .png']) !!}</small>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="form-group hidden">
                                        {!! Form::label('params_fbapp', __('Facebook APP ID')) !!}
                                        {!! Form::text('params[fbapp]', @$item->params['fbapp'] ? $item->params['fbapp'] : old('params[addthis]'),['class' => 'form-control']); !!}
                                    </div>
                                    <div class="form-group hidden">
                                        {!! Form::label('params_ytkey', __('YouTube API Key')) !!}
                                        {!! Form::text('params[ytkey]', @$item->params['ytkey'] ? $item->params['ytkey'] : old('params[ytkey]'),['class' => 'form-control']); !!}
                                    </div>
                                    <div class="form-group hidden">
                                        {!! Form::label('params_addthis', __('Share API Key')) !!}
                                        {!! Form::text('params[addthis]', @$item->params['addthis'] ? $item->params['addthis'] : old('params[addthis]'),['class' => 'form-control']); !!}
                                    </div>
                                    <div class="form-group hidden">
                                        {!! Form::label('params_recaptcha', __('Recaptcha Site Key')) !!}
                                        {!! Form::text('params[recaptcha]', @$item->params['recaptcha'] ? $item->params['recaptcha'] : old('params[recaptcha]'),['class' => 'form-control']); !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            {!! Form::hidden('secure_id', @$secure_id) !!}
                            <button type="submit" class="ladda-button" data-size="s" data-color="green"
                                    data-style="slide-right"><i
                                        class="fas fa-save"></i> <span
                                        class="ladda-label">@lang('buttons.general.save')</span></button>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('after-styles')
    <style type="text/css">
        .file-preview {
            padding: 0 !important;
            width: 100% !important;
            border: none;
        }

        .file-preview .close {
            display: none;
        }

        .file-preview-image {
            max-width: 100% !important;
            width: 100% !important;
            height: 100% !important;
        }

        .file-preview-frame {
            margin: 0 !important;
            width: 100% !important;
            height: 100% !important;
            border: none;
            box-shadow: none;
            padding: 0;
        }

        .krajee-default.file-preview-frame .kv-file-content {
            height: 100% !important;
        }
    </style>
@endpush
@push('after-scripts')
    <script type="text/javascript">
        $('document').ready(function () {
            let $picInput = $(".file-input");
            $picInput.uploadInput({
                browseLabel: "Pilih File",
                removeLabel: "Hapus",
                maxFileCount: 1,
                showCaption: false,
                showPreview: true,
                showUpload: false,
                showRemove: false,
                maxFileSize: {!! imageSize() * 1000 !!},
                allowedFileExtensions: ['jpg', 'png', 'svg'],
                layoutTemplates: {
                    main1: "{preview}\n" +
                        "<div class=\'input-group {class}\'>\n" +
                        "   <div class=\'input-group-btn\ input-group-prepend'>\n" +
                        "       {browse}\n" +
                        "   </div>\n" +
                        "   {caption}\n" +
                        "</div>"
                },
                initialPreview: [
                    @if(@$item->params['logo'])
                        '{{ Html::image(asset($item->params['logo']), app_title(),['class' => 'file-preview-image img-fluid']) }}'
                    @endif
                ]
            });
        });
    </script>
@endpush
