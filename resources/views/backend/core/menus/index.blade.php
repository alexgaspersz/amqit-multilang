@extends('backend._container.layout')
@section('title', $title)
@section('breadcrumbs')
    {{ Breadcrumbs::render('menu') }}
@stop
@section('content')
    <div class="container-fluid">
        <div id="ui-view">
            <div>
                <div class="fade-in">
                    <div class="card card-accent-secondary">
                        <div class="card-header d-flex align-items-center">
                            <h5 class="card-title mb-0"><i class="fas fa-list-alt"></i> {{$title}}</h5>
                            <div class="btn-group mfs-auto" role="group">
                                @if(access('is_create', $route.'.*'))
                                    <button onclick="appModal('{{route($route.'.group')}}','{{__('module.menu.group.title')}}','modal-lg')"
                                            type="button" class="btn btn-outline-primary">
                                        <i class="fas fa-list"></i> @lang('module.menu.group.module')
                                    </button>
                                    <button id="createNav" type="button" class="btn btn-outline-primary">
                                        <i class="fas fa-link"></i> @lang('forms.menu.external')
                                    </button>
                                @endif
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-row mb-3">
                                {!! Form::label('menu_group', __('module.menu.group.module'),['class' => 'col-md-2 pt-1']) !!}
                                <div class="col-lg-3 col-sm-4 col-xs-12">
                                    <select name="menu_group" id="menu_group" class="form-control select2">
                                        @foreach($groupMenus as $key => $row)
                                            <option value="{{$row->menu_group_alias}}">{{$row->menu_group}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="dd nestable" id="menuList"></div>
                            <div class="hidden">
                                {{ Form::open(['route' => $route.'.destroy','id' => 'formDelete']) }}
                                {{ Form::hidden('secure_id', null, ['id' => 'row-id']) }}
                                {{ Form::close() }}
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            <button type="submit" id="updateOrder" class="ladda-button" data-size="s" data-color="green"
                                    data-style="slide-right"><i
                                        class="fas fa-save"></i> @lang('buttons.general.menu_order')</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@push('after-styles')
    {{ Html::style(app_mix('vendors/nestable/nestable.css')) }}
@endpush
@push('after-scripts')
    {{ Html::script(app_mix('vendors/nestable/jquery.nestable.js'), ['type' => 'text/javascript']) }}
    <script type="text/javascript">
        $(document).ready(function () {
            $('.nestable').nestable();
            $('.nestable-menu').on('click', function(e){
                let target = $(e.target),
                    action = target.data('action');
                if (action === 'expand-all') {
                    $('.dd').nestable('expandAll');
                }
                if (action === 'collapse-all') {
                    $('.dd').nestable('collapseAll');
                }
            });

            let $menu = $('#menu_group');
            let $list = $('#menuList');

            $('#createNav').on('click', function () {
                let url = "{{ url('admin/system/menus/create') }}/" + $menu.val();
                window.location.replace(url);
            });

            let func = function (row) {
                $list.html(row.menu).trigger('create');
            };
            let options = {
                url : "{{url('admin/system/menus/generate')}}/" + $menu.val(),
                type: "GET"
            };
            $(document).requestApi($.apply({
                func: func
            },options));

            $menu.on('change', function () {
                let func = function (row) {
                    $list.html(row.menu).trigger('create');
                };
                let options = {
                    url : "{{url('admin/system/menus/generate')}}/" + this.value,
                    type: "GET"
                };
                $(document).requestApi($.apply({
                    func: func
                },options));
            });

            $('#updateOrder').click(function () {
                let output = window.JSON.stringify($('.dd').nestable('serialize'));
                let optParams = function (json) {
                    if (json.stat) {
                        console.log('update order success');
                    }
                    window.location.reload();
                };
                let options = {
                    url : "{{route($route.'.reposition')}}",
                    type: "POST",
                    data: { 'menu': output }
                };
                $(document).requestApi($.apply({
                    func: optParams
                },options));
            });
        });
    </script>
@endpush
