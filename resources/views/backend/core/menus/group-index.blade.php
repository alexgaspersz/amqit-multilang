@if(access('is_create', $route.'.*'))
    <button class="btn btn-default"
            onclick="appModal('{{route($route.'.group_update')}}','{{__('module.menu.group.title')}}',null)"><i
                class="fa fa-plus"></i> @lang('buttons.general.create')</button>
    <div>&nbsp;</div>
@endif
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover" id="grid-menu">
        <thead>
        <tr>
            <th>@lang('module.menu.group.module')</th>
            <th>@lang('strings.common.slug')</th>
            <th>@lang('strings.common.status.label')</th>
            <th width="150">@lang('strings.common.status.action')</th>
        </tr>
        </thead>
    </table>
</div>
<div class="hidden">
    {{ Form::open(['route' => $route.'.group_destroy','id' => 'formGroupDelete']) }}
    {{ Form::hidden('secure_id', null, ['id' => 'row-id']) }}
    {{ Form::close() }}
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#grid-menu').DataTable({
            processing: true,
            serverSide: true,
            bFilter: false,
            paging: false,
            ordering: false,
            ajax: {
                url: '{{ route($route.'.group_grid') }}'
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'alias', name: 'alias', orderable: false},
                {data: 'status', name: 'status', orderable: false},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    });
</script>
