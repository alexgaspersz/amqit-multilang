@extends('backend._container.layout')
@section('title', $title)
@section('breadcrumbs')
    {{ Breadcrumbs::render('menu') }}
@stop
@section('content')
    <div class="container-fluid">
        <div id="ui-view">
            <div>
                <div class="fade-in">
                    <div class="card card-accent-secondary">
                        <div class="card-header d-flex align-items-center">
                            <h5 class="card-title mb-0"><i class="fas fa-edit"></i> {{$title}}</h5>
                            <button class="btn btn-secondary mfs-auto mfe-1 d-print-none"
                                    onclick="clickButton('{{route($route.'.index')}}')"><span
                                        class="fas fa-arrow-left"></span> @lang('buttons.general.back')</button>
                        </div>
                        {!! Form::open(['route' => $route.'.store','id' => 'appForm', 'novalidate' => 'novalidate']) !!}
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6 col-sm-12 col-xs-12">
                                    @if(app_lang())
                                        <div class="form-row">
                                            {!! Form::label('locale', __('strings.backend.dashboard.switch_lang'), ['class' => 'col-md-2 mb-0 pt-1 control-label']); !!}
                                            <div class="col-md-4">
                                                <select name="locale" id="locale" class="select2-nosearch">
                                                    @foreach($localization as $localeCode => $properties)
                                                        <option value="{{ $localeCode }}">{{ $properties['name'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                    @include('backend.errors.list')
                                    @foreach($localization as $localeCode => $properties)
                                        @php
                                            $asterik = $localeCode == defaultLang() ? 'required-label' : '';
                                            $meta[$localeCode] = [
                                                'menu_title' => null,
                                                'meta_title' => null,
                                                'meta_description' => null,
                                            ];
                                            if($item):
                                                foreach($item->translation as $row):
                                                    if($row->locale == $localeCode):
                                                        $meta[$localeCode] = [
                                                            'menu_title' => $row->menu_title,
                                                            'meta_title' => $row->meta_title,
                                                            'meta_description' => $row->meta_description,
                                                        ];
                                                    endif;
                                                endforeach;
                                            endif;
                                            $flag = '';
                                            if (app_lang()):
                                                $flag = '<img src="' . asset($properties['flag']) . '" width="16">';
                                            endif;
                                        @endphp
                                        <div class="form-group {{$localeCode}} {{ $errors->has('menu_title_'.defaultLang()) ? ' has-error' : '' }}">
                                            {!! Form::label('menu_title_'.$localeCode, __('forms.menu.name'),['class' => $asterik]) !!} {!! $flag !!}
                                            {!! Form::text('menu_title_'.$localeCode, @$meta[$localeCode]['menu_title'] ? $meta[$localeCode]['menu_title'] : old('menu_title_'.$localeCode),['class' => 'form-control']); !!}
                                        </div>
                                        <div class="form-group {{$localeCode}}">
                                            {!! Form::label('meta_title_'.$localeCode, __('strings.common.metadata')) !!} {!! $flag !!}
                                            {!! Form::text('meta_title_'.$localeCode, @$meta[$localeCode]['meta_title'] ? $meta[$localeCode]['meta_title'] : old('meta_title_'.$localeCode),['class' => 'form-control']); !!}
                                        </div>
                                        <div class="form-group {{$localeCode}}">
                                            {!! Form::label('meta_description_'.$localeCode, __('strings.common.metadesc')) !!} {!! $flag !!}
                                            {!! Form::textarea('meta_description_'.$localeCode, @$meta[$localeCode]['meta_description'] ? $meta[$localeCode]['meta_description'] : old('meta_description_'.$localeCode),['class' => 'form-control','rows' => 3]); !!}
                                        </div>
                                    @endforeach
                                    <div class="form-group {{ $errors->has('module') ? ' has-error' : '' }}">
                                        {!! Form::label('module', __('forms.menu.route'),['class' => 'reuired-label']) !!}
                                        {!! Form::text('module', @$item->module ? $item->module : old('module'),['required' => 'required', 'class' => 'form-control']); !!}
                                    </div>
                                    <div class="form-group {{ $errors->has('url') ? ' has-error' : '' }}">
                                        {!! Form::label('url', __('forms.menu.url')) !!}
                                        {!! Form::text('url', @$item->url ? $item->url : old('url'),['required' => 'required', 'class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12 col-xs-12">
                                    <div class="form-group {{ $errors->has('menu_type') ? ' has-error' : '' }}">
                                        {!! Form::label('menu_type', __('forms.menu.position'),['class' => 'required-label']); !!}
                                        <div>
                                            @foreach($groupMenus as $row)
                                                <label class="check" style="margin-right: 5px">
                                                    <input type="radio" class="icheckbox" name="menu_type"
                                                           id="{{$row->id}}"
                                                           value="{{$row->menu_group_alias}}"
                                                           @if(@$item->menu_type == $row->menu_group_alias) checked
                                                           @elseif(@$type == $row->menu_group_alias) checked @endif /> {{$row->menu_group}}
                                                </label>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('source', __('forms.menu.type')); !!}
                                        <div>
                                            <label class="check" style="margin-right: 10px">
                                                {{ Form::radio('source', 'page', @$item->source == 'page' ? 'checked' : null, ['id' => 'is_page', 'class' => 'icheckbox']) }} @lang('module.pages.module')
                                            </label>
                                            <label class="check" style="margin-right: 10px">
                                                {{ Form::radio('source', 'post', @$item->source == 'post' ? 'checked' : null, ['id' => 'is_post', 'class' => 'icheckbox']) }} @lang('module.post.module')
                                            </label>
                                            <label class="check" style="margin-right: 20px">
                                                {{ Form::radio('source', 'external', @$item->source == 'external' ? 'checked' : null, ['id' => 'is_external', 'class' => 'icheckbox']) }} @lang('forms.menu.link')
                                            </label>
                                            <div id="clearType" class="btn btn-sm btn-warning"><i
                                                        class="fas fa-sync text-white"></i></div>
                                        </div>
                                    </div>
                                    <div class="form-row mb-4" id="source">
                                        {!! Form::label('source_id', __('forms.menu.source'), ['class' => 'col-md-3 control-label pt-1']) !!}
                                        <div class="col-md-9">
                                            {!! Form::select('source_id', [], null, ['class' => 'form-control select2']) !!}
                                        </div>
                                    </div>
                                    <div class="form-row mb-4" id="external">
                                        {!! Form::label('link', __('forms.menu.link'), ['class' => 'col-md-3 control-label pt-1']) !!}
                                        <div class="col-md-9">
                                            {!! Form::text('link', @$item->link ? $item->link : old('link'),['id' => 'link', 'class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                    <div class="form-row mb-4">
                                        {!! Form::label('parent_id', __('forms.menu.parent'), ['class' => 'col-md-3 control-label pt-1']) !!}
                                        <div class="col-md-9">
                                            {!! Form::select('parent_id', [], 0, ['class' => 'form-control select2']) !!}
                                        </div>
                                    </div>
                                    <div class="form-row mb-2">
                                        {!! Form::label('status', __('strings.common.status.label'), ['class' => 'col-md-3 control-label']); !!}
                                        <div class="col-md-3">
                                            <label class="c-switch c-switch-label c-switch-pill c-switch-success">
                                                <input type="checkbox" name="status" class="c-switch-input"
                                                       @if(@$item->status == true) checked @endif>
                                                <span class="c-switch-slider" data-checked="&#x2713"
                                                      data-unchecked="&#x2715"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-row mb-4">
                                        {!! Form::label('menu_icons', __('forms.menu.icon'),['class' => 'control-label col-lg-3 col-sm-3 col-xs-12 pt-1']) !!}
                                        <div class="col-lg-6 col-sm-6 col-xs-12">
                                            {!! Form::text('menu_icons', @$item->menu_icons ? $item->menu_icons : old('menu_icons'),['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        {!! Form::label('ordering', __('strings.common.order'),['class' => 'control-label col-lg-3 col-sm-3 col-xs-12 pt-1']); !!}
                                        <div class="col-lg-3 col-sm-6 col-xs-12">
                                            {!! Form::number('ordering', @$item->ordering ? $item->ordering : 0,['class' => 'form-control number']); !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            {!! Form::hidden('secure_id', @$secure_id) !!}
                            {!! Form::hidden('bg', @$item->bg, ['id' => 'assetId']) !!}
                            <button onclick="clickButton('{{route($route.'.index')}}')" class="ladda-button"
                                    type="button" data-size="s" data-color="red" data-style="slide-right"><i
                                        class="fas fa-times"></i> <span
                                        class="ladda-label">@lang('buttons.general.cancel')</span></button>
                            <button type="submit" class="ladda-button" data-size="s" data-color="green"
                                    data-style="slide-right"><i
                                        class="fas fa-save"></i> <span
                                        class="ladda-label">@lang('buttons.general.save')</span></button>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@push('after-scripts')
    <script type="text/javascript">
        $('document').ready(function () {
            let $type = $('input[name="source"]');
            let $source = $('#source');
            let $external = $('#external');
            let $source_id = $('#source_id');
            let $menuType = $('input[name="menu_type"]');
            let $link = $('#link');
            let $parent = $('#parent_id');
            let $title = $('#menu_title_{{defaultLang()}}');
            let $url = $('#url');
            let $clearType = $('#clearType');

            $title.prop('required', true);
            $url.prop('required', true);
            $menuType.prop('required', true);

            $title.on('keyup', function () {
                let alias = slugify(this.value, '-');
                $url.val(alias);
            });

            $source.hide();
            $external.hide();
            $type.on('ifChecked', function(e){
                let type = this.value;
                if (type === 'page') {
                    $source.show();
                    $external.hide();
                    $link.prop('required', false);
                    $source_id.cascadeCombo("{{url('common/get_page')}}","{!! @$item->source_id !!}",null,"{!! __('forms.menu_page_choose') !!}");
                    $source_id.prop('required', true);
                } else if (type === 'post') {
                    $source.show();
                    $external.hide();
                    $link.prop('required', false);
                    $source_id.cascadeCombo("{{url('common/get_post')}}","{!! @$item->source_id !!}",null,"{!! __('forms.menu_post_choose') !!}");
                    $source_id.prop('required', true);
                } else if (type === 'external') {
                    $source.hide();
                    $external.show();
                    $source_id.prop('required', false);
                    $link.prop('required', true);
                } else {
                    return false;
                }
            });

            $clearType.on('click', function () {
                $type.iCheck('uncheck');
                $source.hide();
                $external.hide();
                $source_id.empty();
                $link.val('');
            });

            @if(@$item->source == 'page')
                $source.show();
                $external.hide();
                $link.prop('required', false);
                $source_id.cascadeCombo("{{url('common/get_page')}}","{!! @$item->source_id !!}",null,"{!! __('forms.choose.page') !!}");
                $source_id.prop('required', true);
            @elseif(@$item->source == 'post')
                $source.show();
                $external.hide();
                $link.prop('required', false);
                $source_id.cascadeCombo("{{url('common/get_post')}}","{!! @$item->source_id !!}",null,"{!! __('forms.choose.post') !!}");
                $source_id.prop('required', true);
            @elseif(@$item->source == 'external')
                $source.hide();
                $external.show();
                $source_id.prop('required', false);
                $link.prop('required', true);
            @endif
            // url, val, parent, initial_text, parent_val, option_group
            $parent.cascadeCombo("{{route('common.menu.parent',['type' => $type,'id' => @$secure_id])}}", "{!! @$item->parent_id !!}", null, "{!! __('forms.choose.parent_menu') !!}", null, false);
            $menuType.on('ifChecked', function(e){
                let position = this.value;
                $parent.cascadeCombo("{{url('common/get_menu')}}/" + position + "/{{@$secure_id}}", "{!! @$item->parent_id !!}", null, "{!! __('forms.choose.parent_menu') !!}");
            });
        });
    </script>
@endpush
