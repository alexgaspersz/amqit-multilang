{!! Form::open(['route' => $route.'.group_store','id' => 'appForm', 'class' => 'form-horizontal form-label-left','novalidate' => 'novalidate']) !!}
<div class="panel-body">
    <div class="form-group {{ $errors->has('menu_group') ? ' has-error' : '' }}">
        {!! Form::label('menu_group', __('module.menu.group.module').' *',['class' => 'control-label col-lg-3 col-sm-3 col-xs-12']) !!}
        <div class="col-lg-6 col-sm-6 col-xs-12">
            {!! Form::text('menu_group', @$menu_group ? $menu_group : old('menu_group'),['id' => 'menu_group', 'required' => 'required', 'class' => 'form-control']); !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('menu_group_alias') ? ' has-error' : '' }}">
        {!! Form::label('menu_group_alias', __('strings.common.slug').' *',['class' => 'control-label col-lg-3 col-sm-3 col-xs-12']) !!}
        <div class="col-lg-6 col-sm-6 col-xs-12">
            {!! Form::text('menu_group_alias', @$menu_group_alias ? $menu_group_alias : old('menu_group_alias'),['id' => 'menu_group_alias', 'required' => 'required', 'class' => 'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('status', __('strings.common.status.label'),['class' => 'control-label col-lg-3 col-sm-3 col-xs-12']); !!}
        <div class="col-lg-6 col-sm-6 col-xs-12">
            <label class="switch">
                <input type="checkbox" name="status" class="switch" @if(@$status == true) checked @endif>
                <span></span>
            </label>
        </div>
    </div>
</div>
<div class="panel-footer">
    <div class="row">
        <div class="col-lg-12 text-right">
            {!! Form::hidden('secure_id', @$secure_id) !!}
            <a href="{{route($route.'.index')}}" class="btn btn-danger" type="button"><i
                        class="fa fa-close"></i> @lang('buttons.general.cancel')</a>
            <button type="submit" class="btn btn-success"><i
                        class="fa fa-check"></i> @lang('buttons.general.save')</button>
        </div>
    </div>
</div>
{{ Form::close() }}
<script type="text/javascript">
    $('document').ready(function () {
        $("#appForm").validate();
    });
</script>