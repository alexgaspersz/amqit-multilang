@extends('backend.container.layout')
@section('title', $title)
@section('breadcrumbs')
    {{ Breadcrumbs::render('users') }}
@stop
@section('content')
    <div class="row">
        <div class="col-lg-4">
            <div class="panel panel-default">
                <div class="panel-body profile">
                    <div class="profile-image">
                        @if(Gravatar::exists($user->email) and !$user->avatar)
                            <img src="{!! Gravatar::get($user->email); !!}" alt="{{@$user->fullname}}"/>
                        @elseif(@$user->avatar)
                            <img src="{!! @$user->photo->full_url !!}" alt="{{@$user->fullname}}"/>
                        @else
                            <img src="{{asset('assets/img/no-image.jpg')}}" alt="{{@$user->fullname}}"/>
                        @endif
                    </div>
                    <div class="profile-data margin-10-t">
                        <div class="profile-data-name">{{@$user->fullname}}</div>
                        <div class="profile-data-title" style="color: #FFF;">Last Login: {{@$user->last_login}}</div>
                    </div>
                </div>
                <div class="panel-body list-group border-bottom">
                    <ul style="padding-left: 0;">
                        <li class="list-group-item"><label for="user_display">@lang('forms.user.display')</label><span class="pull-right">{{@$user->profile->display_name}}</span></li>
                        <li class="list-group-item"><label for="status">@lang('strings.common.status.label')</label><span class="pull-right"> {!! getActive($user->active) !!}</span></li>
                        <li class="list-group-item"><label for="email">@lang('forms.user.email')</label><span class="pull-right">{{$user->email}}</span></li>
                        <li class="list-group-item"><label for="group_id">@lang('module.group.module')</label><span class="pull-right">{{$user->group->name}}</span></li>
                        <li class="list-group-item"><label for="active">@lang('forms.user.birthdate')</label><span class="pull-right">{!! dateString(@$user->profile->birthdate) !!}</span></li>
                        <li class="list-group-item"><label for="active">@lang('forms.user.age')</label><span class="pull-right">{{@$user->profile->age}}</span></li>
                        <li class="list-group-item"><label for="active">@lang('forms.user.sex')</label><span class="pull-right">{!! gender(@$user->profile->gender) !!}</span></li>
                        <li class="list-group-item"><label for="active">@lang('forms.user.handphone')</label><span class="pull-right">{{@$user->profile->handphone}}</span></li>
                        <li class="list-group-item"><label for="active">@lang('forms.user.telephone')</label><span class="pull-right">{{@$user->profile->telephone}}</span></li>
                        <li class="list-group-item"><label for="active">@lang('forms.user.address')</label><span class="pull-right">{{@$user->profile->address}}</span></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-rss-square"></i> Articles History</h3>
                    <ul class="panel-controls">
                        <li><a href="{{route('admin.system.users.index')}}" data-toggle="tooltip" data-placement="top"
                               data-original-title="@lang('buttons.general.back')"><span class="fa fa-arrow-left"></span></a>
                        </li>
                    </ul>
                </div>
                <div class="panel-body panel-body-table">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="grid-data">
                            <thead>
                            <tr>
                                <th>@lang('module.post.title')</th>
                                <th>@lang('strings.common.reader')</th>
                                <th>@lang('strings.common.status.label')</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
