@extends('backend._container.layout')
@section('title', $title)
@section('breadcrumbs')
    {{ Breadcrumbs::render('password') }}
@stop
@section('content')
    <div class="container-fluid">
        <div id="ui-view">
            <div class="fade-in">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-accent-secondary">
                            <div class="card-header d-flex align-items-center">
                                <h5 class="card-title mb-0"><i class="fas fa-edit"></i> {{$title}}</h5>
                            </div>
                            {!! Form::open(['route' => $route.'.store.password','id' => 'appForm','class' => 'form-horizontal','novalidate' => 'novalidate','files' => true]) !!}
                            <div class="card-body">
                                @include('backend.errors.list')
                                <div class="form-group {{ $errors->has('oldpassword') ? ' has-error' : '' }}">
                                    {!! Form::label('oldpassword', __('strings.common.user.oldpassword'),['class' => 'control-label required-label']); !!}
                                    <div class="input-group">
                                        {!! Form::password('oldpassword', ['required' => true, 'class' => 'form-control no-icon', 'placeholder' => __('placeholder.passwords.old_pass')]); !!}
                                        <div class="custom-append" onclick="togglePassword('oldpassword')">
                                            <i id="icon-append_oldpassword" class="fas fa-eye-slash"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('upassword') ? ' has-error' : '' }}">
                                    {!! Form::label('upassword', __('strings.common.user.newpassword'),['class' => 'control-label required-label']); !!}
                                    <div class="input-group">
                                        {!! Form::password('upassword', ['required' => true, 'autocomplete' => 'new-password', 'class' => 'validate[required,minSize[6],maxSize[15]] form-control no-icon', 'maxlength' => 15, 'placeholder' => __('placeholder.passwords.newpass')]); !!}
                                        <div class="custom-append" onclick="togglePassword('upassword')">
                                            <i id="icon-append_upassword" class="fas fa-eye-slash"></i>
                                        </div>
                                    </div>
                                    <span class="help-block">
                                        <ul class="list-unstyled text-note em">
                                            <li>{!! __('placeholder.forms.rules',['attribute' => __('strings.common.user.password'),'min' => 6, 'max' => 15]) !!}</li>
                                        </ul>
                                    </span>
                                </div>
                                <div class="form-group {{ $errors->has('upassword_confirmation') ? ' has-error' : '' }}">
                                    {!! Form::label('upassword_confirmation', __('strings.common.user.password_retype'),['class' => 'control-label required-label']); !!}
                                    <div class="input-group">
                                        {!! Form::password('upassword_confirmation', ['required' => true, 'class' => 'validate[required,minSize[6],maxSize[15]] form-control no-icon','placeholder' => __('placeholder.passwords.confirm_newpass')]); !!}
                                        <div class="custom-append" onclick="togglePassword('upassword_confirmation')">
                                            <i id="icon-append_upassword_confirmation" class="fas fa-eye-slash"></i>
                                        </div>
                                    </div>
                                    <span class="help-block">
                                        <ul class="list-unstyled text-note em">
                                            <li>{!! __('placeholder.forms.rules',['attribute' => __('strings.common.user.password'),'min' => 6, 'max' => 15]) !!}</li>
                                        </ul>
                                    </span>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="ladda-button" data-size="s" data-color="green"
                                        data-style="slide-right"><i
                                            class="fas fa-lock"></i> <span
                                            class="ladda-label">@lang('strings.common.user.change_password')</span>
                                </button>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@push('after-scripts')
    <script type="text/javascript">
        function togglePassword(input) {
            let elm = $('#' + input);
            let icon = $('#icon-append_' + input);

            icon.toggleClass('fa-eye fa-eye-slash');
            if (elm.attr('type') === 'password') {
                elm.attr('type', 'text');
            } else {
                elm.attr('type', 'password');
            }
        }
    </script>
@endpush
