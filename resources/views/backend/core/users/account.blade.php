@extends('backend._container.layout')
@section('title', $title)
@section('breadcrumbs')
    {{ Breadcrumbs::render('account') }}
@stop
@section('content')
    <div class="container-fluid">
        <div id="ui-view">
            <div class="fade-in">
                <div class="card card-accent-secondary">
                    <div class="card-header d-flex align-items-center">
                        <h5 class="card-title mb-0"><i class="fas fa-edit"></i> {{$title}}</h5>
                        <button class="btn btn-secondary mfs-auto mfe-1 d-print-none"
                                onclick="clickButton('{{route($route.'.index')}}')"><span
                                    class="fas fa-arrow-left"></span> @lang('buttons.general.back')</button>
                    </div>
                    {!! Form::open(['route' => $route.'.store','id' => 'appForm', 'novalidate' => 'novalidate', 'files' => true]) !!}
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3">
                                {!! Form::label('propic', __('Foto Profil'),['class' => 'control-label']); !!}
                                <div class="profile">
                                    <div class="profile-image-edit">
                                        {!! Form::file('propic', ['class' => 'file-input', 'accept' => 'image/png, image/x-png, image/jpeg, image/pjpeg']); !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-9">
                                @include('backend.errors.list')
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('fullname') ? ' has-error' : '' }}">
                                            {!! Form::label('fullname', __('forms.user.fullname'),['class' => 'control-label required-label']); !!}
                                            {!! Form::text('fullname', @$user->fullname ? @$user->fullname : old('fullname'), ['required' => true, 'class' => 'form-control']); !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('display_name', __('forms.user.display'),['class' => 'control-label']); !!}
                                            {!! Form::text('display_name', @$user->profile->display_name ? @$user->profile->display_name : old('display_name'), ['class' => 'form-control']); !!}
                                        </div>
                                        <div class="form-group row">
                                            {!! Form::label('birthdate', __('forms.user.bod'),['class' => 'control-label col-md-12']); !!}
                                            <div class="col-md-6">
                                                {!! Form::text('birthplace', @$user->profile ? $user->profile->birthplace : old('birthplace'), ['class' => 'form-control']); !!}
                                            </div>
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    {!! Form::text('birthdate', @$user->profile ? $user->profile->birthdate : old('birthdate'),['readonly' => 'readonly', 'class' => 'form-control datepicker']); !!}
                                                    <span class="input-group-addon"><span
                                                                class="glyphicon glyphicon-calendar"></span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('gender', __('forms.user.sex'),['class' => 'control-label required-label']); !!}
                                            <div>
                                                <label class="check mr-5">
                                                    <input type="radio" class="icheckbox" name="gender" id="male"
                                                           value="m" {!! @$user->profile->gender == 'm' ? 'checked' : 'checked' !!}/> @lang('forms.user.male')
                                                </label>
                                                <label class="check">
                                                    <input type="radio" class="icheckbox" name="gender" id="female"
                                                           value="f" {!! @$user->profile->gender == 'f' ? 'checked' : '' !!}/> @lang('forms.user.female')
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {!! Form::label('email', __('forms.user.email'),['class' => 'control-label required-label']); !!}
                                            {!! Form::email('email', @$user->email ? @$user->email : old('email'), ['required' => true, 'class' => 'form-control', 'placeholder' => __('forms.user.email')]); !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('handphone', __('forms.user.handphone'),['class' => 'control-label required-label']); !!}
                                            {!! Form::text('handphone', @$user->profile ? $user->profile->handphone : old('handphone'), ['required' => true,'class' => 'form-control', 'placeholder' => __('forms.user.handphone')]); !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('address', __('forms.user.address'),['class' => 'control-label']); !!}
                                            {!! Form::textarea('address', @$user->profile ? $user->profile->address : old('address'), ['rows' => 3, 'class' => 'form-control']); !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <button type="submit" class="ladda-button" data-size="s" data-color="green"
                                data-style="slide-right"><i
                                    class="fas fa-save"></i> <span
                                    class="ladda-label">@lang('buttons.general.save')</span></button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop
@push('after-styles')
<style type="text/css">
    .file-preview {
        padding: 0 !important;
        width: 100% !important;
        border: none;
    }
    .file-preview .close {
        display: none;
    }
    .file-preview-image {
        max-width: 100% !important;
        height: auto !important;
    }

    .krajee-default.file-preview-frame {
        width: 100% !important;
        margin: 0 !important;
        height: auto !important;
        border: none;
        box-shadow: none;
        padding: 0;
    }

    .krajee-default.file-preview-frame .kv-file-content {
        width: 100% !important;
        height: auto !important;
    }

    .btn.btn-file {
        margin-top: -100px;
        color: white;
    }
</style>
@endpush
@push('after-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            let $picInput = $(".file-input");
            $picInput.fileinput({
                showCaption: false,
                maxFileCount: 1,
                showRemove: false,
                showUpload: false,
                showPreview: true,
                dropZoneEnabled: false,
                browseClass: "btn",
                removeClass: "btn btn-danger",
                browseLabel: "",
                removeLabel: "",
                browseIcon: "<i class=\"fas text-white fa-camera fa-4x \"></i>",
                removeIcon: "<i class=\"fas fa-trash\"></i> ",
                maxFileSize: {!! imageSize() * 1000 !!},
                hideFileIcon: true,
                allowedFileTypes: ['image'],
                allowedFileExtensions: ["jpg", "png", "jpeg"],
                layoutTemplates: {
                    main1: "{preview}\n" +
                        "<div class=\'input-group {class}\'>\n" +
                        "   <div class=\'input-group-btn\ input-group-prepend'>\n" +
                        "       {browse}\n" +
                        "   </div>\n" +
                        "   {caption}\n" +
                        "</div>"
                },
                initialPreview: [
                    @if(@$user->avatar)
                        '{{ Html::image($user->photo->getFullURLAttribute(), @$user->fullname,['class' => 'file-preview-image img-responsive']) }}'
                    @endif

                ],
                elErrorContainer: "#errorBlock"
            });
        });
    </script>
@endpush
