@extends('backend._container.layout')
@section('title', $title)
@section('breadcrumbs')
    {{ Breadcrumbs::render('users') }}
@stop
@section('content')
    <div class="container-fluid">
        <div id="ui-view">
            <div>
                <div class="fade-in">
                    <div class="card card-accent-secondary">
                        <div class="card-header d-flex align-items-center">
                            <h5 class="card-title mb-0"><i class="fas fa-edit"></i> {{$title}}</h5>
                            <button class="btn btn-secondary mfs-auto mfe-1 d-print-none"
                                    onclick="clickButton('{{route($route.'.index')}}')"><span
                                        class="fas fa-arrow-left"></span> @lang('buttons.general.back')</button>
                        </div>
                        {!! Form::open(['route' => $route.'.store','id' => 'appForm', 'novalidate' => 'novalidate']) !!}
                        <div class="card-body">
                            @include('backend.errors.list')
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                                        {!! Form::label('username', __('strings.common.user.username'),['class' => 'control-label required-label']); !!}
                                        {!! Form::text('username', @$user->username ? @$user->username : old('username'),['autocomplete' => 'off', 'readonly' => @$user->username ? true : false, 'class' => 'validate[required,minSize[5],maxSize[10]] form-control', 'minlength' => 5, 'maxlength' => 10]); !!}
                                        <span class="help-block">
                                    <ul class="list-unstyled text-note em">
                                        <li>{!! __('placeholder.forms.rules',['attribute' => __('strings.common.user.username'),'min' => 5, 'max' => 10]) !!}</li>
                                    </ul>
                                </span>
                                    </div>
                                    <div class="form-group {{ $errors->has('group_id') ? ' has-error' : '' }}">
                                        {!! Form::label('group_id', __('module.group.module'),['class' => 'control-label required-label']); !!}
                                        {!! Form::select('group_id', [], @$user->group_id ? @$user->group_id : old('group_id'), ['id' => 'group_id', 'required' => true, 'class' => 'form-control select2']); !!}
                                    </div>
                                    <div class="form-group {{ $errors->has('fullname') ? ' has-error' : '' }}">
                                        {!! Form::label('fullname', __('forms.user.fullname'),['class' => 'control-label required-label']); !!}
                                        {!! Form::text('fullname', @$user->fullname ? @$user->fullname : old('fullname'), ['required' => true, 'class' => 'form-control']); !!}
                                    </div>
                                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                        {!! Form::label('email', __('forms.user.email'),['class' => 'control-label required-label']); !!}
                                        {!! Form::email('email', @$user->email ? @$user->email : old('email'), ['required' => true, 'class' => 'form-control']); !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->has('upassword') ? ' has-error' : '' }}">
                                        {!! Form::label('password', __('strings.common.user.password'),['class' => 'control-label required-label']); !!}
                                        <div class="input-group">
                                            {!! Form::password('upassword', ['id' => 'password', 'autocomplete' => 'new-password', 'class' => 'validate[required,minSize[6],maxSize[15]] form-control no-icon', 'maxlength' => 15, 'placeholder' => __('placeholder.passwords.leave_blank')]); !!}
                                            <div class="custom-append" onclick="togglePassword('password')">
                                                <i id="icon-append_password" class="fas fa-eye-slash"></i>
                                            </div>
                                        </div>
                                        <span class="help-block">
                                    <ul class="list-unstyled text-note em">
                                        <li>{!! __('placeholder.forms.rules',['attribute' => __('strings.common.user.password'),'min' => 6, 'max' => 15]) !!}</li>
                                    </ul>
                                </span>
                                    </div>
                                    <div class="form-group {{ $errors->has('upassword_confirmation') ? ' has-error' : '' }}">
                                        {!! Form::label('password_confirmation', __('strings.common.user.password_retype'),['class' => 'control-label required-label']); !!}
                                        <div class="input-group">
                                            {!! Form::password('upassword_confirmation', ['id' => 'password_confirmation', 'class' => 'form-control no-icon', 'placeholder' => __('placeholder.passwords.confirm_pass')]); !!}
                                            <div class="custom-append"
                                                 onclick="togglePassword('password_confirmation')">
                                                <i id="icon-append_password_confirmation" class="fas fa-eye-slash"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('active') ? ' has-error' : '' }}">
                                        {!! Form::label('active', __('strings.common.status.label'),['class' => 'control-label required']); !!}
                                        <div>
                                            <label class="check" style="margin-right: 15px">
                                                <input type="radio" class="icheckbox" name="active" id="is_active"
                                                       value="1" {!! @$user->active == 1 ? 'checked' : '' !!}/> @lang('strings.common.status.active')
                                                <span></span>
                                            </label>

                                            <label class="check" style="margin-right: 15px">
                                                <input type="radio" class="icheckbox" name="active" id="is_inactive"
                                                       value="0" {!! @$user->active == 0 ? 'checked' : '' !!}/> @lang('strings.common.status.inactive')
                                                <span></span>
                                            </label>

                                            <label class="check">
                                                <input type="radio" class="icheckbox" name="active" id="is_blocked"
                                                       value="2" {!! @$user->active == 2 ? 'checked' : '' !!}/> @lang('strings.common.status.blocked')
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            {!! Form::hidden('secure_id', @$secure_id) !!}
                            <button onclick="clickButton('{{route($route.'.index')}}')" class="ladda-button"
                                    type="button" data-size="s" data-color="red" data-style="slide-right"><i
                                        class="fas fa-times"></i> <span
                                        class="ladda-label">@lang('buttons.general.cancel')</span></button>
                            <button type="submit" class="ladda-button" data-size="s" data-color="green"
                                    data-style="slide-right"><i
                                        class="fas fa-save"></i> <span
                                        class="ladda-label">@lang('buttons.general.save')</span></button>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@push('after-scripts')
    <script type="text/javascript">
        function togglePassword(input) {
            let elm = $('#' + input);
            let icon = $('#icon-append_' + input);

            icon.toggleClass('fa-eye fa-eye-slash');
            if (elm.attr('type') === 'password') {
                elm.attr('type', 'text');
            } else {
                elm.attr('type', 'password');
            }
        }
        $('document').ready(function () {
            let $groupId = $('#group_id');

            $('#password').prop('required', true);
            $('#password_confirmation').prop('required', true);

            @if(@$secure_id)
            $('#password').prop('required', false);
                $('#password_confirmation').prop('required', false);
            @endif

            $groupId.cascadeCombo("{{route('common.group')}}", "{!! @$user->group_id ? @$user->group_id : old('group_id') !!}", null, "{!! __('forms.choose.user_group') !!}");
        });
    </script>
@endpush
