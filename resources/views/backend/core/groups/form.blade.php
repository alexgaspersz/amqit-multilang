@extends('backend._container.layout')
@section('title', $title)
@section('breadcrumbs')
    {{ Breadcrumbs::render('groups') }}
@stop
@section('content')
    <div class="container-fluid">
        <div id="ui-view">
            <div>
                <div class="fade-in">
                    <div class="card card-accent-secondary">
                        <div class="card-header d-flex align-items-center">
                            <h5 class="card-title mb-0"><i class="fas fa-edit"></i> {{$title}}</h5>
                            <button class="btn btn-secondary mfs-auto mfe-1 d-print-none"
                                    onclick="clickButton('{{route($route.'.index')}}')"><span
                                        class="fas fa-arrow-left"></span> @lang('buttons.general.back')</button>
                        </div>
                        {!! Form::open(['route' => $route.'.store','id' => 'appForm', 'novalidate' => 'novalidate']) !!}
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-7 col-sm-12 col-xs-12">
                                    @include('backend.errors.list')
                                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                        {!! Form::label('name', __('forms.user.group.name'),['class' => 'required-label']) !!}
                                        {!! Form::text('name', @$item->name ? $item->name : old('name'),['required' => 'required', 'class' => 'form-control']); !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('description', __('strings.common.desc')) !!}
                                        {!! Form::textarea('description', @$item->description ? $item->description : old('description'),['class' => 'form-control','rows' => 3]); !!}
                                    </div>
                                </div>
                                <div class="col-lg-5 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card-header px-0 pt-0">
                                                <strong><i class="fas fa-cogs"></i> @lang('strings.common.option')
                                                </strong>
                                            </div>
                                            <div class="card-body px-0">
                                                <div class="form-row">
                                                    {!! Form::label('allow_cms', __('forms.user.group.allow_cms'), ['class' => 'col-md-5 control-label']); !!}
                                                    <div class="col-md-2">
                                                        <label class="c-switch c-switch-label c-switch-pill c-switch-success">
                                                            <input type="checkbox" name="allow_cms"
                                                                   class="c-switch-input"
                                                                   @if(@$item->allow_cms == true) checked @endif>
                                                            <span class="c-switch-slider" data-checked="&#x2713"
                                                                  data-unchecked="&#x2715"></span>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <small class="text-muted">@lang('strings.common.notes.status')</small>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    {!! Form::label('status', __('strings.common.status.label'), ['class' => 'col-md-5 control-label']); !!}
                                                    <div class="col-md-2">
                                                        <label class="c-switch c-switch-label c-switch-pill c-switch-success">
                                                            <input type="checkbox" name="status" class="c-switch-input"
                                                                   @if(@$item->status == true) checked @endif>
                                                            <span class="c-switch-slider" data-checked="&#x2713"
                                                                  data-unchecked="&#x2715"></span>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <small class="text-muted">@lang('strings.common.notes.status')</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            {!! Form::hidden('secure_id', @$secure_id) !!}
                            <button onclick="clickButton('{{route($route.'.index')}}')" class="ladda-button"
                                    type="button" data-size="s" data-color="red" data-style="slide-right"><i
                                        class="fas fa-times"></i> <span
                                        class="ladda-label">@lang('buttons.general.cancel')</span></button>
                            <button type="submit" class="ladda-button" data-size="s" data-color="green"
                                    data-style="slide-right"><i
                                        class="fas fa-save"></i> <span
                                        class="ladda-label">@lang('buttons.general.save')</span></button>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
