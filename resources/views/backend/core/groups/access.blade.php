@extends('backend._container.layout')
@section('title', $title)
@section('breadcrumbs')
    {{ Breadcrumbs::render('groups_access') }}
@stop
@section('content')
    <div class="container-fluid">
        <div id="ui-view">
            <div>
                <div class="fade-in">
                    <div class="card card-accent-secondary">
                        <div class="card-header d-flex align-items-center">
                            <h5 class="card-title mb-0"><i class="fas fa-edit"></i> {{$title}}</h5>
                            <div class="btn-group mfs-auto" role="group">
                                <button class="btn btn-secondary d-print-none"
                                        onclick="clickButton('{{route($route.'.index')}}')"><span
                                            class="fas fa-arrow-left"></span> @lang('buttons.general.back')</button>
                                <button class="btn btn-outline-secondary d-print-none" onclick="toggle(0)"><span
                                            class="fas fa-arrow-down"></span> @lang('buttons.general.expand')</button>
                                <button class="btn btn-outline-secondary d-print-none" onclick="toggle(1)"><span
                                            class="fas fa-arrow-up"></span> @lang('buttons.general.collapse')</button>
                            </div>
                        </div>
                        {!! Form::open(['route' => $route.'.permission','novalidate' => 'novalidate']) !!}
                        <div class="card-body card-body-table">
                            <input type="hidden" name="group_id" value="{{$id}}">
                            <table class="table table-responsive-sm table-striped table-hover table-bordered table-header-center text-nowrap order-column">
                                <thead>
                                <tr>
                                    <th width="20">#</th>
                                    <th>Menu</th>
                                    <th width="100"><label><input type="checkbox" class="checkread"> Read</label></th>
                                    <th width="100"><label><input type="checkbox" class="checkadd"> Create</label></th>
                                    <th width="100"><label><input type="checkbox" class="checkedit"> Update</label></th>
                                    <th width="100"><label><input type="checkbox" class="checkdel"> Delete</label></th>
                                    <th width="100"><label><input type="checkbox" class="checkapprove"> Approval</label></th>
                                    <th width="100"><label><input type="checkbox" class="checkdownload">
                                            Download</label></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($menu as $key1 => $parent)
                                    <tr>
                                        <td>
                                            @if(count($parent['level1']) > 0)<a href="javascript:;" class="show_detail" data-id="{{$parent['id']}}"
                                                                                data-status="1"><i id="parent{{$parent['id']}}" class="fa fa-minus"></i></a>@endif
                                        </td>
                                        <td>{{$parent['menu_name']}}</td>
                                        <td align="center">
                                            <input type="checkbox" name="is_read[]"
                                                   value="is_read+{{$parent['id']}}" @if($parent['checked']['is_read'] == 1) {{'checked'}} @endif>
                                        </td>
                                        <td align="center">
                                            <input type="checkbox" name="is_create[]"
                                                   value="is_create+{{$parent['id']}}" @if($parent['checked']['is_create'] == 1) {{'checked'}} @endif>
                                        </td>
                                        <td align="center">
                                            <input type="checkbox" name="is_update[]"
                                                   value="is_update+{{$parent['id']}}" @if($parent['checked']['is_update'] == 1) {{'checked'}} @endif>
                                        </td>
                                        <td align="center">
                                            <input type="checkbox" name="is_delete[]"
                                                   value="is_delete+{{$parent['id']}}" @if($parent['checked']['is_delete'] == 1) {{'checked'}} @endif>
                                        </td>
                                        <td align="center">
                                            <input type="checkbox" name="is_approve[]"
                                                   value="is_approve+{{$parent['id']}}" @if($parent['checked']['is_approve'] == 1) {{'checked'}} @endif>
                                        </td>
                                        <td align="center">
                                            <input type="checkbox" name="is_download[]"
                                                   value="is_download+{{$parent['id']}}" @if($parent['checked']['is_download'] == 1) {{'checked'}} @endif>
                                        </td>
                                    </tr>
                                    @if($parent['level1'])
                                        @foreach($parent['level1'] as $key2 => $level1)
                                            <tr class="parent_{{$parent['id']}} hide_child">
                                                <td>
                                                    @if(count($level1['level2']) > 0)<a href="javascript:;" class="show_detail" data-id="{{$level1['id']}}"
                                                                                        data-status="1"><i id="parent{{$level1['id']}}" class="fa fa-minus"></i></a>@endif
                                                </td>
                                                <td style="text-indent: 30px">{{$level1['menu_name']}}</td>
                                                <td align="center">
                                                    <input type="checkbox" name="is_read[]"
                                                           value="is_read+{{$level1['id']}}" @if($level1['checked']['is_read'] == 1) {{'checked'}} @endif>
                                                </td>
                                                <td align="center">
                                                    <input type="checkbox" name="is_create[]"
                                                           value="is_create+{{$level1['id']}}" @if($level1['checked']['is_create'] == 1) {{'checked'}} @endif>
                                                </td>
                                                <td align="center">
                                                    <input type="checkbox" name="is_update[]"
                                                           value="is_update+{{$level1['id']}}" @if($level1['checked']['is_update'] == 1) {{'checked'}} @endif>
                                                </td>
                                                <td align="center">
                                                    <input type="checkbox" name="is_delete[]"
                                                           value="is_delete+{{$level1['id']}}" @if($level1['checked']['is_delete'] == 1) {{'checked'}} @endif>
                                                </td>
                                                <td align="center">
                                                    <input type="checkbox" name="is_approve[]"
                                                           value="is_approve+{{$level1['id']}}" @if($level1['checked']['is_approve'] == 1) {{'checked'}} @endif>
                                                </td>
                                                <td align="center">
                                                    <input type="checkbox" name="is_download[]"
                                                           value="is_download+{{$level1['id']}}" @if($level1['checked']['is_download'] == 1) {{'checked'}} @endif>
                                                </td>
                                            </tr>
                                            @if($level1['level2'])
                                                @foreach($level1['level2'] as $key3 => $level2)
                                                    <tr class="parent_{{$level1['id']}} hide_child">
                                                        <td>
                                                            @if(count($level2['level3']) > 0)<a href="javascript:;" class="show_detail" data-id="{{$level2['id']}}"
                                                                                                data-status="1"><i id="parent{{$level2['id']}}" class="fa fa-minus"></i></a>@endif
                                                        </td>
                                                        <td style="text-indent: 60px">{{$level2['menu_name']}}</td>
                                                        <td align="center">
                                                            <input type="checkbox" name="is_read[]"
                                                                   value="is_read+{{$level2['id']}}" @if($level2['checked']['is_read'] == 1) {{'checked'}} @endif>
                                                        </td>
                                                        <td align="center">
                                                            <input type="checkbox" name="is_create[]"
                                                                   value="is_create+{{$level2['id']}}" @if($level2['checked']['is_create'] == 1) {{'checked'}} @endif>
                                                        </td>
                                                        <td align="center">
                                                            <input type="checkbox" name="is_update[]"
                                                                   value="is_update+{{$level2['id']}}" @if($level2['checked']['is_update'] == 1) {{'checked'}} @endif>
                                                        </td>
                                                        <td align="center">
                                                            <input type="checkbox" name="is_delete[]"
                                                                   value="is_delete+{{$level2['id']}}" @if($level2['checked']['is_delete'] == 1) {{'checked'}} @endif>
                                                        </td>
                                                        <td align="center">
                                                            <input type="checkbox" name="is_approve[]"
                                                                   value="is_approve+{{$level2['id']}}" @if($level2['checked']['is_approve'] == 1) {{'checked'}} @endif>
                                                        </td>
                                                        <td align="center">
                                                            <input type="checkbox" name="is_download[]"
                                                                   value="is_download+{{$level2['id']}}" @if($level2['checked']['is_download'] == 1) {{'checked'}} @endif>
                                                        </td>
                                                    </tr>
                                                    @if($level2['level3'])
                                                        @foreach($level2['level3'] as $key4 => $level3)
                                                            <tr class="parent_{{$level2['id']}} hide_child">
                                                                <td>
                                                                    @if(count($level3['level4']) > 0)<a href="javascript:;" class="show_detail" data-id="{{$level3['id']}}"
                                                                                                        data-status="1"><i id="parent{{$level3['id']}}" class="fa fa-minus"></i></a>@endif
                                                                </td>
                                                                <td style="text-indent: 90px">{{$level3['menu_name']}}</td>
                                                                <td align="center">
                                                                    <input type="checkbox" name="is_read[]"
                                                                           value="is_read+{{$level3['id']}}" @if($level3['checked']['is_read'] == 1) {{'checked'}} @endif>
                                                                </td>
                                                                <td align="center">
                                                                    <input type="checkbox" name="is_create[]"
                                                                           value="is_create+{{$level3['id']}}" @if($level3['checked']['is_create'] == 1) {{'checked'}} @endif>
                                                                </td>
                                                                <td align="center">
                                                                    <input type="checkbox" name="is_update[]"
                                                                           value="is_update+{{$level3['id']}}" @if($level3['checked']['is_update'] == 1) {{'checked'}} @endif>
                                                                </td>
                                                                <td align="center">
                                                                    <input type="checkbox" name="is_delete[]"
                                                                           value="is_delete+{{$level3['id']}}" @if($level3['checked']['is_delete'] == 1) {{'checked'}} @endif>
                                                                </td>
                                                                <td align="center">
                                                                    <input type="checkbox" name="is_approve[]"
                                                                           value="is_approve+{{$level3['id']}}" @if($level3['checked']['is_approve'] == 1) {{'checked'}} @endif>
                                                                </td>
                                                                <td align="center">
                                                                    <input type="checkbox" name="is_download[]"
                                                                           value="is_download+{{$level3['id']}}" @if($level3['checked']['is_download'] == 1) {{'checked'}} @endif>
                                                                </td>
                                                            </tr>
                                                            @if($level3['level4'])
                                                                @foreach($level3['level4'] as $key5 => $level4)
                                                                    <tr class="parent_{{$level3['id']}} hide_child">
                                                                        <td><span style="color: #7F8FA4">{{$level4['id']}}</span>
                                                                        </td>
                                                                        <td style="text-indent: 130px">{{$level4['menu_name']}}</td>
                                                                        <td align="center">
                                                                            <input type="checkbox" name="is_read[]"
                                                                                   value="is_read+{{$level4['id']}}" @if($level4['checked']['is_read'] == 1) {{'checked'}} @endif>
                                                                        </td>
                                                                        <td align="center">
                                                                            <input type="checkbox" name="is_create[]"
                                                                                   value="is_create+{{$level4['id']}}" @if($level4['checked']['is_create'] == 1) {{'checked'}} @endif>
                                                                        </td>
                                                                        <td align="center">
                                                                            <input type="checkbox" name="is_update[]"
                                                                                   value="is_update+{{$level4['id']}}" @if($level4['checked']['is_update'] == 1) {{'checked'}} @endif>
                                                                        </td>
                                                                        <td align="center">
                                                                            <input type="checkbox" name="is_delete[]"
                                                                                   value="is_delete+{{$level4['id']}}" @if($level4['checked']['is_delete'] == 1) {{'checked'}} @endif>
                                                                        </td>
                                                                        <td align="center">
                                                                            <input type="checkbox" name="is_approve[]"
                                                                                   value="is_approve+{{$level4['id']}}" @if($level4['checked']['is_approve'] == 1) {{'checked'}} @endif>
                                                                        </td>
                                                                        <td align="center">
                                                                            <input type="checkbox" name="is_download[]"
                                                                                   value="is_download+{{$level4['id']}}" @if($level4['checked']['is_download'] == 1) {{'checked'}} @endif>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer text-right">
                            {!! Form::hidden('secure_id', @$secure_id) !!}
                            <button onclick="clickButton('{{route($route.'.index')}}')" class="ladda-button"
                                    type="button" data-size="s" data-color="red" data-style="slide-right"><i
                                        class="fas fa-times"></i> <span
                                        class="ladda-label">@lang('buttons.general.cancel')</span></button>
                            <button type="submit" class="ladda-button" data-size="s" data-color="green"
                                    data-style="slide-right"><i
                                        class="fas fa-save"></i> <span
                                        class="ladda-label">@lang('buttons.general.save')</span></button>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('after-scripts')
    <script type="text/javascript">
        function toggle(toggle) {
            toggle == 'undefined' ? toggle = 0 : toggle = toggle;
            if (toggle == 0) {
                $('.show_detail').attr('data-status', '1');
                $('.show_detail').click();
            } else {
                $('.show_detail').attr('data-status', '0');
                $('.show_detail').click();
            }
        }
        $(document).ready(function () {
            $('.hide_child').show();
            $('.show_detail').click(function () {
                let id = $(this).attr('data-id');
                let status = $(this).attr('data-status');
                if (status == 1) {
                    $(this).attr('data-status', '0');
                    $(".parent_" + id).show('fast');
                    $("#parent" + id).removeClass('fa fa-plus').addClass('fa fa-minus');
                } else {
                    $(this).attr('data-status', '1');
                    $(".parent_" + id).hide('fast');
                    $("#parent" + id).removeClass('fa fa-minus').addClass('fa fa-plus');
                }
            });

            $('.checkread').change(function () {
                let checked = $(this).prop('checked');
                $('.table').find('input[name*="is_read[]"]').prop('checked', checked);
            });
            $('.checkadd').change(function () {
                let checked = $(this).prop('checked');
                $('.table').find('input[name*="is_create[]"]').prop('checked', checked);
            });
            $('.checkedit').change(function () {
                let checked = $(this).prop('checked');
                $('.table').find('input[name*="is_update[]"]').prop('checked', checked);
            });
            $('.checkdel').change(function () {
                let checked = $(this).prop('checked');
                $('.table').find('input[name*="is_delete[]"]').prop('checked', checked);
            });
            $('.checkapprove').change(function () {
                let checked = $(this).prop('checked');
                $('.table').find('input[name*="is_approve[]"]').prop('checked', checked);
            });
            $('.checkdownload').change(function () {
                let checked = $(this).prop('checked');
                $('.table').find('input[name*="is_download[]"]').prop('checked', checked);
            });
        });
    </script>
@endpush
