@extends('backend._container.layout')
@section('title', $title)
@section('breadcrumbs')
    {{ Breadcrumbs::render('logs') }}
@stop
@section('content')
    <div class="container-fluid">
        <div id="ui-view">
            <div>
                <div class="fade-in">
                    <div class="card card-accent-secondary">
                        <div class="card-header d-flex align-items-center">
                            <h5 class="card-title mb-0"><i class="fas fa-list-alt"></i> {{$title}}</h5>
                            <div class="btn-group mfs-auto" role="group">
                                @if(access('is_delete', $route.'.*'))
                                    <button id="btnClearAll" type="button" class="btn btn-outline-danger">
                                        <i class="fas fa-trash"></i> @lang('buttons.general.clear')
                                    </button>
                                @endif
                            </div>
                        </div>
                        <div class="card-body card-body-table">
                            <table class="table table-responsive-sm table-striped table-hover table-bordered table-header-center text-nowrap"
                                   data-search="false"
                                   data-show-refresh="false"
                                   data-toggle="table"
                                   data-url="{{route($route.'.grid')}}"
                                   data-sort-name="ids"
                                   data-sort-order="desc"
                                   data-id-field="id"
                                   id="grid-data">
                                <thead>
                                <tr>
                                    <th data-field="user">User</th>
                                    <th data-field="module" data-class="nowrap">Module</th>
                                    <th data-field="task">Task</th>
                                    <th data-field="ipaddress">IP Address</th>
                                    <th data-field="created">Activity Date</th>
                                    <th data-field="note">Description</th>
                                </tr>
                                </thead>
                            </table>
                            <div class="hidden">
                                {{ Form::open(['route' => $route.'.destroy','id' => 'formDelete']) }}
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@push('after-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#btnClearAll').on('click', function (e) {
                Swal.fire({
                    title: '<i class="fas fa-exclamation-triangle text-danger"></i>&nbsp;{!! __('strings.common.messages.delete.confirmation') !!}',
                    html: 'Yakin akan hapus data riwayat aktifitas?',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('buttons.general.yes') !!}',
                    cancelButtonText: '{!! __('buttons.general.no') !!}',
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    allowOutsideClick: false,
                    allowEnterKey: false
                }).then((result) => {
                    result.value && $('#formDelete').submit();
                });
            });
        });
    </script>
@endpush
