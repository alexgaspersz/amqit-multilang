@extends('backend._container.layout')
@section('title', $title)
@section('breadcrumbs')
    {{ Breadcrumbs::render('categories') }}
@stop
@section('content')
    <div class="container-fluid">
        <div id="ui-view">
            <div>
                <div class="fade-in">
                    <div class="card card-accent-secondary">
                        <div class="card-header d-flex align-items-center">
                            <h5 class="card-title mb-0"><i class="fas fa-edit"></i> {{$title}}</h5>
                            <button class="btn btn-secondary mfs-auto mfe-1 d-print-none"
                                    onclick="clickButton('{{route($route.'.index')}}')"><span
                                        class="fas fa-arrow-left"></span> @lang('buttons.general.back')</button>
                        </div>
                        {!! Form::open(['route' => $route.'.store','id' => 'appForm', 'novalidate' => 'novalidate']) !!}
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-8 col-sm-12 col-xs-12">
                                    @if(app_lang())
                                        <div class="form-row">
                                            {!! Form::label('locale', __('strings.backend.dashboard.switch_lang'), ['class' => 'col-md-2 mb-0 pt-1 control-label']); !!}
                                            <div class="col-md-4">
                                                <select name="locale" id="locale" class="select2-nosearch">
                                                    @foreach($localization as $localeCode => $properties)
                                                        <option value="{{ $localeCode }}">{{ $properties['name'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                    @include('backend.errors.list')
                                    @foreach($localization as $localeCode => $properties)
                                        @php
                                            $asterik = $localeCode == defaultLang() ? 'required-label' : '';
                                            $locale[$localeCode] = [
                                                'name' => null,
                                                'slug' => null,
                                                'description' => null,
                                            ];
                                            if($item):
                                                foreach($item->translation as $row):
                                                    if($row->locale == $localeCode):
                                                        $locale[$localeCode] = [
                                                            'name' => $row->name,
                                                            'slug' => $row->slug,
                                                            'description' => $row->description,
                                                        ];
                                                    endif;
                                                endforeach;
                                            endif;
                                            $flag = '';
                                            if (app_lang()):
                                                $flag = '<img src="' . asset($properties['flag']) . '" width="16">';
                                            endif;
                                        @endphp
                                        <div class="form-group {{$localeCode}} {{ $errors->has('name_'.defaultLang()) ? ' has-error' : '' }}">
                                            {!! Form::label('name_'.$localeCode, __('strings.common.category'),['class' => $asterik]) !!} {!! $flag !!}
                                            {!! Form::text('name_'.$localeCode, @$locale[$localeCode]['name'] ? $locale[$localeCode]['name'] : old('name_'.$localeCode),['class' => 'form-control']); !!}
                                        </div>
                                        <div class="form-group {{$localeCode}}">
                                            {!! Form::label('description_'.$localeCode, __('strings.common.desc')) !!} {!! $flag !!}
                                            {!! Form::textarea('description_'.$localeCode, @$locale[$localeCode]['description'] ? $locale[$localeCode]['description'] : old('description_'.$localeCode),['class' => 'form-control','rows' => 3]); !!}
                                        </div>
                                    @endforeach
                                    <div class="form-group {{ $errors->has('slug') ? ' has-error' : '' }}">
                                        {!! Form::label('slug', __('strings.common.slug'),['class' => $asterik]) !!}
                                        {!! Form::text('slug', @$item->slug ? $item->slug : old('slug'),['class' => 'form-control', 'readonly' => 'readonly']); !!}
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card-header px-0 pt-0">
                                                <strong><i class="fas fa-cogs"></i> @lang('strings.common.option')
                                                </strong>
                                            </div>
                                            <div class="card-body px-0">
                                                <div class="form-row mb-3">
                                                    {!! Form::label('sort_order', __('strings.common.order'), ['class' => 'col-md-4 control-label']); !!}
                                                    <div class="col-md-8">
                                                        {!! Form::number('sort_order', @$item->sort_order ? $item->sort_order : old('sort_order'),['class' => 'form-control']); !!}
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    {!! Form::label('status', __('strings.common.status.label'), ['class' => 'col-md-4 control-label']); !!}
                                                    <div class="col-md-3">
                                                        <label class="c-switch c-switch-label c-switch-pill c-switch-success">
                                                            <input type="checkbox" name="status" class="c-switch-input"
                                                                   @if(@$item->status == true) checked @endif>
                                                            <span class="c-switch-slider" data-checked="&#x2713"
                                                                  data-unchecked="&#x2715"></span>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <small class="text-muted">@lang('strings.common.notes.publish')</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            {!! Form::hidden('secure_id', @$secure_id) !!}
                            <button onclick="clickButton('{{route($route.'.index')}}')" class="ladda-button"
                                    type="button" data-size="s" data-color="red" data-style="slide-right"><i
                                        class="fas fa-times"></i> <span
                                        class="ladda-label">@lang('buttons.general.cancel')</span></button>
                            <button type="submit" class="ladda-button" data-size="s" data-color="green"
                                    data-style="slide-right"><i
                                        class="fas fa-save"></i> <span
                                        class="ladda-label">@lang('buttons.general.save')</span></button>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@push('after-scripts')
    <script type="text/javascript">
        $('document').ready(function () {
            let $title = $('#name_{{defaultLang()}}');
            let $slug = $('#slug');

            $title.prop('required', true);
            $slug.prop('required', true);

            $title.on('keyup', function () {
                let alias = slugify(this.value, '-');
                $slug.val(alias);
            });
        });
    </script>
@endpush
