@extends('backend._container.layout')
@section('title', $title)
@section('breadcrumbs')
    {{ Breadcrumbs::render('pages') }}
@stop
@section('content')
    <div class="container-fluid">
        <div id="ui-view">
            <div>
                <div class="fade-in">
                    <div class="card card-accent-secondary">
                        <div class="card-header d-flex align-items-center">
                            <h5 class="card-title mb-0"><i class="fas fa-edit"></i> {{$title}}</h5>
                            <button class="btn btn-secondary mfs-auto mfe-1 d-print-none"
                                    onclick="clickButton('{{route($route.'.index')}}')"><span
                                        class="fas fa-arrow-left"></span> @lang('buttons.general.back')</button>
                        </div>
                        {!! Form::open(['route' => $route.'.store','id' => 'appForm', 'novalidate' => 'novalidate', 'files' => true]) !!}
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-8 col-sm-12 col-xs-12">
                                    @if(app_lang())
                                        <div class="form-row">
                                            {!! Form::label('locale', __('strings.backend.dashboard.switch_lang'), ['class' => 'col-md-2 mb-0 pt-1 control-label']); !!}
                                            <div class="col-md-4">
                                                <select name="locale" id="locale" class="select2-nosearch">
                                                    @foreach($localization as $localeCode => $properties)
                                                        <option value="{{ $localeCode }}">{{ $properties['name'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                    @include('backend.errors.list')
                                    @foreach($localization as $localeCode => $properties)
                                        @php
                                            $asterik = $localeCode == defaultLang() ? 'required-label' : '';
                                            $locale[$localeCode] = [
                                                'page_title' => null,
                                                'page_meta' => null,
                                                'page_desc' => null,
                                                'content' => null,
                                            ];
                                             if($item):
                                                foreach($item->translation as $row):
                                                    if($row->locale == $localeCode):
                                                        $locale[$localeCode] = [
                                                            'page_title' => $row->page_title,
                                                            'page_meta' => $row->page_meta,
                                                            'page_desc' => $row->page_desc,
                                                            'content' => $row->content,
                                                        ];
                                                    endif;
                                                endforeach;
                                            endif;
                                            $flag = '';
                                            if (app_lang()):
                                                $flag = '<img src="' . asset($properties['flag']) . '" width="16">';
                                            endif;
                                        @endphp
                                        <div class="form-group {{$localeCode}} {{ $errors->has('page_title_'.defaultLang()) ? ' has-error' : '' }}">
                                            {!! Form::label('page_title_'.$localeCode, __('strings.common.title'),['class' => $asterik]) !!} {!! $flag !!}
                                            {!! Form::text('page_title_'.$localeCode, @$locale[$localeCode]['page_title'] ? $locale[$localeCode]['page_title'] : old('page_title_'.$localeCode),['class' => 'form-control']); !!}
                                        </div>
                                        <div class="form-group {{$localeCode}} {{ $errors->has('content_'.defaultLang()) ? ' has-error' : '' }}">
                                            {!! Form::label('content_'.$localeCode, __('strings.common.content'),['class' => $asterik]) !!} {!! $flag !!}
                                            {!! Form::textarea('content_'.$localeCode, @$locale[$localeCode]['content'] ? $locale[$localeCode]['content'] : old('content_'.$localeCode),['class' => 'f-editor form-control']); !!}
                                        </div>
                                        <div class="form-group {{$localeCode}}">
                                            {!! Form::label('page_meta_'.$localeCode, __('strings.common.metadata')) !!} {!! $flag !!}
                                            {!! Form::text('page_meta_'.$localeCode, @$locale[$localeCode]['page_meta'] ? $locale[$localeCode]['page_meta'] : old('page_meta_'.$localeCode),['class' => 'form-control']); !!}
                                        </div>
                                        <div class="form-group {{$localeCode}}">
                                            {!! Form::label('page_desc_'.$localeCode, __('strings.common.metadesc')) !!} {!! $flag !!}
                                            {!! Form::textarea('page_desc_'.$localeCode, @$locale[$localeCode]['page_desc'] ? $locale[$localeCode]['page_desc'] : old('page_desc_'.$localeCode),['class' => 'form-control','rows' => 3]); !!}
                                        </div>
                                    @endforeach
                                    <div class="form-group {{ $errors->has('slug') ? ' has-error' : '' }}">
                                        {!! Form::label('slug', __('strings.common.slug'),['class' => $asterik]) !!}
                                        {!! Form::text('slug', @$item->slug ? $item->slug : old('slug'),['class' => 'form-control', 'readonly' => 'readonly']); !!}
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card-header px-0 pt-0">
                                                <strong><i class="fas fa-cogs"></i> @lang('strings.common.option')
                                                </strong>
                                            </div>
                                            <div class="card-body px-0">
                                                <div class="form-row">
                                                    {!! Form::label('visible', __('forms.page.title'), ['class' => 'col-md-4 control-label text-left']); !!}
                                                    <div class="col-md-3">
                                                        <label class="c-switch c-switch-label c-switch-pill c-switch-success">
                                                            <input type="checkbox" name="visible" class="c-switch-input"
                                                                   @if(@$item->visible == true) checked @endif>
                                                            <span class="c-switch-slider" data-checked="&#x2713"
                                                                  data-unchecked="&#x2715"></span>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <small class="text-muted">@lang('strings.common.notes.show')</small>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    {!! Form::label('status', __('strings.common.status.label'), ['class' => 'col-md-4 control-label text-left']); !!}
                                                    <div class="col-md-3">
                                                        <label class="c-switch c-switch-label c-switch-pill c-switch-success">
                                                            <input type="checkbox" name="status" class="c-switch-input"
                                                                   @if(@$item->status == true) checked @endif>
                                                            <span class="c-switch-slider" data-checked="&#x2713"
                                                                  data-unchecked="&#x2715"></span>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <small class="text-muted">@lang('strings.common.notes.publish')</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="card-header px-0">
                                                <strong><span
                                                            class="fa fa-image"></span> @lang('strings.common.header_image')
                                                </strong>
                                            </div>
                                            <div class="card-body pb-0 px-0">
                                                <div class="form-group">
                                                    <div id="uploadImage" class="dropzone dropzone-mini"></div>
                                                    <ul class="list-unstyled text-primary">
                                                        <li>
                                                            <small>{!! __('strings.common.upload_limit',['num' => 1]) !!}</small>
                                                        </li>
                                                        <li>
                                                            <small>{!! __('strings.common.upload_size',['num' => imageSize()]) !!}
                                                                MB;
                                                            </small>
                                                        </li>
                                                        <li>
                                                            <small>{!! __('strings.common.upload_ext',['ext' => imageExt()]) !!}</small>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="card-header pt-0 px-0">
                                                <strong><span
                                                            class="fas fa-file-pdf"></span> @lang('forms.pengumuman.attached')
                                                </strong>
                                            </div>
                                            <div class="card-body px-0">
                                                <div class="form-group">
                                                    <div class="file-loading">
                                                        {!! Form::file('file_pdf',['class' => 'file-upload','accept' => 'application/pdf']); !!}
                                                    </div>
                                                    <div id="errorBlock" class="help-block"></div>
                                                    <ul class="list-unstyled text-primary">
                                                        <li>
                                                            <small>{!! __('strings.common.upload_limit',['num' => 1]) !!}</small>
                                                        </li>
                                                        <li>
                                                            <small>{!! __('strings.common.upload_size',['num' => filesSize()]) !!}
                                                                MB;
                                                            </small>
                                                        </li>
                                                        <li>
                                                            <small>{!! __('strings.common.upload_ext',['ext' => 'pdf']) !!}</small>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            {!! Form::hidden('secure_id', @$secure_id) !!}
                            {!! Form::hidden('bg', @$item->bg ? $item->bg : old('bg'), ['id' => 'assetId']) !!}
                            <button onclick="clickButton('{{route($route.'.index')}}')" class="ladda-button"
                                    type="button" data-size="s" data-color="red" data-style="slide-right"><i
                                        class="fas fa-times"></i> <span
                                        class="ladda-label">@lang('buttons.general.cancel')</span></button>
                            <button type="submit" class="ladda-button" data-size="s" data-color="green"
                                    data-style="slide-right"><i
                                        class="fas fa-save"></i> <span
                                        class="ladda-label">@lang('buttons.general.save')</span></button>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@push('after-styles')
    {{ Html::style(app_vendor('dropzone/dropzone.min.css')) }}
    {{ Html::style(app_vendor('froala/css/froala_editor.pkgd.min.css')) }}
    {{ Html::style(app_vendor('froala/css/froala_style.min.css')) }}
@endpush
@push('after-scripts')
    {{ Html::script(app_vendor('froala/js/froala_editor.pkgd.min.js'), ['type' => 'text/javascript']) }}
    {{ Html::script(app_vendor('froala/js/plugins/image.min.js'), ['type' => 'text/javascript']) }}
    {{ Html::script(app_vendor('froala/js/plugins/image_manager.min.js'), ['type' => 'text/javascript']) }}
    {{ Html::script(app_vendor('froala/js/plugins/fullscreen.min.js'), ['type' => 'text/javascript']) }}
    {{ Html::script(app_vendor('froala/js/languages/id.js'), ['type' => 'text/javascript']) }}
    {{ Html::script(app_vendor('dropzone/dropzone.min.js'), ['type' => 'text/javascript']) }}
    <script type="text/javascript">
        document.querySelector("html").classList.add('js');
        Dropzone.autoDiscover = false;
        $('document').ready(function () {
            let $title = $('#page_title_{{defaultLang()}}');
            let $content = $('#content_{{defaultLang()}}');
            let $slug = $('#slug');

            $title.prop('required', true);
            $content.prop('required', true);
            $slug.prop('required', true);

            $title.on('keyup', function () {
                let alias = slugify(this.value, '-');
                $slug.val(alias);
            });

            let $filePdf = $(".file-upload");
            $filePdf.uploadInput({
                browseLabel: "Pilih File",
                removeLabel: "Hapus",
                showPreview: true,
                maxFileSize: {!! filesSize() * 1000 !!},
                allowedFileExtensions: ['pdf']
            });

            let $upload = $('#uploadImage');
            let csrf = $('input[name="_token"]');
            let $assetId = $('#assetId');
            let optParams = {
                _token: csrf.val(),
                dataId: '{{@$secure_id}}',
                category: 'PAGE',
                baseFolder: 'images',
                title: $('input[name="page_title_{{defaultLang()}}"]').val()
            };

            let $prevUrl = null;
            if (!$.isEmpty($assetId.val())) {
                $prevUrl = '{{url('service/uploads/preview')}}/' + $assetId.val() + '/350/350';
            }
            let uploadOptions = {
                url: '{{route('service.uploads.single')}}',
                previewUrl: $prevUrl,
                removeUrl: '{{route('service.uploads.post.remove')}}',
                acceptedFiles: '{!! imageExt() !!}',
                maxFiles: 1,
                maxFilesize: '{!! imageSize() !!}',
                thumbnailWidth: 350,
                thumbnailHeight: 350
            };

            $upload.singleUpload($.apply({
                params: optParams
            }, uploadOptions));

            let editorOptions = {
                imageUploadURL: "{!! route('service.uploads.editor') !!}",
                imageMaxSize: {!! filesSize() * 1024 * 1024 !!},
                imageManagerPreloader: "{!! app_images('loading.gif') !!}",
                imageManagerLoadURL: "{!! route('service.uploads.manager') !!}",
                imageManagerDeleteURL: "{!! route('service.uploads.manager.remove') !!}",
            };
            let $editor = $('.f-editor');
            $editor.froalaText($.apply({
                imageUploadParams: {
                    baseFolder: 'images',
                    category: 'EDITOR',
                    _token: csrf.val()
                },
                imageManagerLoadParams: {
                    category: 'EDITOR'
                },
                imageManagerDeleteParams: {
                    _token: csrf.val(),
                    _method: "DELETE"
                }
            }, editorOptions));
        });
    </script>
@endpush
