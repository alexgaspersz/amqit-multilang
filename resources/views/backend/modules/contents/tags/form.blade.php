@extends('backend._container.layout')
@section('title', $title)
@section('breadcrumbs')
    {{ Breadcrumbs::render('tags') }}
@stop
@section('content')
    <div class="container-fluid">
        <div id="ui-view">
            <div>
                <div class="fade-in">
                    <div class="card card-accent-secondary">
                        <div class="card-header d-flex align-items-center">
                            <h5 class="card-title mb-0"><i class="fas fa-edit"></i> {{$title}}</h5>
                            <button class="btn btn-secondary mfs-auto mfe-1 d-print-none"
                                    onclick="clickButton('{{route($route.'.index')}}')"><span
                                        class="fas fa-arrow-left"></span> @lang('buttons.general.back')</button>
                        </div>
                        {!! Form::open(['route' => $route.'.store','id' => 'appForm', 'novalidate' => 'novalidate']) !!}
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6 col-sm-6 col-xs-12">
                                    @include('backend.errors.list')
                                    <div class="form-group {{ $errors->has('post_tag') ? ' has-error' : '' }}">
                                        {!! Form::label('post_tag', __('module.tags.module'),['class' => 'required-label']) !!}
                                        {!! Form::text('post_tag', @$item->post_tag ? $item->post_tag : old('post_tag'),['required' => true, 'class' => 'form-control']); !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            {!! Form::hidden('secure_id', @$secure_id) !!}
                            <button onclick="clickButton('{{route($route.'.index')}}')" class="ladda-button"
                                    type="button" data-size="s" data-color="red" data-style="slide-right"><i
                                        class="fas fa-times"></i> <span
                                        class="ladda-label">@lang('buttons.general.cancel')</span></button>
                            <button type="submit" class="ladda-button" data-size="s" data-color="green"
                                    data-style="slide-right"><i
                                        class="fas fa-save"></i> <span
                                        class="ladda-label">@lang('buttons.general.save')</span></button>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
