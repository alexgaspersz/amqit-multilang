@extends('backend._container.layout')
@section('title', $title)
@section('breadcrumbs')
    {{ Breadcrumbs::render('tags') }}
@stop
@section('content')
    <div class="container-fluid">
        <div id="ui-view">
            <div>
                <div class="fade-in">
                    <div class="card card-accent-secondary">
                        <div class="card-header d-flex align-items-center">
                            <h5 class="card-title mb-0"><i class="fas fa-list-alt"></i> {{$title}}</h5>
                            <div class="btn-group mfs-auto" role="group">
                                @if(access('is_create', $route.'.*'))
                                    <button onclick="clickButton('{{ route($route.'.update') }}')" type="button"
                                            class="btn btn-outline-primary">
                                        <i class="fas fa-plus"></i> @lang('buttons.general.create')
                                    </button>
                                @endif
                                @if(access('is_delete', $route.'.*'))
                                    <button id="btnRemoveAll" type="button" class="btn btn-outline-danger">
                                        <i class="fas fa-trash"></i> @lang('buttons.general.delete')
                                    </button>
                                @endif
                            </div>
                        </div>
                        <div class="card-body card-body-table">
                            {{ Form::open(['route' => $route.'.batch','id' => 'batchDelete']) }}
                            <table class="table table-responsive-sm table-striped table-hover table-bordered table-header-center text-nowrap"
                                   data-search="false"
                                   data-show-refresh="false"
                                   data-toggle="table"
                                   data-url="{{route($route.'.grid')}}"
                                   data-sort-name="ids"
                                   data-sort-order="desc"
                                   data-id-field="id"
                                   id="grid-data">
                                <thead>
                                <tr>
                                    <th data-width="25" data-align="center" data-formatter="checkAll">
                                        <input type="checkbox" class="checkall" data-toggle="tooltip"
                                               data-placement="top"
                                               title="@lang('strings.common.select_all')">
                                    </th>
                                    <th data-field="tag">@lang('module.tags.module')</th>
                                    <th data-field="commands"
                                        data-class="nowrap"
                                        data-width="70"
                                        data-formatter="commandAction"
                                        data-events="commandEvents">@lang('strings.common.status.action')
                                    </th>
                                </tr>
                                </thead>
                            </table>
                            {{ Form::close() }}
                            <div class="hidden">
                                {{ Form::open(['route' => $route.'.destroy','id' => 'formDelete']) }}
                                {{ Form::hidden('secure_id', null, ['id' => 'row-id']) }}
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
