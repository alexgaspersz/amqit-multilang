@extends('backend._container.layout')
@section('title', $title)
@section('breadcrumbs')
    {{ Breadcrumbs::render('posts') }}
@stop
@section('content')
    <div class="container-fluid">
        <div id="ui-view">
            <div class="fade-in">
                <div class="card card-accent-secondary">
                    <div class="card-header d-flex align-items-center">
                        <h5 class="card-title mb-0"><i class="fas fa-edit"></i> {{$title}}</h5>
                        <button class="btn btn-secondary mfs-auto mfe-1 d-print-none"
                                onclick="clickButton('{{route($route.'.index')}}')"><span
                                    class="fas fa-arrow-left"></span> @lang('buttons.general.back')</button>
                    </div>
                    {!! Form::open(['route' => $route.'.store','id' => 'appForm', 'novalidate' => 'novalidate']) !!}
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-8 col-sm-12 col-xs-12">
                                @if(app_lang())
                                    <div class="form-row">
                                        {!! Form::label('locale', __('strings.backend.dashboard.switch_lang'), ['class' => 'col-md-2 mb-0 pt-1 control-label']); !!}
                                        <div class="col-md-4">
                                            <select name="locale" id="locale" class="select2-nosearch">
                                                @foreach($localization as $localeCode => $properties)
                                                    <option value="{{ $localeCode }}">{{ $properties['name'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                @endif
                                @include('backend.errors.list')
                                @foreach($localization as $localeCode => $properties)
                                    @php
                                        $asterik = $localeCode == defaultLang() ? 'required-label' : '';
                                        $locale[$localeCode] = [
                                            'title' => null,
                                            'meta' => null,
                                            'intro' => null,
                                            'description' => null,
                                            'content' => null,
                                        ];
                                        if($item):
                                            foreach($item->translation as $row):
                                                if($row->locale == $localeCode):
                                                    $locale[$localeCode] = [
                                                        'title' => $row->title,
                                                        'meta' => $row->meta,
                                                        'intro' => $row->intro,
                                                        'description' => $row->description,
                                                        'content' => $row->content,
                                                    ];
                                                endif;
                                            endforeach;
                                        endif;
                                        $flag = '';
                                        if (app_lang()):
                                            $flag = '<img src="' . asset($properties['flag']) . '" width="16">';
                                        endif;
                                    @endphp
                                    <div class="form-group {{$localeCode}} {{ $errors->has('title_'.defaultLang()) ? ' has-error' : '' }}">
                                        {!! Form::label('title_'.$localeCode, __('strings.common.title'),['class' => $asterik]) !!} {!! $flag !!}
                                        {!! Form::text('title_'.$localeCode, @$locale[$localeCode]['title'] ? $locale[$localeCode]['title'] : old('title_'.$localeCode),['class' => 'form-control']); !!}
                                    </div>
                                    <div class="form-group {{$localeCode}} {{ $errors->has('intro_'.defaultLang()) ? ' has-error' : '' }}">
                                        {!! Form::label('intro_'.$localeCode, __('strings.common.intro'),['class' => $asterik]) !!} {!! $flag !!}
                                        {!! Form::textarea('intro_'.$localeCode, @$locale[$localeCode]['intro'] ? $locale[$localeCode]['intro'] : old('intro_'.$localeCode),['class' => 'form-control', 'rows' => 4]); !!}
                                    </div>
                                    <div class="form-group {{$localeCode}} {{ $errors->has('content_'.defaultLang()) ? ' has-error' : '' }}">
                                        {!! Form::label('content_'.$localeCode, __('strings.common.content'),['class' => $asterik]) !!} {!! $flag !!}
                                        {!! Form::textarea('content_'.$localeCode, @$locale[$localeCode]['content'] ? $locale[$localeCode]['content'] : old('content_'.$localeCode),['class' => 'f-editor form-control']); !!}
                                    </div>
                                    <div class="form-group {{$localeCode}}">
                                        {!! Form::label('meta_'.$localeCode, __('strings.common.metadata')) !!} {!! $flag !!}
                                        {!! Form::text('meta_'.$localeCode, @$locale[$localeCode]['meta'] ? $locale[$localeCode]['meta'] : old('meta_'.$localeCode),['class' => 'form-control']); !!}
                                    </div>
                                    <div class="form-group {{$localeCode}}">
                                        {!! Form::label('description_'.$localeCode, __('strings.common.metadesc')) !!} {!! $flag !!}
                                        {!! Form::textarea('description_'.$localeCode, @$locale[$localeCode]['description'] ? $locale[$localeCode]['description'] : old('description_'.$localeCode),['class' => 'form-control','rows' => 2]); !!}
                                    </div>
                                @endforeach
                                <div class="form-group {{ $errors->has('slug') ? ' has-error' : '' }}">
                                    {!! Form::label('slug', __('strings.common.slug'),['class' => 'required-label']) !!}
                                    {!! Form::text('slug', @$item->slug ? $item->slug : old('slug'),['class' => 'form-control', 'readonly' => 'readonly']); !!}
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card-header px-0 pt-0">
                                            <strong><i class="fas fa-cogs"></i> @lang('strings.common.option')</strong>
                                        </div>
                                        <div class="card-body px-0 pb-0">
                                            <div class="form-row mb-3">
                                                {!! Form::label('author_id', __('strings.common.author'),['class' => 'col-md-4 control-label']); !!}
                                                <div class="col-md-8">
                                                    {!! Form::select('author_id', [], null, ['id' => 'author_id', 'class' => 'form-control select2']); !!}
                                                </div>
                                            </div>
                                            <div class="form-row mb-3">
                                                {!! Form::label('publish_at', __('strings.common.dates.publish_start'), ['class' => 'col-md-4 control-label']) !!}
                                                <div class="col-md-8">
                                                    <div class="input-group">
                                                        {!! Form::text('publish_at', @$item->publish_at ? $item->publish_at : (old('publish_at') ? old('publish_at') : date('Y-m-d')),['class' => 'form-control readonly datepicker']); !!}
                                                        <span class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    <i class="fas fa-calendar"></i>
                                                                </span>
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row mb-3">
                                                {!! Form::label('status', __('strings.common.status.label'), ['class' => 'col-md-4 control-label']); !!}
                                                <div class="col-md-8">
                                                    {!! Form::select('status', ['0' => __('strings.common.status.draft'),'1' => __('strings.common.status.reviewed'),'2' => __('strings.common.status.published')], @$item->status, ['class' => 'form-control select2-nosearch']); !!}
                                                </div>
                                            </div>
                                            <div class="form-row mb-3">
                                                {!! Form::label('allow_comment', __('forms.post.comments'), ['class' => 'col-md-4 control-label']); !!}
                                                <div class="col-md-3">
                                                    <label class="c-switch c-switch-label c-switch-pill c-switch-success">
                                                        <input type="checkbox" name="allow_comment"
                                                               class="c-switch-input"
                                                               @if(@$item->allow_comment == true) checked @endif>
                                                        <span class="c-switch-slider" data-checked="&#x2713"
                                                              data-unchecked="&#x2715"></span>
                                                    </label>
                                                </div>
                                                <div class="col-md-5">
                                                    <small class="text-muted">@lang('strings.common.notes.comment')</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="card-header px-0 pt-0">
                                            <strong><i class="fas fa-cogs"></i> @lang('strings.common.category')
                                            </strong>
                                        </div>
                                        <div class="card-body px-0 pb-0">
                                            <div class="form-group">
                                                <ul class="list-unstyled mb-0">
                                                    @foreach($categories as $row)
                                                        <li class="py-1">
                                                            <label class="check">
                                                                {{ Form::radio('categories', $row->id, $row->id == @$item->categories ? true : false, ['id' => 'cat'.$row->id, 'class' => 'icheckbox']) }} {{@$row->single_translate->name}}
                                                            </label>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="card-header px-0 pt-0">
                                            <strong><i class="fas fa-cogs"></i> @lang('strings.common.feature_image')
                                            </strong>
                                        </div>
                                        <div class="card-body px-0 pb-0">
                                            <div class="form-group">
                                                <div id="uploadImage" class="dropzone dropzone-mini"></div>
                                                <ul class="list-unstyled text-primary">
                                                    <li>
                                                        <small>{!! __('strings.common.upload_limit',['num' => 1]) !!}</small>
                                                    </li>
                                                    <li>
                                                        <small>{!! __('strings.common.upload_size',['num' => imageSize()]) !!}
                                                            MB
                                                        </small>
                                                    </li>
                                                    <li>
                                                        <small>{!! __('strings.common.upload_ext',['ext' => imageExt()]) !!}</small>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        {!! Form::hidden('secure_id', @$secure_id) !!}
                        {!! Form::hidden('images', @$item->images ? $item->images : old('images'), ['id' => 'assetId']) !!}
                        <button onclick="clickButton('{{route($route.'.index')}}')" class="ladda-button" type="button"
                                data-size="s" data-color="red" data-style="slide-right"><i class="fas fa-times"></i>
                            <span class="ladda-label">@lang('buttons.general.cancel')</span></button>
                        <button type="submit" class="ladda-button" data-size="s" data-color="green"
                                data-style="slide-right"><i
                                    class="fas fa-save"></i> <span
                                    class="ladda-label">@lang('buttons.general.save')</span></button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop
@push('after-styles')
    {{ Html::style(app_vendor('dropzone/dropzone.min.css')) }}
    {{ Html::style(app_vendor('froala/css/froala_editor.pkgd.min.css')) }}
    {{ Html::style(app_vendor('froala/css/froala_style.min.css')) }}
@endpush
@push('after-scripts')
    {{ Html::script(app_vendor('froala/js/froala_editor.pkgd.min.js'), ['type' => 'text/javascript']) }}
    {{ Html::script(app_vendor('froala/js/plugins/image.min.js'), ['type' => 'text/javascript']) }}
    {{ Html::script(app_vendor('froala/js/plugins/image_manager.min.js'), ['type' => 'text/javascript']) }}
    {{ Html::script(app_vendor('froala/js/plugins/fullscreen.min.js'), ['type' => 'text/javascript']) }}
    {{ Html::script(app_vendor('froala/js/languages/id.js'), ['type' => 'text/javascript']) }}
    {{ Html::script(app_vendor('dropzone/dropzone.min.js'), ['type' => 'text/javascript']) }}
    <script type="text/javascript">
        document.querySelector("html").classList.add('js');
        Dropzone.autoDiscover = false;
        $('document').ready(function () {
            let $title = $('#title_{{defaultLang()}}');
            let $intro = $('#intro_{{defaultLang()}}');
            let $content = $('#content_{{defaultLang()}}');
            let $slug = $('#slug');

            $title.prop('required', true);
            $intro.prop('required', true);
            $content.prop('required', true);
            $slug.prop('required', true);

            $title.on('keyup', function () {
                let alias = slugify(this.value, '-');
                $slug.val(alias);
            });

            let $author = $('#author_id');
            $author.cascadeCombo("{{route('common.users')}}", "{!! @$item->author_id ? $item->author_id : session('uid') !!}", null, "{!! __('forms.choose.author') !!}");

            let $upload = $('#uploadImage');
            let csrf = $('input[name="_token"]');
            let $assetId = $('#assetId');
            let optParams = {
                _token: csrf.val(),
                dataId: '{{@$secure_id}}',
                category: 'NEWS',
                baseFolder: 'images',
                title: $('input[name="caption"]').val()
            };

            let $prevUrl = null;
            if (!$.isEmpty($assetId.val())) {
                $prevUrl = '{{url('service/uploads/preview')}}/' + $assetId.val() + '/350/350';
            }
            let uploadOptions = {
                url: '{{route('service.uploads.single')}}',
                previewUrl: $prevUrl,
                removeUrl: '{{route('service.uploads.post.remove')}}',
                acceptedFiles: '{!! imageExt() !!}',
                maxFiles: 1,
                maxFilesize: '{!! imageSize() !!}',
                thumbnailWidth: 350,
                thumbnailHeight: 350,
                previewTemplate: '<div class="dz-preview dz-file-preview">\n' +
                    '    <div class="dz-image"><img data-dz-thumbnail /></div>\n' +
                    '    <div class="dz-details">\n' +
                    '        <div class="dz-size"><span data-dz-size></span></div>\n' +
                    '        <div class="dz-filename"><span data-dz-name></span></div>\n' +
                    '    </div>\n' +
                    '    <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>\n' +
                    '    <div class="dz-error-message"><span data-dz-errormessage></span></div>\n' +
                    '    <input type="text" class="form-control mt-2" name="caption" id="caption" value="{!! @$item->featuredImages ? $item->featuredImages->title : old('caption') !!}" placeholder="{!! trans('placeholder.forms.imagecaption') !!}">\n' +
                    '</div>'
            };

            $upload.singleUpload($.apply({
                params: optParams
            }, uploadOptions));

            let editorOptions = {
                imageUploadURL: "{!! route('service.uploads.editor') !!}",
                imageMaxSize: {!! filesSize() * 1024 * 1024 !!},
                imageManagerPreloader: "{!! app_images('loading.gif') !!}",
                imageManagerLoadURL: "{!! route('service.uploads.manager') !!}",
                imageManagerDeleteURL: "{!! route('service.uploads.manager.remove') !!}",
            };
            let $editor = $('.f-editor');
            $editor.froalaText($.apply({
                imageUploadParams: {
                    baseFolder: 'images',
                    category: 'EDITOR',
                    _token: csrf.val()
                },
                imageManagerLoadParams: {
                    category: 'EDITOR'
                },
                imageManagerDeleteParams: {
                    _token: csrf.val(),
                    _method: "DELETE"
                }
            }, editorOptions));
        });
    </script>
@endpush
