@extends('backend._container.layout')
@section('title', $title)
@section('breadcrumbs')
    {{ Breadcrumbs::render('photos') }}
@stop
@section('content')
    <div class="container-fluid">
        <div id="ui-view">
            <div>
                <div class="fade-in">
                    <div class="card card-accent-secondary">
                        <div class="card-header d-flex align-items-center">
                            <h5 class="card-title mb-0"><i class="fas fa-edit"></i> {{$title}}</h5>
                            <button class="btn btn-secondary mfs-auto mfe-1 d-print-none"
                                    onclick="clickButton('{{route($route.'.index')}}')"><span
                                        class="fas fa-arrow-left"></span> @lang('buttons.general.back')</button>
                        </div>
                        {!! Form::open(['route' => $route.'.store','id' => 'appForm', 'novalidate' => 'novalidate']) !!}
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-8 col-sm-12 col-xs-12">
                                    @if(app_lang())
                                        <div class="form-row">
                                            {!! Form::label('locale', __('strings.backend.dashboard.switch_lang'), ['class' => 'col-md-2 mb-0 pt-1 control-label']); !!}
                                            <div class="col-md-4">
                                                <select name="locale" id="locale" class="select2-nosearch">
                                                    @foreach($localization as $localeCode => $properties)
                                                        <option value="{{ $localeCode }}">{{ $properties['name'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                    @include('backend.errors.list')
                                    @foreach($localization as $localeCode => $properties)
                                        @php
                                            $asterik = $localeCode == defaultLang() ? 'required-label' : '';
                                            $locale[$localeCode] = [
                                                'title' => null,
                                                'description' => null
                                            ];
                                             if($item):
                                                foreach($item->translation as $row):
                                                    if($row->locale == $localeCode):
                                                        $locale[$localeCode] = [
                                                            'title' => $row->title,
                                                            'description' => $row->description
                                                        ];
                                                    endif;
                                                endforeach;
                                            endif;
                                            $flag = '';
                                            if (app_lang()):
                                                $flag = '<img src="' . asset($properties['flag']) . '" width="16">';
                                            endif;
                                        @endphp
                                        <div class="form-group {{$localeCode}} {{ $errors->has('title_'.defaultLang()) ? ' has-error' : '' }}">
                                            {!! Form::label('title_'.$localeCode, __('forms.gallery.photo.name'),['class' => $asterik]) !!} {!! $flag !!}
                                            {!! Form::text('title_'.$localeCode, @$locale[$localeCode]['title'] ? $locale[$localeCode]['title'] : old('title_'.$localeCode),['class' => 'form-control']); !!}
                                        </div>
                                        <div class="form-group {{$localeCode}}">
                                            {!! Form::label('description_'.$localeCode, __('strings.common.desc')) !!} {!! $flag !!}
                                            {!! Form::textarea('description_'.$localeCode, @$locale[$localeCode]['description'] ? $locale[$localeCode]['description'] : old('description_'.$localeCode),['class' => 'form-control', 'rows' => 3]); !!}
                                        </div>
                                    @endforeach
                                    <div class="form-group {{ $errors->has('images') ? ' has-error' : '' }}">
                                        {!! Form::label('imagesId', __('forms.gallery.photo.images'),['class' => 'required-label']) !!}
                                        <div id="uploadGallery" class="dropzone"></div>
                                        {!! Form::hidden('images', @$item->images ? $item->images : old('images'), ['id' => 'imagesId', 'required' => true]) !!}
                                        <button class="btn btn-sm btn-outline-danger pull-right mt-1 mfe-auto"
                                                type="button" id="btnUpload"><i class="fas fa-upload"></i> Upload
                                        </button>
                                        <ul class="list-unstyled text-primary mb-0">
                                            <li>
                                                <small>{!! __('strings.common.upload_limit',['num' => 8]) !!}</small>
                                            </li>
                                            <li>
                                                <small>{!! __('strings.common.upload_size',['num' => imageSize()]) !!}MB
                                                    per file;
                                                </small>
                                            </li>
                                            <li>
                                                <small>{!! __('strings.common.upload_ext',['ext' => imageExt()]) !!}</small>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                        <table class="table table-responsive-sm table-striped table-hover table-condensed table-bordered table-header-center text-nowrap"
                                               data-search="false"
                                               data-show-refresh="false"
                                               data-toggle="table"
                                               data-url="{{route($route.'.grid.photos',['id' => @$secure_id])}}"
                                               data-sort-name="ids"
                                               data-sort-order="desc"
                                               data-page-size="3"
                                               data-id-field="id"
                                               id="grid-photos">
                                            <thead>
                                            <tr>
                                                <th data-field="thumb" data-width="50"
                                                    data-formatter="imageFormatter">@lang('forms.gallery.file')</th>
                                                <th data-field="extension">@lang('forms.gallery.type')</th>
                                                <th data-field="size">@lang('forms.gallery.size')</th>
                                                <th data-field="commands"
                                                    data-class="nowrap"
                                                    data-width="70"
                                                    data-formatter="commandAction"
                                                    data-events="commandEvents">@lang('strings.common.status.action')
                                                </th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div class="form-group {{ $errors->has('slug') ? ' has-error' : '' }}">
                                        {!! Form::label('slug', __('strings.common.slug'),['class' => $asterik]) !!}
                                        {!! Form::text('slug', @$item->slug ? $item->slug : old('slug'),['class' => 'form-control', 'readonly' => 'readonly']); !!}
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card-header px-0 pt-0">
                                                <strong><i class="fas fa-cogs"></i> @lang('strings.common.option')
                                                </strong>
                                            </div>
                                            <div class="card-body px-0">
                                                <div class="form-row mb-3">
                                                    {!! Form::label('publish_at', __('strings.common.dates.publish_start'), ['class' => 'col-md-4 control-label']) !!}
                                                    <div class="col-md-8">
                                                        <div class="input-group">
                                                            {!! Form::text('publish_at', @$item->publish_at ? $item->publish_at : (old('publish_at') ? old('publish_at') : date('Y-m-d')),['class' => 'form-control readonly datepicker']); !!}
                                                            <span class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    <i class="fas fa-calendar"></i>
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    {!! Form::label('status', __('strings.common.status.label'), ['class' => 'col-md-4 control-label']); !!}
                                                    <div class="col-md-3">
                                                        <label class="c-switch c-switch-label c-switch-pill c-switch-success">
                                                            <input type="checkbox" name="status" class="c-switch-input"
                                                                   @if(@$item->status == true) checked @endif>
                                                            <span class="c-switch-slider" data-checked="&#x2713"
                                                                  data-unchecked="&#x2715"></span>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <small class="text-muted">@lang('strings.common.notes.publish')</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="card-header px-0">
                                                <strong><span
                                                            class="fa fa-image"></span> @lang('forms.gallery.photo.cover')
                                                </strong>
                                            </div>
                                            <div class="card-body px-0">
                                                <div class="form-group">
                                                    <div id="uploadImage" class="dropzone dropzone-mini"></div>
                                                    <ul class="list-unstyled text-primary">
                                                        <li>
                                                            <small>{!! __('strings.common.upload_limit',['num' => 1]) !!}</small>
                                                        </li>
                                                        <li>
                                                            <small>{!! __('strings.common.upload_size',['num' => imageSize()]) !!}
                                                                MB;
                                                            </small>
                                                        </li>
                                                        <li>
                                                            <small>{!! __('strings.common.upload_ext',['ext' => imageExt()]) !!}</small>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            {!! Form::hidden('secure_id', @$secure_id) !!}
                            {!! Form::hidden('cover', @$item->cover ? $item->cover : old('cover'), ['id' => 'assetId']) !!}
                            <button onclick="clickButton('{{route($route.'.index')}}')" class="ladda-button"
                                    type="button" data-size="s" data-color="red" data-style="slide-right"><i
                                        class="fas fa-times"></i> <span
                                        class="ladda-label">@lang('buttons.general.cancel')</span></button>
                            <button type="submit" class="ladda-button" data-size="s" data-color="green"
                                    data-style="slide-right"><i
                                        class="fas fa-save"></i> <span
                                        class="ladda-label">@lang('buttons.general.save')</span></button>
                        </div>
                        {{ Form::close() }}
                        <div class="hidden">
                            {{ Form::open(['route' => $route.'.remove.asset','id' => 'formDeleteAsset']) }}
                            {{ Form::hidden('itemId', null, ['id' => 'itemId']) }}
                            {{ Form::hidden('dataId', null, ['id' => 'dataId']) }}
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@push('after-styles')
    {{ Html::style(app_vendor('dropzone/dropzone.min.css')) }}
@endpush
@push('after-scripts')
    {{ Html::script(app_vendor('dropzone/dropzone.min.js'), ['type' => 'text/javascript']) }}
    <script type="text/javascript">
        function imageFormatter(value, row) {
            let image = null;
            if (!$.isEmpty(value)) {
                image = '<a data-fancybox href="' + row['url'] + '"><img src="' + value + '" width="50" data-toggle="tooltip" data-title="' + row['title'] + '"></a>';
            }
            return image;
        }

        function commandAction(value, row, index) {
            let button = '';
            @if(access('is_delete', $route.'.*'))
                button += '<button type="button" class="btn btn-danger btn-sm command-delete"><i class="fas fa-trash"></i></button>';
            @endif
                return button;
        }

        document.querySelector("html").classList.add('js');
        Dropzone.autoDiscover = false;
        $('document').ready(function () {
            let $page = $('#title_{{defaultLang()}}');
            let $slug = $('#slug');

            $page.prop('required', true);
            $slug.prop('required', true);

            $page.on('keyup', function () {
                let alias = slugify(this.value, '-');
                $slug.val(alias);
            });

            let csrf = $('input[name="_token"]');
            let $assetId = $('#assetId');
            let optParams = {
                _token: csrf.val(),
                dataId: '{{@$secure_id}}',
                category: 'PHOTOS',
                baseFolder: 'images',
                title: $('input[name="title_{{defaultLang()}}"]').val()
            };

            let $prevUrl = null;
            if (!$.isEmpty($assetId.val())) {
                $prevUrl = '{{url('service/uploads/preview')}}/' + $assetId.val() + '/350/350';
            }
            let uploadOptions = {
                url: '{!! route('service.uploads.single') !!}',
                previewUrl: $prevUrl,
                removeUrl: '{{route('service.uploads.post.remove')}}',
                acceptedFiles: '{!! imageExt() !!}',
                maxFiles: 1,
                maxFilesize: '{!! imageSize() !!}',
                thumbnailWidth: 350,
                thumbnailHeight: 350
            };

            let $upload = $('#uploadImage');
            $upload.singleUpload($.apply({
                params: optParams
            }, uploadOptions));

            let imagesId = $('#imagesId').val();
            let galleryOptions = {
                url: '{!! route('service.uploads.multiple') !!}',
                acceptedFiles: '{!! imageExt() !!}',
                maxFiles: 8,
                uploadMultiple: true,
                autoProcessQueue: false,
                parallelUploads: 8,
                maxFilesize: '{!! imageSize() !!}'
            };

            let $gallery = $('#uploadGallery');
            $gallery.multipleUpload($.apply({
                params: optParams,
                datas: imagesId
            }, galleryOptions));

            window.commandEvents = {
                'click .command-delete': function (e, value, row) {
                    e.preventDefault();
                    $('#itemId').val(row['itemId']);
                    $('#dataId').val(row['dataId']);
                    Swal.fire({
                        title: '<i class="fas fa-exclamation-triangle text-danger"></i>&nbsp;{!! __('strings.common.messages.delete.confirmation') !!}',
                        html: '{!! __('strings.common.messages.delete.confirm_data') !!}' + '<strong>' + row['title'] + '</strong>?',
                        showCancelButton: true,
                        confirmButtonText: '{!! __('buttons.general.yes') !!}',
                        cancelButtonText: '{!! __('buttons.general.no') !!}',
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        allowOutsideClick: false,
                        allowEnterKey: false
                    }).then((result) => {
                        result.value && $('#formDeleteAsset').submit();
                    });
                }
            };
        });
    </script>
@endpush
