@extends('backend._container.layout')
@section('title', $title)
@section('breadcrumbs')
    {{ Breadcrumbs::render('videos') }}
@stop
@section('content')
    <div class="container-fluid">
        <div id="ui-view">
            <div>
                <div class="fade-in">
                    <div class="card card-accent-secondary">
                        <div class="card-header d-flex align-items-center">
                            <h5 class="card-title mb-0"><i class="fas fa-edit"></i> {{$title}}</h5>
                            <button class="btn btn-secondary mfs-auto mfe-1 d-print-none"
                                    onclick="clickButton('{{route($route.'.index')}}')"><span
                                        class="fas fa-arrow-left"></span> @lang('buttons.general.back')</button>
                        </div>
                        {!! Form::open(['route' => $route.'.store','id' => 'appForm', 'novalidate' => 'novalidate', 'files' => true]) !!}
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-8 col-sm-12 col-xs-12">
                                    @if(app_lang())
                                        <div class="form-row">
                                            {!! Form::label('locale', __('strings.backend.dashboard.switch_lang'), ['class' => 'col-md-2 mb-0 pt-1 control-label']); !!}
                                            <div class="col-md-4">
                                                <select name="locale" id="locale" class="select2-nosearch">
                                                    @foreach($localization as $localeCode => $properties)
                                                        <option value="{{ $localeCode }}">{{ $properties['name'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                    @include('backend.errors.list')
                                    <div class="form-group">
                                        {!! Form::label('source', __('forms.gallery.video.source'),['class' => 'required-label']) !!}
                                        <ul class="list-inline">
                                            <li class="list-inline-item">
                                            <li class="list-inline-item">
                                                <label class="check">
                                                    {{ Form::radio('source', 'external', @$item->source == 'external' ? 'checked' : true, ['id' => 'external', 'class' => 'icheckbox', 'required' => true]) }} @lang('forms.gallery.video.external')
                                                </label>
                                            </li>
                                            <label class="check">
                                                {{ Form::radio('source', 'internal', @$item->source == 'internal' ? 'checked' : null, ['id' => 'internal', 'class' => 'icheckbox', 'indeterminate' => true]) }} @lang('forms.gallery.video.internal')
                                            </label>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="form-row external mb-3">
                                        <div class="col-lg-6">
                                            {!! Form::label('code', __('forms.gallery.video.code'), ['class' => 'required-label']) !!}
                                            <div class="input-group">
                                                <span class="input-group-prepend">
                                                    <button class="btn btn-outline-info py-0" id="fetchBtn"
                                                            type="button">
                                                        <svg class="c-icon">
                                                            <use xlink:href="{!! app_font('coreui/sprites/brand.svg#cib-youtube') !!}"></use>
                                                        </svg>
                                                        <span>@lang('buttons.general.find')</span>
                                                    </button>
                                                </span>
                                                {!! Form::text('code', @$item->code ? @$item->code : old('code'),['id' => 'code', 'class' => 'form-control', 'placeholder' => __('forms.gallery.video.ycode')]); !!}
                                                <div class="load-yt ml-2 py-1"></div>
                                            </div>
                                            <small>@lang('forms.gallery.video.youtube')</small>
                                            @if(@$item->source == 'external' && @$item->code)
                                                <a data-fancybox
                                                   href="https://www.youtube.com/watch?v={!! @$item->code !!}"
                                                   id="playYoutube" class="btn btn-sm btn-youtube mt-2">
                                                    <svg class="c-icon">
                                                        <use xlink:href="{!! app_font('coreui/sprites/brand.svg#cib-youtube') !!}"></use>
                                                    </svg>
                                                    <span>Lihat Video</span>
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="internal form-group">
                                        <div class="file-loading">
                                            {!! Form::file('video_file',['id' => 'videoAsset', 'class' => 'file-input','accept' => videoMime()]); !!}
                                        </div>
                                        <div id="errorBlock" class="help-block"></div>
                                        <ul class="list-unstyled text-primary mb-0">
                                            <li>
                                                <small>{!! __('strings.common.upload_size',['num' => videoSize()]) !!}
                                                    MB;
                                                </small>
                                            </li>
                                            <li>
                                                <small>{!! __('strings.common.upload_ext',['ext' => videoExt()]) !!}</small>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        @if(@$item->source == 'internal' && @$item->internal)
                                            <a data-fancybox
                                               href="{!! @$item->internal ? $item->internal->getFullURLAttribute() : '#' !!}"
                                               id="playUpload" class="btn btn-sm btn-youtube mt-2">
                                                <i class="fas fa-video"></i> Lihat Video
                                            </a>
                                        @endif
                                    </div>
                                    @foreach($localization as $localeCode => $properties)
                                        @php
                                            $asterik = $localeCode == defaultLang() ? 'required-label' : '';
                                            $locale[$localeCode] = [
                                                'title' => null,
                                                'description' => null
                                            ];
                                             if($item):
                                                foreach($item->translation as $row):
                                                    if($row->locale == $localeCode):
                                                        $locale[$localeCode] = [
                                                            'title' => $row->title,
                                                            'description' => $row->description
                                                        ];
                                                    endif;
                                                endforeach;
                                            endif;
                                            $flag = '';
                                            if (app_lang()):
                                                $flag = '<img src="' . asset($properties['flag']) . '" width="16">';
                                            endif;
                                        @endphp
                                        <div class="form-group {{$localeCode}} {{ $errors->has('title_'.defaultLang()) ? ' has-error' : '' }}">
                                            {!! Form::label('title_'.$localeCode, __('forms.gallery.video.title'),['class' => $asterik]) !!} {!! $flag !!}
                                            {!! Form::text('title_'.$localeCode, @$locale[$localeCode]['title'] ? $locale[$localeCode]['title'] : old('title_'.$localeCode),['class' => 'form-control']); !!}
                                        </div>
                                        <div class="form-group {{$localeCode}}">
                                            {!! Form::label('description_'.$localeCode, __('strings.common.desc')) !!} {!! $flag !!}
                                            {!! Form::textarea('description_'.$localeCode, @$locale[$localeCode]['description'] ? $locale[$localeCode]['description'] : old('description_'.$localeCode),['class' => 'form-control', 'rows' => 3]); !!}
                                        </div>
                                    @endforeach
                                    <div class="form-group {{ $errors->has('slug') ? ' has-error' : '' }}">
                                        {!! Form::label('slug', __('strings.common.slug'),['class' => $asterik]) !!}
                                        {!! Form::text('slug', @$item->slug ? $item->slug : old('slug'),['class' => 'form-control', 'readonly' => 'readonly']); !!}
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card-header px-0 pt-0">
                                                <strong><i class="fas fa-cogs"></i> @lang('strings.common.option')
                                                </strong>
                                            </div>
                                            <div class="card-body px-0 pb-0">
                                                <div class="form-row">
                                                    {!! Form::label('status', __('strings.common.status.label'), ['class' => 'col-md-4 control-label']); !!}
                                                    <div class="col-md-3">
                                                        <label class="c-switch c-switch-label c-switch-pill c-switch-success">
                                                            <input type="checkbox" name="status" class="c-switch-input"
                                                                   @if(@$item->status == true) checked @endif>
                                                            <span class="c-switch-slider" data-checked="&#x2713"
                                                                  data-unchecked="&#x2715"></span>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <small class="text-muted">@lang('strings.common.notes.publish')</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="card-header px-0">
                                                <strong><span
                                                            class="fa fa-image"></span> @lang('forms.gallery.video.images')
                                                </strong>
                                            </div>
                                            <div class="card-body px-0">
                                                <div class="form-group">
                                                    <div id="uploadImage" class="dropzone dropzone-mini"></div>
                                                    <ul class="list-unstyled text-primary">
                                                        <li>
                                                            <small>{!! __('strings.common.upload_limit',['num' => 1]) !!}</small>
                                                        </li>
                                                        <li>
                                                            <small>{!! __('strings.common.upload_size',['num' => imageSize()]) !!}
                                                                MB;
                                                            </small>
                                                        </li>
                                                        <li>
                                                            <small>{!! __('strings.common.upload_ext',['ext' => imageExt()]) !!}</small>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            {!! Form::hidden('secure_id', @$secure_id) !!}
                            {!! Form::hidden('thumbnail', @$item->thumbnail ? $item->thumbnail : old('thumbnail'), ['id' => 'thumbnail']) !!}
                            {!! Form::hidden('cover', @$item->cover ? $item->cover : old('cover'), ['id' => 'assetId']) !!}
                            <button onclick="clickButton('{{route($route.'.index')}}')" class="ladda-button"
                                    type="button" data-size="s" data-color="red" data-style="slide-right"><i
                                        class="fas fa-times"></i> <span
                                        class="ladda-label">@lang('buttons.general.cancel')</span></button>
                            <button type="submit" class="ladda-button" data-size="s" data-color="green"
                                    data-style="slide-right"><i
                                        class="fas fa-save"></i> <span
                                        class="ladda-label">@lang('buttons.general.save')</span></button>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@push('after-styles')
    {{ Html::style(app_vendor('dropzone/dropzone.min.css')) }}
@endpush
@push('after-scripts')
    {{ Html::script(app_vendor('dropzone/dropzone.min.js'), ['type' => 'text/javascript']) }}
    <script type="text/javascript">
        document.querySelector("html").classList.add('js');
        Dropzone.autoDiscover = false;
        $('document').ready(function () {
            let $page = $('#title_{{defaultLang()}}');
            let $slug = $('#slug');
            let $source = $('input[name="source"]');
            let $internal = $('.internal');
            let $external = $('.external');
            let $code = $('#code');
            let $fetchBtn = $('#fetchBtn');

            $page.prop('required', true);
            $slug.prop('required', true);

            $page.on('keyup', function () {
                let alias = slugify(this.value, '-');
                $slug.val(alias);
            });

            @if(@$item->source == 'external')
            $internal.hide();
            $external.show();
            $code.prop('required', true);
            @elseif (@$item->source == 'internal')
            $internal.show();
            $external.hide();
            $code.prop('required', false);
            @else
            $internal.hide();
            $external.show();
            $code.prop('required', true);
            @endif

            $source.on('ifChecked', function (e) {
                let $value = this.value;
                if ($value === 'external') {
                    $internal.hide();
                    $external.show();
                    $code.prop('required', true);
                } else if ($value === 'internal') {
                    $internal.show();
                    $external.hide();
                    $code.prop('required', false);
                } else {
                    $internal.hide();
                    $external.show();
                    $code.prop('required', true);
                }
            });

            $fetchBtn.on('click', function () {
                $('.load-yt').addClass('file-loading');
                let func = function (row) {
                    console.log(row);
                    if (!$.isEmptyObject(row)) {
                        $('.load-yt').removeClass('file-loading');
                        $page.val(row.title);
                        $('#description_{{defaultLang()}}').val(row.description);
                        $('#thumbnail').val(row.thumbnail);
                        let slug = slugify(row.title, '-');
                        $slug.val(slug);
                    } else {
                        $('.load-yt').removeClass('file-loading');
                        Swal.fire({
                            html: '<i class="fas fa-exclamation-circle text-danger"></i> Video YouTube tidak ditemukan',
                            showConfirmButton: false,
                            timer: 2500
                        })
                    }
                };
                let options = {
                    url: "{{url('admin/gallery/video/fetch')}}/" + $code.val(),
                    type: "GET"
                };
                $(document).requestApi($.apply({
                    func: func
                }, options));
            });

            let $fileInput = $(".file-input");
            $fileInput.uploadInput({
                browseLabel: "Pilih File",
                removeLabel: "Hapus",
                maxFileSize: {!! videoSize() * 1000 !!},
                allowedFileTypes: ['video'],
                allowedFileExtensions: ['mp4', 'avi'],
            });

            let csrf = $('input[name="_token"]');
            let $assetId = $('#assetId');
            let optParams = {
                _token: csrf.val(),
                dataId: '{{@$secure_id}}',
                category: 'VIDEO',
                baseFolder: 'images',
                title: $('input[name="title_{{defaultLang()}}"]').val()
            };
            let uploadOptions = {
                url: '{!! route('service.uploads.single') !!}',
                previewUrl: '{{url('service/uploads/preview')}}/' + $assetId.val(),
                acceptedFiles: '{!! imageExt() !!}',
                maxFiles: 1,
                maxFilesize: '{!! imageSize() !!}',
                thumbnailWidth: 350,
                thumbnailHeight: 350
            };
            let $upload = $('#uploadImage');
            $upload.singleUpload($.apply({
                params: optParams
            }, uploadOptions));
        });
    </script>
@endpush
