@extends('backend._container.layout')
@section('title', $title)
@section('breadcrumbs')
    {{ Breadcrumbs::render('banner') }}
@stop
@section('content')
    <div class="container-fluid">
        <div id="ui-view">
            <div>
                <div class="fade-in">
                    <div class="card card-accent-secondary">
                        <div class="card-header d-flex align-items-center">
                            <h5 class="card-title mb-0"><i class="fas fa-list-alt"></i> {{$title}}</h5>
                            <div class="btn-group mfs-auto" role="group">
                                @if(access('is_create', $route.'.*'))
                                    <button onclick="clickButton('{{ route($route.'.update') }}')" type="button"
                                            class="btn btn-outline-primary">
                                        <i class="fas fa-plus"></i> @lang('buttons.general.create')
                                    </button>
                                @endif
                                @if(access('is_delete', $route.'.*'))
                                    <button id="btnRemoveAll" type="button" class="btn btn-outline-danger">
                                        <i class="fas fa-trash"></i> @lang('buttons.general.delete')
                                    </button>
                                @endif
                            </div>
                        </div>
                        <div class="card-body card-body-table">
                            {{ Form::open(['route' => $route.'.batch','id' => 'batchDelete']) }}
                            <table class="table table-responsive-sm table-striped table-hover table-bordered table-header-center text-nowrap"
                                   data-search="true"
                                   data-show-refresh="true"
                                   data-toggle="table"
                                   data-url="{{route($route.'.grid')}}"
                                   data-sort-name="ids"
                                   data-sort-order="desc"
                                   data-id-field="id"
                                   id="grid-data">
                                <thead>
                                <tr>
                                    <th data-width="25" data-align="center" data-formatter="checkAll">
                                        <input type="checkbox" class="checkall" data-toggle="tooltip"
                                               data-placement="top"
                                               title="@lang('strings.common.select_all')">
                                    </th>
                                    <th data-field="title" data-class="nowrap">@lang('forms.banner.name')</th>
                                    <th data-field="position" data-class="nowrap">@lang('forms.banner.position')</th>
                                    <th data-field="status" data-formatter="statusFormatter"
                                        data-align="center" data-width="100">@lang('strings.common.status.label')</th>
                                    <th data-field="commands" data-class="nowrap" data-width="100"
                                        data-formatter="commandAction"
                                        data-events="commandEvents">@lang('strings.common.status.action')
                                    </th>
                                </tr>
                                </thead>
                            </table>
                            {{ Form::close() }}
                            <div class="hidden">
                                {{ Form::open(['route' => $route.'.destroy','id' => 'formDelete']) }}
                                {{ Form::hidden('secure_id', null, ['id' => 'row-id']) }}
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@push('after-scripts')
    <script type="text/javascript">
        function commandAction(value, row, index) {
            let button = '<div class="btn-group btn-group-sm" role="group">';
            @if(access('is_update', $route.'.*'))
                button += '<button type="button" class="btn btn-info btn-sm command-edit"><i class="fas fa-edit"></i></button>';
            @endif
                    @if(access('is_delete', $route.'.*'))
                button += '<button type="button" class="btn btn-danger btn-sm command-delete"><i class="fas fa-trash"></i></button>';
            @endif
                button += '</div>';
            return button;
        }

        function statusFormatter(value, row) {
            let icon = '<i class="fas fa-check text-success" data-toggle="tooltip" data-placement="top" data-original-title="@lang('strings.common.status.active')"></i>';
            if (row['status'] === false) {
                icon = '<i class="fas fa-times text-danger" data-toggle="tooltip" data-placement="top" data-original-title="@lang('strings.common.status.inactive')"></i>';
            }
            return icon;
        }

        $(document).ready(function () {
            window.commandEvents = {
                'click .command-edit': function (e, value, row) {
                    e.preventDefault();
                    $(location).attr('href', '{{url('admin/extension/banner/update')}}/' + row['secureId']);
                },
                'click .command-delete': function (e, value, row) {
                    e.preventDefault();
                    $('#row-id').val(row['secureId']);
                    Swal.fire({
                        title: '<i class="fas fa-exclamation-triangle text-danger"></i>&nbsp;{!! __('strings.common.messages.delete.confirmation') !!}',
                        html: '{!! __('strings.common.messages.delete.confirm_data') !!}' + '<strong>' + row['title'] + '</strong>?',
                        showCancelButton: true,
                        confirmButtonText: '{!! __('buttons.general.yes') !!}',
                        cancelButtonText: '{!! __('buttons.general.no') !!}',
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        allowOutsideClick: false,
                        allowEnterKey: false
                    }).then((result) => {
                        result.value && $('#formDelete').submit();
                    });
                }
            };
        });
    </script>
@endpush
