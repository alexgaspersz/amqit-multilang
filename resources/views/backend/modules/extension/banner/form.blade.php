@extends('backend._container.layout')
@section('title', $title)
@section('breadcrumbs')
    {{ Breadcrumbs::render('banner') }}
@stop
@section('content')
    <div class="container-fluid">
        <div id="ui-view">
            <div class="fade-in">
                <div class="card card-accent-secondary">
                    <div class="card-header d-flex align-items-center">
                        <h5 class="card-title mb-0"><i class="fas fa-edit"></i> {{$title}}</h5>
                        <button class="btn btn-secondary mfs-auto mfe-1 d-print-none"
                                onclick="clickButton('{{route($route.'.index')}}')"><span
                                    class="fas fa-arrow-left"></span> @lang('buttons.general.back')</button>
                    </div>
                    {!! Form::open(['route' => $route.'.store','id' => 'appForm', 'novalidate' => 'novalidate']) !!}
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-8 col-sm-12 col-xs-12">
                                @include('backend.errors.list')
                                <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                                    {!! Form::label('title', __('forms.banner.name'),['class' => 'required-label']) !!}
                                    {!! Form::text('title', @$item->title ? $item->title : old('title'),['class' => 'form-control', 'required' => true]); !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('link', __('forms.banner.link')) !!}
                                    {!! Form::text('link', @$item->link ? $item->link : old('link'),['class' => 'form-control']); !!}
                                </div>
                                <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
                                    {!! Form::label('assetId', __('forms.banner.image'),['class' => 'required-label']) !!}
                                    <div id="uploadBanner" class="dropzone"></div>
                                    {!! Form::hidden('image', @$item->image ? $item->image : old('image'), ['id' => 'assetId', 'required' => true]) !!}
                                    <ul class="list-unstyled text-primary mb-0">
                                        <li>
                                            <small>{!! __('strings.common.upload_limit',['num' => 1]) !!}</small>
                                        </li>
                                        <li>
                                            <small>{!! __('strings.common.upload_size',['num' => imageSize()]) !!}MB;
                                            </small>
                                        </li>
                                        <li>
                                            <small>{!! __('strings.common.upload_ext',['ext' => imageExt()]) !!}</small>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card-header px-0 pt-0">
                                            <strong><i class="fas fa-cogs"></i> @lang('strings.common.option')</strong>
                                        </div>
                                        <div class="card-body px-0 pb-0">
                                            <div class="form-row mb-4">
                                                {!! Form::label('position', __('forms.banner.position'), ['class' => 'col-md-4 control-label']); !!}
                                                <div class="col-md-8">
                                                    {!! Form::select('position', ['home' => 'Halaman Depan','sidebar' => 'Sidebar'], @$item->position, ['class' => 'form-control select2-nosearch']); !!}
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                {!! Form::label('status', __('strings.common.status.label'), ['class' => 'col-md-4 control-label']); !!}
                                                <div class="col-md-3">
                                                    <label class="c-switch c-switch-label c-switch-pill c-switch-success">
                                                        <input type="checkbox" name="status" class="c-switch-input"
                                                               @if(@$item->status == true) checked @endif>
                                                        <span class="c-switch-slider" data-checked="&#x2713"
                                                              data-unchecked="&#x2715"></span>
                                                    </label>
                                                </div>
                                                <div class="col-md-5">
                                                    <small class="text-muted">@lang('strings.common.notes.publish')</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        {!! Form::hidden('secure_id', @$secure_id) !!}
                        <button onclick="clickButton('{{route($route.'.index')}}')" class="ladda-button" type="button"
                                data-size="s" data-color="red" data-style="slide-right"><i class="fas fa-times"></i>
                            <span class="ladda-label">@lang('buttons.general.cancel')</span></button>
                        <button type="submit" class="ladda-button" data-size="s" data-color="green"
                                data-style="slide-right"><i
                                    class="fas fa-save"></i> <span
                                    class="ladda-label">@lang('buttons.general.save')</span></button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop
@push('after-styles')
    {{ Html::style(app_vendor('dropzone/dropzone.min.css')) }}
@endpush
@push('after-scripts')
    {{ Html::script(app_vendor('dropzone/dropzone.min.js'), ['type' => 'text/javascript']) }}
    <script type="text/javascript">
        document.querySelector("html").classList.add('js');
        Dropzone.autoDiscover = false;
        $('document').ready(function () {
            let $upload = $('#uploadBanner');
            let csrf = $('input[name="_token"]');
            let $assetId = $('#assetId');
            let optParams = {
                _token: csrf.val(),
                dataId: '{{@$secure_id}}',
                category: 'BANNER',
                baseFolder: 'images',
                title: $('input[name="title"]').val()
            };

            let $prevUrl = null;
            if (!$.isEmpty($assetId.val())) {
                $prevUrl = '{{url('service/uploads/preview')}}/' + $assetId.val() + '/750/500';
            }
            let uploadOptions = {
                url: '{{route('service.uploads.single')}}',
                previewUrl: $prevUrl,
                removeUrl: '{{route('service.uploads.post.remove')}}',
                acceptedFiles: '{!! imageExt() !!}',
                maxFiles: 1,
                maxFilesize: '{!! imageSize() !!}',
                thumbnailWidth: 750,
                thumbnailHeight: 500
            };

            $upload.singleUpload($.apply({
                params: optParams
            }, uploadOptions));

        });
    </script>
@endpush
