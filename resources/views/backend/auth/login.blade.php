<!DOCTYPE html>
<html lang="{{config('app.locale')}}" class="body-full-height">
<head>
    <title>{{$title}} | {{app_title()}}</title>
    @include('backend._partials.metadata')
    {{ Html::style(app_mix('css/login.css')) }}
    <link type="text/css" media="screen" rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
</head>
<body class="c-app flex-row align-items-center login-container"
      style="background: url('https://placeimg.com/1280/800/tech') no-repeat 50% fixed;">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-group login-box">
                <div class="card border-0 text-white login-body py-4 d-md-down-none">
                    <div class="card-body px-0 text-center">
                        <div class="img-logo">
                            {{ HTML::image(app_images('logo.svg'), app_title()) }}
                        </div>
                        <div class="login-title text-white mt-2 mb-2">
                            {!! app_version() !!}
                        </div>
                        <div class="login-title-desc">{!! app_desc() !!}</div>
                    </div>
                </div>
                <div class="card border-0 p-4 login-form">
                    <div class="card-body py-0">
                        <div class="login-heading"><i
                                    class="fa fa-info-circle"></i> {!! __('strings.common.notes.login') !!}</div>
                        {!! Form::open(['route' => 'admin.auth.login','id' => 'appForm']) !!}
                        <div class="form-group">
                            {!! Form::label('key',null, ['style' => 'display:none']); !!}
                            <div class="input-group mb-2">
                                {!! Form::text('key', old('key'),['class' => 'form-control', 'autocomplete' => 'off', 'required' => true, 'placeholder' => login_key() == 'username' ? __('placeholder.login.login_username') : __('placeholder.login.login_email'), 'autofocus' => true, 'data-msg' => login_key() == 'username' ? __('strings.common.user.empty_username') : __('strings.common.user.empty_email')]); !!}
                                <div class="custom-append key">
                                    <i class="fa fa-user"></i>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group mb-2">
                                {!! Form::label('inputPassword',null, ['style' => 'display:none']); !!}
                                {!! Form::password('password', ['id' => 'inputPassword', 'autocomplete' => 'new-password', 'required' => true, 'class' => 'form-control', 'maxlength' => 15, 'placeholder' => __('placeholder.login.login_password'), 'data-msg' => __('strings.common.user.empty_password')]); !!}
                                <div class="custom-append" onclick="togglePassword()">
                                    <i id="icon-append" class="fa fa-eye"></i>
                                </div>
                            </div>
                        </div>
                        @if(config('general.login.recaptcha'))
                            <div class="form-row mb-3">
                                <div class="col-12">
                                    {!! NoCaptcha::display(['id' => 'secureCode']) !!}
                                    {!! Form::hidden('recaptcha_status', 'true',['id' => 'recaptcha']); !!}
                                    @if ($errors->has('g-recaptcha-response'))
                                        <span class="help-block">
                                            {{ $errors->first('g-recaptcha-response') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                        @endif
                        <div class="form-row mb-0">
                            <div class="col-md-6 pt-0 pb-0 pr-0">
                                <button type="submit"
                                        id="loginButton"
                                        class="ladda-button"
                                        data-color="green"
                                        data-style="slide-right"
                                        data-size="s">
                                <span class="ladda-label">
                                    {{__('buttons.auth.login')}} <i class="fa fa-sign-in"></i>
                                </span>
                                </button>
                            </div>
                            <div class="col-md-6 pt-0 pb-0 pr-0 text-right">
                                <label class="check" style="padding: .5rem 0;margin-bottom:0;color: #323232"><input
                                            type="checkbox" name="remember"
                                            class="icheckbox"/> {{__('strings.common.user.remember')}}</label>
                                @if(config('general.login.reset_password') == true)
                                    <a href="{{route('admin.auth.password.reset')}}"
                                       class="btn btn-link px-0">{{__('strings.common.user.forget_pass')}}</a>
                                @endif
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8 text-center text-white font-weight-bold mt-2">
            <p>
                {!! __('strings.copyright',['year' => date('Y'), 'app' => config('general.app.company'), 'reserved' => __('strings.reserved')]) !!}
            </p>
        </div>
    </div>
</div>
{{ Html::script(app_mix('js/manifest.js'), ['type' => 'text/javascript']) }}
{{ Html::script(app_mix('js/vendor.js'), ['type' => 'text/javascript']) }}
{{ Html::script(app_mix('js/login.js'), ['type' => 'text/javascript']) }}
{{ Html::script(app_mix('vendors/pace-progress/pace.min.js'), ['type' => 'text/javascript']) }}

@if(config('general.login.recaptcha'))
    {!! NoCaptcha::renderJs(config('app.locale'), true, 'onloadCallback') !!}
@endif
<script type="text/javascript">
    function togglePassword() {
        let input = document.getElementById("inputPassword");
        let icon = document.getElementById("icon-append");

        if (input.type === "password") {
            input.type = "text";
            icon.className = "fa fa-eye-slash";
        } else {
            input.type = "password";
            icon.className = "fa fa-eye";
        }
    }

    function onloadCallback() {
        grecaptcha.render('secureCode', {
            'sitekey': '{!! webConfig('recaptcha') !!}'
        });
    }

    $('document').ready(function () {
        @if(session()->has('message'))
            @if(session('type') == 'error')
        toastr.error("{{session('message')}}");
            @endif
            @if(session('type') == 'warning')
            toastr.warning("{{session('message')}}");
            @endif
        @endif
    });
</script>
</body>
</html>






