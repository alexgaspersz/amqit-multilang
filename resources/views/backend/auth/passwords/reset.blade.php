<!DOCTYPE html>
<html lang="{{config('app.locale')}}" class="body-full-height">
<head>
    <title>{{$title}} | {{app_title()}}</title>
    @include('backend._partials.metadata')
    {{ Html::style(app_mix('css/login.css')) }}
    <link type="text/css" media="screen" rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
</head>
<body class="c-app flex-row align-items-center login-container">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card-group login-box">
                <div class="card border-0 p-2 login-form">
                    <div class="card-body py-0">
                        <div class="login-heading"><i class="fa fa-lock"></i> Setel Ulang <strong>Kata Sandi</strong>
                        </div>
                        @include('backend.errors.list')
                        {!! Form::open(['route' => 'admin.auth.password.reset','id' => 'appForm', 'class' => 'form-horizontal']) !!}
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            {!! Form::label('email',null, ['style' => 'display:none']); !!}
                            <div class="input-group">
                                {!! Form::text('email', @$email ? $email : old('email'),['class' => 'form-control', 'required' => true, 'placeholder' => __('placeholder.login.login_email_reset'), 'autofocus' => true]); !!}
                                <div class="custom-append key">
                                    <i class="fa fa-envelope"></i>
                                </div>
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                            {!! Form::label('password',null, ['style' => 'display:none']); !!}
                            <div class="input-group">
                                {!! Form::password('password', ['class' => 'form-control', 'required' => true, 'placeholder' => __('placeholder.passwords.newpass')]); !!}
                                <div class="custom-append" onclick="togglePassword('password')">
                                    <i id="icon-append_password" class="fa fa-eye"></i>
                                </div>
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('upassword_confirmation') ? ' has-error' : '' }}">
                            {!! Form::label('password_confirmation',null, ['style' => 'display:none']); !!}
                            <div class="input-group">
                                {!! Form::password('password_confirmation', ['class' => 'form-control', 'required' => true, 'placeholder' => __('placeholder.passwords.confirm_newpass')]); !!}
                                <div class="custom-append" onclick="togglePassword('password_confirmation')">
                                    <i id="icon-append_password_confirmation" class="fa fa-eye"></i>
                                </div>
                            </div>
                        </div>
                        @if(config('general.login.recaptcha') == true)
                            <div class="form-group">
                                {!! NoCaptcha::display(['id' => 'secureCode']) !!}
                                {!! Form::hidden('recaptcha_status', 'true',['id' => 'recaptcha']); !!}
                                @if ($errors->has('g-recaptcha-response'))
                                    <span class="help-block">
                                        {{ $errors->first('g-recaptcha-response') }}
                                    </span>
                                @endif
                            </div>
                        @endif
                        <div class="form-group mt-4">
                            <button class="btn btn-info btn-block">{{__('buttons.emails.auth.reset_password')}}</button>
                        </div>
                        <div class="form-group text-right nopadding">
                            <a href="{{route('frontend.index')}}" class="btn btn-link"><i
                                        class="glyphicon glyphicon-lock"></i> Kembali ke Halaman Web</a>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
{{ Html::script(app_mix('js/manifest.js'), ['type' => 'text/javascript']) }}
{{ Html::script(app_mix('js/vendor.js'), ['type' => 'text/javascript']) }}
{{ Html::script(app_mix('js/login.js'), ['type' => 'text/javascript']) }}
{{ Html::script(app_mix('vendors/pace-progress/pace.min.js'), ['type' => 'text/javascript']) }}

@if(config('general.login.recaptcha'))
    {!! NoCaptcha::renderJs(config('app.locale'), true, 'onloadCallback') !!}
@endif

<style>
    .g-recaptcha {
        transform: scale(0.97);
        transform-origin: 0 0;
    }
</style>
<script type="text/javascript">
    function togglePassword(el) {
        let input = document.getElementById(el);
        let icon = document.getElementById("icon-append_" + el);

        if (input.type === "password") {
            input.type = "text";
            icon.className = "fa fa-eye-slash";
        } else {
            input.type = "password";
            icon.className = "fa fa-eye";
        }
    }

    function onloadCallback() {
        grecaptcha.render('secureCode', {
            'sitekey': '{!! webConfig('recaptcha') !!}'
        });
    }

    $('document').ready(function () {
        @if(session()->has('message'))
        @if(session('type') == 'error')
        toastr.error("{{session('message')}}");
        @endif
        @if(session('type') == 'warning')
        toastr.warning("{{session('message')}}");
        @endif
        @endif
    });
</script>
</body>
</html>
