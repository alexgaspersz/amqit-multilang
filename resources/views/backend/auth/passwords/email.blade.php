<!DOCTYPE html>
<html lang="{{config('app.locale')}}" class="body-full-height">
<head>
    <title>{{$title}} | {{app_title()}}</title>
    @include('backend._partials.metadata')
    {{ Html::style(app_mix('css/login.css')) }}
    <link type="text/css" media="screen" rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
</head>
<body class="c-app flex-row align-items-center login-container">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card-group login-box">
                <div class="card border-0 p-2 login-form">
                    <div class="card-body py-0">
                        <div class="login-heading"><i class="fa fa-lock"></i> Lupa <strong>Kata Sandi</strong></div>
                        @include('backend.errors.list')
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                <button type="button" class="close" data-dismiss="alert"><span
                                            aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                <i class="fa fa-info-circle"></i> {{ session('status') }}
                            </div>
                        @endif
                        {!! Form::open(['route' => 'admin.auth.password.email.post','id' => 'appForm', 'class' => 'form-horizontal']) !!}
                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            {!! Form::label('email',null, ['style' => 'display:none']); !!}
                            {!! Form::text('email', old('email'),['class' => 'form-control', 'required' => 'required', 'placeholder' => __('placeholder.login.login_email_reset'), 'autofocus' => 'autofocus']); !!}
                        </div>
                        @if(config('general.login.recaptcha'))
                            <div class="form-group">
                                {!! NoCaptcha::display(['id' => 'secureCode']) !!}
                                {!! Form::hidden('recaptcha_status', 'true',['id' => 'recaptcha']); !!}
                                @if ($errors->has('g-recaptcha-response'))
                                    <span class="help-block">
                                        {{ $errors->first('g-recaptcha-response') }}
                                    </span>
                                @endif
                            </div>
                        @endif
                        <div class="form-group mt-4">
                            <button class="btn btn-info btn-block">{{__('buttons.auth.reset')}}</button>
                        </div>
                        <div class="form-group text-right nopadding">
                            <a href="{{route('frontend.index')}}" class="btn btn-link"><i
                                        class="glyphicon glyphicon-lock"></i> Kembali ke Halaman Web</a>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
{{ Html::script(app_mix('js/manifest.js'), ['type' => 'text/javascript']) }}
{{ Html::script(app_mix('js/vendor.js'), ['type' => 'text/javascript']) }}
{{ Html::script(app_mix('js/login.js'), ['type' => 'text/javascript']) }}
{{ Html::script(app_mix('vendors/pace-progress/pace.min.js'), ['type' => 'text/javascript']) }}

@if(config('general.login.recaptcha'))
    {!! NoCaptcha::renderJs(config('app.locale'), true, 'onloadCallback') !!}
@endif

<style>
    .g-recaptcha {
        transform: scale(0.97);
        transform-origin: 0 0;
    }
</style>
<script type="text/javascript">
    function onloadCallback() {
        grecaptcha.render('secureCode', {
            'sitekey': '{!! webConfig('recaptcha') !!}'
        });
    }

    $('document').ready(function () {
        @if(session()->has('message'))
            @if(session('type') == 'error')
        toastr.error("{{session('message')}}");
            @endif
            @if(session('type') == 'warning')
            toastr.warning("{{session('message')}}");
            @endif
        @endif
    });
</script>
</body>
</html>






