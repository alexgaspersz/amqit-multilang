<div class="modal fade" id="app-modal" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-primary" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
            </div>
            <div class="modal-body" id="app-modal-content">
            </div>
        </div>
    </div>
</div>
{!! Form::open(['route' => 'admin.auth.logout','id' => 'formLogout', 'class' => 'hidden','style'=>'display: none']) !!}
{!! Form::close() !!}
