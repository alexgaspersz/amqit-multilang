<footer class="c-footer">
    <div class="ml-auto">
        {!! app_copyright() !!}
    </div>
</footer>
<div id="stop" class="scrollTop">
    <i class="fa fa-angle-double-up"></i>
</div>
