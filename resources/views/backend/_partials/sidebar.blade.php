<div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
    <div class="c-sidebar-brand logo-full">
        {{ HTML::image(app_images('logo-title.svg'), app_title(),['class' => 'img-fluid']) }}
    </div>
    <div class="c-sidebar-brand logo-mini">
        {{ HTML::image(app_images('logo-mini.svg'), app_title(),['class' => 'py-2 px-1 mt-1 img-fluid']) }}
    </div>
    <ul class="c-sidebar-nav">
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="{{route('admin.dashboard.index')}}">
                <i class="cil-speedometer c-sidebar-nav-icon"></i> {!! __('module.dashboard.title') !!}
            </a>
        </li>
        <li class="divider"></li>
        {!! renderMenu()->generateAdminSideMenu() !!}
    </ul>
    <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent"
            data-class="c-sidebar-minimized"></button>
</div>
