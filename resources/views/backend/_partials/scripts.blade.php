@stack('before-scripts')
{{ Html::script(app_mix('js/manifest.js')) }}
{{ Html::script(app_mix('js/vendor.js')) }}
{{ Html::script(app_mix('js/backend.js')) }}
{{ Html::script(app_mix('js/options.js')) }}
{{ Html::script(app_mix('vendors/pace-progress/pace.min.js'), ['type' => 'text/javascript']) }}
@stack('after-scripts')
<script type="text/javascript">
    function appRemove(id, title) {
        $('#row-id').val(id);
        Swal.fire({
            title: '<i class="fas fa-exclamation-triangle text-danger"></i>&nbsp;{!! __('strings.common.messages.delete.confirmation') !!}',
            html: '{!! __('strings.common.messages.delete.confirm_data') !!}' + '<strong>' + title + '</strong>?',
            showCancelButton: true,
            confirmButtonText: '{!! __('buttons.general.yes') !!}',
            cancelButtonText: '{!! __('buttons.general.no') !!}',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            allowOutsideClick: false,
            allowEnterKey: false
        }).then((result) => {
            result.value && $('#formDelete').submit();
        });
    }

    $('document').ready(function () {
        let _token = "{{csrf_token()}}";
        let _base = "{{ url('/') }}";
        let _public = "{{ asset('/') }}";

        $('#logoutApp').on('click', function (e) {
            Swal.fire({
                title: '{!! __('strings.common.messages.logout') !!}',
                html: '{!! __('strings.backend.dashboard.logout_message') !!}',
                showCancelButton: true,
                confirmButtonText: '{!! __('buttons.general.yes') !!}',
                cancelButtonText: '{!! __('buttons.general.no') !!}',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                allowOutsideClick: false,
                allowEnterKey: false
            }).then((result) => {
                result.value && $('#formLogout').submit();
            });
        });

        $('#btnRemoveAll').on('click', function (e) {
            let ids = $('[name="ids[]"]:checked').length;
            if (ids > 0) {
                $('#data-batch').html(ids);
                Swal.fire({
                    title: '<i class="fas fa-exclamation-triangle text-danger"></i>&nbsp;{!! __('strings.common.messages.delete.confirmation') !!}',
                    html: '{!! __('strings.common.messages.delete.confirm_all') !!}',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('buttons.general.yes') !!}',
                    cancelButtonText: '{!! __('buttons.general.no') !!}',
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    allowOutsideClick: false,
                    allowEnterKey: false
                }).then((result) => {
                    result.value && $('#batchDelete').submit();
                });
            } else {
                toastr.error("{!! __('strings.common.messages.delete.empty') !!}");
            }
        });

        @if(session()->has('message'))
        @if(session('type') == 'success')
        toastr.success("{{session('message')}}");
        @endif
        @if(session('type') == 'error')
        toastr.error("{{session('message')}}");
        @endif
        @if(session('type') == 'info')
        toastr.info("{{session('message')}}");
        @endif
        @if(session('type') == 'warning')
        toastr.warning("{{session('message')}}");
        @endif
        @endif

        @if(session()->has('alert'))
        $(document).alerts({
            title: '{!! app_version() !!}',
            width: '45%',
            timeout: '20000',
            content: "{!! session('alert') !!}",
            color: 'red',
            icon: 'fas fa-exclamation-triangle',
        });
        @endif
    });
</script>
