<header class="c-header c-header-dark">
    <button class="c-header-toggler c-class-toggler d-lg-none mr-auto" type="button" data-target="#sidebar"
            data-class="c-sidebar-show">
        <span class="c-header-toggler-icon"></span>
    </button>
    <button class="c-header-toggler c-class-toggler ml-3 d-md-down-none" type="button" data-target="#sidebar"
            data-class="c-sidebar-lg-show" responsive="true">
        <span class="c-header-toggler-icon"></span>
    </button>
    <ul class="c-header-nav d-md-down-none">
        <li class="c-header-nav-item dropdown d-md-down-none mx-2">
            <a class="c-header-nav-link" href="{{ route('frontend.index') }}" target="_blank">
                <i class="fas fa-home"></i> <span class="xn-text" data-toggle="tooltip"
                                                  data-title="@lang('strings.backend.dashboard.visit')">@lang('strings.backend.dashboard.visit')</span>
            </a>
        </li>
        @if(app_lang())
            <li class="c-header-nav-item dropdown d-md-down-none mx-2">
                <a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
                   aria-expanded="true">
                    <i class="fas fa-flag"></i> <span class="xn-text" data-toggle="tooltip"
                                                      data-title="@lang('strings.backend.dashboard.switch_lang')">@lang('strings.backend.dashboard.language')</span>
                </a>
                <div class="dropdown-menu dropdown-menu-left py-0 mt-4">
                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                        <a class="dropdown-item" rel="alternate" hreflang="{{ $localeCode }}"
                           href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                            <img src="{{asset($properties['flag'])}}" alt="{{$properties['native']}}" class="mr-2"
                                 height="13" width="20"> {{ $properties['name'] }}
                        </a>
                    @endforeach
                </div>
            </li>
        @endif
    </ul>
    <ul class="c-header-nav ml-auto mr-2">
        <li class="c-header-nav-item dropdown">
            <a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
               aria-expanded="false">
                <div class="c-avatar mr-2">
                    @if(!session('avatar') and session('email') and Gravatar::exists(session('email')))
                        <img src="{!! Gravatar::get(session('email')); !!}" alt="{{session('fullname')}}"
                             class="c-avatar-img"/>
                    @elseif(session('thumbnail'))
                        <img src="{{asset(session('thumbnail'))}}" alt="{{session('fullname')}}" class="c-avatar-img"/>
                    @else
                        <img src="{{app_images('no-image.jpg')}}" alt="{{session('fullname')}}" class="c-avatar-img"/>
                    @endif
                </div>
                <span class="d-md-down-none">{!! __('strings.common.user.hi',['name' => session('display')]) !!}</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right py-0 mt-3">
                <a class="dropdown-item" href="{{route('admin.account.index')}}"><i
                            class="fas fa-user-edit"></i> {!! __('forms.user.account') !!}</a>
                <a class="dropdown-item" href="{{route('admin.account.password')}}"><i
                            class="fas fa-key"></i> {!! __('strings.common.user.change_password') !!}</a>
                <a href="#" id="logoutApp" class="dropdown-item"><i
                            class="fas fa-sign-out-alt"></i> @lang('buttons.auth.logout')</a>
            </div>
        </li>
    </ul>
    <div class="c-subheader px-3">
        @yield('breadcrumbs')
    </div>
</header>
