<!DOCTYPE html>
<html lang="{{config('app.locale')}}" class="body-full-height">
<head>
    <title>@yield('title') | {{app_title()}}</title>
    @include('backend._partials.metadata')
    @stack('before-styles')
    {{ Html::style(app_mix('css/backend.css')) }}
    @stack('after-styles')
</head>
<body class="c-app c-dark-theme">
@include('backend._partials.sidebar')
<div class="c-wrapper">
    @include('backend._partials.header')
    <div class="c-body">
        <main class="c-main">
            @yield('content')
        </main>
    </div>
    @include('backend._partials.footer')
</div>
@include('backend._partials.parts')
</body>
@include('backend._partials.scripts')
</html>
