@extends('frontend.errors.layout')
@section('title', __('errors.413.message'))
@section('code', __('errors.413.code'))
@section('message', __('errors.413.desc'))
