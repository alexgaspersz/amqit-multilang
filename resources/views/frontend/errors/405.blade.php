@extends('frontend.errors.layout')
@section('title', __('errors.405.message'))
@section('code', __('errors.405.code'))
@section('message', __('errors.405.desc'))
