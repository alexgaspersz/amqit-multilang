@extends('frontend.errors.layout')
@section('title', __('errors.419.message'))
@section('code', __('errors.419.code'))
@section('message', __('errors.419.desc'))
