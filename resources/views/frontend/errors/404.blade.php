@extends('frontend.errors.layout')
@section('title', __('errors.404.message'))
@section('code', __('errors.404.code'))
@section('message', __('errors.404.desc'))
