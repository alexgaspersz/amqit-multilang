@extends('frontend.errors.layout')
@section('title', __('errors.401.message'))
@section('code', __('errors.401.code'))
@section('message', __('errors.401.desc'))
