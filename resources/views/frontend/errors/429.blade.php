@extends('frontend.errors.layout')
@section('title', __('errors.429.message'))
@section('code', __('errors.429.code'))
@section('message', __('errors.429.desc'))
