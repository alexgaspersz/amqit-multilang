@extends('frontend.errors.layout')
@section('title', __('errors.503.message'))
@section('code', __('errors.503.code'))
@section('message', __($exception->getMessage()) ?: __('errors.500.desc'))
