@extends('frontend.errors.layout')
@section('title', __('errors.403.message'))
@section('code', __('errors.403.code'))
@section('message', __($exception->getMessage()) ?: __('errors.403.desc'))
