<!DOCTYPE html>
<html lang="{{config('app.locale')}}">
<head>
    <title>@yield('title') | {{app_title()}}</title>
    @include('backend._partials.metadata')
    {{ Html::style(app_mix('css/error.css')) }}
</head>
<body style="background: url({!! app_images('error.png') !!}) no-repeat;background-size: cover;background-position: center center;">
<section id="error">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="error-area mt-5">
                    <p>@yield('title')</p>
                    <span>@yield('message')</span>
                    <h2><i class="fas fa-exclamation"></i> @yield('code')</h2>
                    <a class="btn btn-sm btn-danger" href="{{URL::previous()}}"><i
                                class="fas fa-arrow-left"></i> {{__('strings.common.back')}}</a>
                </div>
            </div>
        </div>
    </div>
</section>
</body>
</html>
