@extends('frontend.errors.layout')
@section('title', __('errors.500.message'))
@section('code', __('errors.500.code'))
@section('message', __('errors.500.desc'))
