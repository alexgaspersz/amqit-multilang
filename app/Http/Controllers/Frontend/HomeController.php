<?php
/**
 * Description: HomeController.php generated by PhpStorm.
 * @project     web-sekolah
 * @package     App\Http\Controllers\Frontend
 * @author      alex
 * @created     2020-05-03, modified: 2020-05-03 21:43
 * @copyright   Copyright (c) 2020
 */

namespace App\Http\Controllers\Frontend;


use App\Http\Controllers\Controller;
use App\Repositories\Modules\Content\PostRepository;
use App\Repositories\Modules\Extension\SliderRepository;
use App\Repositories\Modules\Gallery\PhotoRepository;
use App\Repositories\Modules\Gallery\VideoRepository;
use Artesaos\SEOTools\Facades\SEOTools;

/**
 * Class HomeController
 * @package App\Http\Controllers\Frontend
 */
class HomeController extends Controller
{
    private $route = "frontend.index";
    /**
     * @var SliderRepository
     */
    private $sliderRepository;
    /**
     * @var PostRepository
     */
    private $postRepository;
    /**
     * @var PhotoRepository
     */
    private $photoRepository;
    /**
     * @var VideoRepository
     */
    private $videoRepository;

    /**
     * HomeController constructor.
     *
     * @param SliderRepository $sliderRepository
     * @param PostRepository   $postRepository
     * @param PhotoRepository  $photoRepository
     * @param VideoRepository  $videoRepository
     */
    public function __construct(SliderRepository $sliderRepository,
                                PostRepository $postRepository,
                                PhotoRepository $photoRepository,
                                VideoRepository $videoRepository)
    {
        $this->sliderRepository = $sliderRepository;
        $this->postRepository = $postRepository;
        $this->photoRepository = $photoRepository;
        $this->videoRepository = $videoRepository;
        visitorLog();
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $slider = $this->sliderRepository->limit(5)->getRowActive();
        $images = [];
        foreach ($slider as $row) {
            $images[] = $row->asset->getFullURLAttribute();
        }
        SEOTools::setTitle(trans('strings.backend.dashboard.home'));
        SEOTools::setDescription(app_desc());
        SEOTools::setCanonical(route('frontend.index'));
        SEOTools::metatags()->setKeywords(config('general.app.keywords'));
        SEOTools::opengraph()->setSiteName(app_title());
        SEOTools::opengraph()->setTitle(trans('strings.backend.dashboard.home'));
        SEOTools::opengraph()->setDescription(app_desc());
        SEOTools::opengraph()->setUrl(route('frontend.index'));
        SEOTools::opengraph()->addImages($images);
        SEOTools::opengraph()->addProperty('type', 'website');
        SEOTools::opengraph()->addProperty('locale', config('app.locale_system'));
        SEOTools::twitter()->setSite('@' . webConfig('twitter'));
        SEOTools::jsonLd()->setType('website');
        SEOTools::jsonLd()->addImage($images);

        $data['slideshow'] = $slider;
        $data['news'] = $this->postRepository->getRecentPost(8);
        $data['route'] = $this->route;
        return view('frontend.home', $data);
    }
}
