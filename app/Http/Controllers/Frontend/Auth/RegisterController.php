<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use App\Mail\Frontend\SendRegistration;
use App\Models\Core\UserProfileModel;
use App\Models\Core\UsersModel;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

/**
 * @property UsersModel model
 * @property UserProfileModel profile
 */
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->model = new UsersModel();
        $this->profile = new UserProfileModel();
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'fullname' => 'bail|required|string|max:150',
            'email' => 'required|string|email|max:100|unique:tb_users,email',
            'handphone' => 'required|string|max:100',
            'birthdate' => 'required|date',
            'gender' => 'required|string',
        ], [
            'fullname.required' => sprintf(__('validation.required'), __('forms.user_fullname')),
            'handphone.required' => sprintf(__('validation.required'), __('forms.user_handphone')),
            'birthdate.required' => sprintf(__('validation.required'), __('forms.user_birthdate')),
            'birthdate.date' => sprintf(__('validation.date'), __('forms.user_birthdate')),
            'gender.required' => sprintf(__('validation.required'), __('forms.user_sex')),
            'email.required' => sprintf(__('validation.required'), __('text.email')),
            'email.unique' => sprintf(__('validation.unique'), __('text.email')),
        ])->validate();
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function register(Request $request)
    {
        $data = $request->all();
        if ($data) {
            $this->validator($data);
            $firstname = explode(' ', $request->input('fullname'));
            $password = $this->quickRandom();
            $user = $this->model;
            $user->fullname = $request->input('fullname');
            $user->username = strtolower($firstname[0] . $this->quickRandom(3));
            $user->password = Hash::make($password);
            $user->email = $request->input('email');
            $user->group_id = 3;
            $user->active = 0;
            $user->created_at = Carbon::now();
            if ($user->save()) {
                $profile = $this->profile;
                $profile->user_id = $user->user_id;
                $profile->display_name = $firstname[0];
                $profile->handphone = $request->input('handphone');
                $profile->birthdate = $request->input('birthdate');
                $profile->gender = $request->input('gender');
                $profile->address = $request->input('address');
                $profile->competency = $request->input('competency');
                $profile->motivate = $request->input('motivate');
                $profile->save();

                $mailData = [
                    'user_id' => $user->user_id,
                    'password' => $password
                ];
                $this->sendMail($mailData);

                return redirect()->route('frontend.auth.login')->with('message', __('email.reg_success'))
                    ->with('type', 'success');
            } else {
                return redirect()->route('frontend.auth.login')->with('message', __('email.reg_failed'))
                    ->with('type', 'error')->withInput();
            }
        } else {
            return redirect()->route('frontend.auth.login')->with('message', __('email.reg_failed'))
                ->with('type', 'error')->withInput();
        }
    }

    /**
     * @param int $length
     * @return bool|string
     */
    public static function quickRandom($length = 6)
    {
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle(str_repeat($pool, $length)), 0, $length);
    }

    /**
     * @param array $mailData
     * @return mixed
     */
    private function sendMail($mailData = [])
    {
        $data = $this->model->findOrFail($mailData['user_id']);
        $data['passwordNew'] = $mailData['password'];
        return Mail::queue(new SendRegistration($data));
    }
}
