<?php
/**
 * Description: NewsController.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     20/01/2018, modified: 20/01/2018 21:09
 * @copyright   Copyright (c) 2018.
 */

namespace App\Http\Controllers\Frontend\Modules;


use App\Http\Controllers\Controller;
use App\Models\Core\MenuModel;
use App\Models\Core\MenuTranslationModel;
use App\Models\Modules\Content\CategoryModel;
use App\Models\Modules\Content\CategoryTranslationModel;
use App\Models\Modules\Content\PagesModel;
use App\Models\Modules\Content\PagesTranslationModel;
use App\Models\Modules\Content\PostsModel;
use App\Repositories\Core\MenuRepository;
use App\Repositories\Modules\Content\CategoryRepository;
use App\Repositories\Modules\Content\PostRepository;
use Artesaos\SEOTools\Facades\SEOTools;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/**
 * @property MenuModel                model
 * @property Route                    currentRoute
 * @property PostsModel               post
 * @property LaravelLocalization      locale
 * @property CategoryModel            category
 * @property CategoryTranslationModel catranslate
 * @property PagesModel               page
 * @property PagesTranslationModel    ptranslate
 * @property MenuTranslationModel     mtranslate
 */
class NewsController extends Controller
{
    private $template = "frontend.modules.news";
    protected $data = [];
    /**
     * @var PostRepository
     */
    private $repository;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var MenuRepository
     */
    private $menuRepository;

    /**
     * PageController constructor.
     *
     * @param PostRepository                $repository
     * @param CategoryRepository            $categoryRepository
     * @param MenuRepository                $menuRepository
     */
    public function __construct(PostRepository $repository,
                                CategoryRepository $categoryRepository,
                                MenuRepository $menuRepository)
    {
        $this->currentRoute = Route::currentRouteName();
        $this->repository = $repository;
        $this->categoryRepository = $categoryRepository;
        $this->menuRepository = $menuRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $menu = $this->menuRepository->where('module', $this->currentRoute, '=')->first();
        if (!$menu) {
            abort(404);
        }
        $menuItem = $this->menuRepository->getTranslationById($menu->menu_id);
        $title = $menu->single_translate ? $menu->single_translate->menu_title : trans('strings.common.messages.not_translated');

        SEOTools::setTitle($title);
        SEOTools::setDescription(@$menuItem->single_translate->meta_description);
        SEOTools::setCanonical(route($this->currentRoute));
        SEOTools::metatags()->setKeywords(config('general.app.keywords'));
        SEOTools::opengraph()->setSiteName(app_title());
        SEOTools::opengraph()->setTitle($title);
        SEOTools::opengraph()->setDescription(@$menuItem->single_translate->meta_description);
        SEOTools::opengraph()->setUrl(route($this->currentRoute));
        SEOTools::opengraph()->addImage(app_images('logo.png'));
        SEOTools::opengraph()->addProperty('type', 'website');
        SEOTools::opengraph()->addProperty('locale', config('app.locale_system'));
        SEOTools::twitter()->setSite('@' . webConfig('twitter'));
        SEOTools::twitter()->setImage(app_images('logo.png'));
        SEOTools::twitter()->setUrl(route($this->currentRoute));
        SEOTools::jsonLd()->setType('website');
        SEOTools::jsonLd()->addImage(app_images('logo.png'));

        $data['title'] = $title;
        $data['post'] = $this->repository->getRecentNewsAttribute();;
        $data['menus'] = $menu;
        return view($this->template . '.index', $data);
    }

    /**
     * @param string $url
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function category(string $url)
    {
        $category = $this->categoryRepository->getTranslationBySlug($url);
        if (!$category) {
            abort(404);
        }
        $title = $category->single_translate ? $category->single_translate->name : trans('strings.common.messages.not_translated');

        SEOTools::setTitle($title);
        SEOTools::setDescription(@$category->single_translate->description);
        SEOTools::setCanonical(route($this->currentRoute, ['url' => $url]));
        SEOTools::metatags()->setKeywords(config('general.app.keywords'));
        SEOTools::opengraph()->setSiteName(app_title());
        SEOTools::opengraph()->setTitle($title);
        SEOTools::opengraph()->setDescription(@$category->single_translate->description);
        SEOTools::opengraph()->setUrl(route($this->currentRoute, ['url' => $url]));
        SEOTools::opengraph()->addImage(app_images('logo.png'));
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::opengraph()->addProperty('locale', config('app.locale_system'));
        SEOTools::twitter()->setSite('@' . webConfig('twitter'));
        SEOTools::twitter()->setImage(app_images('logo.png'));
        SEOTools::twitter()->setUrl(route($this->currentRoute, ['url' => $url]));
        SEOTools::jsonLd()->setType('articles');
        SEOTools::jsonLd()->addImage(app_images('logo.png'));

        $data['title'] = $title;
        $data['news'] = $this->repository->getCategoryPostAttribute($category->id, 9);
        return view($this->template . '.category', $data);
    }

    /**
     * @param string $slug
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function read(string $slug)
    {
        $post = $this->repository->getTranslationBySlug($slug);
        if(!$post){
            abort(404);
        }

        $title = $post->single_translate ? $post->single_translate->title : trans('strings.common.messages.not_translated');
        $image = @$post->featuredImages ? asset($post->featuredImages->getFullURLAttribute()) : asset(config('general.default_image'));

        SEOTools::setTitle($title);
        SEOTools::setDescription(@$post->single_translate->intro);
        SEOTools::setCanonical($post->getPostRouteAttribute());
        SEOTools::metatags()->setKeywords(config('general.app.keywords'));
        SEOTools::opengraph()->setSiteName(app_title());
        SEOTools::opengraph()->setTitle($title);
        SEOTools::opengraph()->setDescription(@$post->single_translate->intro);
        SEOTools::opengraph()->setUrl($post->getPostRouteAttribute());
        SEOTools::opengraph()->addImage($image);
        SEOTools::opengraph()->addProperty('type', 'article');
        SEOTools::opengraph()->addProperty('locale', config('app.locale_system'));
        SEOTools::opengraph()->setArticle([
            'published_time' => Carbon::createFromFormat('Y-m-d H:i:s', $post->publish_at)->timestamp,
            'modified_time' => Carbon::createFromFormat('Y-m-d H:i:s', $post->updated_at)->timestamp,
            'author' => $post->getAuthorNameAttribute(),
            'section' => $title
        ]);
        SEOTools::jsonLd()->addImage($image);
        SEOTools::jsonLd()->setType('article');
        SEOTools::twitter()->setSite('@' . webConfig('twitter'));
        SEOTools::twitter()->setImage($image);
        SEOTools::twitter()->setUrl($post->getPostRouteAttribute());

        $this->repository->updateById($post->id, ['hit' => $post->hit + 1]);

        $data['title'] = $title;
        $data['categories'] = $post->category->single_translate->name;
        $data['post'] = $post;
        $data['slug'] = $slug;
        $data['category'] = $this->repository->getCategoryOtherAttribute($post->categories, $post->slug);
        return view($this->template . '.read', $data);
    }
}
