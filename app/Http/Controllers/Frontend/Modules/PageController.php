<?php
/**
 * Description: PageController.php PhpStorm.
 * @package     amqit-mutilang
 * @author      alex
 * @created     21/01/2018, modified: 21/01/2018 03:51
 * @copyright   Copyright (c) 2018.
 */

namespace App\Http\Controllers\Frontend\Modules;


use App\Http\Controllers\Controller;
use App\Repositories\Core\MenuRepository;
use App\Repositories\Modules\Content\PageRepository;
use Artesaos\SEOTools\Facades\SEOTools;
use Illuminate\Support\Facades\Route;

/**
 * Class PageController
 * @property string|null currentRoute
 * @package App\Http\Controllers\Frontend\Modules
 */
class PageController extends Controller
{
    private $template = "frontend.modules";

    /**
     * @var PageRepository
     */
    private $repository;
    /**
     * @var MenuRepository
     */
    private $menuRepository;

    /**
     * PageController constructor.
     *
     * @param PageRepository $repository
     * @param MenuRepository $menuRepository
     */
    public function __construct(PageRepository $repository,
                                MenuRepository $menuRepository)
    {
        $this->currentRoute = Route::currentRouteName();
        $this->repository = $repository;
        $this->menuRepository = $menuRepository;
    }

    /**
     * @param $url
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($url)
    {
        $menu = $this->menuRepository->where('url', $url, '=')
            ->where('module', '#', '<>')
            ->where('status', true)->first();
        $item = null;
        if ($menu->source == 'page') {
            $item = $this->repository->getTranslationById($menu->source_id);
        }
        $title = @$item->single_translate ? $item->single_translate->page_title : trans('strings.common.messages.not_translated');
        $desc = @$item->single_translate->page_desc ? $item->single_translate->page_desc : app_desc();

        SEOTools::setTitle($title);
        SEOTools::setDescription($desc);
        SEOTools::setCanonical(route($this->currentRoute, ['url' => $url]));
        SEOTools::metatags()->setKeywords(config('general.app.keywords'));
        SEOTools::opengraph()->setSiteName(app_title());
        SEOTools::opengraph()->setTitle($title);
        SEOTools::opengraph()->setDescription($desc);
        SEOTools::opengraph()->setUrl(route($this->currentRoute, ['url' => $url]));
        SEOTools::opengraph()->addImage(@$item->getHeaderAttribute());
        SEOTools::opengraph()->addProperty('type', 'blog');
        SEOTools::opengraph()->addProperty('locale', config('app.locale_system'));
        SEOTools::twitter()->setSite('@' . webConfig('twitter'));
        SEOTools::twitter()->setImage(@$item->getHeaderAttribute());
        SEOTools::twitter()->setUrl(route($this->currentRoute, ['url' => $url]));
        SEOTools::jsonLd()->setType('blog');
        SEOTools::jsonLd()->addImage(@$item->getHeaderAttribute());

        $data['title'] = $title;
        $data['page'] = $item;
        return view($this->template . '.pages.index', $data);
    }
}
