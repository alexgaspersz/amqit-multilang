<?php
/**
 * Description: DashboardController.php PhpStorm.
 *
 * @package     amqit multi language
 * @author      alex
 * @created     19/09/2018, modified: 19/09/2018 01:05
 * @copyright   Copyright (c) 2018.
 */

namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;
use App\Repositories\Core\UsersRepository;
use App\Repositories\Modules\Content\PostRepository;

/**
 * Class       DashboardController
 * @package App\Http\Controllers\Backend
 * $
 *
 */
class DashboardController extends Controller
{
    private $route = "admin.dashboard";
    /**
     * @var UsersRepository
     */
    private $usersRepository;
    /**
     * @var PostRepository
     */
    private $postRepository;

    /**
     * DashboardController constructor.
     *
     * @param UsersRepository $usersRepository
     * @param PostRepository  $postRepository
     */
    public function __construct(UsersRepository $usersRepository,
                                PostRepository $postRepository)
    {
        $this->usersRepository = $usersRepository;
        $this->postRepository = $postRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data['title'] = __('module.dashboard.title');
        $data['route'] = $this->route;

        return view('backend.dashboard', $data);
    }
}
