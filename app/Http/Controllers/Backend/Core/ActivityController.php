<?php
/**
 * Description: ActivityController.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     11/12/2017, modified: 11/12/2017 03:57
 * @copyright   Copyright (c) 2018.
 */

namespace App\Http\Controllers\Backend\Core;


use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use App\Models\Core\AuditModel;
use Illuminate\Http\Request;

/**
 * @property AuditModel model
 */
class ActivityController extends Controller
{
    private $route = "admin.system.logs";
    private $template = "backend.core.audit";

    /**
     * ActivityController constructor.
     */
    public function __construct()
    {
        $this->model = new AuditModel();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        permission('is_read', $this->getRoute(), false, 'module');
        $data['title'] = __('module.logs.title');
        $data['route'] = $this->route;
        return view($this->template.'.index', $data);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function grid()
    {
        $dataArray = $this->model->join('tb_users', 'tb_logs.user_id', '=', 'tb_users.user_id')
            ->join('tb_groups', 'tb_users.group_id', '=', 'tb_groups.group_id')
            ->whereNotIn('tb_groups.is_super', [1])
            ->select('tb_logs.*')
            ->latest('tb_logs.log_id')->get();
        $_data = [];
        foreach ($dataArray as $row) {
            $_data[] = [
                'secureId' => encode_id($row->group_id),
                'ids' => $row->group_id,
                'user' => $row->user->fullname,
                'module' => $row->module,
                'task' => $row->task,
                'ipaddress' => $row->ipaddress,
                'created' => dateString($row->created_at, true, true),
                'note' => $row->note
            ];
        }
        return response()->json($_data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     */
    public function destroy(Request $request)
    {
        permission('is_delete', $this->getRoute(), false, 'module');
        if ($request->all()) {
            $result = $this->model->truncate();
            if ($result) {
                return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.delete.success'))
                    ->with('type', 'success');
            } else {
                throw new GeneralException(__('strings.common.messages.delete.failed'));
            }
        } else {
            throw new GeneralException(__('strings.common.messages.empty'));
        }
    }

    /**
     * @param string $prefix
     * @return string
     */
    public function getRoute($prefix = '*'): string
    {
        return $this->route.'.'.$prefix;
    }
}
