<?php
/**
 * Description: MenuController.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     03/12/2018, modified: 03/12/2018 19:17
 * @copyright   Copyright (c) 2018.
 */

namespace App\Http\Controllers\Backend\Core;


use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use App\Models\Core\AccessModel;
use App\Repositories\Core\GroupRepository;
use App\Repositories\Core\MenuGroupRepository;
use App\Repositories\Core\MenuRepository;
use App\Repositories\Core\MenuTranslationRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Mcamara\LaravelLocalization\LaravelLocalization;

/**
 * Class MenuController
 * @package App\Http\Controllers\Backend\Core
 */
class MenuController extends Controller
{
    private $route = "admin.system.menus";
    private $template = "backend.core.menus";

    /**
     * @var MenuRepository
     */
    private $repository;
    /**
     * @var MenuTranslationRepository
     */
    private $translationRepository;
    /**
     * @var GroupRepository
     */
    private $groupRepository;
    /**
     * @var LaravelLocalization
     */
    private $locale;

    /**
     * MenuController constructor.
     *
     * @param MenuRepository            $repository
     * @param MenuTranslationRepository $translationRepository
     * @param MenuGroupRepository       $groupRepository
     * @param LaravelLocalization       $locale
     */
    public function __construct(MenuRepository $repository,
                                MenuTranslationRepository $translationRepository,
                                MenuGroupRepository $groupRepository,
                                LaravelLocalization $locale)
    {
        $this->middleware(['isRestricted']);
        $this->repository = $repository;
        $this->translationRepository = $translationRepository;
        $this->groupRepository = $groupRepository;
        $this->locale = $locale;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function group()
    {
        $data['route'] = $this->route;
        $data['title'] = __('module.menu.group.title');
        return view($this->template.'.group-index', $data);
    }

    /**
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function group_update($id = null)
    {
        if($id){
            $key = decode_id($id);
            $data = $this->groupRepository->getById($key);
            $data['title'] = __('strings.common.edit') . ' ' . __('module.menu.group.module');
        } else {
            $data = null;
            $data['title'] = __('strings.common.add') . ' ' . __('module.menu.group.module');
        }
        $data['secure_id'] = $id;
        $data['route'] = $this->route;
        return view($this->template.'.group-form', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     */
    public function group_store(Request $request)
    {
        if ($request->all()) {
            $keyId = decode_id($request->input('secure_id'));
            Validator::make($request->all(), [
                'menu_group' => 'required|'.Rule::unique('tb_menu_group')->ignore($keyId),
                'menu_group_alias' => 'required|'.Rule::unique('tb_menu_group')->ignore($keyId)
            ], [
                'menu_group.required' => __('validation.required',['field' => __('module.menu.group.module')]),
                'menu_group.unique' => __('validation.unique',['data' => __('module.menu.group.module')]),
                'menu_group_alias.required' => __('validation.required',['field' => __('strings.common.slug')]),
                'menu_group_alias.unique' => __('validation.unique',['data' => __('strings.common.slug')]),
            ])->validate();

            $data = $request->all();
            $data['status'] = $request->has('status');
            if (!$request->input('secure_id')) {
                permission('is_create', $this->getRoute(), false, 'module');
                $data['created_by'] = Auth::id();
                $result = $this->groupRepository->create($data);
                if ($result) {
                    logActivity($request, __('strings.backend.logs.add',['val' => strtolower(__('module.menu.group.module')) .' ' . $result->menu_group]));
                    return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.save.success'))
                        ->with('type', 'success');
                } else {
                    throw new GeneralException(__('strings.common.messages.save.failed'));
                }
            } else {
                permission('is_update', $this->getRoute(), false, 'module');
                $keyId = decode_id($request->input('secure_id'));
                $data['updated_by'] = Auth::id();
                $result = $this->groupRepository->updateById($keyId, $data);
                if ($result) {
                    logActivity($request, __('strings.backend.logs.edit',['val' => strtolower(__('module.menu.group.module')) .' ' . $result->menu_group]));
                    return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.update.success'))
                        ->with('type', 'success');
                } else {
                    throw new GeneralException(__('strings.common.messages.update.failed'));
                }
            }
        }
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function group_grid()
    {
        $dataArray = $this->groupRepository->get();
        $_data = [];
        foreach ($dataArray as $row) {
            $_data[] = [
                'secureId' => encode_id($row->group_id),
                'ids' => $row->group_id,
                'group' => $row->menu_group,
                'alias' => $row->menu_group_alias,
                'status' => $row->status
            ];
        }
        return response()->json($_data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        permission('is_read', $this->getRoute(), false, 'module');
        $data['groupMenus'] = $this->groupRepository->where('status', true)->orderBy('id', 'ASC')->get();
        $data['route'] = $this->route;
        $data['title'] = __('module.menu.module');
        return view($this->template.'.index', $data);
    }

    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Mcamara\LaravelLocalization\Exceptions\SupportedLocalesNotDefined
     */
    public function create($slug)
    {
        permission('is_create', $this->getRoute(), false, 'module');
        $data['item'] = null;
        $data['type'] = $slug;
        $data['groupMenus'] = $this->groupRepository->where('status', true)->orderBy('id', 'ASC')->get();
        $data['route'] = $this->route;
        $data['localization'] = $this->locale->getSupportedLocales();
        $data['title'] = __('strings.common.add') . ' ' . __('module.menu.module');
        return view($this->template.'.form', $data);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Mcamara\LaravelLocalization\Exceptions\SupportedLocalesNotDefined
     */
    public function update($id)
    {
        permission('is_update', $this->getRoute(), false, 'module');
        $key = decode_id($id);
        $items = $this->repository->getById($key);
        $data['item'] = $items;
        $data['type'] = $items->menu_type;
        $data['groupMenus'] = $this->groupRepository->where('status', true)->orderBy('id', 'ASC')->get();
        $data['secure_id'] = $id;
        $data['route'] = $this->route;
        $data['localization'] = $this->locale->getSupportedLocales();
        $data['title'] = __('strings.common.edit') . ' ' . __('module.menu.module');
        return view($this->template.'.form', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     * @throws \Mcamara\LaravelLocalization\Exceptions\SupportedLocalesNotDefined
     */
    public function store(Request $request)
    {
        $insert = [];
        if ($request->all()) {
            Validator::make($request->all(), [
                'menu_title_'.defaultLang() => 'required',
                'menu_type' => 'required',
                'module' => 'required',
                'url' => 'required',
            ], [
                'menu_title_'.defaultLang().'.required' => __('validation.required',['attribute' => __('forms.menu.name')]),
                'module.required' => __('validation.required',['attribute' => __('forms.menu.route')]),
                'url.required' => __('validation.required',['attribute' => __('forms.menu.url')]),
                'menu_type.required' => __('validation.required',['attribute' => __('forms.menu.type')]),
            ])->validate();

            $data = $request->all();
            $data['source'] = $request->input('source') ? $request->input('source') : null;
            $data['source_id'] = $request->input('source_id') ? $request->input('source_id') : null;
            $data['parent_id'] = $request->input('parent_id') ? $request->input('parent_id') : 0;
            $data['menu_icons'] = $request->input('menu_icons') ? $request->input('menu_icons') : 'cil-arrow-circle-right';
            $data['status'] = $request->has('status');
            if (!$request->input('secure_id')) {
                permission('is_create', $this->getRoute(), false, 'module');
                $data['created_by'] = Auth::id();
                $result = $this->repository->create($data);
                if ($result) {
                    foreach ($this->locale->getSupportedLocales() as $locale => $val) {
                        $insert[$locale] = [
                            'menu_id' => $result->menu_id,
                            'menu_title' => $data['menu_title_' . $locale],
                            'meta_title' => $data['meta_title_' . $locale],
                            'meta_description' => $data['meta_description_' . $locale],
                            'locale' => $locale,
                            'created_at' => Carbon::now()
                        ];
                        $this->translationRepository->create($insert[$locale]);
                    }
                    logActivity($request, __('strings.backend.logs.add',['val' => strtolower(__('module.menu.module')) .' ' . $result->url]));
                    return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.save.success'))
                        ->with('type', 'success');
                } else {
                    throw new GeneralException(__('strings.common.messages.save.failed'));
                }
            } else {
                permission('is_update', $this->getRoute(), false, 'module');
                $keyId = decode_id($request->input('secure_id'));
                $data['updated_by'] = Auth::id();
                $result = $this->repository->updateById($keyId, $data);
                if ($result) {
                    $translation = $this->repository->getById($keyId);
                    foreach ($this->locale->getSupportedLocales() as $locale => $val) {
                        $insert[$locale] = [
                            'menu_id' => $result->menu_id,
                            'menu_title' => $data['menu_title_' . $locale],
                            'meta_title' => $data['meta_title_' . $locale],
                            'meta_description' => $data['meta_description_' . $locale],
                            'locale' => $locale
                        ];
                        $existing = $translation->translation()->where('locale', '=', $locale);
                        if($existing->count() > 0){
                            $insert[$locale]['updated_at'] = Carbon::now();
                            $existing->update($insert[$locale]);
                        } else {
                            $insert[$locale]['created_at'] = Carbon::now();
                            $this->translationRepository->create($insert[$locale]);
                        }
                    }
                    logActivity($request, __('strings.backend.logs.edit',['val' => strtolower(__('module.menu.module')) .' ' . $result->url]));
                    return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.update.success'))
                        ->with('type', 'success');
                } else {
                    throw new GeneralException(__('strings.common.messages.update.failed'));
                }
            }
        } else {
            throw new GeneralException(__('strings.common.messages.empty'));
        }
    }

    /**
     * Parsing json data menu & reorder
     */
    public function reorderMenu()
    {
        $decoded = json_decode($_POST['menu']);
        $this->parsingMenu($decoded);
        echo json_encode(["stat" => true]);
        Session::flash('message', __('strings.common.messages.save.success'));
        Session::flash('type', 'success');
    }

    /**
     * @param $dec
     * @param int $order
     * @param int $level
     */
    public function parsingMenu($dec, $order = 0, $level = 0)
    {
        foreach ($dec as $key) {
            $order++;
            $this->repository->updateByColumn('menu_id',$key->id,['ordering' => $order]);

            if (isset($key->children)) {
                foreach ($key->children as $keychild) {
                   $this->repository->updateByColumn('menu_id',$keychild->id,['parent_id' => $key->id]);
                }
                $this->parsingMenu($key->children, 0, $level + 1);
            } else {
                if ($level == 0) {
                    $this->repository->updateByColumn('menu_id',$key->id,['parent_id' => 0]);
                }
            }
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Request $request)
    {
        permission('is_delete', $this->getRoute(), false, 'module');
        if (!$request->input('secure_id')) {
            throw new GeneralException(__('strings.common.messages.empty'));
        }

        $id = decode_id($request->input('secure_id'));
        $data = $this->repository->getById($id);
        if ($data) {
            $child = $this->repository->where('parent_id', $id,'=')->count();
            if ($child > 0) {
                throw new GeneralException('Failed, this parent menu have child(s) menu assigned.');
            }

            if ($data->delete()) {
                logActivity($request, __('strings.backend.logs.delete',['val' => strtolower(__('module.menu.module')) .' ' . $data->url]));
                return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.delete.success'))
                    ->with('type', 'success');
            } else {
                throw new GeneralException(__('strings.common.messages.delete.failed'));
            }
        } else {
            throw new GeneralException(__('strings.common.messages.not_found'));
        }
    }

    /**
     * @param string $type
     * @param bool   $active
     *
     * @return string
     */
    public function generateAdminSideMenu($type = 'adminsidebar', $active = true)
    {
        $parent = $this->repository->getParentByTypeStatus($type);
        $html = '';
        foreach ($parent as $p1) {
            $child2 = $this->repository->getMenuByParent($p1->menu_id);
            $access1 = permission('is_read', $p1->menu_id, true);
            $ch1 = count($child2) > 0 ? 'c-sidebar-nav-dropdown' : 'c-sidebar-nav-item';
            $link1 = count($child2) > 0 ? 'c-sidebar-nav-dropdown-toggle' : 'c-sidebar-nav-link';
            if ($access1 == true) {
                $active1 = $active == true ? ' ' . active([$p1->module]) : null;
                $html .= '<li class="' . $ch1 . $active1 . '"><a class="' . $link1 . '" href="' . url($p1->url) . '"><i class="' . $p1->menu_icons . ' c-sidebar-nav-icon"></i> ' . @$p1->single_translate->menu_title;
                if (count($child2) > 0) {
                    $html .= '</a>';
                    $html .= '<ul class="c-sidebar-nav-dropdown-items">';
                    foreach ($child2 as $p2) {
                        $child3 = $this->repository->getMenuByParent($p2->menu_id);
                        $access2 = permission('is_read', $p2->menu_id, true);
                        $ch2 = count($child3) > 0 ? 'c-sidebar-nav-dropdown' : 'c-sidebar-nav-item';
                        $link2 = count($child3) > 0 ? 'c-sidebar-nav-dropdown-toggle' : 'c-sidebar-nav-link';
                        if ($access2 == true) {
                            $active2 = $active == true ? ' ' . active([$p2->module]) : null;
                            $html .= '<li class="' . $ch2 . $active2 . '"><a class="' . $link2 . '" href="' . url($p2->url) . '"><i class="' . $p2->menu_icons . ' c-sidebar-nav-icon"></i> ' . @$p2->single_translate->menu_title;
                            if (count($child3) > 0) {
                                $html .= '</a>';
                                $html .= '<ul class="c-sidebar-nav-dropdown-items">';
                                foreach ($child3 as $p3) {
                                    $child4 = $this->repository->getMenuByParent($p3->menu_id);
                                    $access3 = permission('is_read', $p3->menu_id, true);
                                    $ch3 = count($child4) > 0 ? 'c-sidebar-nav-dropdown' : 'c-sidebar-nav-item';
                                    $link3 = count($child4) > 0 ? 'c-sidebar-nav-dropdown-toggle' : 'c-sidebar-nav-link';
                                    if ($access3 == true) {
                                        $active3 = $active == true ? ' ' . active([$p3->module]) : null;
                                        $html .= '<li class="' . $ch3 . $active3 . '"><a class="' . $link3 . '" href="' . url($p3->url) . '"><i class="' . $p3->menu_icons . ' c-sidebar-nav-icon"></i> ' . @$p3->single_translate->menu_title;
                                        if (count($child4) > 0) {
                                            $html .= '</a>';
                                            $html .= '<ul class="c-sidebar-nav-dropdown-items">';
                                            foreach ($child4 as $p4) {
                                                $html .= '<li class="c-sidebar-nav-item ' . active([$p4->module]) . '"><a class="c-sidebar-nav-link" href="' . url($p4->url) . '"><i class="' . $p4->menu_icons . ' c-sidebar-nav-icon"></i>' . @$p4->single_translate->menu_title;
                                            }
                                            $html .= '</ul>';
                                        } else {
                                            $html .= '</a>';
                                        }
                                        $html .= '</li>';
                                    }
                                }
                                $html .= '</ul>';
                            } else {
                                $html .= '</a>';
                            }
                            $html .= '</li>';
                        }
                    }
                    $html .= '</ul>';
                } else {
                    $html .= '</a>';
                }
                $html .= '</li>';
            }
        }
        $html .= '';
        return $html;
    }

    /**
     * @param string $type
     * @return string
     */
    public function generateWebNavigation($type = 'webtopbar')
    {
        $parent = $this->repository->getParentByTypeStatus($type);
        $html = '<li class="' . active(['frontend.index']) . '"><a href="'.route('frontend.index').'">'.__('strings.backend.dashboard.home').'</a></li>';
        foreach ($parent as $p1) {
            $child = $this->repository->getMenuByParent($p1->menu_id);
            $hasChild = count($child) > 0 ? ' dropdown' : '';
            $dropChild = count($child) > 0 ? 'dropdown-menu' : '';
            $linkChild = count($child) > 0 ? 'class="dropdown-toggle" data-toggle="dropdown"' : '';
            if($p1->url == '/' || $p1->url == '#'):
                $link1 = count($child) > 0 || $p1->module == '#' ? '#' : route($p1->module);
            else:
                $link1 = count($child) > 0 || $p1->module == '#' ? '#' : route($p1->module, ['url' => $p1->url]);
            endif;

            if($p1->is_restrict == false) :
                $html .= '<li class="' . active([$p1->module]) . $hasChild . '"><a href="' . $link1 . '" ' . $linkChild . '>' . @$p1->single_translate->menu_title;
                if (count($child) > 0) {
                    $html .= '  <span class="fa fa-angle-down"></span></a>';
                    $html .= '<ul class="' . $dropChild . ' " role="menu">';
                    foreach ($child as $p2) {

                        if($p2->url == '/' || $p2->url == '#'):
                            $link2 = $p2->module == '#' ? '#' : route($p2->module);
                        else:
                            $link2 = $p2->module == '#' ? '#' : route($p2->module, ['url' => $p2->url]);
                        endif;

                        if($p2->is_restrict == false) :
                            $html .= '<li><a href="' . $link2 . '">' . @$p2->single_translate->menu_title . '</a></li>';
                        elseif ($p2->is_restrict == true && Auth::check()) :
                            $html .= '<li><a href="' . $link2 . '">' . @$p2->single_translate->menu_title . '</a></li>';
                        endif;
                    }
                    $html .= '</ul>';
                } else {
                    $html .= '</a>';
                }
                $html .= '</li>';
            elseif ($p1->is_restrict == true && Auth::check()) :
                $html .= '<li class="' . $hasChild . '"><a href="' . $link1 . '">' . @$p1->single_translate->menu_title . '</a></li>';
            endif;
        }
        $html .= '';

        return $html;
    }

    /**
     * @param $id
     * @param $type
     * @return array
     */
    public function getMenu($id, $type)
    {
        $parent = $this->repository->getMenuByParentPosition(0,$type);
        $_parent = [];
        foreach ($parent as $row) {
            $lev1 = $this->repository->getMenuByParent($row->menu_id);
            $_lev1 = [];
            foreach ($lev1 as $l1) {
                $lev2 = $this->repository->getMenuByParent($l1->menu_id);
                $_lev2 = [];
                foreach ($lev2 as $l2) {
                    $lev3 = $this->repository->getMenuByParent($l2->menu_id);
                    $_lev3 = [];
                    foreach ($lev3 as $l3) {
                        $lev4 = $this->repository->getMenuByParent($l3->menu_id);
                        $_lev4 = [];
                        foreach ($lev4 as $l4) {
                            $lev4Access = AccessModel::where('group_id', '=', $id)->where('menu_id', '=', $l4->menu_id)->first();
                            $_lev4[] = [
                                'id' => $l4->menu_id,
                                'menu_name' => $l4->single_translate->menu_title,
                                'ordering' => $l4->ordering,
                                'checked' => [
                                    "is_create" => $lev4Access ? $lev4Access->is_create : 0,
                                    "is_read" => $lev4Access ? $lev4Access->is_read : 0,
                                    "is_update" => $lev4Access ? $lev4Access->is_update : 0,
                                    "is_delete" => $lev4Access ? $lev4Access->is_delete : 0,
                                    "is_approve" => $lev4Access ? $lev4Access->is_approve : 0,
                                    "is_download" => $lev4Access ? $lev4Access->is_download : 0,
                                ]
                            ];
                        }
                        $lev3Access = AccessModel::where('group_id', '=', $id)->where('menu_id', '=', $l3->menu_id)->first();
                        $_lev3[] = [
                            'id' => $l3->menu_id,
                            'menu_name' => $l3->single_translate->menu_title,
                            'ordering' => $l3->ordering,
                            'level4' => $_lev4,
                            'checked' => [
                                "is_create" => $lev3Access ? $lev3Access->is_create : 0,
                                "is_read" => $lev3Access ? $lev3Access->is_read : 0,
                                "is_update" => $lev3Access ? $lev3Access->is_update : 0,
                                "is_delete" => $lev3Access ? $lev3Access->is_delete : 0,
                                "is_approve" => $lev3Access ? $lev3Access->is_approve : 0,
                                "is_download" => $lev3Access ? $lev3Access->is_download : 0,
                            ]
                        ];
                    }
                    $lev2Access = AccessModel::where('group_id', '=', $id)->where('menu_id', '=', $l2->menu_id)->first();
                    $_lev2[] = [
                        'id' => $l2->menu_id,
                        'menu_name' => $l2->single_translate->menu_title,
                        'ordering' => $l2->ordering,
                        'level3' => $_lev3,
                        'checked' => [
                            "is_create" => $lev2Access ? $lev2Access->is_create : 0,
                            "is_read" => $lev2Access ? $lev2Access->is_read : 0,
                            "is_update" => $lev2Access ? $lev2Access->is_update : 0,
                            "is_delete" => $lev2Access ? $lev2Access->is_delete : 0,
                            "is_approve" => $lev2Access ? $lev2Access->is_approve : 0,
                            "is_download" => $lev2Access ? $lev2Access->is_download : 0,
                        ]
                    ];
                }

                $lev1Access = AccessModel::where('group_id', '=', $id)->where('menu_id', '=', $l1->menu_id)->first();
                $_lev1[] = [
                    'id' => $l1->menu_id,
                    'menu_name' => $l1->single_translate->menu_title,
                    'ordering' => $l1->ordering,
                    'level2' => $_lev2,
                    'checked' => [
                        "is_create" => $lev1Access ? $lev1Access->is_create : 0,
                        "is_read" => $lev1Access ? $lev1Access->is_read : 0,
                        "is_update" => $lev1Access ? $lev1Access->is_update : 0,
                        "is_delete" => $lev1Access ? $lev1Access->is_delete : 0,
                        "is_approve" => $lev1Access ? $lev1Access->is_approve : 0,
                        "is_download" => $lev1Access ? $lev1Access->is_download : 0,
                    ]
                ];
            }

            $parentAccess = AccessModel::where('group_id', '=', $id)->where('menu_id', '=', $row->menu_id)->first();
            $_parent[] = [
                'id' => $row->menu_id,
                'menu_name' => $row->single_translate->menu_title,
                'ordering' => $row->ordering,
                'level1' => $_lev1,
                'checked' => [
                    "is_create" => $parentAccess ? $parentAccess->is_create : 0,
                    "is_read" => $parentAccess ? $parentAccess->is_read : 0,
                    "is_update" => $parentAccess ? $parentAccess->is_update : 0,
                    "is_delete" => $parentAccess ? $parentAccess->is_delete : 0,
                    "is_approve" => $parentAccess ? $parentAccess->is_approve : 0,
                    "is_download" => $parentAccess ? $parentAccess->is_download : 0,
                ]
            ];

        }

        return $_parent;
    }

    /**
     * @param string $position
     * @return null|string
     */
    public function generateMenu($position = 'adminsidebar')
    {
        $group = $this->groupRepository->getByColumn($position,'menu_group_alias');
        $parent = $this->repository->getParentByType($position);
        $html = null;
        if (count($parent) > 0) {
            $html = '<ol class="dd-list dd-nodrag">';
            foreach ($parent as $lev1) {
                $child1 = $this->repository->getMenuByParent($lev1->menu_id, [0, 1]);
                $action1 = '<div style="float:right">';
                if (access('is_update', $this->getRoute())):
                    $action1 .= '<a href="' . route($this->getRoute('update'), ['id' => encode_id($lev1->menu_id)]) . '" class="btn btn-sm btn-warning"><i class="fas fa-pencil-alt text-white"></i></a>';
                endif;
                if (access('is_delete', $this->getRoute())):
                    $action1 .= '&nbsp;<button type="button" class="btn btn-sm btn-danger" onclick="appRemove(\'' . encode_id($lev1->menu_id) . '\',\'' . $lev1->single_translate->menu_title . '\')"><i class="fas fa-trash"></i></button>';
                endif;
                $action1 .= '</div>';
                $html .= '<li class="dd-item" data-id="' . $lev1->menu_id . '">';
                $html .= '<div class="dd-handle">' . $lev1->single_translate->menu_title . ' <i class="fas fa-link"></i> <em>' . $lev1->url . '</em>&nbsp;&nbsp;' . getActive($lev1->status) . $action1 . '</div>';
                if (count($child1) > 0) {
                    $html .= '<ol class="dd-list dd-nodrag">';
                    foreach ($child1 as $lev2) {
                        $child2 = $this->repository->getMenuByParent($lev2->menu_id, [0, 1]);
                        $action2 = '<div style="float:right">';
                        if (access('is_update', $this->getRoute())):
                            $action2 .= '<a href="' . route($this->getRoute('update'), ['id' => encode_id($lev2->menu_id)]) . '" class="btn btn-sm btn-warning"><i class="fas fa-pencil-alt text-white"></i></a>';
                        endif;
                        if (access('is_delete', $this->getRoute())):
                            $action2 .= '&nbsp;<button type="button" class="btn btn-sm btn-danger" onclick="appRemove(\'' . encode_id($lev2->menu_id) . '\',\'' . $lev2->single_translate->menu_title . '\')"><i class="fas fa-trash"></i></button>';
                        endif;
                        $action2 .= '</div>';
                        $html .= '<li class="dd-item" data-id="' . $lev2->menu_id . '">';
                        $html .= '<div class="dd-handle">' . $lev2->single_translate->menu_title . ' <i class="fas fa-link"></i> <em>' . $lev2->url . '</em>&nbsp;&nbsp;' . getActive($lev2->status) . $action2 . '</div>';
                        if (count($child2) > 0) {
                            $html .= '<ol class="dd-list dd-nodrag">';
                            foreach ($child2 as $lev3) {
                                $child3 = $this->repository->getMenuByParent($lev3->menu_id, [0, 1]);
                                $action3 = '<div style="float:right">';
                                if (access('is_update', $this->getRoute())):
                                    $action3 .= '<a href="' . route($this->getRoute('update'), ['id' => encode_id($lev3->menu_id)]) . '" class="btn btn-sm btn-warning"><i class="fas fa-pencil-alt text-white"></i></a>';
                                endif;
                                if (access('is_delete', $this->getRoute())):
                                    $action3 .= '&nbsp;<button type="button" class="btn btn-sm btn-danger" onclick="appRemove(\'' . encode_id($lev3->menu_id) . '\',\'' . $lev3->single_translate->menu_title . '\')"><i class="fas fa-trash"></i></button>';
                                endif;
                                $action3 .= '</div>';
                                $html .= '<li class="dd-item" data-id="' . $lev3->menu_id . '">';
                                $html .= '<div class="dd-handle">' . $lev3->single_translate->menu_title . ' <i class="fas fa-link"></i> <em>' . $lev3->url . '</em>&nbsp;&nbsp;' . getActive($lev3->status) . $action3 . '</div>';
                                if (count($child3) > 0) {
                                    $html .= '<ol class="dd-list dd-nodrag">';
                                    foreach ($child3 as $lev4) {
                                        $child4 = $this->repository->getMenuByParent($lev4->menu_id, [0, 1]);
                                        $action4 = '<div style="float:right">';
                                        if (access('is_update', $this->getRoute())):
                                            $action4 .= '<a href="' . route($this->getRoute('update'), ['id' => encode_id($lev4->menu_id)]) . '" class="btn btn-sm btn-warning"><i class="fas fa-pencil-alt text-white"></i></a>';
                                        endif;
                                        if (access('is_delete', $this->getRoute())):
                                            $action4 .= '&nbsp;<button type="button" class="btn btn-sm btn-danger" onclick="appRemove(\'' . encode_id($lev4->menu_id) . '\',\'' . $lev4->single_translate->menu_title . '\')"><i class="fas fa-trash"></i></button>';
                                        endif;
                                        $action4 .= '</div>';
                                        $html .= '<li class="dd-item" data-id="' . $lev4->menu_id . '">';
                                        $html .= '<div class="dd-handle">' . $lev4->single_translate->menu_title . ' <i class="fas fa-link"></i> <em>' . $lev4->url . '</em>&nbsp;&nbsp;' . getActive($lev4->status) . $action4 . '</div>';
                                        if (count($child4) > 0) {
                                            $html .= '<ol class="dd-list dd-nodrag">';
                                            foreach ($child4 as $lev5) {
                                                $action5 = '<div style="float:right">';
                                                if (access('is_update', $this->getRoute())):
                                                    $action5 .= '<a href="' . route($this->getRoute('update'), ['id' => encode_id($lev5->menu_id)]) . '" class="btn btn-sm btn-warning"><i class="fas fa-pencil-alt text-white"></i></a>';
                                                endif;
                                                if (access('is_delete', $this->getRoute())):
                                                    $action5 .= '&nbsp;<button type="button" class="btn btn-sm btn-danger" onclick="appRemove(\'' . encode_id($lev5->menu_id) . '\',\'' . $lev5->single_translate->menu_title . '\')"><i class="fas fa-trash"></i></button>';
                                                endif;
                                                $action5 .= '</div>';
                                                $html .= '<li class="dd-item" data-id="' . $lev5->menu_id . '">';
                                                $html .= '<div class="dd-handle">' . $lev5->single_translate->menu_title . ' <i class="fas fa-link"></i> <em>' . $lev5->url . '</em>&nbsp;&nbsp;' . getActive($lev5->status) . $action5 . '</div>';
                                                $html .= '</li>';
                                            }
                                            $html .= '</ol>';
                                        }
                                        $html .= '</li>'; //end li $child3
                                    }
                                    $html .= '</ol>';
                                }
                                $html .= '</li>'; //end li $child2
                            }
                            $html .= '</ol>';
                        }
                        $html .= '</li>'; //end li $child1
                    }
                    $html .= '</ol>';
                }
                $html .= '</li>'; //end li $parent
            }
        }
        return response()->json(['group' => $group->menu_group, 'menu' => $html]);
    }

    /**
     * @param $group
     * @return mixed
     */
    public function getNavigation($group)
    {
        return $this->repository->getActiveByPosition($group);
    }

    /**
     * @param $parent
     * @return mixed
     */
    public function getChildNavigation($parent)
    {
        return $this->repository->getMenuByParent($parent);
    }

    /**
     * @param string $prefix
     * @return string
     */
    public function getRoute($prefix = '*'): string
    {
        return $this->route.'.'.$prefix;
    }
}
