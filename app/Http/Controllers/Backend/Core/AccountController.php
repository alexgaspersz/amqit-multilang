<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 2019-03-25
 * Time: 02:51
 */

namespace App\Http\Controllers\Backend\Core;


use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use App\Repositories\Core\UserProfileRepository;
use App\Repositories\Core\UsersRepository;
use App\Repositories\Handler\UploadHandlerRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;

class AccountController extends Controller
{
    private $route = "admin.account";
    private $template = "backend.core.users";
    /**
     * @var UsersRepository
     */
    private $usersRepository;
    /**
     * @var UserProfileRepository
     */
    private $profileRepository;
    /**
     * @var UploadHandlerRepository
     */
    private $handlerRepository;

    /**
     * AccountController constructor.
     * @param UsersRepository $usersRepository
     * @param UserProfileRepository $profileRepository
     * @param UploadHandlerRepository $handlerRepository
     */
    public function __construct(UsersRepository $usersRepository,
                                UserProfileRepository $profileRepository,
                                UploadHandlerRepository $handlerRepository)
    {
        $this->usersRepository = $usersRepository;
        $this->profileRepository = $profileRepository;
        $this->handlerRepository = $handlerRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (session('logged_in') == false){
            abort('403');
        }

        $keyId = session('uid');
        $user = $this->usersRepository->getById($keyId);
        $data['user'] = $user;
        $data['route'] = $this->route;
        $data['title'] = __('forms.user.account');
        return view($this->template.'.account', $data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function password()
    {
        if (session('logged_in') == false){
            abort('403');
        }

        $keyId = session('uid');
        $user = $this->usersRepository->getById($keyId);
        $data['user'] = $user;
        $data['route'] = $this->route;
        $data['title'] = __('strings.common.user.change_password');
        return view($this->template.'.password', $data);
    }

    /**
     * Profile data update function
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     */
    public function store(Request $request)
    {
        if ($request->all()) {
            $key = Auth::id();
            Validator::make($request->all(), [
                'fullname' => 'required',
                'email' => 'required|' . Rule::unique('tb_users')->ignore($key, 'user_id')
            ],[
                'fullname.required' => __('validation.required', ['field' => __('forms.user.fullname')]),
                'email.required' => __('validation.required', ['field' => __('forms.user.email')]),
                'email.unique' => __('validation.unique', ['field' => __('forms.user.email')])
            ])->validate();

            $data = $request->all();
            $profile = request()->except(['_token', 'group_id', 'fullname', 'email', 'wali_kelas', 'secure_id', 'regnumber', 'siswa_kelas', 'propic']);
            if ($request->hasFile('propic')) {
                $image = $request->file('propic');
                $folder = 'users';
                $destinationPath = public_path(uploadFolder() . $folder);
                $imageMime = $image->getClientMimeType();
                $imageExtension = $image->getClientOriginalExtension();
                $imageSize = $image->getSize();
                $newFilename = uniqid('user_').'.'.$imageExtension;

                if (!File::exists($destinationPath)) {
                    File::isDirectory($destinationPath) or File::makeDirectory($destinationPath, 0777, true, true);
                }

                $uploaded = $image->move($destinationPath,$newFilename);
                if ($uploaded) {
                    $image = Image::make($destinationPath.'/'.$newFilename);
                    $thumbDir = $image->dirname . '/thumbs';
                    if (!File::exists($thumbDir)) {
                        File::makeDirectory($thumbDir, 0777, true, true);
                    }

                    $thumbnail = $thumbDir . '/' . $image->filename . '_' . 350 . 'x' . 350 . '.' . $image->extension;
                    if (!File::exists($thumbnail)) {
                        $image->fit(350, 350)->save($thumbnail);
                    }

                    $insert = [
                        'category' => 'USER',
                        'title' => request('fullname'),
                        'base_folder' => $folder,
                        'base_path' => $newFilename,
                        'mime_type' => $imageMime,
                        'extension' => $imageExtension,
                        'size' => $imageSize,
                        'created_by' => Auth::id()
                    ];
                    $insert['created_by'] = Auth::id();
                    $asset = $this->handlerRepository->create($insert);
                    $data['avatar'] = $asset->assetId;
                }
            }

            $data['user_id'] = $key;
            $data['updated_by'] = $key;
            $data['updated_at'] = Carbon::now();
            $result = $this->usersRepository->updateById($key,$data);
            if ($result) {
                $user = $this->usersRepository->getById($key);
                $userProfile = $user->profile();

                $display = explode(' ', $request->input('fullname'));
                $profile['user_id'] = $user->user_id;
                $profile['display_name'] = $request->input('display_name') ? $request->input('display_name') : $display[0];

                if ($userProfile->count() > 0) {
                    $userProfile->update($profile);
                } else {
                    $this->profileRepository->create($profile);
                }

                if ($key == $user->user_id) {
                    $session = [
                        'fullname' => $user->fullname,
                        'email' => $user->email,
                        'display' => @$user->profile && @$user->profile->display_name ? $user->profile->display_name : $user->fullname,
                        'avatar' => @$user->photo ? $user->photo->file_url : null,
                        'thumbnail' => @$user->photo ? getThumbnail($user->photo->file_url, 350, 350) : null,
                    ];
                    session($session);
                }
                return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.update.success'))
                    ->with('type', 'success');
            } else {
                throw new GeneralException(__('strings.common.messages.update.failed'));
            }
        } else {
            throw new GeneralException(__('strings.common.messages.empty'));
        }
    }

    /**
     * Change password function
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     */
    public function save(Request $request)
    {
        if ($request->all()) {
            $key = Auth::id();
            $user = $this->usersRepository->getById($key);
            $check = Hash::check($request->input('oldpassword'), $user->password);
            if(!$check){
                return redirect()->back()->with('message', __('strings.common.user.old_pass_wrong'))
                    ->with('type', 'error');
            }

            Validator::make($request->all(), [
                'oldpassword' => 'required',
                'upassword' => 'required|confirmed|min:6|max:15',
                'upassword_confirmation' => 'required|min:6|max:15',
            ],[
                'oldpassword.required' => __('validation.required', ['field' => __('strings.common.user.oldpassword')]),
                'upassword.required' => __('validation.required', ['field' => __('strings.common.user.newpassword')]),
                'upassword_confirmation.required' => __('validation.required', ['field' => __('strings.common.user.password_retype')]),
                'upassword.confirmed' => __('validation.confirmed', ['field' => __('strings.common.user.newpassword')]),
                'upassword.min' => __('validation.min.string', ['field' => __('strings.common.user.newpassword')]),
                'upassword.max' => __('validation.max.string', ['field' => __('strings.common.user.newpassword')]),
                'upassword_confirmation.min' => __('validation.min.string', ['field' => __('strings.common.user.password_retype')]),
                'upassword_confirmation.max' => __('validation.max.string', ['field' => __('strings.common.user.password_retype')]),
            ])->validate();

            $data = $request->all();
            $data['password'] = Hash::make($request->input('upassword'));
            $data['updated_by'] = $key;
            $data['updated_at'] = Carbon::now();
            $result = $this->usersRepository->updateById($key,$data);
            if ($result) {
                return redirect()->route($this->getRoute('index'))->with('message', __('passwords.reset'))
                    ->with('type', 'success');
            } else {
                throw new GeneralException(__('strings.common.messages.update.failed'));
            }
        } else {
            throw new GeneralException(__('strings.common.messages.empty'));
        }
    }

    /**
     * @param string $prefix
     * @return string
     */
    public function getRoute($prefix = '*'): string
    {
        return $this->route.'.'.$prefix;
    }
}
