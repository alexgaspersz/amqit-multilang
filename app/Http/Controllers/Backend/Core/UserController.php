<?php
/**
 * Description: UserController.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     03/12/2018, modified: 03/12/2018 19:17
 * @copyright   Copyright (c) 2018.
 */

namespace App\Http\Controllers\Backend\Core;


use App\Exceptions\GeneralException;
use App\Http\Controllers\Api\CommonApiController;
use App\Http\Controllers\Api\UploadHandlerController;
use App\Http\Controllers\Controller;
use App\Models\Core\GroupsModel;
use App\Models\Core\UserProfileModel;
use App\Models\Core\UsersModel;
use App\Repositories\Core\GroupRepository;
use App\Repositories\Core\UserProfileRepository;
use App\Repositories\Core\UsersRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

/**
 * @property UsersModel model
 * @property GroupsModel group
 * @property CommonApiController api
 * @property UserProfileModel profile
 */
class UserController extends Controller
{
    private $route = "admin.system.users";
    private $template = "backend.core.users";

    /**
     * @var UsersRepository
     */
    private $repository;
    /**
     * @var UserProfileRepository
     */
    private $profileRepository;
    /**
     * @var GroupRepository
     */
    private $groupRepository;
    /**
     * @var UploadHandlerController
     */
    private $handlerController;

    /**
     * UserController constructor.
     * @param UsersRepository $repository
     * @param UserProfileRepository $profileRepository
     * @param GroupRepository $groupRepository
     * @param UploadHandlerController $handlerController
     */
    public function __construct(UsersRepository $repository,
                                UserProfileRepository $profileRepository,
                                GroupRepository $groupRepository,
                                UploadHandlerController $handlerController)
    {
        $this->repository = $repository;
        $this->profileRepository = $profileRepository;
        $this->groupRepository = $groupRepository;
        $this->handlerController = $handlerController;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        permission('is_read', $this->getRoute(), false, 'module');
        $data['title'] = __('strings.common.list') . ' ' . __('module.user.module');
        $data['route'] = $this->route;
        return view($this->template.'.index', $data);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function grid()
    {
        $dataArray = $this->repository->gridOnlyData([6]);
        $_data = [];
        foreach ($dataArray as $row) {
            $_data[] = [
                'secureId' => encode_id($row->user_id),
                'ids' => $row->user_id,
                'fullname' => $row->fullname,
                'email' => $row->email,
                'group' => $row->group->name,
                'is_login' => $row->is_online,
                'is_core' => $row->is_core,
                'last_login' => dateString($row->last_login, true, true),
                'status' => getActive($row->active)
            ];
        }
        return response()->json($_data);
    }

    /**
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update($id = null)
    {
        if($id){
            permission('is_update', $this->getRoute(), false, 'module');
            $keyId = decode_id($id);
            $data['user'] = $this->repository->getById($keyId);
            $data['title'] = __('strings.common.edit') . ' ' . __('module.user.module');
        } else {
            permission('is_create', $this->getRoute(), false, 'module');
            $data['user'] = null;
            $data['title'] = __('strings.common.add') . ' ' . __('module.user.module');
        }
        $data['secure_id'] = $id;
        $data['route'] = $this->route;
        return view($this->template.'.form', $data);
    }

    /**
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        permission('is_update', $this->getRoute(), false, 'module');
        $keyId = decode_id($id);
        $user = $this->repository->getById($keyId);
        $data['user'] = $user;
        $data['title'] = __('strings.common.user.profile') . ' ' . __('module.user.module');
        return view($this->template.'.view', $data);
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     * @throws GeneralException
     */
    public function store(Request $request)
    {
        if ($request->all()) {
            $keyId = decode_id($request->input('secure_id'));
            Validator::make($request->all(), [
                'fullname' => 'required',
                'username' => 'required|min:5|max:10|' . Rule::unique('tb_users')->ignore($keyId, 'user_id'),
                'email' => 'required|' . Rule::unique('tb_users')->ignore($keyId, 'user_id'),
                'group_id' => 'required'
            ],[
                'fullname.required' => __('validation.required',['field' => __('forms.user.fullname')]),
                'username.required' => __('validation.required',['field' => __('strings.common.user.username')]),
                'email.required' => __('validation.required',['field' => __('forms.user.email')]),
                'group_id.required' => __('validation.required',['field' => __('module.group.module')]),
                'username.unique' => __('validation.unique', ['field' => __('strings.common.user.username')]),
                'email.unique' => __('validation.unique', ['field' => __('forms.user.email')])
            ])->validate();

            $data = $request->all();
            if (!$request->input('secure_id')) {
                permission('is_create', $this->getRoute(), false, 'module');
                Validator::make($request->all(), [
                    'upassword' => 'required|confirmed|min:6|max:15',
                    'upassword_confirmation' => 'required|min:6|max:15',
                ],[
                    'upassword.required' => __('validation.required',['field' => __('strings.common.user.password')]),
                    'upassword_confirmation.required' => __('validation.required',['field' => __('strings.common.user.password_confirm')]),
                    'upassword.confirmed' => __('validation.confirmed',['field' => __('strings.common.user.password')]),
                    'upassword.min' => __('validation.min.string', ['field' => __('strings.common.user.password')]),
                    'upassword.max' => __('validation.max.string', ['field' => __('strings.common.user.password')]),
                    'upassword_confirmation.min' => __('validation.min.string', ['field' => __('strings.common.user.password_confirm')]),
                    'upassword_confirmation.max' => __('validation.max.string', ['field' => __('strings.common.user.password_confirm')]),
                ])->validate();
                $data['password'] = Hash::make($request->input('upassword'));
                $data['created_at'] = Carbon::now();
                $result = $this->repository->create($data);
                if ($result) {
                    $display = explode(' ', $request->input('fullname'));
                    $profile['user_id'] = $result->user_id;
                    $profile['display_name'] = $display[0];
                    $this->profileRepository->create($profile);
                    logActivity($request, __('strings.backend.logs.add',['val' => strtolower(__('module.user.module')).' '.$result->fullname]));
                    return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.save.success'))
                        ->with('type', 'success');
                } else {
                    throw new GeneralException(__('strings.common.messages.save.failed'));
                }
            } else {
                permission('is_update', $this->getRoute(), false, 'module');
                if ($request->input('upassword')) {
                    Validator::make($request->all(), [
                        'upassword' => 'required|confirmed|min:6|max:15',
                        'upassword_confirmation' => 'required|min:6|max:15',
                    ],[
                        'upassword.confirmed' => __('validation.confirmed',['field' => __('strings.common.user.password')]),
                        'upassword.min' => __('validation.min.string', ['field' => __('strings.common.user.password')]),
                        'upassword.max' => __('validation.max.string', ['field' => __('strings.common.user.password')]),
                        'upassword_confirmation.min' => __('validation.min.string', ['field' => __('strings.common.user.password_confirm')]),
                        'upassword_confirmation.max' => __('validation.max.string', ['field' => __('strings.common.user.password_confirm')]),
                    ])->validate();
                    $data['password'] = Hash::make($request->input('upassword'));
                }

                $data['updated_at'] = Carbon::now();
                $result = $this->repository->updateById($keyId,$data);
                if ($result) {
                    if (session('user_id') == $keyId) {
                        $user = $this->repository->getById($keyId);
                        $session = [
                            'fullname' => $user->fullname,
                            'email' => $user->email,
                            'group' => $user->group_id,
                            'group_name' => $user->group->name,
                            'alias' => $user->group->alias
                        ];
                        session($session);
                    }
                    logActivity($request, __('strings.backend.logs.edit',['val' => strtolower(__('module.user.module')).' '.$result->fullname]));
                    return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.update.success'))
                        ->with('type', 'success');
                } else {
                    throw new GeneralException(__('strings.common.messages.update.failed'));
                }
            }
        } else {
            throw new GeneralException(__('strings.common.messages.empty'));
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     * @throws \Exception
     */
    public function destroy(Request $request)
    {
        permission('is_delete', $this->getRoute(), false, 'module');
        if (!$request->input('secure_id')) {
            throw new GeneralException(__('strings.common.messages.empty'));
        }

        $id = decode_id($request->input('secure_id'));
        $data = $this->repository->getById($id);
        if ($data) {
            if ($data->delete()) {
                if ($data->avatar) $this->handlerController->fileRemove($data->avatar);
                logActivity($request, __('strings.backend.logs.delete',['val' => strtolower(__('module.user.module')).' '.$data->fullname]));
                return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.delete.success'))
                    ->with('type', 'success');
            } else {
                throw new GeneralException(__('strings.common.messages.delete.failed'));
            }
        } else {
            throw new GeneralException(__('strings.common.messages.not_found'));
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     * @throws \Exception
     */
    public function batch(Request $request)
    {
        permission('is_delete', $this->getRoute(), false, 'module');
        if (!$request->input('ids')) {
            throw new GeneralException(__('strings.common.messages.empty'));
        }

        $all = $this->repository->whereIn('user_id',$request->input('ids'))->get();
        $_images = [];
        foreach ($all as $row){
            if ($row->avatar) $_images[] = $row->avatar;
        }
        $this->handlerController->fileRemoveBatch($_images);
        $result = $this->repository->deleteMultipleById($request->input('ids'));
        if ($result) {
            logActivity($request, __('strings.backend.logs.batch',['val' => implode(',', $request->input('ids'))]));
            return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.delete.success'))
                ->with('type', 'success');
        } else {
            throw new GeneralException(__('strings.common.messages.delete.failed'));
        }
    }

    /**
     * @param string $prefix
     * @return string
     */
    public function getRoute($prefix = '*'): string
    {
        return $this->route.'.'.$prefix;
    }
}
