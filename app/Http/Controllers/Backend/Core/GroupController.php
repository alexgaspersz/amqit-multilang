<?php
/**
 * Description: GroupController.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     03/12/2018, modified: 03/12/2018 19:16
 * @copyright   Copyright (c) 2018.
 */

namespace App\Http\Controllers\Backend\Core;


use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use App\Models\Core\AccessModel;
use App\Repositories\Core\AccessRepository;
use App\Repositories\Core\GroupRepository;
use App\Repositories\Core\MenuRepository;
use App\Repositories\Core\UsersRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

/**
 * Class GroupController
 * @package App\Http\Controllers\Backend\Core
 */
class GroupController extends Controller
{
    private $route = 'admin.system.groups';
    private $template = "backend.core.groups";

    /**
     * @var UsersRepository
     */
    private $usersRepository;
    /**
     * @var GroupRepository
     */
    private $groupRepository;
    /**
     * @var MenuController
     */
    private $menuController;
    /**
     * @var AccessRepository
     */
    private $accessRepository;
    /**
     * @var MenuRepository
     */
    private $menuRepository;

    /**
     * GroupController constructor.
     *
     * @param UsersRepository $usersRepository
     * @param GroupRepository $groupRepository
     * @param MenuController $menuController
     * @param MenuRepository $menuRepository
     * @param AccessRepository $accessRepository
     */
    public function __construct(UsersRepository $usersRepository,
                                GroupRepository $groupRepository,
                                MenuController $menuController,
                                MenuRepository $menuRepository,
                                AccessRepository $accessRepository)
    {
        $this->usersRepository = $usersRepository;
        $this->groupRepository = $groupRepository;
        $this->menuController = $menuController;
        $this->accessRepository = $accessRepository;
        $this->menuRepository = $menuRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data['title'] = __('strings.common.list') . ' ' . __('module.group.module');
        $data['route'] = $this->route;
        return view($this->template.'.index', $data);
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function grid()
    {
        $dataArray = $this->groupRepository->whereNotIn('is_super', [1])->get();
        $_data = [];
        foreach ($dataArray as $row) {
            $_data[] = [
                'secureId' => encode_id($row->group_id),
                'ids' => $row->group_id,
                'name' => $row->name,
                'description' => $row->description,
                'role' => $row->allow_cms,
                'is_core' => $row->is_core,
                'status' => $row->status
            ];
        }
        return response()->json($_data);
    }

    /**
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update($id = null)
    {
        if($id){
            permission('is_update', $this->getRoute(), false, 'module');
            $keyId = decode_id($id);
            $data['item'] = $this->groupRepository->getById($keyId);
            $data['title'] = __('strings.common.edit') . ' ' . __('module.group.module');
        } else {
            permission('is_create', $this->getRoute(), false, 'module');
            $data['item'] = null;
            $data['title'] = __('strings.common.add') . ' ' . __('module.group.module');
        }
        $data['secure_id'] = $id;
        $data['route'] = $this->route;
        return view($this->template.'.form', $data);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function access($id)
    {
        permission('is_update', $this->getRoute(), false, 'module');
        $key = decode_id($id);
        $data = $this->groupRepository->getById($key);
        $data['id'] = $id;
        $data['menu'] = $this->menuController->getMenu($key,'adminsidebar');
        $data['title'] = __('module.group.access.title',['val' => $data->name]);
        $data['route'] = $this->route;
        return view($this->template.'.access', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     */
    public function store(Request $request)
    {

        if ($request->all()) {
            $keyId = decode_id($request->input('secure_id'));
            Validator::make($request->all(), [
                'name' => 'required|'.Rule::unique('tb_groups')->ignore($keyId, 'group_id')
            ],[
                'name.required' => __('validation.required',['field' => __('forms.user.group.name')]),
                'name.unique' => __('validation.unique',['data' => __('forms.user.group.name')]),
            ])->validate();

            $data = $request->all();
            $data['allow_cms'] = $request->has('allow_cms');
            $data['status'] = $request->has('status');
            $data['alias'] = slugify($request->input('name'));
            if (!$request->input('secure_id')) {
                permission('is_create', $this->getRoute(), false, 'module');
                $result = $this->groupRepository->create($data);
                if ($result) {
                    logActivity($request, __('strings.backend.logs.add',['val' => strtolower(__('module.group.module')).' '.$result->name]));
                    return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.save.success'))
                        ->with('type', 'success');
                } else {
                    throw new GeneralException(__('strings.common.messages.save.failed'));
                }
            } else {
                permission('is_update', $this->getRoute(), false, 'module');
                $result = $this->groupRepository->updateById($keyId, $data);
                if ($result) {
                    logActivity($request, __('strings.backend.logs.edit',['val' => strtolower(__('module.group.module')).' '.$result->name]));
                    return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.update.success'))
                        ->with('type', 'success');
                } else {
                    throw new GeneralException(__('strings.common.messages.update.failed'));
                }
            }
        } else {
            throw new GeneralException(__('strings.common.messages.empty'));
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     */
    public function save_permission(Request $request)
    {
        permission('is_update', $this->getRoute(), false, 'module');
        $id = decode_id($request->input('group_id'));
        $_read = [];
        if ($request->has('is_read')) {
            foreach ($request->input('is_read') as $val) {
                $_read[] = ['menu_id' => $val];
            }
        }

        $_create = [];
        if ($request->has('is_create')) {
            foreach ($request->input('is_create') as $val) {
                $_create[] = ['menu_id' => $val];
            }
        }

        $_update = [];
        if ($request->has('is_update')) {
            foreach ($request->input('is_update') as $val) {
                $_update[] = ['menu_id' => $val];
            }
        }

        $_delete = [];
        if ($request->has('is_delete')) {
            foreach ($request->input('is_delete') as $val) {
                $_delete[] = ['menu_id' => $val];
            }
        }

        $_approve = [];
        if ($request->has('is_approve')) {
            foreach ($request->input('is_approve') as $val) {
                $_approve[] = ['menu_id' => $val];
            }
        }

        $_download = [];
        if ($request->has('is_download')) {
            foreach ($request->input('is_download') as $val) {
                $_download[] = ['menu_id' => $val];
            }
        }

        $merged = array_merge($_read, $_create, $_update, $_delete, $_approve, $_download);
        $result = [];
        foreach ($merged as $key => $data) {
            $access = trimId($data['menu_id']);
            $module = $this->menuRepository->getById($access[1]);
            if (isset($result[$access[1]])) {
                $result[$access[1]][$access[0]] = 1;
            } else {
                $result[$access[1]] = ['menu_id' => intval($access[1]), $access[0] => 1, 'group_id' => intval($id), 'module' => $module->module];
            }
        }

        /**
         * Merge all privileges into json
         */
        $group = $this->groupRepository->getById(intval($id));
        if ($merged) {
            $current = $this->accessRepository->where('group_id',intval($id))->get();
            $insert = null;
            if ($current->count() > 0) {
                $deletedRows = $this->accessRepository->deleteWhere('group_id',intval($id));
                if ($deletedRows) {
                    foreach ($result as $val) {
                        $insert = $this->accessRepository->create($val);
                    }
                }
            } else {
                foreach ($result as $val) {
                    $insert = $this->accessRepository->create($val);
                }
            }

            if ($insert) {
                logActivity($request, __('strings.backend.logs.edit',['val' => strtolower(__('module.group.access.title',['val' => $group->name]))]));
                return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.save.success'))
                    ->with('type', 'success');
            } else {
                throw new GeneralException(__('strings.common.messages.save.failed'));
            }
        } else {
            $current = $this->accessRepository->where('group_id', intval($id))->count();
            if ($current > 0) {
                $this->accessRepository->deleteWhere('group_id',intval($id));
                logActivity($request, __('strings.backend.logs.delete',['val' => strtolower(__('module.access_title',['val' => $group->name]))]));
                return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.update.success'))
                    ->with('type', 'success');
            } else {
                throw new GeneralException(__('module.group.access.empty'));
            }
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws Exception
     */
    public function destroy(Request $request)
    {
        permission('is_delete', $this->getRoute(), false, 'module');
        if (!$request->input('secure_id')) {
            throw new GeneralException(__('strings.common.messages.empty'));
        }

        $id = decode_id($request->input('secure_id'));
        $data = $this->groupRepository->getById($id);
        if ($data) {
            $child = $this->usersRepository->where('group_id', $id)->count();
            if ($child > 0) {
                throw new GeneralException('Failed to delete this group. ' . $child . ' user(s) assigned.');
            }
            if ($data->delete()) {
                logActivity($request, __('strings.backend.logs.delete', ['val' => strtolower(__('module.group.module')) . ' ' . $data->name]));
                return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.delete.success'))
                    ->with('type', 'success');
            } else {
                throw new GeneralException(__('strings.common.messages.delete.failed'));
            }
        } else {
            throw new GeneralException(__('strings.common.messages.not_found'));
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     */
    public function batch(Request $request) {
        permission('is_delete', $this->getRoute(), false, 'module');
        if (!$request->input('ids')) {
            throw new GeneralException(__('strings.common.messages.empty'));
        }

        $result = $this->groupRepository->deleteMultipleById($request->input('ids'));
        if ($result) {
            logActivity($request, __('strings.backend.logs.batch',['val' => implode(',', $request->input('ids'))]));
            return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.delete.success'))
                ->with('type', 'success');
        } else {
            throw new GeneralException(__('strings.common.messages.delete.failed'));
        }
    }

    /**
     * @param $access
     * @param $key
     * @param bool $view
     * @param string $method
     * @return bool
     */
    public static function permission($access, $key, $view = false, $method = 'menu')
    {
        if (session('superuser') == false) {
            if($method == 'module'){
                $query = AccessModel::where($access, 1)->where('module', 'like', $key.'%')->where('group_id', session('group'))->count();
            } else {
                $query = AccessModel::where($access, 1)->where('menu_id', $key)->where('group_id', session('group'))->count();
            }

            if ($query > 0) {
                if ($view) {
                    return true;
                }
            } else if (session('administrator') == true) {
                if ($view) {
                    return false;
                }
            } else {
                if ($view) {
                    return false;
                } else {
                    abort(403);
                }
            }
        } else {
            return true;
        }
    }

    /**
     * @param $access
     * @param $key
     * @return bool
     */
    public static function privileged($access, $key)
    {
        if (session('superuser') == false) {
            $query = AccessModel::where($access, 1)->where('module', $key)->where('group_id', session('group'))->count();
            if ($query > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    /**
     * @param string $prefix
     * @return string
     */
    public function getRoute($prefix = '*'): string
    {
        return $this->route.'.'.$prefix;
    }
}
