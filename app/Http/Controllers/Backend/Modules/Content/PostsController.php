<?php
/**
 * Description: ArticlesController.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     28/12/2017, modified: 28/12/2017 08:03
 * @copyright   Copyright (c) 2018.
 */

namespace App\Http\Controllers\Backend\Modules\Content;


use App\Exceptions\GeneralException;
use App\Http\Controllers\Api\UploadHandlerController;
use App\Http\Controllers\Controller;
use App\Repositories\Modules\Content\CategoryRepository;
use App\Repositories\Modules\Content\PostRepository;
use App\Repositories\Modules\Content\PostTranslationRepository;
use App\Repositories\Modules\Content\TagsRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Mcamara\LaravelLocalization\LaravelLocalization;

/**
 * Class       PostsController
 * @property string route
 * @property string template
 * @package App\Http\Controllers\Backend\Modules\Content
 * $
 *
 */
class PostsController extends Controller
{
    private $route = "admin.content.posts";
    private $template = "backend.modules.contents.posts";
    /**
     * @var PostRepository
     */
    private $repository;
    /**
     * @var PostTranslationRepository
     */
    private $translationRepository;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var TagsRepository
     */
    private $tagsRepository;
    /**
     * @var UploadHandlerController
     */
    private $handlerController;
    /**
     * @var LaravelLocalization
     */
    private $locale;

    /**
     * PostsController constructor.
     * @param PostRepository $repository
     * @param PostTranslationRepository $translationRepository
     * @param CategoryRepository $categoryRepository
     * @param TagsRepository $tagsRepository
     * @param UploadHandlerController $handlerController
     * @param LaravelLocalization $locale
     */
    public function __construct(PostRepository $repository,
                                PostTranslationRepository $translationRepository,
                                CategoryRepository $categoryRepository,
                                TagsRepository $tagsRepository,
                                UploadHandlerController $handlerController,
                                LaravelLocalization $locale)
    {
        $this->repository = $repository;
        $this->translationRepository = $translationRepository;
        $this->categoryRepository = $categoryRepository;
        $this->tagsRepository = $tagsRepository;
        $this->handlerController = $handlerController;
        $this->locale = $locale;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        permission('is_read', $this->getRoute(), false, 'module');
        $data['route'] = $this->route;
        $data['title'] = __('strings.common.list') . ' ' . __('module.post.module');
        return view($this->template.'.index', $data);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function grid()
    {
        $dataArray = $this->repository->getTranslation();
        $_data = [];
        foreach ($dataArray as $row) {
            $_data[] = [
                'secureId' => encode_id($row->id),
                'ids' => $row->id,
                'title' => text_limit($row->single_translate->title, 50),
                'category' => $row->category->single_translate->name,
                'author' => $row->getAuthorNameAttribute(),
                'publish_at' => dateString($row->publish_at),
                'status' => published($row->status)
            ];
        }
        return response()->json($_data);
    }

    /**
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Mcamara\LaravelLocalization\Exceptions\SupportedLocalesNotDefined
     */
    public function update($id = null)
    {
        if($id){
            permission('is_update', $this->getRoute(), false, 'module');
            $keyId = decode_id($id);
            $post = $this->repository->getById($keyId);
            $data['item'] = $post;
            $data['title'] = __('strings.common.edit') . ' ' . __('module.post.module');
        } else {
            permission('is_create', $this->getRoute(), false, 'module');
            $data['item'] = null;
            $data['title'] = __('strings.common.add') . ' ' . __('module.post.module');
        };
        $data['categories'] = $this->categoryRepository->getTranslation();
        $data['secure_id'] = $id;
        $data['route'] = $this->route;
        $data['localization'] = $this->locale->getSupportedLocales();
        return view($this->template.'.form', $data);
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     * @throws \Exception
     * @throws \Mcamara\LaravelLocalization\Exceptions\SupportedLocalesNotDefined
     */
    public function store(Request $request)
    {
        if ($request->all()) {
            $keyId = decode_id($request->input('secure_id'));
            $validator = Validator::make($request->all(), [
                'title_'.defaultLang() => 'required',
                'intro_'.defaultLang() => 'required',
                'content_'.defaultLang() => 'required',
                'categories' => 'sometimes|required',
                'slug' => 'required|' . Rule::unique('ms_post')->ignore($keyId)
            ], [
                'title_'.defaultLang().'.required' => __('validation.required',['field' => __('strings.common.title')]),
                'intro_'.defaultLang().'.required' => __('validation.required',['field' => __('strings.common.intro')]),
                'content_' . defaultLang() . '.required' => __('validation.required', ['field' => __('strings.common.category')]),
                'categories.required' => __('validation.required', ['field' => __('categ')]),
                'slug.required' => __('validation.required',['field' => __('strings.common.slug')]),
                'slug.unique' => __('validation.unique',['data' => __('strings.common.slug')])
            ]);

            if ($validator->fails() && $request->input('assetId') != null) {
                $this->handlerController->fileRemove($request->input('assetId'));
                return redirect()->route($this->getRoute('update'),['id' => $request->input('secure_id')])
                    ->withErrors($validator)
                    ->withInput();
            }

            $insert = [];
            $data = $request->all();
            $data['slug'] = $request->input('slug') ? $request->input('slug') : slugify($request->input('title_' . defaultLang()));
            $data['author_id'] = $request->input('author_id') ? $request->input('author_id') : Auth::id();
            $data['featured'] = $request->has('featured');
            $data['allow_guest'] = $request->has('allow_guest');
            $data['allow_comment'] = $request->has('allow_comment');
            if (!$request->input('secure_id')) {
                permission('is_create', $this->getRoute(), false, 'module');
                $data['created_by'] = Auth::id();
                $result = $this->repository->create($data);
                if ($result) {
                    $caption = $this->repository->getById($result->id);
                    if ($request->has('caption')) {
                        $featuredImage = $caption->featuredImages();
                        if ($featuredImage->count()) {
                            $featuredImage->update(['title' => $request->caption]);
                        }
                    }
                    foreach ($this->locale->getSupportedLocales() as $locale => $val) {
                        $insert[$locale] = [
                            'entry_id' => $result->id,
                            'title' => $request->input('title_' . $locale),
                            'meta' => $request->input('meta_' . $locale),
                            'intro' => $request->input('intro_' . $locale),
                            'description' => $request->input('description_' . $locale),
                            'content' => $request->input('content_' . $locale),
                            'locale' => $locale,
                            'created_at' => Carbon::now()
                        ];
                        $this->translationRepository->create($insert[$locale]);
                    }
                    logActivity($request, __('strings.backend.logs.add',['val' => strtolower(__('module.post.module')).' '.$result->slug]));
                    return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.save.success'))
                        ->with('type', 'success');
                } else {
                    throw new GeneralException(__('strings.common.messages.save.failed'));
                }
            } else {
                permission('is_update', $this->getRoute(), false, 'module');
                $data['updated_by'] = Auth::id();
                $result = $this->repository->updateById($keyId, $data);
                if ($result) {
                    $translation = $this->repository->getById($keyId);
                    if ($request->has('caption')) {
                        $featuredImage = $translation->featuredImages();
                        if ($featuredImage->count()) {
                            $featuredImage->update(['title' => $request->caption]);
                        }
                    }
                    foreach ($this->locale->getSupportedLocales() as $locale => $val) {
                        $insert[$locale] = [
                            'entry_id' => $keyId,
                            'title' => $request->input('title_' . $locale),
                            'meta' => $request->input('meta_' . $locale),
                            'intro' => $request->input('intro_' . $locale),
                            'description' => $request->input('description_' . $locale),
                            'content' => $request->input('content_' . $locale),
                            'locale' => $locale
                        ];
                        $existing = $translation->translation()->where('locale', '=', $locale);
                        if($existing->count() > 0){
                            $insert[$locale]['updated_at'] = Carbon::now();
                            $existing->update($insert[$locale]);
                        } else {
                            $insert[$locale]['created_at'] = Carbon::now();
                            $this->translationRepository->create($insert[$locale]);
                        }
                    }
                    logActivity($request, __('strings.backend.logs.edit',['val' => strtolower(__('module.post.module')).' '.$result->slug]));
                    return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.update.success'))
                        ->with('type', 'success');
                } else {
                    throw new GeneralException(__('strings.common.messages.update.failed'));
                }
            }
        } else {
            throw new GeneralException(__('strings.common.messages.empty'));
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     * @throws \Exception
     */
    public function destroy(Request $request)
    {
        permission('is_delete', $this->getRoute(), false, 'module');
        if (!$request->input('secure_id')) {
            throw new GeneralException(__('strings.common.messages.empty'));
        }

        $id = decode_id($request->input('secure_id'));
        $data = $this->repository->getById($id);
        if ($data) {
            if ($data->delete()) {
                if ($data->images) $this->handlerController->fileRemove($data->images);
                logActivity($request, __('strings.backend.logs.delete',['val' => strtolower(__('module.post.module')).' '.$data->slug]));
                return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.delete.success'))
                    ->with('type', 'success');
            } else {
                throw new GeneralException(__('strings.common.messages.delete.failed'));
            }
        } else {
            throw new GeneralException(__('strings.common.messages.not_found'));
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function batch(Request $request)
    {
        permission('is_delete', $this->getRoute(), false, 'module');
        if (!$request->input('ids')) {
            throw new GeneralException(__('strings.common.messages.empty'));
        }

        $all = $this->repository->whereIn('id',$request->input('ids'))->get();
        $_images = [];
        foreach ($all as $row){
            $_images[] = $row->images;
        }
        $result = $this->repository->deleteMultipleById($request->input('ids'));
        if ($result) {
            $this->handlerController->fileRemoveBatch($_images);
            logActivity($request, __('strings.backend.logs.batch',['val' => implode(',', $request->input('ids'))]));
            return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.delete.success'))
                ->with('type', 'success');
        } else {
            throw new GeneralException(__('strings.common.messages.delete.failed'));
        }
    }

    /**
     * @param string $prefix
     * @return string
     */
    public function getRoute($prefix = '*'): string
    {
        return $this->route.'.'.$prefix;
    }
}
