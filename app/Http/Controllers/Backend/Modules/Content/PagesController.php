<?php
/**
 * Description: PagesController.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     29/01/2018, modified: 29/01/2018 21:37
 * @copyright   Copyright (c) 2018.
 */

namespace App\Http\Controllers\Backend\Modules\Content;


use App\Exceptions\GeneralException;
use App\Http\Controllers\Api\UploadHandlerController;
use App\Http\Controllers\Controller;
use App\Repositories\Handler\UploadHandlerRepository;
use App\Repositories\Modules\Content\PageRepository;
use App\Repositories\Modules\Content\PageTranslationRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Mcamara\LaravelLocalization\LaravelLocalization;

/**
 * Class PagesController
 * @property string route
 * @property string template
 * @package App\Http\Controllers\Backend\Modules\Content
 */
class PagesController extends Controller
{
    private $route = "admin.content.pages";
    private $template = "backend.modules.contents.pages";
    /**
     * @var PageRepository
     */
    private $repository;
    /**
     * @var PageTranslationRepository
     */
    private $translationRepository;
    /**
     * @var LaravelLocalization
     */
    private $locale;
    /**
     * @var UploadHandlerController
     */
    private $handlerController;
    /**
     * @var UploadHandlerRepository
     */
    private $handlerRepository;

    /**
     * PagesController constructor.
     *
     * @param PageRepository            $repository
     * @param PageTranslationRepository $translationRepository
     * @param UploadHandlerController   $handlerController
     * @param UploadHandlerRepository   $handlerRepository
     * @param LaravelLocalization       $locale
     */
    public function __construct(PageRepository $repository,
                                PageTranslationRepository $translationRepository,
                                UploadHandlerController $handlerController,
                                UploadHandlerRepository $handlerRepository,
                                LaravelLocalization $locale)
    {
        $this->repository = $repository;
        $this->translationRepository = $translationRepository;
        $this->handlerController = $handlerController;
        $this->handlerRepository = $handlerRepository;
        $this->locale = $locale;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        permission('is_read', $this->getRoute(), false, 'module');
        $data['route'] = $this->route;
        $data['title'] = __('strings.common.list') . ' ' . __('module.pages.module');
        return view($this->template.'.index', $data);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function grid()
    {
        $dataArray = $this->repository->getTranslation();
        $_data = [];
        foreach ($dataArray as $row) {
            $_data[] = [
                'secureId' => encode_id($row->id),
                'ids' => $row->id,
                'page_title' => $row->single_translate->page_title,
                'status' => $row->status
            ];
        }
        return response()->json($_data);
    }

    /**
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Mcamara\LaravelLocalization\Exceptions\SupportedLocalesNotDefined
     */
    public function update($id = null)
    {
        if($id){
            permission('is_update', $this->getRoute(), false, 'module');
            $keyId = decode_id($id);
            $data['item'] = $this->repository->getById($keyId);
            $data['title'] = __('strings.common.edit') . ' ' . __('module.pages.module');
        } else {
            permission('is_create', $this->getRoute(), false, 'module');
            $data['item'] = null;
            $data['title'] = __('strings.common.add') . ' ' . __('module.pages.module');
        }
        $data['secure_id'] = $id;
        $data['route'] = $this->route;
        $data['localization'] = $this->locale->getSupportedLocales();
        return view($this->template.'.form', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     * @throws \Mcamara\LaravelLocalization\Exceptions\SupportedLocalesNotDefined
     */
    public function store(Request $request)
    {
        if ($request->all()) {
            $keyId = decode_id($request->input('secure_id'));
            Validator::make($request->all(), [
                'page_title_'.defaultLang() => 'required',
                'slug' => 'required|' . Rule::unique('ms_pages')->ignore($keyId),
            ], [
                'page_title_'.defaultLang().'.required' => __('validation.required',['attribute' => __('strings.common.title')]),
                'slug.required' => __('validation.required',['attribute' => __('strings.common.slug')]),
                'slug.unique' => __('validation.unique',['data' => __('strings.common.slug')]),
            ])->validate();

            $insert = [];
            $data = $request->all();

            $slug = $request->input('slug') ? $request->input('slug') : slugify($request->input('page_title_' . defaultLang()));
            $data['status'] = $request->has('status');
            $data['visible'] = $request->has('visible');
            $data['slug'] = $slug;

            if ($request->hasFile('file_pdf')) {
                $pdf = $request->file('file_pdf');
                $folder = 'documents';
                $destinationPath = public_path(uploadFolder() . $folder);
                $current = Carbon::now()->format('Y/m/d');
                $path = $destinationPath . '/' . $current;
                $baseFolder = $folder . '/' . $current;

                $pdfName = $pdf->getClientOriginalName();
                $pdfMime = $pdf->getClientMimeType();
                $pdfExtension = $pdf->getClientOriginalExtension();
                $pdfSize = $pdf->getSize();
                $newFilename = uniqid($slug . '_') . '.' . $pdfExtension;

                if (!File::exists($path)) {
                    File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
                }

                $uploaded = $pdf->move($path, $newFilename);
                if ($uploaded) {
                    $insert = [
                        'category' => 'PAGE',
                        'title' => $pdfName,
                        'base_folder' => $baseFolder,
                        'base_path' => $newFilename,
                        'mime_type' => $pdfMime,
                        'extension' => $pdfExtension,
                        'size' => $pdfSize,
                        'created_by' => Auth::id()
                    ];
                    $insert['created_by'] = Auth::id();
                    $asset = $this->handlerRepository->create($insert);
                    $data['attachment'] = $asset->assetId;
                }
            }

            if (!$request->input('secure_id')) {
                permission('is_create', $this->getRoute(), false, 'module');
                $data['created_by'] = Auth::id();
                $result = $this->repository->create($data);
                if ($result) {
                    foreach ($this->locale->getSupportedLocales() as $locale => $val) {
                        $insert[$locale] = [
                            'page_id' => $result->id,
                            'page_title' => $request->input('page_title_' . $locale),
                            'page_meta' => $request->input('page_meta_' . $locale),
                            'page_desc' => $request->input('page_desc_' . $locale),
                            'content' => $request->input('content_' . $locale),
                            'locale' => $locale,
                            'created_at' => Carbon::now()
                        ];
                        $this->translationRepository->create($insert[$locale]);
                    }
                    logActivity($request, __('strings.backend.logs.add',['val' => strtolower(__('module.pages.module')) .' ' . $result->slug]));
                    return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.save.success'))
                        ->with('type', 'success');
                } else {
                    throw new GeneralException(__('strings.common.messages.save.failed'));
                }
            } else {
                permission('is_update', $this->getRoute(), false, 'module');
                $data['updated_by'] = Auth::id();
                $result = $this->repository->updateById($keyId, $data);
                if ($result) {
                    $translation = $this->repository->getById($keyId);
                    foreach ($this->locale->getSupportedLocales() as $locale => $val) {
                        $insert[$locale] = [
                            'page_id' => $keyId,
                            'page_title' => $request->input('page_title_' . $locale),
                            'page_meta' => $request->input('page_meta_' . $locale),
                            'page_desc' => $request->input('page_desc_' . $locale),
                            'content' => $request->input('content_' . $locale),
                            'locale' => $locale,
                        ];
                        $existing = $translation->translation()->where('locale', '=', $locale);
                        if($existing->count() > 0){
                            $insert[$locale]['updated_at'] = Carbon::now();
                            $existing->update($insert[$locale]);
                        } else {
                            $insert[$locale]['created_at'] = Carbon::now();
                            $this->translationRepository->create($insert[$locale]);
                        }
                    }
                    logActivity($request, __('strings.backend.logs.edit',['val' => strtolower(__('module.pages.module')) .' ' . $result->slug]));
                    return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.update.success'))
                        ->with('type', 'success');
                } else {
                    throw new GeneralException(__('strings.common.messages.update.failed'));
                }
            }
        } else {
            throw new GeneralException(__('strings.common.messages.empty'));
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     * @throws \Exception
     */
    public function destroy(Request $request)
    {
        permission('is_delete', $this->getRoute(), false, 'module');
        if (!$request->input('secure_id')) {
            throw new GeneralException(__('strings.common.messages.empty'));
        }

        $id = decode_id($request->input('secure_id'));
        $data = $this->repository->getById($id);
        if ($data) {
            if ($data->delete()) {
                if ($data->bg) $this->handlerController->fileRemove($data->bg);
                logActivity($request, __('strings.backend.logs.delete',['val' => strtolower(__('module.pages.module')) .' ' . $data->slug]));
                return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.delete.success'))
                    ->with('type', 'success');
            } else {
                throw new GeneralException(__('strings.common.messages.delete.failed'));
            }
        } else {
            throw new GeneralException(__('strings.common.messages.not_found'));
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     * @throws \Exception
     */
    public function batch(Request $request)
    {
        permission('is_delete', $this->getRoute(), false, 'module');
        if (!$request->input('ids')) {
            throw new GeneralException(__('strings.common.messages.empty'));
        }

        $all = $this->repository->whereIn('id',$request->input('ids'))->get();
        $_images = [];
        foreach ($all as $row){
            $_images[] = $row->bg;
        }
        $this->handlerController->fileRemoveBatch($_images);
        $result = $this->repository->deleteMultipleById($request->input('ids'));
        if ($result) {
            logActivity($request, __('strings.backend.logs.batch',['val' => implode(',', $request->input('ids'))]));
            return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.delete.success'))
                ->with('type', 'success');
        } else {
            throw new GeneralException(__('strings.common.messages.delete.failed'));
        }
    }

    /**
     * @param string $prefix
     * @return string
     */
    public function getRoute($prefix = '*'): string
    {
        return $this->route.'.'.$prefix;
    }
}
