<?php
/**
 * Description: TagsController.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     28/12/2017, modified: 28/12/2017 08:03
 * @copyright   Copyright (c) 2018.
 */

namespace App\Http\Controllers\Backend\Modules\Content;


use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use App\Repositories\Modules\Content\TagsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

/**
 * Class       TagsController
 * @property string route
 * @property string template
 * @package App\Http\Controllers\Backend\Modules\Content
 * $
 *
 */
class TagsController extends Controller
{
    private $route = "admin.content.tags";
    private $template = "backend.modules.contents.tags";
    /**
     * @var TagsRepository
     */
    private $repository;

    /**
     * TagsController constructor.
     * @param TagsRepository $repository
     */
    public function __construct(TagsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        permission('is_read', $this->getRoute(), false, 'module');
        $data['route'] = $this->route;
        $data['title'] = __('strings.common.list') . ' ' . __('module.tags.module');
        return view($this->template.'.index', $data);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function grid()
    {
        $dataArray = $this->repository->get();
        $_data = [];
        foreach ($dataArray as $row) {
            $_data[] = [
                'secureId' => encode_id($row->id),
                'ids' => $row->id,
                'tag' => $row->post_tag,
                'status' => $row->status
            ];
        }
        return response()->json($_data);
    }

    /**
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update($id = null)
    {
        if($id){
            permission('is_update', $this->getRoute(), false, 'module');
            $keyId = decode_id($id);
            $data['item'] = $this->repository->getById($keyId);
            $data['title'] = __('strings.common.edit') . ' ' . __('module.tags.module');
        } else {
            permission('is_create', $this->getRoute(), false, 'module');
            $data['item'] = null;
            $data['title'] = __('strings.common.add') . ' ' . __('module.tags.module');
        }
        $data['secure_id'] = $id;
        $data['route'] = $this->route;
        return view($this->template.'.form', $data);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function search()
    {
        $data = $this->repository->limit(10);
        $results = [];
        foreach ($data as $val) {
            $results[] = $val->post_tag;
        }
        return response()->json($results);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     */
    public function store(Request $request)
    {
        if ($request->all()) {
            $keyId = decode_id($request->input('secure_id'));
            Validator::make($request->all(), [
                'post_tag' => 'required|' . Rule::unique('ms_post_tags')->ignore($keyId),
            ], [
                'post_tag.required' => __('validation.required',['field' => __('module.tags.module')]),
                'post_tag.unique' => __('validation.unique',['data' => __('module.tags.module')]),
            ])->validate();

            $data = $request->all();
            if (!$request->input('secure_id')) {
                permission('is_create', $this->getRoute(), false, 'module');
                $data['created_by'] = Auth::id();
                $result = $this->repository->create($data);
                if ($result) {
                    logActivity($request, __('strings.backend.logs.add',['val' => strtolower(__('module.tags.module')) .' ' . $result->post_tag]));
                    return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.save.success'))
                        ->with('type', 'success');
                } else {
                    throw new GeneralException(__('strings.common.messages.save.failed'));
                }
            } else {
                permission('is_update', $this->getRoute(), false, 'module');
                $data['updated_by'] = Auth::id();
                $result = $this->repository->updateById($keyId, $data);
                if ($result) {
                    logActivity($request, __('strings.backend.logs.edit',[strtolower(__('module.tags.module')) .' ' . $result->post_tag]));
                    return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.update.success'))
                        ->with('type', 'success');
                } else {
                    throw new GeneralException(__('strings.common.messages.update.failed'));
                }
            }
        } else {
            throw new GeneralException(__('strings.common.messages.empty'));
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     * @throws \Exception
     */
    public function destroy(Request $request)
    {
        permission('is_delete', $this->getRoute(), false, 'module');
        if (!$request->input('secure_id')) {
            throw new GeneralException(__('strings.common.messages.empty'));
        }

        $id = decode_id($request->input('secure_id'));
        $data = $this->repository->getById($id);
        if ($data) {
            if ($data->delete()) {
                logActivity($request, __('strings.backend.logs.delete',['val' => strtolower(__('module.tags.module')) .' ' . $data->post_tag]));
                return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.delete.success'))
                    ->with('type', 'success');
            } else {
                throw new GeneralException(__('strings.common.messages.delete.failed'));
            }
        } else {
            throw new GeneralException(__('strings.common.messages.not_found'));
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     */
    public function batch(Request $request)
    {
        permission('is_delete', $this->getRoute(), false, 'module');
        if (!$request->input('ids')) {
            throw new GeneralException(__('strings.common.messages.empty'));
        }

        $result = $this->repository->deleteMultipleById($request->input('ids'));
        if ($result) {
            logActivity($request, __('strings.backend.logs.batch',['val' => implode(',', $request->input('ids'))]));
            return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.delete.success'))
                ->with('type', 'success');
        } else {
            throw new GeneralException(__('strings.common.messages.delete.failed'));
        }
    }

    /**
     * @param string $prefix
     * @return string
     */
    public function getRoute($prefix = '*'): string
    {
        return $this->route.'.'.$prefix;
    }
}
