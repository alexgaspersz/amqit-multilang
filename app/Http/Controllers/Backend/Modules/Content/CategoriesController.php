<?php
/**
 * Description: CategoryController.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     28/12/2017, modified: 28/12/2017 07:59
 * @copyright   Copyright (c) 2018.
 */

namespace App\Http\Controllers\Backend\Modules\Content;


use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use App\Repositories\Modules\Content\CategoryRepository;
use App\Repositories\Modules\Content\CategoryTranslationRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Mcamara\LaravelLocalization\LaravelLocalization;

/**
 * Class CategoriesController
 * @property string route
 * @property string template
 * @package App\Http\Controllers\Backend\Modules\Content
 */
class CategoriesController extends Controller
{
    private $route = "admin.content.categories";
    private $template = "backend.modules.contents.categories";
    /**
     * @var CategoryRepository
     */
    private $repository;
    /**
     * @var CategoryTranslationRepository
     */
    private $translationRepository;
    /**
     * @var LaravelLocalization
     */
    private $locale;

    /**
     * CategoriesController constructor.
     * @param CategoryRepository $repository
     * @param CategoryTranslationRepository $translationRepository
     * @param LaravelLocalization $locale
     */
    public function __construct(CategoryRepository $repository,
                                CategoryTranslationRepository $translationRepository,
                                LaravelLocalization $locale)
    {
        $this->locale = $locale;
        $this->repository = $repository;
        $this->translationRepository = $translationRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        permission('is_read', $this->getRoute(), false, 'module');
        $data['route'] = $this->route;
        $data['title'] = __('strings.common.list') . ' ' . __('module.category.module');
        return view($this->template.'.index', $data);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function grid()
    {
        $dataArray = $this->repository->getTranslation();
        $_data = [];
        foreach ($dataArray as $row) {
            $_data[] = [
                'secureId' => encode_id($row->id),
                'ids' => $row->id,
                'name' => $row->single_translate->name,
                'description' => $row->single_translate->description,
                'slug' => $row->slug,
                'status' => $row->status
            ];
        }
        return response()->json($_data);
    }

    /**
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Mcamara\LaravelLocalization\Exceptions\SupportedLocalesNotDefined
     */
    public function update($id = null)
    {
        if($id){
            permission('is_update', $this->getRoute(), false, 'module');
            $keyId = decode_id($id);
            $data['item'] = $this->repository->getById($keyId);
            $data['title'] = __('strings.common.edit') . ' ' . __('module.category.module');
        } else {
            permission('is_create', $this->getRoute(), false, 'module');
            $data['item'] = null;
            $data['title'] = __('strings.common.add') . ' ' . __('module.category.module');
        }
        $data['secure_id'] = $id;
        $data['route'] = $this->route;
        $data['localization'] = $this->locale->getSupportedLocales();
        return view($this->template.'.form', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     * @throws \Mcamara\LaravelLocalization\Exceptions\SupportedLocalesNotDefined
     */
    public function store(Request $request)
    {
        if ($request->all()) {
            $keyId = decode_id($request->input('secure_id'));
            Validator::make($request->all(), [
                'name_'.defaultLang() => 'required',
                'slug' => 'required|' . Rule::unique('ms_category')->ignore($keyId),
            ], [
                'name_'.defaultLang().'.required' => __('validation.required',['field' => __('strings.common.category')]),
                'slug.required' => __('validation.required',['field' => __('strings.common.slug')]),
                'slug.unique' => __('validation.unique',['data' => __('strings.common.slug')]),
            ])->validate();

            $insert = [];
            $data = $request->all();
            $data['slug'] = $request->input('slug') ? $request->input('slug') : slugify($request->input('name_' . defaultLang()));
            $data['status'] = $request->has('status');
            if (!$request->input('secure_id')) {
                permission('is_create', $this->getRoute(), false, 'module');
                $data['created_by'] = Auth::id();
                $result = $this->repository->create($data);
                if ($result) {
                    foreach ($this->locale->getSupportedLocales() as $locale => $val) {
                        $insert[$locale] = [
                            'category_id' => $result->id,
                            'name' => $request->input('name_' . $locale),
                            'description' => $request->input('description_' . $locale),
                            'locale' => $locale,
                            'created_at' => Carbon::now()
                        ];
                        $this->translationRepository->create($insert[$locale]);
                    }
                    logActivity($request, __('strings.backend.logs.add',['val' => strtolower(__('module.category.module')) .' ' . $result->slug]));
                    return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.save.success'))
                        ->with('type', 'success');
                } else {
                    throw new GeneralException(__('strings.common.messages.save.failed'));
                }
            } else {
                permission('is_update', $this->getRoute(), false, 'module');
                $data['updated_by'] = Auth::id();
                $result = $this->repository->updateById($keyId, $data);
                if ($result) {
                    $translation = $this->repository->getById($keyId);
                    foreach ($this->locale->getSupportedLocales() as $locale => $val) {
                        $insert[$locale] = [
                            'category_id' => $keyId,
                            'name' => $request->input('name_' . $locale),
                            'description' => $request->input('description_' . $locale),
                            'locale' => $locale
                        ];
                        $existing = $translation->translation()->where('locale', '=', $locale);
                        if($existing->count() > 0){
                            $insert[$locale]['updated_at'] = Carbon::now();
                            $existing->update($insert[$locale]);
                        } else {
                            $insert[$locale]['created_at'] = Carbon::now();
                            $this->translationRepository->create($insert[$locale]);
                        }
                    }
                    logActivity($request, __('strings.backend.logs.edit',['val' => strtolower(__('module.category.module')) .' ' . $result->slug]));
                    return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.update.success'))
                        ->with('type', 'success');
                } else {
                    throw new GeneralException(__('strings.common.messages.update.failed'));
                }
            }
        } else {
            throw new GeneralException(__('strings.common.messages.empty'));
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     */
    public function destroy(Request $request)
    {
        permission('is_delete', $this->getRoute(), false, 'module');
        if (!$request->input('secure_id')) {
            throw new GeneralException(__('strings.common.messages.empty'));
        }

        $id = decode_id($request->input('secure_id'));
        $data = $this->repository->getById($id);
        if ($data) {
            if ($data->delete()) {
                logActivity($request, __('strings.backend.logs.delete',['val' => strtolower(__('module.category.module')) .' ' . $data->slug]));
                return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.delete.success'))
                    ->with('type', 'success');
            } else {
                throw new GeneralException(__('strings.common.messages.delete.failed'));
            }
        } else {
            throw new GeneralException(__('strings.common.messages.not_found'));
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     */
    public function batch(Request $request)
    {
        permission('is_delete', $this->getRoute(), false, 'module');
        if (!$request->input('ids')) {
            throw new GeneralException(__('strings.common.messages.empty'));
        }

        $result = $this->repository->deleteMultipleById($request->input('ids'));
        if ($result) {
            logActivity($request, __('strings.backend.logs.batch',['val' => implode(',', $request->input('ids'))]));
            return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.delete.success'))
                ->with('type', 'success');
        } else {
            throw new GeneralException(__('strings.common.messages.delete.failed'));
        }
    }

    /**
     * @param string $prefix
     * @return string
     */
    public function getRoute($prefix = '*'): string
    {
        return $this->route.'.'.$prefix;
    }
}
