<?php
/**
 * Description: SliderController.php generated by PhpStorm.
 * @project     web-sekolah
 * @package     App\Http\Controllers\Backend\Modules\Extension
 * @author      alex
 * @created     2020-05-14, modified: 2020-05-14 02:01
 * @copyright   Copyright (c) 2020
 */

namespace App\Http\Controllers\Backend\Modules\Extension;

use App\Exceptions\GeneralException;
use App\Http\Controllers\Api\UploadHandlerController;
use App\Http\Controllers\Controller;
use App\Repositories\Modules\Extension\SliderRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class SliderController extends Controller
{
    private $route = "admin.extension.slideshow";
    private $template = "backend.modules.extension.slideshow";
    /**
     * @var SliderRepository
     */
    private $repository;
    /**
     * @var UploadHandlerController
     */
    private $handlerController;

    /**
     * SliderController constructor.
     *
     * @param SliderRepository        $repository
     * @param UploadHandlerController $handlerController
     */
    public function __construct(SliderRepository $repository,
                                UploadHandlerController $handlerController)
    {
        $this->repository = $repository;
        $this->handlerController = $handlerController;
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        permission('is_read', $this->getRoute(), false, 'module');
        $data['route'] = $this->route;
        $data['title'] = __('strings.common.list') . ' ' . __('module.slideshow.module');
        return view($this->template . '.index', $data);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function grid()
    {
        $dataArray = $this->repository->get();
        $_data = [];
        foreach ($dataArray as $row) {
            $_data[] = [
                'secureId' => encode_id($row->id),
                'ids' => $row->id,
                'title' => $row->title,
                'image' => $row->asset->getFullURLAttribute(),
                'status' => $row->status
            ];
        }
        return response()->json($_data);
    }

    /**
     * @param null $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update($id = null)
    {
        if ($id) {
            permission('is_update', $this->getRoute(), false, 'module');
            $keyId = decode_id($id);
            $data['item'] = $this->repository->getById($keyId);
            $data['title'] = __('strings.common.edit') . ' ' . __('module.slideshow.module');
        } else {
            permission('is_create', $this->getRoute(), false, 'module');
            $data['item'] = null;
            $data['title'] = __('strings.common.add') . ' ' . __('module.slideshow.module');
        }
        $data['secure_id'] = $id;
        $data['route'] = $this->route;
        return view($this->template . '.form', $data);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     */
    public function store(Request $request)
    {
        if ($request->all()) {
            $keyId = decode_id($request->input('secure_id'));
            Validator::make($request->all(), [
                'title' => 'required',
                'image' => 'required',
            ], [
                'title.required' => __('validation.required', ['attribute' => __('forms.banner.name')]),
                'image.required' => __('validation.required', ['attribute' => __('forms.banner.image')]),
            ])->validate();

            $insert = [];
            $data = $request->all();
            $data['status'] = $request->has('status');
            if (!$request->input('secure_id')) {
                permission('is_create', $this->getRoute(), false, 'module');
                $data['created_by'] = Auth::id();
                $result = $this->repository->create($data);
                if ($result) {
                    logActivity($request, __('strings.backend.logs.add', ['val' => strtolower(__('module.slideshow.module')) . ' ' . $result->title]));
                    return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.save.success'))
                        ->with('type', 'success');
                } else {
                    throw new GeneralException(__('strings.common.messages.save.failed'));
                }
            } else {
                permission('is_update', $this->getRoute(), false, 'module');
                $data['updated_by'] = Auth::id();
                $result = $this->repository->updateById($keyId, $data);
                if ($result) {
                    logActivity($request, __('strings.backend.logs.edit', ['val' => strtolower(__('module.slideshow.module')) . ' ' . $result->title]));
                    return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.update.success'))
                        ->with('type', 'success');
                } else {
                    throw new GeneralException(__('strings.common.messages.update.failed'));
                }
            }
        } else {
            throw new GeneralException(__('strings.common.messages.empty'));
        }
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     * @throws \Exception
     */
    public function destroy(Request $request)
    {
        permission('is_delete', $this->getRoute(), false, 'module');
        if (!$request->input('secure_id')) {
            throw new GeneralException(__('strings.common.messages.empty'));
        }

        $id = decode_id($request->input('secure_id'));
        $data = $this->repository->getById($id);
        if ($data) {
            if ($data->delete()) {
                if ($data->image) $this->handlerController->fileRemove($data->image);
                logActivity($request, __('strings.backend.logs.delete', ['val' => strtolower(__('module.slideshow.module')) . ' ' . $data->title]));
                return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.delete.success'))
                    ->with('type', 'success');
            } else {
                throw new GeneralException(__('strings.common.messages.delete.failed'));
            }
        } else {
            throw new GeneralException(__('strings.common.messages.not_found'));
        }
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     * @throws \Exception
     */
    public function batch(Request $request)
    {
        permission('is_delete', $this->getRoute(), false, 'module');
        if (!$request->input('ids')) {
            throw new GeneralException(__('strings.common.messages.empty'));
        }

        $all = $this->repository->whereIn('id', $request->input('ids'))->get();
        $_images = [];
        foreach ($all as $row) {
            $_images[] = $row->image;
        }
        $this->handlerController->fileRemoveBatch($_images);
        $result = $this->repository->deleteMultipleById($request->input('ids'));
        if ($result) {
            logActivity($request, __('strings.backend.logs.batch', ['val' => implode(',', $request->input('ids'))]));
            return redirect()->route($this->getRoute('index'))->with('message', __('strings.common.messages.delete.success'))
                ->with('type', 'success');
        } else {
            throw new GeneralException(__('strings.common.messages.delete.failed'));
        }
    }

    /**
     * @param string $prefix
     *
     * @return string
     */
    public function getRoute($prefix = '*'): string
    {
        return $this->route . '.' . $prefix;
    }
}
