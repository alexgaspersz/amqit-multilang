<?php
/**
 * Filename: SearchController.php
 * Package: pt-sma
 * Author: alex
 * Created: 11/30/18 12:53 PM
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\Modules\Content\PagesModel;
use App\Models\Modules\Content\PostsModel;
use App\Repositories\Core\MenuRepository;
use App\Repositories\Modules\Content\PageRepository;
use App\Repositories\Modules\Content\PostRepository;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
     * @var PostRepository
     */
    private $postRepository;
    /**
     * @var PageRepository
     */
    private $pageRepository;
    /**
     * @var MenuRepository
     */
    private $menuRepository;

    /**
     * SearchController constructor.
     * @param PostRepository $postRepository
     * @param PageRepository $pageRepository
     * @param MenuRepository $menuRepository
     */
    public function __construct(PostRepository $postRepository,
                                PageRepository $pageRepository,
                                MenuRepository $menuRepository)
    {
        $this->postRepository = $postRepository;
        $this->pageRepository = $pageRepository;
        $this->menuRepository = $menuRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function query(Request $request)
    {
        // queries to Algolia search index and returns matched records as Eloquent Models
        $keyword = $request->get('key');
        $items = [];
        $posts = PostsModel::search($keyword)->get();
        $pages = PagesModel::search($keyword)->get();

        //loop indexed post model
        foreach ($posts as $rows) {
            $items[] = [
                'source'    => 'post',
                'route'     => $rows->getPostRouteAttribute(),
                'title'     => $rows->single_translate->title
            ];
        }

        //loop indexed pages model
        foreach ($pages as $rows) {
            $source = $this->menuRepository->where('source','page')->where('source_id',$rows->id)->first();
            $items[] = [
                'source'    => 'page',
                'route'     => route($source->module,['slug' => $rows->slug]),
                'title'     => $rows->single_translate->page_title
            ];
        }

        $data['keys'] = $keyword;
        $data['items'] = $items;
        return view('frontend.' . config('general.themes') . '.modules.search.index', $data);
    }

    public function add()
    {
        // this post should be indexed at Algolia right away!
        $post = new PostsModel();
        $post->setAttribute('slug', 'Another Post');
        $post->setAttribute('id', '1');
        $post->save();
    }

    public function delete()
    {
        // this post should be removed from the index at Algolia right away!
        $post = PostsModel::find(1);
        $post->delete();
    }
}