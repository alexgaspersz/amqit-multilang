<?php
/**
 * Description: UploadHandlerController.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     31/01/2018, modified: 31/01/2018 00:58
 * @copyright   Copyright (c) 2018.
 */

namespace App\Http\Controllers\Api;


use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use App\Repositories\Handler\UploadHandlerRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

/**
 * Class UploadHandlerController
 * @package App\Http\Controllers\Api
 */
class UploadHandlerController extends Controller
{
    /**
     * @var UploadHandlerRepository
     */
    private $repository;

    /**
     * UploadController constructor.
     * @param UploadHandlerRepository $repository
     */
    public function __construct(UploadHandlerRepository $repository){
        $this->repository = $repository;
    }

    /**
     * Upload single image file
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function fileSingleUpload(Request $request)
    {
        if ($request->hasFile('filename')) {
            $image = $request->file('filename');

            $destinationPath = public_path(uploadFolder() . $request->baseFolder);
            $current = Carbon::now()->format('Y/m/d');
            $path = $destinationPath . '/' . $current;
            $baseFolder = $request->baseFolder . '/' . $current;
            $imageName = $image->getClientOriginalName();
            $imageMime = $image->getClientMimeType();
            $imageExtension = $image->getClientOriginalExtension();
            $imageSize = $image->getSize();
            $newFilename = uniqid('img_').'.'.$imageExtension;

            if (!File::exists($path)) {
                File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);

                //Create thumbnails folder
                if (thumbSize()) {
                    $thumbnailPath = $path . '/thumbs';
                    if (!File::exists($thumbnailPath)) {
                        File::makeDirectory($thumbnailPath, 0777, true, true);
                    }
                }
            }

            $filePath = $current.'/'.$newFilename;
            $uploaded = $image->move($path,$newFilename);
            if ($uploaded) {
                //Generate thumbnails
                if (thumbSize()) {
                    $basePath = uploadFolder() . $request->baseFolder . '/' . $filePath;
                    $thumbnailSize = explode('|', thumbSize());
                    foreach ($thumbnailSize as $row) {
                        $thumb = explode('x', $row);
                        $this->getImageThumbnail($basePath, $thumb[0], $thumb[1]);
                    }
                }

                $insert = [
                    'category' => $request->category,
                    'title' => $request->title ? $request->title : $imageName,
                    'base_folder' => $baseFolder,
                    'base_path' => $newFilename,
                    'mime_type' => $imageMime,
                    'extension' => $imageExtension,
                    'size' => $imageSize,
                    'created_by' => Auth::id()
                ];
                $asset = $this->repository->create($insert);
                return response()->json(['success' => $asset->assetId], 200);
            } else {
                return response()->json('error upload', 400);
            }
        } else {
            return response()->json('file not found', 400);
        }
    }

    /**
     * Upload single document file
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function docSingleUpload(Request $request)
    {
        if ($request->hasFile('filename')) {
            $doc = $request->file('filename');
            $destinationPath = public_path(uploadFolder() . $request->baseFolder);
            $current = Carbon::now()->format('Y/m/d');
            $path = $destinationPath . '/' . $current;
            $baseFolder = $request->baseFolder . '/' . $current;
            $docName = $doc->getClientOriginalName();
            $docMime = $doc->getClientMimeType();
            $docExtension = $doc->getClientOriginalExtension();
            $docSize = $doc->getSize();
            $newFilename = uniqid('doc_').'.'.$docExtension;

            if (!File::exists($path)) {
                File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
            }

            $uploaded = $doc->move($path,$newFilename);
            if ($uploaded) {
                $insert = [
                    'category' => $request->category,
                    'title' => $request->title ? $request->title : $docName,
                    'base_folder' => $baseFolder,
                    'base_path' => $newFilename,
                    'mime_type' => $docMime,
                    'extension' => $docExtension,
                    'size' => $docSize,
                    'created_by' => Auth::id()
                ];
                $asset = $this->repository->create($insert);
                return response()->json(['success' => $asset->assetId], 200);
            } else {
                return response()->json('error upload', 400);
            }
        } else {
            return response()->json('file not found', 400);
        }
    }

    /**
     * Upload multiple file
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function fileMultipleUpload(Request $request)
    {
        if ($request->hasFile('filename')) {
            $images = $request->file('filename');
            $destinationPath = public_path(uploadFolder() . $request->baseFolder);
            $current = Carbon::now()->format('Y/m/d');
            $path = $destinationPath . '/' . $current;
            $baseFolder = $request->baseFolder . '/' . $current;

            if (!File::exists($path)) {
                File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);

                //Create thumbnails folder
                if (thumbSize()) {
                    $thumbnailPath = $path . '/thumbs';
                    if (!File::exists($thumbnailPath)) {
                        File::makeDirectory($thumbnailPath, 0777, true, true);
                    }
                }
            }

            $ids = [];
            foreach ($images as $n => $file) {
                $imageName = $images[$n]->getClientOriginalName();
                $imageMime = $images[$n]->getClientMimeType();
                $imageExtension = $images[$n]->getClientOriginalExtension();
                $imageSize = $images[$n]->getSize();
                $newFilename = uniqid('img_').'.'.$imageExtension;
                $filePath = $current . '/' . $newFilename;
                $insert = [
                    'category' => $request->category,
                    'title' => $request->title ? $request->title : $imageName,
                    'base_folder' => $baseFolder,
                    'base_path' => $newFilename,
                    'mime_type' => $imageMime,
                    'extension' => $imageExtension,
                    'size' => $imageSize,
                    'created_at' => Carbon::now(),
                    'created_by' => Auth::id()
                ];
                if ($images[$n]->move($path, $newFilename)) {

                    $basePath = uploadFolder() . $request->baseFolder . '/' . $filePath;
                    $thumbnailSize = explode('|', thumbSize());
                    foreach ($thumbnailSize as $row){
                        $thumb = explode('x',$row);
                        $this->getImageThumbnail($basePath, $thumb[0], $thumb[1]);
                    }

                    $result = $this->repository->create($insert);
                    $ids[] = $result->assetId;
                }
            }
            return response()->json(['success' => json_encode($ids)], 200);
        } else {
            return response()->json('error', 400);
        }
    }

    /**
     * Display single image
     *
     * @param null $assetId
     * @param int  $w
     * @param int  $h
     * @return \Illuminate\Http\JsonResponse
     */
    public function filePreview($assetId = null, $w = 350, $h = 350)
    {
        $images = [];
        if($assetId){
            $files = $this->repository->getById($assetId);
            $imagePath = uploadFolder() . $files->base_folder . '/' . $files->base_path;
            if (File::exists($imagePath)) {
                $thumb = $this->getImageThumbnail($imagePath, $w, $h);
                $images[] = [
                    'original' => $files->title,
                    'server' => $thumb,
                    'size' => $files->size
                ];
            }
        }

        return response()->json([
            'images' => $images
        ]);
    }

    /**
     * Display multiple image
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function filePreviewMultiple(Request $request)
    {
        $images = [];
        if($request->input('postData')){
            $ids = json_decode($request->input('postData'));
            $files = $this->repository->getById($ids);
            foreach ($files as $row) {
                $imagePath = uploadFolder() . $row->base_folder . '/' . $row->base_path;
                if (File::exists($imagePath)) {
                    $images[] = [
                        'original' => $row->title,
                        'server' => $this->getImageThumbnail($imagePath, '350', '350'),
                        'size' => $row->size
                    ];
                }
            }
        }
        return response()->json([
            'images' => $images
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function editorUpload(Request $request)
    {
        if ($request->hasFile('file')) {
            $image = $request->file('file');

            $destinationPath = public_path(uploadFolder() . $request->baseFolder);
            $current = Carbon::now()->format('Y/m/d');
            $path = $destinationPath . '/' . $current;
            $baseFolder = $request->baseFolder . '/' . $current;
            $imageName = $image->getClientOriginalName();
            $imageMime = $image->getClientMimeType();
            $imageExtension = $image->getClientOriginalExtension();
            $imageSize = $image->getSize();
            $newFilename = uniqid('img_') . '.' . $imageExtension;

            if (!File::exists($path)) {
                File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);

                //Create thumbnails folder
//                if(thumbSize()){
//                    $thumbnailPath = $path . '/thumbs';
//                    if (!File::exists($thumbnailPath)) {
//                        File::makeDirectory($thumbnailPath, 0777, true, true);
//                    }
//                }
            }

//            $filePath = $current.'/'.$newFilename;
            $uploaded = $image->move($path, $newFilename);
            if ($uploaded) {
                //Generate thumbnails
//                if(thumbSize()) {
//                    $basePath = uploadFolder() . $request->baseFolder . '/' . $filePath;
//                    $this->getImageThumbnail($basePath, 250, 150);
//                }

                $insert = [
                    'category' => $request->category,
                    'title' => $imageName,
                    'base_folder' => $baseFolder,
                    'base_path' => $newFilename,
                    'mime_type' => $imageMime,
                    'extension' => $imageExtension,
                    'size' => $imageSize,
                    'created_by' => Auth::id()
                ];
                $asset = $this->repository->create($insert);
                return response()->json(['link' => asset(config('general.files.upload') . $asset->base_folder . '/' . $asset->base_path)], 200);
            } else {
                return response()->json('error upload', 400);
            }
        } else {
            return response()->json('file not found', 400);
        }
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function editorManager(Request $request)
    {
        $images = [];
        $files = $this->repository->getByColumns(['category' => $request->category], ['assetId', 'title', 'base_folder', 'base_path', 'size']);
        if (count($files) > 0) {
            foreach ($files as $row) {
                $imagePath = uploadFolder() . $row->base_folder . '/' . $row->base_path;
                if (File::exists($imagePath)) {
                    $images[] = [
                        'url' => asset($imagePath),
                        'thumb' => asset($imagePath),//$this->getImageThumbnail($imagePath, '250', '150'),
                        'name' => $row->title,
                        'id' => $row->assetId,
                        'size' => formatBytes($row->size),
                    ];
                }
            }
        }

        return response()->json($images);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function editorManagerDelete(Request $request)
    {
        $trimed = explode('/', $request->src);
        $asset = $this->repository->getByColumn(end($trimed), 'base_path');
        if ($asset) {
            if ($this->fileRemove($asset->assetId)) {
                return response()->json('success remove image');
            } else {
                return response()->json('failed remove image');
            }
        } else {
            return response()->json('data empty');
        }
    }

    /**
     * Remove uploaded single file
     *
     * @param $assetId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function fileRemove($assetId)
    {
        $asset = $this->repository->getById($assetId);
        if ($asset) {
            $path = public_path(uploadFolder() . $asset->base_folder);
            $filePath = $path . '/' . $asset->base_path;
            if (!File::exists($filePath)) {
                return response()->json('empty', 403);
            }
            if (File::exists($path . '/thumbs')) $this->thumbnailsRemove($filePath);
            if (File::delete($filePath)) {
                $asset->delete();
                return response()->json('success', 200);
            } else {
                return response()->json('error', 400);
            }
        } else {
            return response()->json('not found', 404);
        }
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function postRemove(Request $request)
    {
        if ($request->input('assetId')) {
            $asset = $this->repository->getById($request->input('assetId'));
            if ($asset) {
                $path = public_path(uploadFolder() . $asset->base_folder);
                $filePath = $path . '/' . $asset->base_path;
                if (!File::exists($filePath)) {
                    return response()->json('empty', 403);
                }
                if (File::exists($path . '/thumbs')) $this->thumbnailsRemove($filePath);
                if (File::delete($filePath)) {
                    $asset->delete();
                    return response()->json('success', 200);
                } else {
                    return response()->json('error', 400);
                }
            } else {
                return response()->json('not found', 404);
            }
        } else {
            return response()->json('no request', 204);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     * @throws \Exception
     */
    public function removeAsset(Request $request)
    {
        if (!$request->input('asset_id')) {
            throw new GeneralException(__('strings.common.messages.empty'));
        }
        $id = decode_id($request->input('asset_id'));
        $data = $this->repository->getById($id);
        if ($data) {
            if ($this->fileRemove($id)) {
                return redirect()
                    ->back()
                    ->with('message', __('strings.common.messages.delete.success'))
                    ->with('type', 'success')
                    ->withInput();
            } else {
                throw new GeneralException(__('strings.common.messages.delete.failed'));
            }
        } else {
            throw new GeneralException(__('strings.common.messages.not_found'));
        }
    }

    /**
     * Batch remove uploaded file
     *
     * @param $assetId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function fileRemoveBatch($assetId = [])
    {
        if (count($assetId) > 0) {
            foreach ($assetId as $row) {
                $asset = $this->repository->getByColumn($row, 'assetId');
                if ($asset) {
                    $path = public_path(uploadFolder() . $asset->base_folder);
                    $filePath = $path . '/' . $asset->base_path;
                    if (!File::exists($filePath)) {
                        return response()->json('empty', 403);
                    }
                    if (File::exists($path . '/thumbs')) $this->thumbnailsRemove($filePath);
                    if (File::delete($filePath)) {
                        $asset->delete();
                        echo response()->json('success', 200);
                    } else {
                        echo response()->json('error', 400);
                    }
                }
            }
        } else {
            return response()->json('error', 400);
        }
    }

    /**
     * Remove thumbnails
     * @param $path
     */
    function thumbnailsRemove($path)
    {
        $image = Image::make($path);
        $thumbDir = $image->dirname . '/thumbs';
        $thumbnailSize = explode('|', thumbSize());
        foreach ($thumbnailSize as $row) {
            $thumb = explode('x', $row);
            $thumbnail = $thumbDir . '/' . $image->filename . '_' . $thumb[0] . 'x' . $thumb[1] . '.' . $image->extension;
            if (File::exists($thumbnail)) File::delete($thumbnail);
        }
    }

    /**
     * @param $assetId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function docRemove($assetId)
    {
        $asset = $this->repository->getById($assetId);
        if ($asset) {
            $path = public_path(uploadFolder() . $asset->base_folder . '/' . $asset->base_path);
            if (!File::exists($path)) {
                return response()->json('empty', 403);
            }

            $remove = File::delete($path);
            if ($remove) {
                $asset->delete();
                return response()->json('success', 200);
            } else {
                return response()->json('error', 400);
            }
        } else {
            return response()->json('failed', 500);
        }
    }

    /**
     * Batch remove uploaded file
     *
     * @param $assetId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function docRemoveBatch($assetId = [])
    {
        if(count($assetId) >  0){
            if ($assetId) {
                foreach ($assetId as $row){
                    $asset = $this->repository->getById($row);
                    $path = public_path(uploadFolder() . $asset->base_folder . '/' . $asset->base_path);
                    if (File::exists($path)) {
                        File::delete($path);
                        $asset->delete();
                    }
                }
                return response()->json('success', 200);
            } else {
                return response()->json('error', 400);
            }
        } else {
            return response()->json('error', 400);
        }
    }

    /**
     * Display thumbnail
     *
     * @param $path
     * @param int $width
     * @param int $height
     * @return string
     */
    public function getImageThumbnail($path, $width = 350, $height = 350)
    {
        $isExists = File::exists(public_path($path));
        if ($isExists) {
            $image = Image::make(public_path($path));
            $thumbDir = $image->dirname . '/thumbs';
            if (!File::exists($thumbDir)) {
                File::makeDirectory($thumbDir, 0777, true, true);
            }

            $newfile = $image->filename . '_' . $width . 'x' . $height . '.' . $image->extension;
            $thumbnail = $thumbDir . '/' . $newfile;
            if (!File::exists($thumbnail)) {
                $image->fit($width, $height)->save($thumbnail);
            }
            $trimPath = explode($image->basename, $path);

            return asset($trimPath[0] . 'thumbs/' . $newfile);
        } else {
            return null;
        }
    }
}
