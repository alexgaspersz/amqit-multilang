<?php
/**
 * Description: CommonApiController.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     10/12/2017, modified: 10/12/2017 02:40
 * @copyright   Copyright (c) 2018.
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Core\GroupRepository;
use App\Repositories\Core\MenuRepository;
use App\Repositories\Core\ModuleRepository;
use App\Repositories\Core\UsersRepository;
use App\Repositories\Modules\Content\PageRepository;
use App\Repositories\Modules\Content\PostRepository;

/**
 * Class CommonApiController
 * @package App\Http\Controllers\Api
 */
class CommonApiController extends Controller
{
    /**
     * @var MenuRepository
     */
    private $menuRepository;
    /**
     * @var GroupRepository
     */
    private $groupRepository;
    /**
     * @var PageRepository
     */
    private $pageRepository;
    /**
     * @var PostRepository
     */
    private $postRepository;
    /**
     * @var ModuleRepository
     */
    private $moduleRepository;
    /**
     * @var UsersRepository
     */
    private $usersRepository;


    /**
     * CommonApiController constructor.
     *
     * @param MenuRepository $menuRepository
     * @param GroupRepository $groupRepository
     * @param PageRepository $pageRepository
     * @param PostRepository $postRepository
     * @param UsersRepository $usersRepository
     * @param ModuleRepository $moduleRepository
     */
    public function __construct(MenuRepository $menuRepository,
                                GroupRepository $groupRepository,
                                PageRepository $pageRepository,
                                PostRepository $postRepository,
                                UsersRepository $usersRepository,
                                ModuleRepository $moduleRepository)
    {
        $this->menuRepository = $menuRepository;
        $this->groupRepository = $groupRepository;
        $this->pageRepository = $pageRepository;
        $this->postRepository = $postRepository;
        $this->usersRepository = $usersRepository;
        $this->moduleRepository = $moduleRepository;
    }

    /**
     * Rest api get menu
     *
     * @param null $type
     * @param null $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_menu($type = null, $id = null)
    {
        if($id){
            $key = decode_id($id);
            $data = $this->menuRepository->getTranslationIgnoreKey(['field' => 'menu_type', 'val' => $type, 'column' => null, 'key' => $key]);
        } else {
            $data = $this->menuRepository->getTranslationByColumn('menu_type', $type);
        }
        $_data = [];
        foreach ($data as $row) {
            $_data[] = [
                htmlspecialchars($row->menu_id, ENT_QUOTES),
                htmlspecialchars($row->single_translate->menu_title, ENT_QUOTES),
            ];
        }
        return response()->json($_data);
    }

    /**
     * @param null   $type
     * @param string $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_menu_parent($type = null, $id = '')
    {
        $_data = [];
        if ($type) {
            $key = null;
            if ($id) $key = decode_id($id);
            $data = $this->menuRepository->getTranslationByColumns(
                ['menu_type' => $type, 'parent_id' => 0, 'status' => true],
                ['*'],
                ['column' => null, 'key' => $key]);
            foreach ($data as $row) {
                $_data[] = [
                    htmlspecialchars($row->menu_id, ENT_QUOTES),
                    htmlspecialchars($row->single_translate->menu_title, ENT_QUOTES),
                    'child' => $this->get_menu_child($row->menu_id)
                ];
            }
        }
        return response()->json($_data);
    }

    /**
     * @param $id
     *
     * @return array
     */
    public function get_menu_child($id)
    {
        $_data = [];
        if ($id) {
            $data = $this->menuRepository->getTranslationByColumns(['parent_id' => $id, 'status' => true]);
            foreach ($data as $row) {
                $_data[] = [
                    htmlspecialchars($row->menu_id, ENT_QUOTES),
                    htmlspecialchars($row->single_translate->menu_title, ENT_QUOTES),
                    'child' => $this->get_menu_child($row->menu_id)
                ];
            }
        }

        return $_data;
    }

    /**
     * Rest api get group
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_group()
    {
        $data = $this->groupRepository->whereNotIn('is_super',[1])->where('status',true)->get();
        $_data = [];
        foreach ($data as $row) {
            $_data[] = [
                htmlspecialchars($row->group_id, ENT_QUOTES),
                htmlspecialchars($row->name, ENT_QUOTES)
            ];
        }
        return response()->json($_data);
    }

    /**
     * Rest api get group
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_group_admin()
    {
        $data = $this->groupRepository->whereNotIn('is_super', [1])
            ->where('is_admin', true)
            ->where('status', true)
            ->get();
        $_data = [];
        foreach ($data as $row) {
            $_data[] = [
                htmlspecialchars($row->group_id, ENT_QUOTES),
                htmlspecialchars($row->name, ENT_QUOTES)
            ];
        }
        return response()->json($_data);
    }

    /**
     * Rest api get module
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_module()
    {
        $data = $this->moduleRepository->get();
        $_data = [];
        foreach ($data as $row) {
            $_data[] = [
                htmlspecialchars($row->id, ENT_QUOTES),
                htmlspecialchars($row->module_name, ENT_QUOTES)
            ];
        }
        return response()->json($_data);
    }

    /**
     * Rest api get page
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_page()
    {
        $data = $this->pageRepository->getTranslationByColumn('status',true);
        $_data = [];
        foreach ($data as $row) {
            $_data[] = [
                htmlspecialchars($row->single_translate->page_id, ENT_QUOTES),
                htmlspecialchars($row->single_translate->page_title, ENT_QUOTES)
            ];
        }
        return response()->json($_data);
    }

    /**
     * Rest api get post category
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_post()
    {
        $data = $this->postRepository->getTranslation();
        $_data = [];
        foreach ($data as $row) {
            $_data[] = [
                htmlspecialchars($row->single_translate->category_id, ENT_QUOTES),
                htmlspecialchars($row->single_translate->name, ENT_QUOTES)
            ];
        }
        return response()->json($_data);
    }


    /**
     * Rest api get post category
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_users()
    {
        $data = $this->usersRepository->getActiveUsers();
        $_data = [];
        foreach ($data as $row) {
            $_data[] = [
                htmlspecialchars($row->user_id, ENT_QUOTES),
                htmlspecialchars($row->fullname, ENT_QUOTES)
            ];
        }
        return response()->json($_data);
    }
}
