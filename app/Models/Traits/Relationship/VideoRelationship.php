<?php
/**
 * Description: VideoRelationship.php generated by PhpStorm.
 * @project     web-sekolah
 * @package     App\Models\Traits\Relationship
 * @author      alex
 * @created     2020-05-08, modified: 2020-05-08 19:09
 * @copyright   Copyright (c) 2020
 */

namespace App\Models\Traits\Relationship;


trait VideoRelationship
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translation()
    {
        return $this->hasMany('App\Models\Modules\Gallery\VideoTranslationModel', 'vid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function single_translate()
    {
        return $this->hasOne('App\Models\Modules\Gallery\VideoTranslationModel', 'vid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function videoImages()
    {
        return $this->belongsTo('App\Models\Services\UploadHandlerModel', 'cover', 'assetId');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function internal()
    {
        return $this->belongsTo('App\Models\Services\UploadHandlerModel', 'asset', 'assetId');
    }
}
