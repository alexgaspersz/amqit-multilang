<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 2019-03-29
 * Time: 01:43
 */

namespace App\Models\Traits\Auth;


use App\Notifications\Backend\UserNeedsPasswordReset;

trait SendUserPasswordReset
{
    /**
     * Send the password reset notification.
     *
     * @param string $token
     *
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new UserNeedsPasswordReset($token));
    }
}