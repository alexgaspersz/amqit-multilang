<?php
/**
 * Description: PermalinkModel.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     03/03/2018, modified: 03/03/2018 19:36
 * @copyright   Copyright (c) 2018.
 */

namespace App\Models\Services;


use Illuminate\Database\Eloquent\Model;

/**
 * @property string id
 * @property int source_id
 * @property string permalink
 * @property false|float|int expires_at
 * @property false|float|int single_use
 * @property mixed created_at
 * @property int|null created_by
 * @property mixed updated_at
 * @property int|null updated_by
 */
class PermalinkModel extends Model
{
    protected $table = 'ms_permalink';
    protected $primaryKey = 'id';
    protected $fillable = ['source_id','permalink','single_use','is_accessed'];
    protected $casts = ['single_use' => 'boolean', 'is_accessed' => 'boolean'];
    protected $guarded = ['created_at', 'created_by'];

    public function getActiveAttribute()
    {

    }
}