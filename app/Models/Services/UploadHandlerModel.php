<?php
/**
 * Description: UploadHandlerModel.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     31/01/2018, modified: 31/01/2018 00:59
 * @copyright   Copyright (c) 2018.
 */

namespace App\Models\Services;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

/**
 * @property mixed category
 * @property mixed title
 * @property string base_folder
 * @property string base_path
 * @property null|string mime_type
 * @property mixed created_at
 * @property int|null created_by
 * @property mixed assetId
 */
class UploadHandlerModel extends Model
{
    protected $table = 'ms_assets';
    protected $primaryKey = 'assetId';
    protected $fillable = ['category','title','base_folder','base_path','mime_type','extension','size', 'created_by', 'updated_by'];
    protected $guarded = ['created_at','updated_at'];

    /**
     * @return mixed
     */
    public function getFileURLAttribute()
    {
        return config('general.files.upload') . $this->base_folder . '/' . $this->base_path;
    }

    /**
     * @return mixed
     */
    public function getFullURLAttribute()
    {
        $isExists = File::exists(public_path($this->getFileURLAttribute()));
        return $isExists ? asset($this->getFileURLAttribute()) : 'https://placeimg.com/800/600/tech';
    }

    /**
     * @param $width
     * @param $height
     * @return string
     */
    public function getImageThumbnailAttribute($width, $height)
    {
        $path = config('general.files.upload') . $this->base_folder . '/' . $this->base_path;
        $isExists = File::exists(public_path($path));
        if ($isExists) {
            $image = Image::make(public_path($path));
            $file = $image->filename . '_' . $width . 'x' . $height . '.' . $image->extension;
            $trimPath = explode($image->basename, $path);

            return asset($trimPath[0] . 'thumbs/' . $file);
        } else {
            return 'https://placeimg.com/' . $width . '/' . $height . '/tech';
        }
    }
}
