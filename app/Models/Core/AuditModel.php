<?php
/**
 * Description: AuditModel.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     03/12/2018, modified: 03/12/2018 19:19
 * @copyright   Copyright (c) 2018.
 */

namespace App\Models\Core;


use Illuminate\Database\Eloquent\Model;

class AuditModel extends Model
{
    protected $table = 'tb_logs';
    protected $primaryKey = 'log_id';
    protected $fillable = ['module', 'task', 'user_id', 'ipaddress', 'useragent', 'note'];
    protected $guarded = ['created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Core\UsersModel','user_id','user_id');
    }
}