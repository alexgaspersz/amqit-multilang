<?php
/**
 * Description: UsersModel.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     03/12/2018, modified: 03/12/2018 19:19
 * @copyright   Copyright (c) 2018.
 */

namespace App\Models\Core;

use App\Http\Controllers\Api\UploadHandlerController;
use App\Models\Traits\Attribute\UserAttribute;
use App\Models\Traits\Auth\SendUserPasswordReset;
use App\Models\Traits\Method\UserMethod;
use App\Models\Traits\Relationship\UserRelationship;
use App\Models\Traits\Scope\UserScope;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * Class       UsersModel
 * @property mixed                   is_admin
 * @property array|string            fullname
 * @property array|string            username
 * @property mixed                   password
 * @property array|string            email
 * @property array|string            photo
 * @property int                     group_id
 * @property int                     active
 * @property mixed                   created_at
 * @property mixed                   user_id
 * @property array|string            avatar
 * @property int                     created_by
 * @property UploadHandlerController filehandler
 * @package App\Models\Core
 * $
 */
class UsersModel extends Authenticatable
{
    use Notifiable,
        UserMethod,
        UserRelationship,
        UserAttribute,
        UserScope,
        SendUserPasswordReset;

    protected $table = 'tb_users';
    protected $primaryKey = 'user_id';
    protected $fillable = [
        'group_id',
        'username',
        'password',
        'email',
        'fullname',
        'avatar',
        'active',
        'is_online',
        'last_login',
        'ip_address',
        'remember_token',
        'remember_selector',
        'forgotten_password_selector',
        'created_by',
        'updated_by'
    ];
    protected $hidden = ['password', 'remember_token', 'remember_selector', 'forgotten_password_selector'];
    protected $guarded = ['is_admin', 'is_core', 'created_at', 'updated_at'];
    protected $dates = ['last_login'];
}
