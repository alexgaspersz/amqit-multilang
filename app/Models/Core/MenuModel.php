<?php
/**
 * Description: MenuModel.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     09/12/2017, modified: 09/12/2017 21:01
 * @copyright   Copyright (c) 2018.
 */

namespace App\Models\Core;

use App\Models\Traits\Relationship\MenuRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class       MenuModel
 * @property string module
 * @property string url
 * @property string menu_icons
 * @property int    parent_id
 * @property int    ordering
 * @property int    status
 * @property mixed  menu_id
 * @property string menu_type
 * @property string source
 * @property int    source_id
 * @package App\Models\Core
 * $
 */
class MenuModel extends Model
{
    use MenuRelationship;

    /**
     * @var string
     */
    protected $table = 'tb_menu';
    protected $primaryKey = 'menu_id';
    protected $fillable = [
        'parent_id', 'module', 'url', 'menu_type', 'source', 'source_id', 'link', 'allow_guest', 'is_restrict',
        'ordering', 'status', 'show_title', 'menu_icons', 'created_by', 'updated_by'
    ];
    protected $guarded = ['created_at', 'updated_at'];
    protected $attributes = [
        'is_restrict' => false,
        'status' => true,
        'allow_guest' => false,
        'show_title' => true
    ];
    protected $casts = [
        'is_restrict' => 'boolean',
        'status' => 'boolean',
        'allow_guest' => 'boolean',
        'show_title' => 'boolean'
    ];
}
