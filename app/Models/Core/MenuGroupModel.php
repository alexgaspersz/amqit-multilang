<?php
/**
 * Description: MenuGroupModel.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     06/01/2018, modified: 06/01/2018 03:04
 * @copyright   Copyright (c) 2018.
 */

namespace App\Models\Core;


use App\Models\Traits\Method\StatusMethod;
use Illuminate\Database\Eloquent\Model;

/**
 * @property  string menu_group
 * @property  string menu_group_alias
 */
class MenuGroupModel extends Model
{
    use StatusMethod;

    /**
     * @var string
     */
    protected $table = 'tb_menu_group';
    protected $primaryKey = 'id';
    protected $fillable = ['menu_group','menu_group_alias','status','is_core','created_by','updated_by'];
    protected $guarded = ['created_at', 'updated_at'];
    protected $attributes = [
        'is_core' => false,
        'status' => true
    ];
    protected $casts = [
        'status' => 'boolean',
        'is_core' => 'boolean'
    ];
}