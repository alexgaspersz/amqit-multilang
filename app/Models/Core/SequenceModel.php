<?php
/**
 * Description: SequenceModel.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     03/12/2018, modified: 03/12/2018 19:20
 * @copyright   Copyright (c) 2018.
 */

namespace App\Models\Core;


use Illuminate\Database\Eloquent\Model;

class SequenceModel extends Model
{
    protected $table = 'tb_sequence';
    protected $primaryKey = 'seq_id';
    protected $fillable = [
        'ref_name',
        'curr_number',
        'curr_month',
        'curr_year'
    ];
    public $timestamps = false;
}
