<?php
/**
 * Description: GroupsModel.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     03/12/2018, modified: 03/12/2018 19:19
 * @copyright   Copyright (c) 2018.
 */

namespace App\Models\Core;


use App\Models\Traits\Method\StatusMethod;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string name
 * @property string description
 * @property string alias
 * @property mixed group_id
 * @property bool allow_cms
 */
class GroupsModel extends Model
{
    use StatusMethod;

    protected $table = 'tb_groups';
    protected $primaryKey = 'group_id';
    protected $fillable = ['name', 'description', 'alias', 'allow_cms', 'is_admin', 'status', 'created_by', 'updated_by'];
    protected $hidden = ['is_core', 'is_super'];
    protected $guarded = ['is_core', 'is_super', 'created_at', 'updated_at'];
    protected $attributes = [
        'allow_cms' => false,
        'is_core' => false,
        'is_super' => false,
        'is_admin' => false,
        'status' => true
    ];
    protected $casts = [
        'allow_cms' => 'boolean',
        'is_core' => 'boolean',
        'is_super' => 'boolean',
        'is_admin' => 'boolean',
        'status' => 'boolean'
    ];
}
