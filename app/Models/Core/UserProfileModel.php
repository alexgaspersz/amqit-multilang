<?php
/**
 * Description: UserProfileModel.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     07/01/2018, modified: 07/01/2018 05:45
 * @copyright   Copyright (c) 2018.
 */

namespace App\Models\Core;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int    user_id
 * @property mixed  display_name
 * @property int    angkatan
 * @property string birthdate
 * @property string birthplace
 * @property string gender
 * @property string handphone
 * @property string address
 * @property string pelajaran
 * @property string jabatan
 * @property string pangkat
 * @property string facebook
 * @property string instagram
 * @property string sosmed_lain
 * @property string pekerjaan
 */
class UserProfileModel extends Model
{
    /**
     * @var string
     */
    protected $table = 'tb_user_profile';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'display_name',
        'birthdate',
        'birthplace',
        'gender',
        'handphone',
        'address',
        'facebook',
        'instagram',
        'twitter',
        'linkedin',
    ];
    protected $guarded = ['created_at'];

    /**
     * @return mixed
     */
    public function getAgeAttribute()
    {
        return Carbon::parse($this->birthdate)->age;
    }

    /**
     * @return mixed
     */
    public function getSexAttribute()
    {
        return gender($this->gender);
    }

    /**
     * @return string
     */
    public function getBodAttribute()
    {
        return $this->birthplace . ', ' . dateString($this->birthdate);
    }
}
