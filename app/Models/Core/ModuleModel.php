<?php
/**
 * Description: ModuleModel.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     11/02/2018, modified: 11/02/2018 00:43
 * @copyright   Copyright (c) 2018.
 */

namespace App\Models\Core;


use Illuminate\Database\Eloquent\Model;

class ModuleModel extends Model
{
    protected $table = 'tb_module';
    protected $primaryKey = 'id';
    protected $fillable = ['module_name', 'module_title', 'module_table', 'module_table_key', 'module_namespace', 'module_note', 'module_config'];
    protected $guarded = array('created_at', 'updated_at');
}