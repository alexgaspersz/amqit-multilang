<?php
/**
 * Description: MenuTranslationModel.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     28/12/2017, modified: 28/12/2017 06:46
 * @copyright   Copyright (c) 2018.
 */

namespace App\Models\Core;
use Illuminate\Database\Eloquent\Model;

/**
 * @property  integer menu_id
 * @property  string menu_title
 * @property  string meta_title
 * @property  string meta_description
 * @property  string locale
 */
class MenuTranslationModel extends Model
{
    /**
     * @var string
     */
    protected $table = 'tb_menu_translation';
    protected $related = 'tb_menu';
    protected $primaryKey = 'id';
    protected $fillable = ['menu_id', 'menu_title', 'meta_title', 'meta_description', 'locale'];
    protected $guarded = ['created_at', 'updated_at'];

    /**
     * @param array $fillable
     * @return MenuTranslationModel
     */
    public function setFillable(array $fillable): MenuTranslationModel
    {
        $this->fillable = $fillable;
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parents()
    {
        return $this->belongsTo('App\Models\Core\MenuModel', 'menu_id');
    }
}