<?php
/**
 * Description: AccessModel.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     03/12/2018, modified: 03/12/2018 19:19
 * @copyright   Copyright (c) 2018.
 */

namespace App\Models\Core;


use Illuminate\Database\Eloquent\Model;

class AccessModel extends Model
{
    protected $table = 'tb_access';
    protected $primaryKey = 'access_id';
    protected $guarded = ['created_at', 'updated_at'];
}