<?php
/**
 * Description: VideoModel.php generated by PhpStorm.
 * @project     web-sekolah
 * @package     App\Models\Modules\Gallery
 * @author      alex
 * @created     2020-05-08, modified: 2020-05-08 17:53
 * @copyright   Copyright (c) 2020
 */

namespace App\Models\Modules\Gallery;


use App\Models\Traits\Method\StatusMethod;
use App\Models\Traits\Relationship\VideoRelationship;
use Illuminate\Database\Eloquent\Model;

class VideoModel extends Model
{
    use StatusMethod, VideoRelationship;

    protected $table = 'ms_videos';
    protected $primaryKey = 'id';
    protected $fillable = [
        'slug',
        'source',
        'cover',
        'asset',
        'code',
        'thumbnail',
        'status',
        'allow_comment',
        'sort_order',
        'created_by',
        'updated_by'
    ];
    protected $guarded = ['created_at', 'updated_at'];
    protected $attributes = [
        'status' => true,
        'allow_comment' => false
    ];
    protected $casts = [
        'status' => 'boolean',
        'allow_comment' => 'boolean'
    ];
}
