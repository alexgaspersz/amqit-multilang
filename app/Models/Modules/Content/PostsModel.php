<?php
/**
 * Description: PostsModel.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     02/02/2018, modified: 02/02/2018 03:42
 * @copyright   Copyright (c) 2018.
 */

namespace App\Models\Modules\Content;


use App\Models\Traits\Content\PostsTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * Class       PostsModel
 * @package App\Models\Modules\Content
 * $
 * @property boolean     featured
 * @property array|null  categories
 * @property int|null    author_id
 * @property mixed       publish_at
 * @property string      slug
 * @property string|null tags
 * @property int         images
 * @property int         status
 * @property boolean     allow_guest
 * @property boolean     allow_comment
 * @property int         sort_order
 * @property int|null    created_by
 * @property array       createBy
 * @property array       authorBy
 * @property array       single_translate
 * @property array       translation
 * @property array       featuredImages
 * @property mixed       id
 * @method static find(int $int)
 */

class PostsModel extends Model
{
    use PostsTrait;

    protected $table = 'ms_post';
    protected $primaryKey = 'id';
    protected $fillable = [
        'categories', 'author_id', 'publish_at', 'slug', 'tags', 'images',
        'status', 'featured', 'allow_guest', 'allow_comment', 'sort_order', 'hit',
        'created_by', 'updated_by'
    ];
    protected $guarded = ['created_at', 'updated_at'];
    protected $attributes = [
        'featured' => false,
        'allow_guest' => true,
        'allow_comment' => true
    ];
    protected $casts = [
        'featured' => 'boolean',
        'allow_guest' => 'boolean',
        'allow_comment' => 'boolean',
        'tags' => 'array'
    ];
}
