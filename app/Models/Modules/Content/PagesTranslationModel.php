<?php
/**
 * Description: PagesTranslationModel.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     29/01/2018, modified: 29/01/2018 22:46
 * @copyright   Copyright (c) 2018.
 */

namespace App\Models\Modules\Content;


use Illuminate\Database\Eloquent\Model;

/**
 * Class       PagesTranslationModel
 * @package App\Models\Modules\Content
 * $
 * @property integer page_id
 * @property string page_title
 * @property string page_meta
 * @property string page_desc
 * @property string slug
 * @property string content
 * @property mixed locale
 * @property string created_at
 */
class PagesTranslationModel extends Model
{
    protected $table = 'ms_pages_translation';
    protected $primaryKey = 'id';
    protected $fillable = ['page_id', 'page_title', 'page_meta', 'page_desc', 'content', 'locale'];
    protected $guarded = ['created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parents()
    {
        return $this->belongsTo('App\Models\Modules\Content\PagesModel', 'page_id');
    }
}
