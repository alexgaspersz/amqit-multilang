<?php
/**
 * Description: PageModel.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     21/01/2018, modified: 21/01/2018 03:52
 * @copyright   Copyright (c) 2018.
 */

namespace App\Models\Modules\Content;


use App\Models\Traits\Method\StatusMethod;
use App\Models\Traits\Content\PagesTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * Class       PagesModel
 * @package App\Models\Modules
 * $
 * @property integer id
 * @property integer|null parent_id
 * @property integer|null visible
 * @property integer enabled
 * @property integer|null is_private
 * @property string|null theme_layout
 * @property integer|null sort_order
 * @property int|null created_by
 * @property bool social
 */
class PagesModel extends Model
{
    use StatusMethod, PagesTrait;

    protected $table = 'ms_pages';
    protected $primaryKey = 'id';
    protected $fillable = [
        'slug',
        'parent_id',
        'visible',
        'is_private',
        'social',
        'bg',
        'attachment',
        'sort_order',
        'hit',
        'status',
        'created_by',
        'updated_by'
    ];
    protected $guarded = ['created_at', 'updated_at'];
    protected $attributes = [
        'visible' => true,
        'status' => true
    ];
    protected $casts = [
        'visible' => 'boolean',
        'status' => 'boolean'
    ];
}
