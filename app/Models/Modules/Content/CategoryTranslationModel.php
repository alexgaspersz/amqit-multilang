<?php
/**
 * Description: CategoryTranslationModel.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     02/02/2018, modified: 02/02/2018 03:38
 * @copyright   Copyright (c) 2018.
 */

namespace App\Models\Modules\Content;


use Illuminate\Database\Eloquent\Model;

/**
 * Class       CategoryTranslationModel
 * @package App\Models\Modules\Content
 * $
 * @property int category_id
 * @property string name
 * @property string description
 * @property string slug
 * @property mixed locale
 */
class CategoryTranslationModel extends Model
{
    protected $table = 'ms_category_translation';
    protected $primaryKey = 'id';
    protected $fillable = ['category_id', 'name', 'description', 'locale'];
    protected $guarded = ['created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parents()
    {
        return $this->belongsTo('App\Models\Modules\Content\CategoryModel', 'category_id');
    }

    /**
     * @param array $fillable
     * @return CategoryTranslationModel
     */
    public function setFillable(array $fillable): CategoryTranslationModel
    {
        $this->fillable = $fillable;
        return $this;
    }
}