<?php
/**
 * Description: CategoryModel.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     02/02/2018, modified: 02/02/2018 03:36
 * @copyright   Copyright (c) 2018.
 */

namespace App\Models\Modules\Content;


use App\Models\Traits\Method\StatusMethod;
use App\Models\Traits\Relationship\CategoryRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class       CategoryModel
 * @package App\Models\Modules\Content
 * $
 * @property integer sort_order
 * @property int created_by
 */
class CategoryModel extends Model
{
    use StatusMethod,
        CategoryRelationship;

    protected $table = 'ms_category';
    protected $primaryKey = 'id';
    protected $fillable = ['slug','status','sort_order','created_by','updated_by'];
    protected $guarded = ['created_at', 'updated_at'];
    protected $attributes = [
        'status' => true
    ];
    protected $casts = [
        'status' => 'boolean'
    ];

}
