<?php
/**
 * Description: TagsModel.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     08/02/2018, modified: 08/02/2018 01:13
 * @copyright   Copyright (c) 2018.
 */

namespace App\Models\Modules\Content;


use Illuminate\Database\Eloquent\Model;

/**
 * Class       TagsModel
 * @package App\Models\Modules\Content
 * $
 * @property string|null post_tag
 * @property int|null created_by
 */
class TagsModel extends Model
{
    protected $table = 'ms_post_tags';
    protected $primaryKey = 'id';
    protected $fillable = ['post_tag','created_by','updated_by'];
    protected $guarded = ['created_at', 'updated_at'];
}