<?php
/**
 * Description: PostsTranslationModel.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     02/02/2018, modified: 02/02/2018 03:42
 * @copyright   Copyright (c) 2018.
 */

namespace App\Models\Modules\Content;


use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

/**
 * Class       PostsTranslationModel
 * @package App\Models\Modules\Content
 * $
 * @property int entry_id
 * @property string title
 * @property string|null meta
 * @property string|null intro
 * @property string|null description
 * @property string content
 * @property mixed locale
 */

class PostsTranslationModel extends Model
{
    protected $table = 'ms_post_translation';
    protected $primaryKey = 'id';
    protected $fillable = ['entry_id', 'title', 'meta', 'intro', 'description', 'content', 'locale'];
    protected $guarded = ['created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parents()
    {
        return $this->belongsTo('App\Models\Modules\Content\PostsModel', 'entry_id');
    }
}
