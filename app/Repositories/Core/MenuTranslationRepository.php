<?php
/**
 * Description: MenuTranslationRepository.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     19/09/2018, modified: 19/09/2018 01:22
 * @copyright   Copyright (c) 2018.
 */

namespace App\Repositories\Core;


use App\Models\Core\MenuTranslationModel;
use App\Repositories\BaseRepository;

class MenuTranslationRepository extends BaseRepository
{

    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    public function model()
    {
        return MenuTranslationModel::class;
    }
}