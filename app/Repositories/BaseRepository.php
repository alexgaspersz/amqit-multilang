<?php
/**
 * Description: BaseRepository.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     16/03/2018, modified: 17/03/2018 01:57
 * @copyright   Copyright (c) 2018.
 */

namespace App\Repositories;

use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseRepository.
 */
abstract class BaseRepository implements RepositoryInterface
{
    /**
     * The repository model.
     *
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * The query builder.
     *
     * @var \Illuminate\Database\Eloquent\Builder
     */
    protected $query;

    /**
     * Alias for the query limit.
     *
     * @var int
     */
    protected $take;

    /**
     * Array of related models to eager load.
     *
     * @var array
     */
    protected $with = [];

    /**
     * Array of one or more where clause parameters.
     *
     * @var array
     */
    protected $wheres = [];

    /**
     * Group array by column clause parameters.
     * @var array
     */
    protected $groups = [];

    /**
     * Array of one or more where clause parameters.
     * @var array
     */
    protected $wheresLike = [];

    /**
     * Array of one or more where clause parameters.
     * @var array
     */
    protected $orWheresLike = [];

    /**
     * Array of one or more where clause parameters.
     * @var array
     */
    protected $orWheres = [];

    /**
     * Array of one or more where clause parameters.
     *
     * @var array
     */
    protected $wheresNull = [];

    /**
     * Array of one or more where clause parameters.
     *
     * @var array
     */
    protected $wheresNotNull = [];

    /**
     * Array of one or more where in clause parameters.
     *
     * @var array
     */
    protected $whereIns = [];

    /**
     * Array of one or more where not in clause parameters.
     *
     * @var array
     */
    protected $whereNotIns = [];

    /**
     * Array of one or more ORDER BY column/value pairs.
     *
     * @var array
     */
    protected $orderBys = [];

    /**
     * Array of scope methods to call on the model.
     *
     * @var array
     */
    protected $scopes = [];

    /**
     * BaseRepository constructor.
     */
    public function __construct()
    {
        $this->makeModel();
    }

    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    abstract public function model();

    /**
     * @return Model|mixed
     * @throws GeneralException
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function makeModel()
    {
        $model = app()->make($this->model());

        if (! $model instanceof Model) {
            throw new GeneralException("Class {$this->model()} must be an instance of ".Model::class);
        }

        return $this->model = $model;
    }

    /**
     * Get all the model records in the database.
     *
     * @param array $columns
     *
     * @return Collection|static[]
     */
    public function all(array $columns = ['*'])
    {
        $this->newQuery()->eagerLoad();

        $models = $this->query->get($columns);

        $this->unsetClauses();

        return $models;
    }

    /**
     * Count the number of specified model records in the database.
     *
     * @return int
     */
    public function count() : int
    {
        return $this->get()->count();
    }

    /**
     * Create a new model record in the database.
     *
     * @param array $data
     * @param array $options
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $data, array $options = [])
    {
        $this->unsetClauses();

        return $this->model->create($data, $options);
    }

    /**
     * Create one or more new model records in the database.
     *
     * @param array $data
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function createMultiple(array $data)
    {
        $models = new Collection();

        foreach ($data as $d) {
            $models->push($this->create($d));
        }

        return $models;
    }

    /**
     * Delete one or more model records from the database.
     *
     * @return mixed
     */
    public function delete()
    {
        $this->newQuery()->setClauses()->setScopes();

        $result = $this->query->delete();

        $this->unsetClauses();

        return $result;
    }

    /**
     * Delete one or more model records from the database.
     *
     * @return mixed
     */
    public function forceDelete()
    {
        $this->newQuery()->setClauses()->setScopes();

        $result = $this->query->forceDelete();

        $this->unsetClauses();

        return $result;
    }

    /**
     * Delete the specified model record from the database.
     *
     * @param $id
     *
     * @return bool|null
     * @throws \Exception
     */
    public function deleteById($id) : bool
    {
        $this->unsetClauses();

        return $this->getById($id)->delete();
    }

    /**
     * Delete the specified model record from the database with where condition.
     *
     * @param $column
     * @param $id
     * @return bool
     */
    public function deleteWhere($column, $id) :bool
    {
        $this->unsetClauses();

        return $this->where($column,$id,'=')->delete();
    }

    /**
     * Delete multiple records.
     *
     * @param array $ids
     *
     * @return int
     */
    public function deleteMultipleById(array $ids) : int
    {
        return $this->model->destroy($ids);
    }

    /**
     * Force Delete multiple records.
     * @param       $column
     * @param array $ids
     *
     * @return bool
     */
    public function forceDeleteMultiple($column, array $ids): bool
    {
        $this->unsetClauses();

        return $this->whereIn($column, $ids)->forceDelete();
    }

    /**
     * Get the first specified model record from the database.
     *
     * @param array $columns
     *
     * @return Model|static
     */
    public function first(array $columns = ['*'])
    {
        $this->newQuery()->eagerLoad()->setClauses()->setScopes();

        $model = $this->query->firstOrFail($columns);

        $this->unsetClauses();

        return $model;
    }

    /**
     * Get all the specified model records in the database.
     *
     * @param array $columns
     *
     * @return Collection|static[]
     */
    public function get(array $columns = ['*'])
    {
        $this->newQuery()->eagerLoad()->setClauses()->setScopes();

        $models = $this->query->get($columns);

        $this->unsetClauses();

        return $models;
    }

    /**
     * Get all soft deleted records in the database.
     * @param array $columns
     *
     * @return mixed
     */
    public function getTrashed(array $columns = ['*'])
    {
        $this->newQuery()->eagerLoad()->setClauses()->setScopes();

        $models = $this->query->onlyTrashed()->get($columns);;

        $this->unsetClauses();

        return $models;
    }

    /**
     * Get single soft deleted records in the database.
     * @return mixed
     */
    public function withTrashed()
    {
        $this->newQuery()->eagerLoad()->setClauses()->setScopes();

        $models = $this->query->withTrashed();

        $this->unsetClauses();

        return $models;
    }

    /**
     * Get list of specified model records in the database.
     *
     * @param string $columns
     * @param int|null $key
     *
     * @return \Illuminate\Support\Collection
     */
    public function lists(string $columns, int $key = null)
    {
        $this->newQuery()->eagerLoad()->setClauses()->setScopes();

        $models = $this->query->pluck($columns, $key);

        $this->unsetClauses();

        return $models;
    }

    /**
     * Get all the specified model records in the database.
     * @return mixed
     */
    public function queryGet()
    {
        $this->newQuery()->eagerLoad();
        $this->newQuery()->eagerLoad()->setClauses()->setScopes();

        $models = $this->query->get();

        $this->unsetClauses();

        return $models;
    }

    /**
     * Get the specified model record from the database.
     *
     * @param       $id
     * @param array $columns
     *
     * @return Collection|Model
     */
    public function getById($id, array $columns = ['*'])
    {
        $this->unsetClauses();

        $this->newQuery()->eagerLoad();

        return $this->query->findOrFail($id, $columns);
    }

    /**
     * Get multiple data record from the database.
     * @param array $ids
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|Collection
     */
    public function getMultipleById(array $ids)
    {
        $this->newQuery()->setClauses()->setScopes();

        $result = $this->query->whereIn('id', $ids)->get();

        $this->unsetClauses();

        return $result;
    }

    /**
     * Get the specified model record from the database.
     * @param $id
     *
     * @return mixed
     */
    public function getTrashedById($id)
    {
        $this->unsetClauses();

        $this->newQuery()->eagerLoad();

        return $this->query->withTrashed()->findOrFail($id);
    }

    /**
     * @param       $item
     * @param       $column
     * @param array $columns
     *
     * @return Model|null|static
     */
    public function getByColumn($item, $column, array $columns = ['*'])
    {
        $this->unsetClauses();

        $this->newQuery()->eagerLoad();

        return $this->query->where($column, $item)->first($columns);
    }

    /**
     * @param array $columns
     * @param array $fields
     *
     * @return BaseRepository[]|Collection
     */
    public function getByColumns(array $columns = [], array $fields = ['*'])
    {
        $this->unsetClauses();

        $this->newQuery()->eagerLoad();

        foreach ($columns as $key => $val) {
            $result = $this->where($key, $val);
        }

        /** @var TYPE_NAME $result */
        return $result->get($fields);
    }

    /**
     * @param $keyword
     * @return mixed
     */
    public function getBySearch(string $keyword)
    {
        $this->unsetClauses();

        $this->newQuery()->eagerLoad();

        $models = $this->query->search($keyword)->get();

        return $models;
    }

    /**
     * @param $slug
     * @return Collection|Model
     */
    public function getBySlug(string $slug)
    {
        $this->unsetClauses();

        $this->newQuery()->eagerLoad();

        return $this->query->whereSlug($slug)->findOrFail();
    }

    /**
     * @param string      $field
     * @param array       $columns
     * @param string|null $order
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|Collection
     */
    public function getRowActive($field = 'status', array $columns = ['*'], string $order = 'id')
    {
        $this->newQuery()->eagerLoad()->setClauses()->setScopes();

        $models = $this->query->where($field, true)->orderBy($order, 'DESC')->get($columns);

        $this->unsetClauses();

        return $models;
    }

    /**
     * @param array $params
     * @return \Illuminate\Database\Eloquent\Builder[]|Collection|\Illuminate\Database\Query\Builder[]|\Illuminate\Support\Collection
     */
    public function getIgnoreKey(array $params = [])
    {
        $this->unsetClauses();

        $this->newQuery()->eagerLoad();

        if (!$params['column']){
            $keyTable = $this->model->getKeyName();
        } else {
            $keyTable = $params['column'];
        }

        $arrKey = [];
        if ($params['key']){
            $arrKey = is_array($params['key']) ? $params['key'] : [$params['key']];
        }

        return $this->query->where($params['field'], '=', $params['val'])->whereNotIn($keyTable, $arrKey)->get();
    }

    /**
     * @return string
     */
    public function getPrimaryKey()
    {
        $this->unsetClauses();

        $this->newQuery()->eagerLoad();

        return $this->model->getKeyName();
    }

    /**
     * @param null   $sort
     * @param string $dir
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|Collection
     */
    public function getTranslation($sort = null, $dir = 'desc')
    {
        $this->unsetClauses();

        $this->newQuery()->eagerLoad();

        if (!$sort){
            $key = $this->model->getKeyName();
        } else {
            $key = $sort;
        }

        $models = $this->query->with(['single_translate' => function ($query) {
            $query->whereLocale(config('app.locale'));
        }])->orderBy($key,$dir)->get();

        return $models;
    }

    /**
     * @param string $keyword
     *
     * @return mixed
     */
    public function getTranslationBySearch(string $keyword)
    {
        $this->unsetClauses();

        $this->newQuery()->eagerLoad();

        $models = $this->query->with(['single_translate' => function ($query) {
            $query->whereLocale(config('app.locale'));
        }])->search($keyword)->get();

        return $models;
    }

    /**
     * @param int    $limit
     * @param null   $sort
     * @param string $dir
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getTranslationPaginate(int $limit = 10, $sort = null, $dir = 'desc')
    {
        $this->unsetClauses();

        $this->newQuery()->eagerLoad();

        if (!$sort) {
            $key = $this->model->getKeyName();
        } else {
            $key = $sort;
        }

        return $this->query->with(['single_translate' => function ($query) {
            $query->whereLocale(config('app.locale'));
        }])->orderBy($key, $dir)->paginate($limit);
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|Collection|Model
     */
    public function getTranslationById(int $id)
    {
        $this->newQuery()->eagerLoad()->setClauses()->setScopes();

        $models = $this->query->with(['single_translate' => function ($query) {
            $query->whereLocale(config('app.locale'));
        }])->findOrFail($id);

        $this->unsetClauses();

        return $models;
    }

    /**
     * @param string $slug
     *
     * @return mixed
     */
    public function getTranslationBySlug(string $slug)
    {
        $this->newQuery()->eagerLoad()->setClauses()->setScopes();

        $models = $this->query->with(['single_translate' => function ($query) {
            $query->whereLocale(config('app.locale'));
        }])->whereSlug($slug)->first();

        $this->unsetClauses();

        return $models;
    }

    /**
     * @param $key
     * @param $val
     * @return \Illuminate\Database\Eloquent\Builder[]|Collection
     */
    public function getTranslationByColumn($key, $val)
    {
        $this->newQuery()->eagerLoad()->setClauses()->setScopes();

        $models = $this->query->with(['single_translate' => function ($query) {
            $query->whereLocale(config('app.locale'));
        }])->where($key, $val)->get();

        $this->unsetClauses();

        return $models;
    }

    /**
     * @param array $columns
     * @param array $fields
     * @param array $ignore
     *
     * @return BaseRepository[]|Collection
     */
    public function getTranslationByColumns(array $columns = [], array $fields = ['*'], array $ignore = [])
    {
        $this->unsetClauses();

        $this->newQuery()->eagerLoad();

        $query = $this->query->with(['single_translate' => function ($query) {
            $query->whereLocale(config('app.locale'));
        }]);

        if ($ignore){
            if (!$ignore['column']) {
                $keyTable = $this->model->getKeyName();
            } else {
                $keyTable = $ignore['column'];
            }

            if ($ignore['key']){
                $arrKey = is_array($ignore['key']) ? $ignore['key'] : [$ignore['key']];
                $query = $this->whereNotIn($keyTable, $arrKey);
            }
        }

        foreach ($columns as $key => $val) {
            $query = $this->where($key, $val);
        }

        return $query->get($fields);
    }

    /**
     * @param array $columns
     * @param int   $limit
     * @param array $ignore
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getTranslationPaginateByColumns(array $columns = [], int $limit = 10, array $ignore = [])
    {
        $this->unsetClauses();

        $this->newQuery()->eagerLoad();

        $query = $this->query->with(['single_translate' => function ($query) {
            $query->whereLocale(config('app.locale'));
        }]);

        if ($ignore) {
            if (!$ignore['column']) {
                $keyTable = $this->model->getKeyName();
            } else {
                $keyTable = $ignore['column'];
            }

            if ($ignore['key']) {
                $arrKey = is_array($ignore['key']) ? $ignore['key'] : [$ignore['key']];
                $query = $this->whereNotIn($keyTable, $arrKey);
            }
        }

        foreach ($columns as $key => $val) {
            $query = $this->where($key, $val);
        }

        return $query->paginate($limit);
    }

    /**
     * @param array $params
     * @return \Illuminate\Database\Eloquent\Builder[]|Collection|\Illuminate\Database\Query\Builder[]|\Illuminate\Support\Collection
     */
    public function getTranslationIgnoreKey(array $params = [])
    {
        $this->unsetClauses();

        $this->newQuery()->eagerLoad();

        if (!$params['column']){
            $keyTable = $this->model->getKeyName();
        } else {
            $keyTable = $params['column'];
        }

        $arrKey = [];
        if ($params['key']){
            $arrKey = is_array($params['key']) ? $params['key'] : [$params['key']];
        }

        return $this->query->with(['single_translate' => function ($query) {
            $query->whereLocale(config('app.locale'));
        }])->where($params['field'],'=',$params['val'])->whereNotIn($keyTable,$arrKey)->get();
    }

    /**
     * @param int    $limit
     * @param array  $columns
     * @param string $pageName
     * @param null   $page
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginate($limit = 25, array $columns = ['*'], $pageName = 'page', $page = null)
    {
        $this->newQuery()->eagerLoad()->setClauses()->setScopes();

        $models = $this->query->paginate($limit, $columns, $pageName, $page);

        $this->unsetClauses();

        return $models;
    }

    /**
     * Update the specified model record in the database.
     *
     * @param       $id
     * @param array $data
     * @param array $options
     *
     * @return Collection|Model
     */
    public function updateById($id, array $data, array $options = [])
    {
        $this->unsetClauses();

        $model = $this->getById($id);

        $model->update($data, $options);

        return $model;
    }

    /**
     * @param array $ids
     * @param array $data
     *
     * @return int
     */
    public function updateBatch(array $ids, array $data)
    {
        $this->newQuery()->setClauses()->setScopes();

        $result = $this->query->whereIn('id', $ids)->update($data);

        $this->unsetClauses();

        return $result;
    }

    /**
     * Update the specified model column record in the database.
     *
     * @param       $column
     * @param       $id
     * @param array $data
     * @param array $options
     *
     * @return Collection|Model
     */
    public function updateByColumn($column, $id, array $data, array $options = [])
    {
        $this->unsetClauses();

        $model = $this->getByColumn($id,$column);

        $model->update($data, $options);

        return $model;
    }

    /**
     * Set the query limit.
     *
     * @param int $limit
     *
     * @return $this
     */
    public function limit($limit)
    {
        $this->take = $limit;

        return $this;
    }

    /**
     * Set an ORDER BY clause.
     *
     * @param string $column
     * @param string $direction
     * @return $this
     */
    public function orderBy($column, $direction = 'asc')
    {
        $this->orderBys[] = compact('column', 'direction');

        return $this;
    }

    /**
     * Set an GROUP BY clause.
     *
     * @param string $column
     * @return $this
     */
    public function groupBy($column)
    {
        $this->groups[] = compact('column');

        return $this;
    }

    /**
     * Add a simple where clause to the query.
     *
     * @param string $column
     * @param string $value
     * @param string $operator
     *
     * @return $this
     */
    public function where($column, $value, $operator = '=')
    {
        $this->wheres[] = compact('column', 'value', 'operator');

        return $this;
    }

    /**
     * Add a simple where like clause to the query.
     *
     * @param $column
     * @param $value
     *
     * @return $this
     */
    public function whereLike($column, $value)
    {
        $this->wheresLike[] = compact('column', 'value');

        return $this;
    }

    /**
     * Add a simple or where like clause to the query.
     *
     * @param $column
     * @param $value
     *
     * @return $this
     */
    public function orWhereLike($column, $value)
    {
        $this->orWheresLike[] = compact('column', 'value');

        return $this;
    }

    /**
     * Add a simple or where clause to the query.
     *
     * @param string $column
     * @param string $value
     * @param string $operator
     *
     * @return $this
     */
    public function orWhere($column, $value, $operator = '=')
    {
        $this->orWheres[] = compact('column', 'value', 'operator');

        return $this;
    }

    /**
     * Add a simple or where is null clause to the query.
     *
     * @param string $column
     *
     * @return $this
     */
    public function whereNull($column)
    {
        $this->wheresNull[] = compact('column');

        return $this;
    }

    /**
     * Add a simple or where is not null clause to the query.
     *
     * @param string $column
     *
     * @return $this
     */
    public function whereNotNull($column)
    {
        $this->wheresNotNull[] = compact('column');

        return $this;
    }

    /**
     * Add a simple where in clause to the query.
     *
     * @param string $column
     * @param mixed  $values
     *
     * @return $this
     */
    public function whereIn($column, $values)
    {
        $values = is_array($values) ? $values : [$values];

        $this->whereIns[] = compact('column', 'values');

        return $this;
    }

    /**
     * Add a simple where not in clause to the query.
     *
     * @param string $column
     * @param mixed  $values
     *
     * @return $this
     */
    public function whereNotIn($column, $values)
    {
        $values = is_array($values) ? $values : [$values];

        $this->whereNotIns[] = compact('column', 'values');

        return $this;
    }

    /**
     * Set Eloquent relationships to eager load.
     *
     * @param $relations
     *
     * @return $this
     */
    public function with($relations)
    {
        if (is_string($relations)) {
            $relations = func_get_args();
        }

        $this->with = $relations;

        return $this;
    }

    /**
     * Create a new instance of the model's query builder.
     *
     * @return $this
     */
    protected function newQuery()
    {
        $this->query = $this->model->newQuery();

        return $this;
    }

    /**
     * Add relationships to the query builder to eager load.
     *
     * @return $this
     */
    protected function eagerLoad()
    {
        foreach ($this->with as $relation) {
            $this->query->with($relation);
        }

        return $this;
    }

    /**
     * Set clauses on the query builder.
     *
     * @return $this
     */
    protected function setClauses()
    {
        foreach ($this->wheres as $where) {
            $this->query->where($where['column'], $where['operator'], $where['value']);
        }

        foreach ($this->wheresLike as $like) {
            $this->query->where($like['column'], 'ilike', "%{$like['value']}%");
        }

        foreach ($this->orWheresLike as $like) {
            $this->query->orWhere($like['column'], 'ilike', "%{$like['value']}%");
        }

        foreach ($this->orWheres as $orWhere) {
            $this->query->orWhere($orWhere['column'], $orWhere['operator'], $orWhere['value']);
        }

        foreach ($this->wheresNull as $whereNull) {
            $this->query->whereNull($whereNull['column']);
        }

        foreach ($this->wheresNotNull as $whereNotNull) {
            $this->query->whereNotNull($whereNotNull['column']);
        }

        foreach ($this->whereIns as $whereIn) {
            $this->query->whereIn($whereIn['column'], $whereIn['values']);
        }

        foreach ($this->whereNotIns as $whereNotIn) {
            $this->query->whereNotIn($whereNotIn['column'], $whereNotIn['values']);
        }

        foreach ($this->orderBys as $orders) {
            $this->query->orderBy($orders['column'], $orders['direction']);
        }

        foreach ($this->groups as $column) {
            $this->query->groupBy($column['column']);
        }

        if (isset($this->take) and ! is_null($this->take)) {
            $this->query->take($this->take);
        }

        return $this;
    }

    /**
     * Set query scopes.
     *
     * @return $this
     */
    protected function setScopes()
    {
        foreach ($this->scopes as $method => $args) {
            $this->query->$method(implode(', ', $args));
        }

        return $this;
    }

    /**
     * Reset the query clause parameter arrays.
     *
     * @return $this
     */
    protected function unsetClauses()
    {
        $this->wheres = [];
        $this->wheresLike = [];
        $this->orWheresLike = [];
        $this->orWheres = [];
        $this->wheresNull = [];
        $this->wheresNotNull = [];
        $this->whereIns = [];
        $this->whereNotIns = [];
        $this->scopes = [];
        $this->take = null;

        return $this;
    }
}
