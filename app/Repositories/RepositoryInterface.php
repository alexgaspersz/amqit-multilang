<?php
/**
 * Description: RepositoryInterface.phpp PhpStorm.
 * @package     amqit-mutilang
 * @author      alex
 * @created     16/03/2018, modified: 19/03/2018 02:42
 * @copyright   Copyright (c) 2018.
 */

namespace App\Repositories;

/**
 * Interface RepositoryInterface.
 */
interface RepositoryInterface
{
    public function all(array $columns = ['*']);

    public function count();

    public function create(array $data);

    public function createMultiple(array $data);

    public function delete();

    public function forceDelete();

    public function deleteById($id);

    public function deleteWhere($columns, $id);

    public function forceDeleteMultiple($columns, array $ids);

    public function deleteMultipleById(array $ids);

    public function first(array $columns = ['*']);

    public function get(array $columns = ['*']);

    public function getTrashed(array $columns = ['*']);

    public function lists(string $columns, int $key = null);

    public function queryGet();

    public function getById($id, array $columns = ['*']);

    public function getMultipleById(array $ids);

    public function getTrashedById($id);

    public function getByColumn($item, $column, array $columns = ['*']);

    public function getByColumns(array $columns = [], array $fields = ['*']);

    public function getBySearch(string $keyword);

    public function getBySlug(string $slug);

    public function getPrimaryKey();

    public function getTranslation($sort = null, $dir = null);

    public function getTranslationBySearch(string $keyword);

    public function getTranslationPaginate(int $limit, $sort = null, $dir = 'desc');

    public function getTranslationById(int $id);

    public function getTranslationBySlug(string $slug);

    public function getTranslationByColumn($key, $val);

    public function getTranslationByColumns(array $columns = [], array $fields = ['*']);

    public function getTranslationIgnoreKey(array $params = []);

    public function getRowActive($field, array $columns = ['*']);

    public function getIgnoreKey(array $params = []);

    public function paginate($limit = 25, array $columns = ['*'], $pageName = 'page', $page = null);

    public function updateById($id, array $data, array $options = []);

    public function updateBatch(array $ids, array $data);

    public function updateByColumn($column, $id, array $data, array $options = []);

    public function limit($limit);

    public function orderBy($column, $value);

    public function groupBy($column);

    public function where($column, $value, $operator = '=');

    public function whereLike($column, $value);

    public function orWhereLike($column, $value);

    public function orWhere($column, $value, $operator = '=');

    public function whereNull($column);

    public function whereNotNull($column);

    public function whereIn($column, $value);

    public function whereNotIn($column, $value);

    public function with($relations);
}
