<?php
/**
 * Filename: PageRepository.php
 * Package: amqit-mutilang
 * Author: alex
 * Created: 11/8/18 10:10 PM
 */

namespace App\Repositories\Modules\Content;


use App\Models\Modules\Content\PagesModel;
use App\Repositories\BaseRepository;

class PageRepository extends BaseRepository
{

    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    public function model()
    {
        return PagesModel::class;
    }
}