<?php
/**
 * Filename: PostTranslationRepository.php
 * Package: amqit-mutilang
 * Author: alex
 * Created: 11/9/18 3:15 PM
 */

namespace App\Repositories\Modules\Content;


use App\Models\Modules\Content\PostsTranslationModel;
use App\Repositories\BaseRepository;

class PostTranslationRepository extends BaseRepository
{

    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    public function model()
    {
        return PostsTranslationModel::class;
    }
}