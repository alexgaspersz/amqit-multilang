<?php
/**
 * Filename: PostRepository.php
 * Package: amqit-mutilang
 * Author: alex
 * Created: 11/9/18 3:15 PM
 */

namespace App\Repositories\Modules\Content;


use App\Models\Modules\Content\PostsModel;
use App\Repositories\BaseRepository;

class PostRepository extends BaseRepository
{

    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    public function model()
    {
        return PostsModel::class;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model[]
     */
    public function gridData()
    {
        return $this->model->with(['single_translate' => function ($query) {
            $query->whereLocale(config('app.locale'));
        }])->orderBy('publish_at', 'desc')->get();
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function searchById($id)
    {
        return $this->model->with(['single_translate' => function ($query) {
            $query->whereLocale(config('app.locale'));
        }])->find($id);
    }

    /**
     * @return mixed
     */
    public function translation()
    {
        return $this->model->translation();
    }

    /**
     * @return mixed
     */
    public function single_translation()
    {
        return $this->model->single_translate();
    }

    /**
     * @param int $val
     * @param int $limit
     * @return mixed
     */
    public function getCategoryPostAttribute(int $val, int $limit = 16)
    {
        return $this->model->with(['single_translate' => function ($query) {
            $query->whereLocale(config('app.locale'));
        }])
            ->whereStatus(2)
            ->where('categories', $val)
            ->whereRaw('ms_post.publish_at <= NOW()')
            ->orderBy('publish_at', 'DESC')
            ->paginate($limit);
    }

    /**
     * @param int $val
     * @param int $limit
     * @param $slug
     * @return mixed
     */
    public function getCategoryOtherAttribute(int $val, string $slug, int $limit = 16)
    {
        return $this->model->with(['single_translate' => function ($query) {
            $query->whereLocale(config('app.locale'));
        }])
            ->whereStatus(2)
            ->where('slug','!=',$slug)
            ->where('categories', $val)
            ->whereRaw('ms_post.publish_at <= NOW()')
            ->orderBy('publish_at', 'DESC')
            ->paginate($limit);
    }

    /**
     * @param int $limit
     *
     * @return mixed
     */
    public function getRecentNewsAttribute($limit = 6)
    {
        return $this->model->with(['single_translate' => function ($query) {
            $query->whereLocale(config('app.locale'));
        }])
            ->whereStatus(2)
            ->whereRaw('ms_post.publish_at <= NOW()')
            ->orderBy('publish_at', 'DESC')
            ->paginate($limit);
    }

    /**
     * @param int $limit
     * @return mixed
     */
    public function getRecentPost($limit = 5)
    {
        return $this->model->with(['single_translate' => function ($query) {
            $query->whereLocale(config('app.locale'));
        }])->whereStatus(2)
            ->limit($limit)
            ->whereRaw('ms_post.publish_at <= NOW()')
            ->orderBy('publish_at', 'DESC')
            ->get();
    }

    /**
     * @param     $slug
     * @param int $limit
     * @return mixed
     */
    public function getOtherPost($slug, $limit = 5)
    {
        return $this->model->with(['single_translate' => function ($query) {
            $query->whereLocale(config('app.locale'));
        }])->whereStatus(2)
            ->where('slug','!=',$slug)
            ->limit($limit)
            ->whereRaw('ms_post.publish_at <= NOW()')
            ->orderBy('publish_at', 'DESC')
            ->get();
    }

    /**
     * @param int $limit
     * @return mixed
     */
    public function getRecentAdd($limit = 5)
    {
        return $this->model->with(['single_translate' => function ($query) {
            $query->whereLocale(config('app.locale'));
        }])->limit($limit)
            ->orderBy('publish_at', 'DESC')
            ->get();
    }

    /**
     * @param int $limit
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model[]
     */
    public function getPopular($limit = 5)
    {
        return $this->model->with(['single_translate' => function ($query) {
            $query->whereLocale(config('app.locale'));
        }])->limit($limit)
            ->orderBy('hit', 'DESC')
            ->get();
    }
}
