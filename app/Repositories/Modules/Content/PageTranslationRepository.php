<?php
/**
 * Filename: PageTranslationRepository.php
 * Package: amqit-mutilang
 * Author: alex
 * Created: 11/8/18 10:47 PM
 */

namespace App\Repositories\Modules\Content;


use App\Models\Modules\Content\PagesTranslationModel;
use App\Repositories\BaseRepository;

class PageTranslationRepository extends BaseRepository
{

    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    public function model()
    {
        return PagesTranslationModel::class;
    }
}