<?php
/**
 * Filename: CategoryTranslationRepository.php
 * Package: amqit-mutilang
 * Author: alex
 * Created: 11/9/18 3:00 PM
 */

namespace App\Repositories\Modules\Content;


use App\Models\Modules\Content\CategoryTranslationModel;
use App\Repositories\BaseRepository;

class CategoryTranslationRepository extends BaseRepository
{

    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    public function model()
    {
        return CategoryTranslationModel::class;
    }
}