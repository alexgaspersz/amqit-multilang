<?php
/**
 * Filename: TagsRepository.php
 * Package: amqit-mutilang
 * Author: alex
 * Created: 11/9/18 5:32 PM
 */

namespace App\Repositories\Modules\Content;


use App\Models\Modules\Content\TagsModel;
use App\Repositories\BaseRepository;

class TagsRepository extends BaseRepository
{

    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    public function model()
    {
        return TagsModel::class;
    }
}