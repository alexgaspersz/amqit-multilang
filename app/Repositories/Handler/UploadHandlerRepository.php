<?php
/**
 * Description: UploadHandlerRepository.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     23/04/2018, modified: 23/04/2018 00:34
 * @copyright   Copyright (c) 2018.
 */

namespace App\Repositories\Handler;


use App\Models\Services\UploadHandlerModel;
use App\Repositories\BaseRepository;

class UploadHandlerRepository extends BaseRepository
{

    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    public function model()
    {
        return UploadHandlerModel::class;
    }

}