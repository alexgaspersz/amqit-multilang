<?php
/**
 * Description: SendContact.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     05/03/2018, modified: 05/03/2018 21:41
 * @copyright   Copyright (c) 2018.
 */

namespace App\Mail\Frontend;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class SendContact
 *
 * @package App\Mail\Frontend
 */
class SendContact extends Mailable
{
    use Queueable, SerializesModels;

    public $request;

    /**
     * SendContact constructor.
     * @param $request
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to(app_email(), app_name())
            ->view('frontend.' . app_themes_front() . '.templates.mail-contact')
            ->subject($this->request->subject)
            ->from($this->request->email, $this->request->from);
    }
}
