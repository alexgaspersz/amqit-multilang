<?php
/**
 * Filename: ReplyContact.php
 * Package: pt-sma
 * Author: alex
 * Created: 11/29/18 11:12 PM
 */

namespace App\Mail\Backend;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class ReplyContact
 *
 * @package App\Mail\Backend
 */
class ReplyContact extends Mailable
{
    use Queueable, SerializesModels;

    public $request;

    /**
     * ReplyContact constructor.
     *
     * @param $request
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->request->email, $this->request->from)
            ->view('frontend.' . app_themes_front() . '.templates.mail-reply')
            ->subject($this->request->subject)
            ->from(config('mail.from.address'), config('mail.from.name'));
    }
}
