<?php
/**
 * Description: System helpers file with misc functions.
 * @package     amqit-mdb
 * @author      alex
 * @created     10/03/2020, modified: 11/03/2020 03:05
 * @copyright   Copyright (c) 2020.
 */

use App\Repositories\Core\VisitorRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

if (!function_exists('include_files_in_folder')) {
    /**
     * Loops through a folder and requires all PHP files
     * Searches sub-directories as well.
     *
     * @param $folder
     */
    function include_files_in_folder($folder)
    {
        try {
            $rdi = new RecursiveDirectoryIterator($folder);
            $it = new RecursiveIteratorIterator($rdi);

            while ($it->valid()) {
                if (!$it->isDot() && $it->isFile() && $it->isReadable() && $it->current()->getExtension() === 'php') {
                    require $it->key();
                }

                $it->next();
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}

if (!function_exists('include_route_files')) {
    /**
     * Loops through a folder and requires all PHP files
     * Searches sub-directories as well.
     *
     * @param $folder
     */
    function include_route_files($folder)
    {
        include_files_in_folder($folder);
    }
}

if (!function_exists('text_limit')) {
    /**
     * @param        $string
     * @param int    $limit
     * @param string $end
     *
     * @return string
     */
    function text_limit($string, $limit = 100, $end = '...')
    {
        return Str::limit($string, $limit, $end);
    }
}

if (!function_exists('visitorLog')) {
    /**
     * Visitor log
     */
    function visitorLog()
    {
        $repository = app(VisitorRepository::class);
        $request = app(Request::class);
        $existing = $repository->countExisting($request);
        if ($existing < 1) {
            $data = [
                'ipaddress' => $request->getClientIp(),
                'useragent' => $request->header('User-Agent'),
                'created_at' => Carbon::now()
            ];
            $repository->create($data);
        }
    }
}

