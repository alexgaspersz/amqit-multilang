<?php
/**
 * Description: General helpers file with misc functions.
 * @package     amqit-mdb
 * @author      alex
 * @created     11/03/2020, modified: 11/03/2020 03:04
 * @copyright   Copyright (c) 2020.
 */

if (!function_exists('app_name')) {
    /**
     * Helper to grab the application name.
     * @return mixed
     */
    function app_name()
    {
        return config('app.name');
    }
}

if (!function_exists('admin_route')) {
    /**
     * Return the route to the "home" page depending on authentication/authorization status.
     * @return string
     */
    function admin_route()
    {
        if (auth()->check()) {
            return 'admin.dashboard.index';
        }

        return 'admin.auth.login';
    }
}

if (!function_exists('web_route')) {
    /**
     * Return the route to the "web" page depending on authentication/authorization status.
     * @return string
     */
    function web_route()
    {
        if (auth()->check()) {
            return 'frontend.account.index';
        }

        return 'frontend.index';
    }
}

if (!function_exists('app_version')) {
    /**
     * Helper to grab the application version.
     * @return string
     */
    function app_version()
    {
        return config('app.name') . ' <small>' . config('general.app.version') . '</small>';
    }
}

if (!function_exists('app_title')) {
    /**
     * Helper to grab the application title.
     * @return \Illuminate\Config\Repository|mixed
     */
    function app_title()
    {
        return config('general.app.title');
    }
}

if (!function_exists('app_copyright')) {
    /**
     * Helper to grab the application copyright.
     * @return \Illuminate\Config\Repository|mixed
     */
    function app_copyright()
    {
        return trans('strings.copyright', ['year' => date('Y'), 'app' => app_title()]);
    }
}

if (!function_exists('app_company')) {
    /**
     * Helper to grab the company name.
     * @return \Illuminate\Config\Repository|mixed
     */
    function app_company()
    {
        return config('general.app.company');
    }
}

if (!function_exists('app_desc')) {
    /**
     * Helper to grab the application description.
     * @return \Illuminate\Config\Repository|mixed
     */
    function app_desc()
    {
        return config('general.app.description');
    }
}

if (!function_exists('app_email')) {
    /**
     * Helper to grab the application email.
     * @return \Illuminate\Config\Repository|mixed
     */
    function app_email()
    {
        return config('general.app.email');
    }
}

if (!function_exists('app_lang')) {
    /**
     * Helper to grab the application language.
     * @return \Illuminate\Config\Repository|mixed
     */
    function app_lang()
    {
        return config('general.lang.multi');
    }
}

if (!function_exists('app_compiled')) {
    /**
     * Helper to grab the compiled asset.
     *
     * @param $var
     *
     * @return string
     */
    function app_compiled($var)
    {
        return asset('build/' . $var);
    }
}

if (!function_exists('app_build')) {
    /**
     * Helper to grab the compiled asset.
     * @return string
     */
    function app_build()
    {
        return 'build/';
    }
}

if (!function_exists('app_images')) {
    /**
     * Helper to grab the compiled asset.
     *
     * @param $var
     *
     * @return string
     */
    function app_images($var)
    {
        return asset(app_build() . 'images/' . $var);
    }
}

if (!function_exists('app_vendor')) {
    /**
     * Helper to grab the compiled asset.
     *
     * @param $var
     *
     * @return string
     */
    function app_vendor($var)
    {
        return asset(app_build() . 'vendors/' . $var);
    }
}

if (!function_exists('app_js')) {
    /**
     * Helper to grab the compiled asset.
     *
     * @param $var
     *
     * @return string
     */
    function app_js($var)
    {
        return asset(app_build() . 'js/' . $var);
    }
}

if (!function_exists('app_mix')) {
    /**
     * Helper to grab the compiled asset.
     *
     * @param $var
     *
     * @return string
     * @throws Exception
     */
    function app_mix($var)
    {
        return mix(app_build() . $var);
    }
}

if (!function_exists('app_css')) {
    /**
     * Helper to grab the compiled asset.
     *
     * @param $var
     *
     * @return string
     */
    function app_css($var)
    {
        return asset(app_build() . 'css/' . $var);
    }
}


if (!function_exists('app_font')) {
    /**
     * Helper to grab the compiled asset.
     *
     * @param $var
     *
     * @return string
     */
    function app_font($var)
    {
        return asset(app_build() . 'fonts/' . $var);
    }
}

if (!function_exists('login_key')) {
    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    function login_key()
    {
        return config('general.login.key');
    }
}

if (!function_exists('defaultLang')) {
    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    function defaultLang()
    {
        return config('general.lang.main_lang');
    }
}

if (!function_exists('uploadFolder')) {
    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    function uploadFolder()
    {
        return config('general.files.upload');
    }
}

if (!function_exists('thumbSize')) {
    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    function thumbSize()
    {
        return config('general.files.thumbsize');
    }
}

if (!function_exists('imageSize')) {
    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    function imageSize()
    {
        return config('general.files.image_size');
    }
}

if (!function_exists('imageExt')) {
    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    function imageExt()
    {
        return config('general.files.image_extension');
    }
}

if (!function_exists('imageMime')) {
    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    function imageMime()
    {
        return config('general.files.image_mime');
    }
}

if (!function_exists('filesSize')) {
    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    function filesSize()
    {
        return config('general.files.file_size');
    }
}

if (!function_exists('filesExt')) {
    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    function filesExt()
    {
        return config('general.files.file_extension');
    }
}

if (!function_exists('filesMime')) {
    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    function filesMime()
    {
        return config('general.files.file_mime');
    }
}

if (!function_exists('videoSize')) {
    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    function videoSize()
    {
        return config('general.files.video_size');
    }
}

if (!function_exists('videoExt')) {
    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    function videoExt()
    {
        return config('general.files.video_extension');
    }
}

if (!function_exists('videoMime')) {
    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    function videoMime()
    {
        return config('general.files.video_mime');
    }
}

if (!function_exists('gravatar')) {
    /**
     * Access the gravatar helper.
     * @return \Illuminate\Contracts\Foundation\Application|mixed
     */
    function gravatar()
    {
        return app('gravatar');
    }
}

if (!function_exists('dateString')) {

    /**
     * Date format based on active locale
     *
     * @param      $date
     * @param bool $format
     * @param bool $time
     *
     * @return false|string
     */
    function dateString($date, $format = false, $time = false)
    {
        $result = $date;
        if (!empty($date)) {
            if (!$format) {
                if (config('app.locale') == 'id') {
                    $month = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
                    $day = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"];
                } else {
                    $month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                    $day = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                }
                $tahun = substr($date, 0, 4);
                $bulan = substr($date, 5, 2);
                $tgl = substr($date, 8, 2);
                $hari = $day[(int)date('N', strtotime($date)) - 1];
                $result = $hari . ", " . $tgl . " " . $month[(int)$bulan - 1] . " " . $tahun;
            } else {
                if ($time) {
                    $result = date('d/m/Y H:i', strtotime($date));
                } else {
                    $result = date('d/m/Y', strtotime($date));
                }
            }
        }
        return ($result);
    }
}

if (!function_exists('dateSortString')) {

    /**
     * Date format based on active locale
     *
     * @param $date
     *
     * @return string
     */
    function dateSortString($date)
    {
        $result = $date;
        if (!empty($date)) {
            $month = ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"];
            $tahun = substr($date, 0, 4);
            $bulan = substr($date, 5, 2);
            $tgl = substr($date, 8, 2);
            $result = $tgl . " " . $month[(int)$bulan - 1] . " " . $tahun;
        }
        return ($result);
    }
}

if (!function_exists('monthString')) {

    /**
     * Month format based on active locale
     *
     * @param $date
     *
     * @return string
     */
    function monthString($date)
    {
        $result = $date;
        if (!empty($date)) {
            if (config('app.locale') == 'id') {
                $month = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
            } else {
                $month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            }
            $tahun = substr($date, 0, 4);
            $bulan = substr($date, 5, 2);
            $result = $month[(int)$bulan - 1] . " " . $tahun;
        }
        return ($result);
    }
}

if (!function_exists('published')) {

    /**
     * @param $val
     *
     * @return mixed
     */
    function published($val)
    {
        $active = [
            0 => '<span class="badge badge-warning text-white">' . __('strings.common.status.draft') . '</span>',
            1 => '<span class="badge badge-info">' . __('strings.common.status.reviewed') . '</span>',
            2 => '<span class="badge badge-success">' . __('strings.common.status.published') . '</span>',
        ];
        return $active[$val];
    }

}

if (!function_exists('gender')) {

    /**
     * @param $val
     *
     * @return mixed
     */
    function gender($val)
    {
        $active = [
            null => '',
            'm' => __('forms.user.male'),
            'f' => __('forms.user.female'),
        ];
        return $active[$val];
    }

}

if (!function_exists('removeCommas')) {

    /**
     * @param $val
     *
     * @return int
     */
    function removeCommas($val)
    {
        return (int)preg_replace('/[^\d]/', '', $val);
    }
}

if (!function_exists('getThumbnail')) {
    /**
     * @param $img_path
     * @param $width
     * @param $height
     *
     * @return mixed
     */
    function getThumbnail($img_path, $width = 350, $height = 350)
    {
        return app('App\Http\Controllers\Api\UploadHandlerController')->getImageThumbnail($img_path, $width, $height);
    }
}

if (!function_exists('iconStatus')) {

    /**
     * @param $val
     *
     * @return mixed
     */
    function iconStatus($val)
    {
        $active = [
            0 => '<i class="fas fa-times-circle text-danger" data-toggle="tooltip" data-placement="top" data-original-title="' . __('strings.common.status.inactive') . '"></i>',
            1 => '<i class="fas fa-check-circle text-success" data-toggle="tooltip" data-placement="top" data-original-title="' . __('strings.common.status.active') . '"></i>',
        ];
        return $active[$val];
    }

}

if (!function_exists('privateArea')) {

    /**
     * @param $val
     *
     * @return mixed
     */
    function privateArea($val)
    {
        $active = [
            0 => '<i class="fas fa-times text-danger" data-toggle="tooltip" data-placement="top" data-original-title="' . __('strings.common.login_norequired') . '"></i>',
            1 => '<i class="fas fa-lock text-success" data-toggle="tooltip" data-placement="top" data-original-title="' . __('strings.common.login_required') . '"></i>',
        ];
        return $active[$val];
    }

}

if (!function_exists('active_menu')) {
    /**
     * Get the active class if an active path is provided.
     *
     * @param  mixed  $routes
     * @param  string $class
     *
     * @return string|null
     */
    function active_menu($routes = null, $class = null)
    {
        $request = (new Illuminate\Http\Request)->segment(2);
        if ($routes == $request) {
            $class = 'active';
        }
        return $class;
    }
}

if (!function_exists('encode_id')) {
    /**
     * make secure id
     *
     * @param $val
     *
     * @return string
     */
    function encode_id($val = '')
    {
        $params = ['val' => $val];
        $secure = preg_replace('/[=]+$/', '', base64_encode(serialize($params)));
        return $secure;
    }
}

if (!function_exists('decode_id')) {
    /**
     * decode encrypted id
     *
     * @param string
     *
     * @return int
     */
    function decode_id($val = '')
    {
        $secure = unserialize(base64_decode($val));
        return $secure['val'];
    }
}

if (!function_exists('slugify')) {
    /**
     * @param        $string
     * @param string $replacement
     *
     * @return string
     */
    function slugify($string, $replacement = '-')
    {
        $slug = preg_replace('/[^A-Za-z0-9-]+/', $replacement, $string);
        return strtolower($slug);
    }
}

if (!function_exists('unixTime')) {
    /**
     * @param $val
     *
     * @return string
     * @throws Exception
     */
    function unixTime($val)
    {
        $dt = new DateTime('@' . $val);
        $dt->setTimeZone(new DateTimeZone('Asia/Jakarta'));
        return $dt->format('F j, Y, g:i a');
    }
}

if (!function_exists('cleanString')) {
    /**
     * @param        $string
     * @param string $replacement
     *
     * @return string
     */
    function cleanString($string, $replacement = '')
    {
        $string = str_replace(' ', $replacement, $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', $replacement, $string); // Removes special chars.

        return strtolower(preg_replace('/-+/', $replacement, $string)); // Replaces multiple hyphens with single one.
    }
}

if (!function_exists('trimId')) {
    /**
     * @param $val
     *
     * @return array
     */
    function trimId($val)
    {
        $string = explode('+', $val);
        return $string;
    }
}

if (!function_exists('cleanSpace')) {
    /**
     * @param        $string
     * @param string $replacement
     *
     * @return string
     */
    function cleanSpace($string, $replacement = '')
    {
        return str_replace(' ', $replacement, $string);
    }
}

if (!function_exists('getActive')) {
    /**
     * @param $val
     *
     * @return string
     */
    function getActive($val)
    {
        $active = [
            0 => '<span class="badge badge-danger">' . __('strings.common.status.inactive') . '</span>',
            1 => '<span class="badge badge-success">' . __('strings.common.status.active') . '</span>',
            2 => '<span class="badge badge-warning text-white">' . __('strings.common.status.blocked') . '</span>',
        ];
        return $active[$val];
    }
}

if (!function_exists('taskLabel')) {
    /**
     * @param $val
     *
     * @return string
     */
    function taskLabel($val)
    {
        if ($val == 'store') {
            $task = 'save';
        } elseif ($val == 'save_permission') {
            $task = 'save group access';
        } elseif ($val == 'destroy') {
            $task = 'delete';
        } elseif ($val == 'batch') {
            $task = 'delete';
        } else {
            $task = $val;
        }

        return $task;
    }
}

if (!function_exists('formatBytes')) {

    /**
     * @param     $bytes
     * @param int $precision
     *
     * @return string
     */
    function formatBytes($bytes, $precision = 2)
    {
        $units = ['B', 'KB', 'MB', 'GB', 'TB'];
        if ($bytes) {
            $bytes = max($bytes, 0);
            $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
            $pow = min($pow, count($units) - 1);

            $bytes /= (1 << (10 * $pow));

            return round($bytes, $precision) . ' ' . $units[$pow];
        }
        return null;
    }

}

if (!function_exists('convertToNull')) {
    /**
     * @param $val
     *
     * @return string
     */
    function convertToNull($val)
    {
        return $val == 'null' ? null : $val;
    }
}
