<?php
/**
 * Description: Plugins helpers file with misc functions.
 * @package     amqit-mdb
 * @author      alex
 * @created     09/03/2020, modified: 09/03/2020 16:45
 * @copyright   Copyright (c) 2020.
 */

use App\Repositories\BaseRepository;
use \Jenssegers\Agent\Agent;

if (!function_exists('navigation')) {
    /**
     * Get menu by position
     *
     * @param $group
     *
     * @return null
     */
    function navigation($group)
    {
        $repository = app(\App\Repositories\Core\MenuRepository::class);
        if ($repository->count() > 0) {
            return $repository->getActiveByPosition($group);
        } else {
            return null;
        }
    }
}

if (!function_exists('logActivity')) {
    /**
     * @param $request
     * @param $note
     */
    function logActivity($request, $note)
    {
        $repository = app(\App\Repositories\Core\LogRepository::class);
        $data = [
            'module' => $request->route()->getAction('prefix'),
            'task' => taskLabel($request->route()->getActionMethod()),
            'user_id' => session('uid'),
            'ipaddress' => $request->getClientIp(),
            'useragent' => $request->header('User-Agent'),
            'note' => $note,
            'created_at' => \Carbon\Carbon::now()
        ];

        if (session('superuser') == false)
            $repository->create($data);
    }
}

if (!function_exists('permission')) {
    /**
     * @param        $access
     * @param        $key
     * @param bool   $view
     * @param string $method
     *
     * @return mixed
     */
    function permission($access, $key, $view = false, $method = 'menu')
    {
        $controller = app(\App\Http\Controllers\Backend\Core\GroupController::class);
        $data = $controller->permission($access, $key, $view, $method);
        return $data;
    }
}

if (!function_exists('access')) {
    /**
     * @param $access
     * @param $key
     *
     * @return bool
     */
    function access($access, $key)
    {
        $controller = app(\App\Http\Controllers\Backend\Core\GroupController::class);
        $data = $controller->privileged($access, $key);
        return $data;
    }
}

if (!function_exists('navigation_child')) {
    /**
     * @param $parent
     *
     * @return mixed
     */
    function navigation_child($parent)
    {
        $controller = app(\App\Http\Controllers\Backend\Core\MenuController::class);
        $menu = $controller->getChildNavigation($parent);
        return $menu;
    }
}

if (!function_exists('renderMenu')) {
    /**
     *  Helper to grab menu repository classes.
     * @return string
     */
    function renderMenu()
    {
        return app(\App\Http\Controllers\Backend\Core\MenuController::class);
    }
}

if (!function_exists('comboTree')) {

    /**
     * @param array $data
     * @param null  $id
     *
     * @return string|null
     */
    function comboTree($data, $id = null)
    {
        $html = null;
        foreach ($data as $key => $row) {
            if (is_array($id)) {
                $select = (in_array($key, $id)) ? 'selected' : '';
            } else {
                $select = ($id == $key) ? 'selected' : '';
            }
            $html .= '<option value="' . $key . '" ' . $select . '>' . htmlspecialchars($row, ENT_QUOTES) . '</option>';
        }
        return $html;
    }
}

if (!function_exists('renderClass')) {
    /**
     * @param BaseRepository $repository
     * @param string         $field
     *
     * @return array
     */
    function renderClass(BaseRepository $repository, string $field): array
    {
        $arr = [];
        $data = $repository->get();
        foreach ($data as $key => $row) {
            $arr[$row->id] = $row->$field;
        }
        return $arr;
    }
}

if (!function_exists('renderUser')) {
    /**
     * @param array $group
     *
     * @return array
     */
    function renderUser(array $group = []): array
    {
        $app = app(\App\Repositories\Core\UsersRepository::class);
        $arr = [];
        $data = $app->whereIn('group_id', $group)
            ->where('active', 1)
            ->where('is_core', false)
            ->orderBy('fullname')
            ->get();
        foreach ($data as $key => $row) {
            $arr[$row->user_id] = $row->fullname;
        }
        return $arr;
    }
}

if (!function_exists('implodeData')) {

    /**
     * @param BaseRepository $repository
     * @param string         $field
     * @param array|null     $arr
     *
     * @return string
     */
    function implodeData(BaseRepository $repository, string $field, array $arr = null): string
    {
        $_data = [];
        $data = $repository->whereIn('id', $arr)->get();
        foreach ($data as $key => $row) {
            $_data[] = $row->$field;
        }
        return implode($_data, ', ');
    }
}

if (!function_exists('recentPhotos')) {
    /**
     * @return array|null
     */
    function recentPhotos()
    {
        $repository = app(\App\Repositories\Modules\Gallery\PhotoRepository::class);
        if ($repository->count() > 0) {
            $data = $repository->getRowActive();
            $_data = [];
            if ($data) {
                foreach ($data as $key => $row) {
                    $_data = array_merge($_data, $row->getAssetsAttribute(350, 350));
                }
            }
            return $_data;
        } else {
            return null;
        }
    }
}

if (!function_exists('otherPost')) {
    /**
     * @param     $slug
     * @param int $limit
     *
     * @return null
     */
    function otherPost($slug, $limit = 10)
    {
        $repository = app(\App\Repositories\Modules\Content\PostRepository::class);
        if ($repository->count() > 0) {
            return $repository->getOtherPost($slug, $limit);
        } else {
            return null;
        }
    }
}

if (!function_exists('recentVideos')) {
    /**
     * @return array|null
     */
    function recentVideos()
    {
        $repository = app(\App\Repositories\Modules\Gallery\VideoRepository::class);
        if ($repository->count() > 0) {
            $data = $repository->getRowActive();
            $_data = [];
            if ($data) {
                foreach ($data as $key => $row) {
                    if ($row->videoImages) {
                        $cover = $row->videoImages->getImageThumbnailAttribute(350, 350);
                    } else {
                        $cover = @$row->thumbnail ? $row->thumbnail : null;
                    }
                    $internal = @$row->internal ? $row->internal->getFullURLAttribute() : '#';
                    $_data[] = [
                        'thumbnail' => $cover,
                        'title' => $row->single_translate->title,
                        'url' => $row->source == 'external' ? 'https://www.youtube.com/watch?v=' . @$row->code : $internal,
                    ];
                }
            }
            return $_data;
        } else {
            return null;
        }
    }
}

if (!function_exists('visitorCounter')) {
    /**
     * @return array|null
     */
    function visitorCounter()
    {
        $repository = app(\App\Repositories\Core\VisitorRepository::class);
        if ($repository->count() > 0) {
            $data = $repository->countVisitor();
            return $data;
        } else {
            return null;
        }
    }
}

if (!function_exists('webConfig')) {
    /**
     * @param $params
     *
     * @return string|null
     */
    function webConfig($params)
    {
        $repository = app(\App\Repositories\Core\ConfigRepository::class);
        if ($repository->count() > 0) {
            if ($params) {
                $data = $repository->first();
                return @$data->params[$params] ? $data->params[$params] : null;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}

if (!function_exists('formatPhone')) {
    /**
     * @param        $val
     * @param string $kode
     *
     * @return string
     */
    function formatPhone($val, $kode = '+62')
    {
        if ($val) {
            return $kode . substr($val, 1);
        }
    }
}

if (!function_exists('isMobile')) {
    /**
     * @return bool
     */
    function isMobile()
    {
        $agent = new Agent();
        return $agent->isMobile();
    }
}

if (!function_exists('banner')) {
    /**
     * @param $position
     *
     * @return string|null
     */
    function banner($position)
    {
        $repository = app(\App\Repositories\Modules\Extension\BannerRepository::class);
        if ($position) {
            $data = $repository->where('position', $position)->where('status', true)->limit(1)->get();
            if ($data->count() > 0) {
                return $data[0];
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
