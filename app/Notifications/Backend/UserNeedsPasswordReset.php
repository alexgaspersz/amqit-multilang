<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 2019-03-29
 * Time: 01:40
 */

namespace App\Notifications\Backend;


use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

/**
 * Class UserNeedsPasswordReset.
 */
class UserNeedsPasswordReset extends Notification
{
    use Queueable;
    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * UserNeedsPasswordReset constructor.
     *
     * @param $token
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's channels.
     *
     * @param mixed $notifiable
     *
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->subject(app_name().': '.__('email.auth.password_reset_subject'))
            ->greeting(__('email.auth.greeting'))
            ->line(__('email.auth.password_cause_of_email'))
            ->action(__('buttons.emails.auth.reset_password'), route('admin.auth.password.reset.form', $this->token))
            ->line(__('email.auth.password_if_not_requested'));
    }
}
