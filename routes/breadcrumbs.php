<?php
/**
 * Description: breadcrumbs.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     28/12/2017, modified: 28/12/2017 04:52
 * @copyright   Copyright (c) 2017.
 */

// Dashboard
Breadcrumbs::register('dashboard', function ($breadcrumbs) {
    $breadcrumbs->push(__('module.dashboard.title'), route('admin.dashboard.index'));
});
Breadcrumbs::register('system', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(__('module.dashboard.setting'), "#");
});
Breadcrumbs::register('menu', function ($breadcrumbs) {
    $breadcrumbs->parent('system');
    $breadcrumbs->push(__('module.menu.title'), route('admin.system.menus.index'));
});
Breadcrumbs::register('groups', function ($breadcrumbs) {
    $breadcrumbs->parent('system');
    $breadcrumbs->push(__('module.group.title'), route('admin.system.groups.index'));
});
Breadcrumbs::register('groups_access', function ($breadcrumbs) {
    $breadcrumbs->parent('groups');
    $breadcrumbs->push(__('module.group.access.title'), "#");
});
Breadcrumbs::register('users', function ($breadcrumbs) {
    $breadcrumbs->parent('system');
    $breadcrumbs->push(__('module.user.title'), route('admin.system.users.index'));
});
Breadcrumbs::register('logs', function ($breadcrumbs) {
    $breadcrumbs->parent('system');
    $breadcrumbs->push(__('module.logs.title'), route('admin.system.logs.index'));
});
Breadcrumbs::register('config', function ($breadcrumbs) {
    $breadcrumbs->parent('system');
    $breadcrumbs->push(__('module.config.title'), route('admin.system.config.index'));
});
Breadcrumbs::register('content', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(__('module.dashboard.content'), "#");
});
Breadcrumbs::register('pages', function ($breadcrumbs) {
    $breadcrumbs->parent('content');
    $breadcrumbs->push(__('module.pages.title'), route('admin.content.pages.index'));
});
Breadcrumbs::register('posts', function ($breadcrumbs) {
    $breadcrumbs->parent('content');
    $breadcrumbs->push(__('module.post.title'), route('admin.content.posts.index'));
});
Breadcrumbs::register('tags', function ($breadcrumbs) {
    $breadcrumbs->parent('content');
    $breadcrumbs->push(__('module.tags.title'), route('admin.content.tags.index'));
});
Breadcrumbs::register('categories', function ($breadcrumbs) {
    $breadcrumbs->parent('content');
    $breadcrumbs->push(__('module.category.title'), route('admin.content.categories.index'));
});
Breadcrumbs::register('account', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(__('forms.user.account'), route('admin.account.index'));
});
Breadcrumbs::register('password', function ($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push(__('strings.common.user.change_password'), route('admin.account.password'));
});
//GALLERY
Breadcrumbs::register('gallery', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(__('module.gallery.module'), "#");
});
Breadcrumbs::register('photos', function ($breadcrumbs) {
    $breadcrumbs->parent('gallery');
    $breadcrumbs->push(__('module.gallery.photo.subtitle'), route('admin.gallery.photo.index'));
});
Breadcrumbs::register('videos', function ($breadcrumbs) {
    $breadcrumbs->parent('gallery');
    $breadcrumbs->push(__('module.gallery.video.subtitle'), route('admin.gallery.video.index'));
});
//MASTER DATA
Breadcrumbs::register('master', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(__('module.master.module'), "#");
});
//EXTENSION
Breadcrumbs::register('extension', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(__('module.extension.module'), "#");
});
Breadcrumbs::register('slideshow', function ($breadcrumbs) {
    $breadcrumbs->parent('extension');
    $breadcrumbs->push(__('module.slideshow.title'), route('admin.extension.slideshow.index'));
});
Breadcrumbs::register('banner', function ($breadcrumbs) {
    $breadcrumbs->parent('extension');
    $breadcrumbs->push(__('module.banner.title'), route('admin.extension.banner.index'));
});
