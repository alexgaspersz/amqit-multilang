<?php
/**
 * Description: content.php generated by PhpStorm.
 * @package     amqit-mdb
 * @author      alex
 * @created     09/03/2020, modified: 09/03/2020 16:45
 * @copyright   Copyright (c) 2020.
 */

Route::group(['middleware' => ['admin'],
    'prefix' => 'content',
    'as' => 'content.',
    'namespace' => 'Modules\Content'], function () {

    Route::group([
        'prefix' => 'pages',
        'as' => 'pages.'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'PagesController@index']);
        Route::get('grid', ['as' => 'grid', 'uses' => 'PagesController@grid']);
        Route::get('update/{id?}', ['as' => 'update', 'uses' => 'PagesController@update']);
        Route::post('store', ['as' => 'store', 'uses' => 'PagesController@store']);
        Route::post('destroy', ['as' => 'destroy', 'uses' => 'PagesController@destroy']);
        Route::post('batch', ['as' => 'batch', 'uses' => 'PagesController@batch']);
    });

    Route::group([
        'prefix' => 'posts',
        'as' => 'posts.'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'PostsController@index']);
        Route::get('grid', ['as' => 'grid', 'uses' => 'PostsController@grid']);
        Route::get('update/{id?}', ['as' => 'update', 'uses' => 'PostsController@update']);
        Route::post('store', ['as' => 'store', 'uses' => 'PostsController@store']);
        Route::post('destroy', ['as' => 'destroy', 'uses' => 'PostsController@destroy']);
        Route::post('batch', ['as' => 'batch', 'uses' => 'PostsController@batch']);
    });

    Route::group([
        'prefix' => 'categories',
        'as' => 'categories.'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'CategoriesController@index']);
        Route::get('grid', ['as' => 'grid', 'uses' => 'CategoriesController@grid']);
        Route::get('update/{id?}', ['as' => 'update', 'uses' => 'CategoriesController@update']);
        Route::post('store', ['as' => 'store', 'uses' => 'CategoriesController@store']);
        Route::post('destroy', ['as' => 'destroy', 'uses' => 'CategoriesController@destroy']);
        Route::post('batch', ['as' => 'batch', 'uses' => 'CategoriesController@batch']);
    });

    Route::group([
        'prefix' => 'tags',
        'as' => 'tags.'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'TagsController@index']);
        Route::get('grid', ['as' => 'grid', 'uses' => 'TagsController@grid']);
        Route::get('update/{id?}', ['as' => 'update', 'uses' => 'TagsController@update']);
        Route::post('store', ['as' => 'store', 'uses' => 'TagsController@store']);
        Route::post('destroy', ['as' => 'destroy', 'uses' => 'TagsController@destroy']);
        Route::post('batch', ['as' => 'batch', 'uses' => 'TagsController@batch']);
        Route::get('search', ['as' => 'search', 'uses' => 'TagsController@search']);
    });
});
