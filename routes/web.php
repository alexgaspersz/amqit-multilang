<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
], function () {

    /*
     * Frontend Routes
     */
    Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function () {
        include_route_files(__DIR__.'/frontend/');
    });

    /*
     * Backend Routes
     */
    Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.'], function () {
        include_route_files(__DIR__ . '/backend/');
    });

    Route::get('search/query', ['as' => 'search.query', 'uses' => 'Api\SearchController@query']);
    Route::get('search/add', ['as' => 'search.add', 'uses' => 'Api\SearchController@add']);
    Route::get('search/delete', ['as' => 'search.delete', 'uses' => 'Api\SearchController@delete']);

});

Route::group([
    'prefix' => 'common',
    'as'  => 'common.',
    'namespace' => 'Api',
    'middleware' => 'admin'
], function () {
    Route::get('get_menu/{type}/{id?}', ['as' => 'menu', 'uses' => 'CommonApiController@get_menu']);
    Route::get('get_menu_parent/{type}/{id?}', ['as' => 'menu.parent', 'uses' => 'CommonApiController@get_menu_parent']);
    Route::get('get_group', ['as' => 'group', 'uses' => 'CommonApiController@get_group']);
    Route::get('get_group_admin', ['as' => 'group.admin', 'uses' => 'CommonApiController@get_group_admin']);
    Route::get('get_page', ['as' => 'page', 'uses' => 'CommonApiController@get_page']);
    Route::get('get_module', ['as' => 'module', 'uses' => 'CommonApiController@get_module']);
    Route::get('get_post', ['as' => 'post', 'uses' => 'CommonApiController@get_post']);
    Route::get('get_users', ['as' => 'users', 'uses' => 'CommonApiController@get_users']);
});

//Uploader Route
Route::group([
    'prefix' => 'service/uploads',
    'as'  => 'service.uploads.',
    'namespace' => 'Api',
    'middleware' => 'admin'
], function () {
    Route::post('multiple', ['as'=>'multiple','uses'=>'UploadHandlerController@fileMultipleUpload']);
    Route::post('single', ['as'=>'single','uses'=>'UploadHandlerController@fileSingleUpload']);
    Route::post('document', ['as'=>'document','uses'=>'UploadHandlerController@docSingleUpload']);
    Route::get('remove/{id}', ['as'=>'remove','uses'=>'UploadHandlerController@fileRemove']);
    Route::get('batch/{id}', ['as'=>'batch','uses'=>'UploadHandlerController@fileRemoveBatch']);
    Route::get('preview/{id?}/{w?}/{h?}', ['as'=>'preview','uses'=>'UploadHandlerController@filePreview']);
    Route::post('all', ['as'=>'all','uses'=>'UploadHandlerController@filePreviewMultiple']);
    Route::post('post-remove', ['as' => 'post.remove', 'uses' => 'UploadHandlerController@postRemove']);
    Route::post('remove-asset', ['as' => 'remove.asset', 'uses' => 'UploadHandlerController@removeAsset']);
    Route::post('editor', ['as' => 'editor', 'uses' => 'UploadHandlerController@editorUpload']);
    Route::get('manager', ['as' => 'manager', 'uses' => 'UploadHandlerController@editorManager']);
    Route::delete('manager-remove', ['as' => 'manager.remove', 'uses' => 'UploadHandlerController@editorManagerDelete']);
});
