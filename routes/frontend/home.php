<?php
/**
 * Description: home.php PhpStorm.
 *
 * @package     amqit-mutilang
 * @author      alex
 * @created     30/12/2017, modified: 30/12/2017 01:31
 * @copyright   Copyright (c) 2017.
 */

/**
 * Frontend Routes Controller
 * All route names are prefixed with 'frontend.'.
 */
Route::get('/', ['as' => 'index', 'uses' => 'HomeController@index']);

Route::group(['namespace' => 'Modules'], function() {

    Route::group([
        'prefix' => 'news',
        'as' => 'news.'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'NewsController@index']);
        Route::get('{url?}', ['as' => 'category', 'uses' => 'NewsController@category']);
        Route::get('read/{slug}', ['as' => 'read', 'uses' => 'NewsController@read']);
    });

    Route::group([
        'prefix' => 'gallery',
        'as' => 'gallery.'], function () {

        Route::group([
            'prefix' => 'videos',
            'as' => 'videos.'], function () {
            Route::get('/', ['as' => 'index', 'uses' => 'VideoController@index']);
        });

        Route::group([
            'prefix' => 'photos',
            'as' => 'photos.'], function () {
            Route::get('/', ['as' => 'index', 'uses' => 'PhotoController@index']);
            Route::get('{album}', ['as' => 'read', 'uses' => 'PhotoController@read']);
            Route::get('{album}/{id}', ['as' => 'view', 'uses' => 'PhotoController@view']);
        });
    });

    Route::group([
        'prefix' => 'account',
        'as' => 'account.',
        'middleware' => 'userlogin'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'AccountController@index']);
    });

});
