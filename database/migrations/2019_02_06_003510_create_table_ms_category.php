<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMsCategory extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('ms_category');
        Schema::create('ms_category', function (Blueprint $table) {
            $table->id();
            $table->string('slug', 100)->unique();
            $table->tinyInteger('sort_order')->nullable()->default(0);
            $table->boolean('status')->nullable()->default(true)->comment('True/False');
            $table->unsignedTinyInteger('created_by')->default(0)->nullable();
            $table->unsignedTinyInteger('updated_by')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_category');
    }
}
