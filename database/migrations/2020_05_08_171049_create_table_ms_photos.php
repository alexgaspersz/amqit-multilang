<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMsPhotos extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('ms_photos');
        Schema::create('ms_photos', function (Blueprint $table) {
            $table->id();
            $table->string('slug', 190)->unique();
            $table->unsignedBigInteger('cover')->nullable()->comment('FK ms_assets untuk cover gallery');
            $table->json('images')->nullable()->comment('images asset id untuk galleri foto');
            $table->unsignedBigInteger('post_gallery')->nullable()->comment('FK post/article gallery');
            $table->dateTime('publish_at')->nullable();
            $table->boolean('status')->default(true)->comment('True/False');
            $table->boolean('featured')->default(false)->comment('True/False');
            $table->boolean('allow_comment')->default(false)->comment('True/False');
            $table->mediumInteger('hit')->nullable()->default(0);
            $table->tinyInteger('sort_order')->default(0);
            $table->unsignedTinyInteger('created_by')->default(0)->nullable();
            $table->unsignedTinyInteger('updated_by')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_photos');
    }
}
