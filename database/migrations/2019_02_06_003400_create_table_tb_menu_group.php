<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTbMenuGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tb_menu_group');
        Schema::create('tb_menu_group', function (Blueprint $table) {
            $table->id();
            $table->string('menu_group',50)->unique();
            $table->string('menu_group_alias',50)->unique();
            $table->boolean('status')->default(true)->comment('True/False');
            $table->boolean('is_core')->default(false)->comment('True/False');
            $table->unsignedTinyInteger('created_by')->default(0)->nullable();
            $table->unsignedTinyInteger('updated_by')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_menu_group');
    }
}
