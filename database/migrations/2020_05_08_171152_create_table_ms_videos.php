<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMsVideos extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('ms_videos');
        Schema::create('ms_videos', function (Blueprint $table) {
            $table->id();
            $table->string('slug', 190)->unique();
            $table->enum('source', ['external', 'internal'])->nullable()->default('external');
            $table->unsignedBigInteger('cover')->nullable()->comment('FK ms_asset untuk gambar cover video');
            $table->unsignedBigInteger('asset')->nullable()->comment('FK ms_asset untuk internal video');
            $table->string('code', 50)->nullable()->comment('youtube ID code');
            $table->string('thumbnail', 150)->nullable()->comment('thumbnail hasil fetch data dari youtube');
            $table->boolean('status')->default(true)->comment('True/False');
            $table->boolean('allow_comment')->default(false)->comment('True/False');
            $table->mediumInteger('hit')->nullable()->default(0);
            $table->tinyInteger('sort_order')->default(0);
            $table->unsignedTinyInteger('created_by')->default(0)->nullable();
            $table->unsignedTinyInteger('updated_by')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_videos');
    }
}
