<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTbAccess extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tb_access');
        Schema::create('tb_access', function (Blueprint $table) {
            $table->id('access_id');
            $table->unsignedBigInteger('group_id')->comment('FK tb_groups');
            $table->foreign('group_id')->references('group_id')->on('tb_groups')->onUpdate('no action')->onDelete('cascade');
            $table->string('module',150)->nullable();
            $table->unsignedBigInteger('menu_id')->comment('FK tb_menu');
            $table->foreign('menu_id')->references('menu_id')->on('tb_menu')->onUpdate('no action')->onDelete('cascade');
            $table->char('menu_group')->nullable()->default('adminsidebar');
            $table->boolean('is_create')->default(false);
            $table->boolean('is_read')->default(false);
            $table->boolean('is_update')->default(false);
            $table->boolean('is_delete')->default(false);
            $table->boolean('is_approve')->default(false);
            $table->boolean('is_download')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_access');
    }
}
