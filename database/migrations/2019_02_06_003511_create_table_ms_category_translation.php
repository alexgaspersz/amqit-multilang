<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMsCategoryTranslation extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('ms_category_translation');
        Schema::create('ms_category_translation', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('category_id')->default(0)->comment('FK ms_category id');
            $table->foreign('category_id')->references('id')->on('ms_category')->onUpdate('no action')->onDelete('cascade');
            $table->string('name', 100)->nullable();
            $table->mediumText('description')->nullable();
            $table->string('locale', 5)->index()->default('id')->comment('init language');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_category_translation');
    }
}
