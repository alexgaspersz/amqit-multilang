<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMsVideosTranslation extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('ms_videos_translation');
        Schema::create('ms_videos_translation', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('vid')->index()->comment('FK ms_videos');
            $table->foreign('vid')->references('id')->on('ms_videos')->onUpdate('no action')->onDelete('cascade');
            $table->string('title', 190)->nullable();
            $table->mediumText('description')->nullable();
            $table->string('locale', 5)->index()->default('id')->comment('init language');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_videos_translation');
    }
}
