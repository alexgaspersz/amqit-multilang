<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbVisitor extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tb_visitor');
        Schema::create('tb_visitor', function (Blueprint $table) {
            $table->id();
            $table->ipAddress('ipaddress')->nullable();
            $table->mediumText('useragent')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_visitor');
    }
}
