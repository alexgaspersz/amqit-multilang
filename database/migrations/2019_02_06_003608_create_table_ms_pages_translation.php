<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMsPagesTranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('ms_pages_translation');
        Schema::create('ms_pages_translation', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('page_id')->default(0)->comment('FK ms_pages id');
            $table->foreign('page_id')->references('id')->on('ms_pages')->onUpdate('no action')->onDelete('cascade');
            $table->string('page_title', 190)->nullable();
            $table->string('page_meta', 190)->nullable();
            $table->mediumText('page_desc')->nullable();
            $table->longText('content')->nullable();
            $table->string('locale',5)->index()->default('id')->comment('init language');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_pages_translation');
    }
}
