<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTbGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tb_groups');
        Schema::create('tb_groups', function (Blueprint $table) {
            $table->id('group_id');
            $table->string('name', 100)->unique();
            $table->mediumText('description')->nullable();
            $table->string('alias', 100)->unique();
            $table->boolean('is_core')->default(false)->comment('True/False');
            $table->boolean('is_admin')->default(false)->comment('True/False');
            $table->boolean('is_super')->default(false)->comment('True/False');
            $table->boolean('allow_cms')->default(false)->comment('True/False');
            $table->boolean('status')->default(true)->comment('True/False');
            $table->unsignedTinyInteger('created_by')->default(0)->nullable();
            $table->unsignedTinyInteger('updated_by')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('tb_groups');
    }
}
