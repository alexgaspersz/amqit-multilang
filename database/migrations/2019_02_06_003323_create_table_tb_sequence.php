<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTbSequence extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tb_sequence');
        Schema::create('tb_sequence', function (Blueprint $table) {
            $table->id('seq_id');
           $table->string('ref_name',150)->nullable();
           $table->unsignedTinyInteger('curr_number')->default(0);
           $table->unsignedTinyInteger('curr_month')->default(0);
           $table->year('curr_year')->default(0000);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_sequence');
    }
}
