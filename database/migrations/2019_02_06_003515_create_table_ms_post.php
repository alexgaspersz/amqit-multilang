<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMsPost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('ms_post');
        Schema::create('ms_post', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('categories')->index()->comment('FK ms_category');
            $table->foreign('categories')->references('id')->on('ms_category')->onUpdate('no action')->onDelete('cascade');
            $table->unsignedBigInteger('author_id')->index();
            $table->dateTime('publish_at')->nullable();
            $table->string('slug', 190)->unique();
            $table->mediumText('tags')->nullable()->comment('Multiple string tags array');
            $table->unsignedBigInteger('images')->nullable()->comment('FK ms_assets');
            $table->tinyInteger('status')->default(0)->comment('0: Draft; 1:Review; 2: Publish');
            $table->boolean('featured')->default(false)->comment('True/False');
            $table->boolean('allow_guest')->default(true)->comment('True/False');
            $table->boolean('allow_comment')->default(true)->comment('True/False');
            $table->tinyInteger('sort_order')->default(0);
            $table->mediumInteger('hit')->nullable()->default(0);
            $table->unsignedTinyInteger('created_by')->default(0)->nullable();
            $table->unsignedTinyInteger('updated_by')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_post');
    }
}
