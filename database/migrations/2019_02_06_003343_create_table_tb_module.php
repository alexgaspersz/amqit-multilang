<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTbModule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tb_module');
        Schema::create('tb_module', function (Blueprint $table) {
            $table->id();
            $table->string('module_name',100)->nullable();
            $table->string('module_title',150)->nullable();
            $table->string('module_table',50)->nullable();
            $table->string('module_table_key',10)->nullable();
            $table->string('module_namespace', 150)->nullable();
            $table->mediumText('module_note')->nullable();
            $table->json('module_config')->nullable();
            $table->unsignedTinyInteger('created_by')->default(0)->nullable();
            $table->unsignedTinyInteger('updated_by')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_module');
    }
}
