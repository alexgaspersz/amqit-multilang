<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsBanner extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('ms_banner');
        Schema::create('ms_banner', function (Blueprint $table) {
            $table->id();
            $table->string('title', 100)->nullable();
            $table->unsignedBigInteger('image')->comment('FK ms_assets');
            $table->string('position', 50)->nullable();
            $table->string('link', 150)->nullable();
            $table->boolean('status')->default(true)->comment('True/False');
            $table->unsignedTinyInteger('created_by')->default(0)->nullable();
            $table->unsignedTinyInteger('updated_by')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_banner');
    }
}
