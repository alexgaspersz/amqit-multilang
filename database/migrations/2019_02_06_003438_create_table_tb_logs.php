<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTbLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tb_logs');
        Schema::create('tb_logs', function (Blueprint $table) {
            $table->bigIncrements('log_id');
            $table->string('module', 190)->nullable();
            $table->string('task',100)->nullable();
            $table->unsignedBigInteger('user_id')->index()->nullable();
            $table->ipAddress('ipaddress')->nullable();
            $table->mediumText('useragent')->nullable();
            $table->mediumText('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_logs');
    }
}
