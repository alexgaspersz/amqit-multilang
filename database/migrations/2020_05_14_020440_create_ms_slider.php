<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsSlider extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('ms_slider');
        Schema::create('ms_slider', function (Blueprint $table) {
            $table->id();
            $table->string('title', 100)->nullable();
            $table->unsignedBigInteger('image')->comment('FK ms_assets');
            $table->boolean('status')->default(true)->comment('True/False');
            $table->tinyInteger('sort_order')->default(0);
            $table->unsignedTinyInteger('created_by')->default(0)->nullable();
            $table->unsignedTinyInteger('updated_by')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_slider');
    }
}
