<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMsAssets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('ms_assets');
        Schema::create('ms_assets', function (Blueprint $table) {
            $table->id('assetId');
            $table->string('category',100)->index()->nullable();
            $table->string('title',150)->nullable();
            $table->string('base_folder',150)->nullable();
            $table->string('base_path',150)->nullable();
            $table->string('mime_type',50)->nullable();
            $table->string('extension',10)->nullable();
            $table->integer('size')->nullable();
            $table->unsignedTinyInteger('created_by')->default(0)->nullable();
            $table->unsignedTinyInteger('updated_by')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_assets');
    }
}
