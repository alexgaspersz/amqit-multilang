<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTbUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tb_users');
        Schema::create('tb_users', function (Blueprint $table) {
            $table->id('user_id');
            $table->unsignedBigInteger('group_id')->index();
            $table->foreign('group_id')->references('group_id')->on('tb_groups')->onUpdate('cascade')->onDelete('restrict');
            $table->string('username', 15)->unique();
            $table->string('password',150);
            $table->string('email',50)->unique();
            $table->string('fullname', 150);
            $table->unsignedBigInteger('avatar')->index()->nullable();
            $table->tinyInteger('active')->default(1)->comment('0:inactive;1:active;2:blocked');
            $table->boolean('is_core')->default(false);
            $table->boolean('is_admin')->default(false);
            $table->boolean('is_online')->default(false);
            $table->dateTime('last_login')->nullable();
            $table->ipAddress('ip_address')->nullable();
            $table->string('activation_selector', 190)->nullable();
            $table->string('remember_token', 150)->nullable();
            $table->string('remember_selector', 190)->nullable();
            $table->string('forgotten_password_selector', 190)->nullable();
            $table->unsignedTinyInteger('created_by')->default(0)->nullable();
            $table->unsignedTinyInteger('updated_by')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('tb_users');
    }
}
