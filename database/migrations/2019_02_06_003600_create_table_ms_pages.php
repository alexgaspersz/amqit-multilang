<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMsPages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('ms_pages');
        Schema::create('ms_pages', function (Blueprint $table) {
            $table->id();
            $table->string('slug', 190)->unique();
            $table->unsignedBigInteger('parent_id')->index()->nullable()->comment('Lookup parent page id');
            $table->boolean('visible')->default(true)->comment('True/False');
            $table->boolean('is_private')->default(false)->comment('True/False');
            $table->boolean('social')->default(false)->comment('True/False');
            $table->unsignedBigInteger('bg')->nullable()->comment('FK ms_assets');
            $table->unsignedBigInteger('attachment')->nullable()->comment('FK ms_assets');
            $table->tinyInteger('sort_order')->default(0);
            $table->mediumInteger('hit')->nullable()->default(0);
            $table->boolean('status')->default(true)->comment('True/False');
            $table->unsignedTinyInteger('created_by')->default(0)->nullable();
            $table->unsignedTinyInteger('updated_by')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_pages');
    }
}
