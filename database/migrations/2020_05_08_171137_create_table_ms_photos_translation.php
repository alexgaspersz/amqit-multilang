<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMsPhotosTranslation extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('ms_photos_translation');
        Schema::create('ms_photos_translation', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('gid')->index()->comment('FK ms_photos');
            $table->foreign('gid')->references('id')->on('ms_photos')->onUpdate('no action')->onDelete('cascade');
            $table->string('title', 190)->nullable();
            $table->mediumText('description')->nullable();
            $table->string('locale', 5)->index()->default('id')->comment('init language');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_photos_translation');
    }
}
