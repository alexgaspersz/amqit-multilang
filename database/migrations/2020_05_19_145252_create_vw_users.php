<?php

use Illuminate\Database\Migrations\Migration;

class CreateVwUsers extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_active_users");
        DB::statement("CREATE VIEW vw_active_users AS SELECT tb_users.user_id AS uid,
                        tb_users.fullname,
                        tb_users.email,
                        tb_users.group_id,
                        tb_groups.name AS `group`,
                        tb_groups.allow_cms,
                        tb_user_profile.display_name,
                        tb_user_profile.birthdate,
                        tb_user_profile.birthplace,
                        tb_user_profile.gender,
                        tb_user_profile.handphone,
                        tb_user_profile.address,
                        tb_user_profile.facebook,
                        tb_user_profile.instagram,
                        tb_user_profile.twitter,
                        tb_user_profile.linkedin
                        FROM tb_users
                        JOIN tb_groups ON tb_users.group_id = tb_groups.group_id
                        JOIN tb_user_profile ON tb_users.user_id = tb_user_profile.user_id
                        WHERE tb_users.active = 1
                        ORDER BY tb_users.user_id ASC");
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_active_users");
    }
}
