<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTbMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tb_menu');
        Schema::create('tb_menu', function (Blueprint $table) {
            $table->id('menu_id');
            $table->unsignedBigInteger('parent_id')->index()->default(0)->comment('idx menu_id');
            $table->string('module',150)->nullable();
            $table->string('url',150)->nullable();
            $table->string('menu_type', 50)->index()->nullable()->comment('tb_menu_group alias');
            $table->string('source',50)->nullable()->comment('module source: page,post,ext.link');
            $table->unsignedInteger('source_id')->nullable()->comment('source key id');
            $table->string('link',150)->nullable();
            $table->string('menu_icons',50)->nullable();
            $table->tinyInteger('ordering')->default(0);
            $table->boolean('allow_guest')->default(false)->comment('True/False');
            $table->boolean('is_restrict')->default(false)->comment('True/False');
            $table->boolean('show_title')->default(true)->comment('True/False');
            $table->boolean('status')->default(true)->comment('True/False');
            $table->unsignedTinyInteger('created_by')->default(0)->nullable();
            $table->unsignedTinyInteger('updated_by')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_menu');
    }
}
