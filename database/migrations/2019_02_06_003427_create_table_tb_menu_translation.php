<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTbMenuTranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tb_menu_translation');
        Schema::create('tb_menu_translation', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('menu_id')->default(0)->comment('FK tb_menu menu_id');
            $table->foreign('menu_id')->references('menu_id')->on('tb_menu')->onUpdate('no action')->onDelete('cascade');
            $table->string('menu_title',100)->nullable();
            $table->string('meta_title',100)->nullable();
            $table->mediumText('meta_description')->nullable();
            $table->string('locale',5)->index()->default('id')->comment('init language');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_menu_translation');
    }
}
