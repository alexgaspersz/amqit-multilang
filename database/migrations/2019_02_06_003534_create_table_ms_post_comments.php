<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMsPostComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('ms_post_comments');
        Schema::create('ms_post_comments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('entry_id')->default(0)->comment('FK ms_post id');
            $table->foreign('entry_id')->references('id')->on('ms_post')->onUpdate('no action')->onDelete('cascade');
            $table->string('author',100)->nullable();
            $table->string('author_email',50)->nullable();
            $table->string('author_url',100)->nullable();
            $table->mediumText('description')->nullable();
            $table->ipAddress('author_ip')->nullable();
            $table->mediumText('agent')->nullable();
            $table->text('content')->nullable();
            $table->tinyInteger('approved')->default(0)->comment('0: New; 1: Approved; 2: Reject');
            $table->unsignedBigInteger('parent_id')->index()->default(0)->comment('Lookup parent comments for reply');
            $table->unsignedTinyInteger('created_by')->default(0)->nullable();
            $table->unsignedTinyInteger('updated_by')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_post_comments');
    }
}
