<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTbUserProfile extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tb_user_profile');
        Schema::create('tb_user_profile', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index();
            $table->foreign('user_id')->references('user_id')->on('tb_users')->onUpdate('cascade')->onDelete('cascade');
            $table->string('display_name', 100)->nullable();
            $table->date('birthdate')->nullable();
            $table->string('birthplace', 50)->nullable();
            $table->enum('gender', ['m', 'f'])->nullable();
            $table->string('handphone', 50)->nullable();
            $table->mediumText('address')->nullable();
            $table->string('facebook', 100)->nullable();
            $table->string('instagram', 100)->nullable();
            $table->string('twitter', 100)->nullable();
            $table->string('linkedin', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_user_profile');
    }
}
