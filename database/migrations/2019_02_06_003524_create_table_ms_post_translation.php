<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMsPostTranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('ms_post_translation');
        Schema::create('ms_post_translation', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('entry_id')->default(0)->comment('FK ms_post id');
            $table->foreign('entry_id')->references('id')->on('ms_post')->onUpdate('no action')->onDelete('cascade');
            $table->string('title', 190)->nullable();
            $table->string('meta', 190)->nullable();
            $table->mediumText('intro')->nullable();
            $table->mediumText('description')->nullable();
            $table->longText('content')->nullable();
            $table->string('locale',5)->index()->default('id')->comment('init language');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_post_translation');
    }
}
