<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
        DB::table('tb_menu')->insert([
            ['menu_id' => 1, 'parent_id' => 0, 'module' => 'admin.content.*', 'url' => '#', 'menu_type' => 'adminsidebar', 'menu_icons' => 'cil-newspaper', 'source' => null, 'source_id' => null, 'ordering' => 5, 'created_by' => 2, 'created_at' => Carbon::now()],
            ['menu_id' => 2, 'parent_id' => 1, 'module' => 'admin.content.pages.*', 'url' => 'admin/content/pages', 'menu_type' => 'adminsidebar', 'menu_icons' => 'cil-arrow-circle-right', 'source' => null, 'source_id' => null, 'ordering' => 1, 'created_by' => 2, 'created_at' => Carbon::now()],
            ['menu_id' => 3, 'parent_id' => 1, 'module' => 'admin.content.categories.*', 'url' => 'admin/content/categories', 'menu_type' => 'adminsidebar', 'menu_icons' => 'cil-arrow-circle-right', 'source' => null, 'source_id' => null, 'ordering' => 2, 'created_by' => 2, 'created_at' => Carbon::now()],
            ['menu_id' => 4, 'parent_id' => 1, 'module' => 'admin.content.posts.*', 'url' => 'admin/content/posts', 'menu_type' => 'adminsidebar', 'menu_icons' => 'cil-arrow-circle-right', 'source' => null, 'source_id' => null, 'ordering' => 3, 'created_by' => 2, 'created_at' => Carbon::now()],
            ['menu_id' => 5, 'parent_id' => 1, 'module' => 'admin.content.tags.*', 'url' => 'admin/content/tags', 'menu_type' => 'adminsidebar', 'menu_icons' => 'cil-arrow-circle-right', 'source' => null, 'source_id' => null, 'ordering' => 4, 'created_by' => 2, 'created_at' => Carbon::now()],
            ['menu_id' => 6, 'parent_id' => 0, 'module' => 'admin.gallery.*', 'url' => '#', 'menu_type' => 'adminsidebar', 'menu_icons' => 'cil-folder', 'source' => null, 'source_id' => null, 'ordering' => 6, 'created_by' => 2, 'created_at' => Carbon::now()],
            ['menu_id' => 7, 'parent_id' => 6, 'module' => 'admin.gallery.photo.*', 'url' => 'admin/gallery/photo', 'menu_type' => 'adminsidebar', 'menu_icons' => 'cil-arrow-circle-right', 'source' => null, 'source_id' => null, 'ordering' => 1, 'created_by' => 2, 'created_at' => Carbon::now()],
            ['menu_id' => 8, 'parent_id' => 6, 'module' => 'admin.gallery.video.*', 'url' => 'admin/gallery/video', 'menu_type' => 'adminsidebar', 'menu_icons' => 'cil-arrow-circle-right', 'source' => null, 'source_id' => null, 'ordering' => 2, 'created_by' => 2, 'created_at' => Carbon::now()],
            ['menu_id' => 9, 'parent_id' => 0, 'module' => 'admin.extension.*', 'url' => '#', 'menu_type' => 'adminsidebar', 'menu_icons' => 'cil-image1', 'source' => null, 'source_id' => null, 'ordering' => 9, 'created_by' => 2, 'created_at' => Carbon::now()],
            ['menu_id' => 10, 'parent_id' => 9, 'module' => 'admin.extension.slideshow.*', 'url' => 'admin/extension/slideshow', 'menu_type' => 'adminsidebar', 'menu_icons' => 'cil-arrow-circle-right', 'source' => null, 'source_id' => null, 'ordering' => 1, 'created_by' => 2, 'created_at' => Carbon::now()],
            ['menu_id' => 11, 'parent_id' => 9, 'module' => 'admin.extension.banner.*', 'url' => 'admin/extension/banner', 'menu_type' => 'adminsidebar', 'menu_icons' => 'cil-arrow-circle-right', 'source' => null, 'source_id' => null, 'ordering' => 2, 'created_by' => 2, 'created_at' => Carbon::now()],
            ['menu_id' => 12, 'parent_id' => 0, 'module' => 'admin.system.*', 'url' => '#', 'menu_type' => 'adminsidebar', 'menu_icons' => 'cil-settings', 'source' => null, 'source_id' => null, 'ordering' => 10, 'created_by' => 2, 'created_at' => Carbon::now()],
            ['menu_id' => 13, 'parent_id' => 12, 'module' => 'admin.system.config.*', 'url' => 'admin/system/config', 'menu_type' => 'adminsidebar', 'menu_icons' => 'cil-home', 'source' => null, 'source_id' => null, 'ordering' => 1, 'created_by' => 2, 'created_at' => Carbon::now()],
            ['menu_id' => 14, 'parent_id' => 12, 'module' => 'admin.system.groups.*', 'url' => 'admin/system/groups', 'menu_type' => 'adminsidebar', 'menu_icons' => 'cil-people', 'source' => null, 'source_id' => null, 'ordering' => 2, 'created_by' => 2, 'created_at' => Carbon::now()],
            ['menu_id' => 15, 'parent_id' => 12, 'module' => 'admin.system.users.*', 'url' => 'admin/system/users', 'menu_type' => 'adminsidebar', 'menu_icons' => 'cil-user', 'source' => null, 'source_id' => null, 'ordering' => 3, 'created_by' => 2, 'created_at' => Carbon::now()],
            ['menu_id' => 16, 'parent_id' => 12, 'module' => 'admin.system.logs.*', 'url' => 'admin/system/logs', 'menu_type' => 'adminsidebar', 'menu_icons' => 'cil-pin', 'source' => null, 'source_id' => null, 'ordering' => 4, 'created_by' => 2, 'created_at' => Carbon::now()],
        ]);
    }
}
