<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AccessTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_access')->insert([
            ['group_id' => 2, 'module' => 'admin.content.*', 'menu_id' => 1, 'is_create' => 0, 'is_read' => 1, 'is_update' => 0, 'is_delete' => 0, 'is_approve' => 0, 'is_download' => 0, 'created_at' => Carbon::now()],
            ['group_id' => 2, 'module' => 'admin.content.pages.*', 'menu_id' => 2, 'is_create' => 0, 'is_read' => 1, 'is_update' => 1, 'is_delete' => 0, 'is_approve' => 0, 'is_download' => 0, 'created_at' => Carbon::now()],
            ['group_id' => 2, 'module' => 'admin.content.posts.*', 'menu_id' => 4, 'is_create' => 1, 'is_read' => 1, 'is_update' => 1, 'is_delete' => 1, 'is_approve' => 1, 'is_download' => 0, 'created_at' => Carbon::now()],
            ['group_id' => 2, 'module' => 'admin.gallery.*', 'menu_id' => 6, 'is_create' => 0, 'is_read' => 1, 'is_update' => 0, 'is_delete' => 0, 'is_approve' => 0, 'is_download' => 0, 'created_at' => Carbon::now()],
            ['group_id' => 2, 'module' => 'admin.gallery.photo.*', 'menu_id' => 7, 'is_create' => 1, 'is_read' => 1, 'is_update' => 1, 'is_delete' => 1, 'is_approve' => 0, 'is_download' => 0, 'created_at' => Carbon::now()],
            ['group_id' => 2, 'module' => 'admin.gallery.video.*', 'menu_id' => 8, 'is_create' => 1, 'is_read' => 1, 'is_update' => 1, 'is_delete' => 1, 'is_approve' => 0, 'is_download' => 0, 'created_at' => Carbon::now()],
            ['group_id' => 2, 'module' => 'admin.extension.*', 'menu_id' => 9, 'is_create' => 0, 'is_read' => 1, 'is_update' => 0, 'is_delete' => 0, 'is_approve' => 0, 'is_download' => 0, 'created_at' => Carbon::now()],
            ['group_id' => 2, 'module' => 'admin.extension.slideshow.*', 'menu_id' => 10, 'is_create' => 1, 'is_read' => 1, 'is_update' => 1, 'is_delete' => 1, 'is_approve' => 0, 'is_download' => 0, 'created_at' => Carbon::now()],
            ['group_id' => 2, 'module' => 'admin.extension.banner.*', 'menu_id' => 11, 'is_create' => 1, 'is_read' => 1, 'is_update' => 1, 'is_delete' => 1, 'is_approve' => 0, 'is_download' => 0, 'created_at' => Carbon::now()],
            ['group_id' => 2, 'module' => 'admin.system.*', 'menu_id' => 12, 'is_create' => 0, 'is_read' => 1, 'is_update' => 0, 'is_delete' => 0, 'is_approve' => 0, 'is_download' => 0, 'created_at' => Carbon::now()],
            ['group_id' => 2, 'module' => 'admin.system.config.*', 'menu_id' => 13, 'is_create' => 0, 'is_read' => 1, 'is_update' => 1, 'is_delete' => 0, 'is_approve' => 0, 'is_download' => 0, 'created_at' => Carbon::now()],
            ['group_id' => 2, 'module' => 'admin.system.groups.*', 'menu_id' => 14, 'is_create' => 0, 'is_read' => 1, 'is_update' => 1, 'is_delete' => 0, 'is_approve' => 0, 'is_download' => 0, 'created_at' => Carbon::now()],
            ['group_id' => 2, 'module' => 'admin.system.users.*', 'menu_id' => 15, 'is_create' => 1, 'is_read' => 1, 'is_update' => 1, 'is_delete' => 1, 'is_approve' => 0, 'is_download' => 0, 'created_at' => Carbon::now()],
            ['group_id' => 2, 'module' => 'admin.system.logs.*', 'menu_id' => 16, 'is_create' => 0, 'is_read' => 1, 'is_update' => 0, 'is_delete' => 1, 'is_approve' => 0, 'is_download' => 0, 'created_at' => Carbon::now()],
            ['group_id' => 3, 'module' => 'admin.content.*', 'menu_id' => 1, 'is_create' => 0, 'is_read' => 1, 'is_update' => 0, 'is_delete' => 0, 'is_approve' => 0, 'is_download' => 0, 'created_at' => Carbon::now()],
            ['group_id' => 3, 'module' => 'admin.content.pages.*', 'menu_id' => 2, 'is_create' => 0, 'is_read' => 1, 'is_update' => 1, 'is_delete' => 0, 'is_approve' => 0, 'is_download' => 0, 'created_at' => Carbon::now()],
            ['group_id' => 3, 'module' => 'admin.content.posts.*', 'menu_id' => 4, 'is_create' => 1, 'is_read' => 1, 'is_update' => 1, 'is_delete' => 1, 'is_approve' => 1, 'is_download' => 0, 'created_at' => Carbon::now()],
            ['group_id' => 3, 'module' => 'admin.gallery.*', 'menu_id' => 6, 'is_create' => 0, 'is_read' => 1, 'is_update' => 0, 'is_delete' => 0, 'is_approve' => 0, 'is_download' => 0, 'created_at' => Carbon::now()],
            ['group_id' => 3, 'module' => 'admin.gallery.photo.*', 'menu_id' => 7, 'is_create' => 1, 'is_read' => 1, 'is_update' => 1, 'is_delete' => 1, 'is_approve' => 0, 'is_download' => 0, 'created_at' => Carbon::now()],
            ['group_id' => 3, 'module' => 'admin.gallery.video.*', 'menu_id' => 8, 'is_create' => 1, 'is_read' => 1, 'is_update' => 1, 'is_delete' => 1, 'is_approve' => 0, 'is_download' => 0, 'created_at' => Carbon::now()],
            ['group_id' => 3, 'module' => 'admin.extension.*', 'menu_id' => 9, 'is_create' => 0, 'is_read' => 1, 'is_update' => 0, 'is_delete' => 0, 'is_approve' => 0, 'is_download' => 0, 'created_at' => Carbon::now()],
            ['group_id' => 3, 'module' => 'admin.extension.slideshow.*', 'menu_id' => 10, 'is_create' => 1, 'is_read' => 1, 'is_update' => 1, 'is_delete' => 1, 'is_approve' => 0, 'is_download' => 0, 'created_at' => Carbon::now()],
            ['group_id' => 3, 'module' => 'admin.extension.banner.*', 'menu_id' => 11, 'is_create' => 1, 'is_read' => 1, 'is_update' => 1, 'is_delete' => 1, 'is_approve' => 0, 'is_download' => 0, 'created_at' => Carbon::now()],

        ]);
    }
}
