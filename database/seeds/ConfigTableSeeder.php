<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfigTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
        DB::table('ms_config')->insert([
            [
                'id' => 1,
                'params' => '{"address":null,"phone":null,"website":null,"email":null,"whatsapp":null,"facebook":null,"instagram":null,"youtube":null,"linkedin":null,"registration":false,"maintenance":false,"sso":false,"fbapp":"2614372128878489","ytkey":"AIzaSyAY1EGdg9wLwUoKX7XPXpmavbIZ_gpFxLs","addthis":"ra-5ebc5ef67e5ba545","recaptcha":"6Lc1IOEUAAAAANw9_VIaBNi4CQEoMfrCtJKQ3K0K"}',
                'created_at' => Carbon::now()
            ]
        ]);
    }
}
