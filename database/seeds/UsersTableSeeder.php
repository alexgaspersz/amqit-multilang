<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_users')->insert([
            [
                'user_id' => 1,
                'group_id' => 1,
                'username' => 'superadmin',
                'password' => '$2y$10$t1CRReZ0iLbygI9KC.ccguNEVVaMOYPIYZpfEmKyfdNN8Rs3LRKJy', //*k3l3mumuR@123
                'email' => 'superadmin@app.com',
                'fullname' => 'Super Admin',
                'active' => 1,
                'is_core' => 1,
                'created_at' => Carbon::now()
            ], [
                'user_id' => 2,
                'group_id' => 2,
                'username' => 'admin',
                'password' => '$2y$10$AJDjTxhEVL8uGXFzEKwNhOauaBp7jEyNH1sFGgO2xP0puJvrb4VEK', //admin123
                'email' => 'admin@app.com',
                'fullname' => 'Administrator',
                'active' => 1,
                'is_core' => 1,
                'created_at' => Carbon::now()
            ]
        ]);
    }
}
