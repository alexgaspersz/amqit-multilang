<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_groups')->insert([
            [
                'group_id' => 1,
                'name' => 'Super Admin',
                'description' => 'Superadmin level with no limit access',
                'alias' => 'superadmin',
                'is_core' => true,
                'is_super' => true,
                'allow_cms' => true,
                'status' => 1,
                'created_at' => Carbon::now()
            ],[
                'group_id' => 2,
                'name' => 'Administrator',
                'description' => 'Top level administrator with all access and modified all modules content',
                'alias' => 'administrator',
                'is_core' => true,
                'is_super' => false,
                'allow_cms' => true,
                'status' => 1,
                'created_at' => Carbon::now()
            ], [
                'group_id' => 3,
                'name' => 'Editor',
                'description' => 'Content editor administrator with limited access',
                'alias' => 'editor',
                'is_core' => true,
                'is_super' => false,
                'allow_cms' => true,
                'status' => 1,
                'created_at' => Carbon::now()
            ]
        ]);
    }
}
