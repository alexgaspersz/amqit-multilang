<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenusTranslationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_menu_translation')->insert([
            ['menu_id' => 1, 'menu_title' => 'Pengaturan Konten', 'locale' => 'id', 'created_at' => Carbon::now()],
            ['menu_id' => 1, 'menu_title' => 'Content Management', 'locale' => 'en', 'created_at' => Carbon::now()],
            ['menu_id' => 2, 'menu_title' => 'Halaman', 'locale' => 'id', 'created_at' => Carbon::now()],
            ['menu_id' => 2, 'menu_title' => 'Pages', 'locale' => 'en', 'created_at' => Carbon::now()],
            ['menu_id' => 3, 'menu_title' => 'Kategori', 'locale' => 'id', 'created_at' => Carbon::now()],
            ['menu_id' => 3, 'menu_title' => 'Categories', 'locale' => 'en', 'created_at' => Carbon::now()],
            ['menu_id' => 4, 'menu_title' => 'Berita', 'locale' => 'id', 'created_at' => Carbon::now()],
            ['menu_id' => 4, 'menu_title' => 'News', 'locale' => 'en', 'created_at' => Carbon::now()],
            ['menu_id' => 5, 'menu_title' => 'Label', 'locale' => 'id', 'created_at' => Carbon::now()],
            ['menu_id' => 5, 'menu_title' => 'Tags', 'locale' => 'en', 'created_at' => Carbon::now()],
            ['menu_id' => 6, 'menu_title' => 'Galeri', 'locale' => 'id', 'created_at' => Carbon::now()],
            ['menu_id' => 6, 'menu_title' => 'Gallery', 'locale' => 'en', 'created_at' => Carbon::now()],
            ['menu_id' => 7, 'menu_title' => 'Foto', 'locale' => 'id', 'created_at' => Carbon::now()],
            ['menu_id' => 7, 'menu_title' => 'Photos', 'locale' => 'en', 'created_at' => Carbon::now()],
            ['menu_id' => 8, 'menu_title' => 'Video', 'locale' => 'id', 'created_at' => Carbon::now()],
            ['menu_id' => 8, 'menu_title' => 'Videos', 'locale' => 'en', 'created_at' => Carbon::now()],
            ['menu_id' => 9, 'menu_title' => 'Pengaturan Banner', 'locale' => 'id', 'created_at' => Carbon::now()],
            ['menu_id' => 9, 'menu_title' => 'Banner Configuration', 'locale' => 'en', 'created_at' => Carbon::now()],
            ['menu_id' => 10, 'menu_title' => 'Slideshow', 'locale' => 'id', 'created_at' => Carbon::now()],
            ['menu_id' => 10, 'menu_title' => 'Slideshow', 'locale' => 'en', 'created_at' => Carbon::now()],
            ['menu_id' => 11, 'menu_title' => 'Banner', 'locale' => 'id', 'created_at' => Carbon::now()],
            ['menu_id' => 11, 'menu_title' => 'Banner', 'locale' => 'en', 'created_at' => Carbon::now()],
            ['menu_id' => 12, 'menu_title' => 'Pengaturan Sistem', 'locale' => 'id', 'created_at' => Carbon::now()],
            ['menu_id' => 12, 'menu_title' => 'System Settings', 'locale' => 'en', 'created_at' => Carbon::now()],
            ['menu_id' => 13, 'menu_title' => 'Pengaturan Web', 'locale' => 'id', 'created_at' => Carbon::now()],
            ['menu_id' => 13, 'menu_title' => 'Web Configuration', 'locale' => 'en', 'created_at' => Carbon::now()],
            ['menu_id' => 14, 'menu_title' => 'Pengaturan Kelompok', 'locale' => 'id', 'created_at' => Carbon::now()],
            ['menu_id' => 14, 'menu_title' => 'Groups Management', 'locale' => 'en', 'created_at' => Carbon::now()],
            ['menu_id' => 15, 'menu_title' => 'Pengaturan Pengguna', 'locale' => 'id', 'created_at' => Carbon::now()],
            ['menu_id' => 15, 'menu_title' => 'Users Management', 'locale' => 'en', 'created_at' => Carbon::now()],
            ['menu_id' => 16, 'menu_title' => 'Aktifitas Pengguna', 'locale' => 'id', 'created_at' => Carbon::now()],
            ['menu_id' => 16, 'menu_title' => 'Logs Activity', 'locale' => 'en', 'created_at' => Carbon::now()]
        ]);
    }
}
