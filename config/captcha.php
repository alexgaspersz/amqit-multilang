<?php

return [
    'secret' => env('NOCAPTCHA_SECRET', '6Lc1IOEUAAAAAEGGD6x4HJwc4q6wcWJcsbGo8UFh'),
    'sitekey' => env('NOCAPTCHA_SITEKEY', '6Lc1IOEUAAAAANw9_VIaBNi4CQEoMfrCtJKQ3K0K'),
    'options' => [
        'timeout' => 30,
    ],
];
