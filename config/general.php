<?php
/**
 * Description: General application config.
 * @package     amqit-mdb
 * @author      alex
 * @created     10/03/2020, modified: 11/03/2020 03:08
 * @copyright   Copyright (c) 2020.
 */

return [
    'app' => [
        'title' => env('APP_TITLE', ''),
        'version' => env('APP_VERSION', '3.0'),
        'alias' => env('APP_ALIAS', ''),
        'company' => env('APP_COMPANY', ''),
        'keywords' => env('APP_KEYWORD', ''),
        'description' => env('APP_DESC', ''),
        'logo' => env('APP_LOGO', ''),
        'generator' => env('APP_GEN', ''),
        'author' => env('APP_AUTHOR', ''),
        'email' => env('APP_EMAIL', ''),
        'copyright' => env('APP_COPYRIGHT', ''),
    ],
    'lang' => [
        'multi' => env('APP_MULTILANG', false),
        'main_lang' => env('APP_LOCALE', 'id'),
    ],
    'files' => [
        'upload' => env('UPLOAD_FOLDER',''),
        'image_extension' => env('IMAGE_EXT', ".jpg, .gif, .png, .jpeg"),
        'image_size' => env('IMAGE_SIZE', '2'),
        'image_mime' => env('IMAGE_MIME',''),
        'file_extension' => env('FILE_EXT', ".xls, .xlsx, .doc, .docx, .pdf"),
        'file_size' => env('FILE_SIZE', '2'),
        'file_mime' => env('FILE_MIME',''),
        'video_extension' => env('VIDEO_EXT', ".mp4"),
        'video_size' => env('VIDEO_SIZE','100'),
        'video_mime' => env('VIDEO_MIME','video/mp4'),
        'thumbsize' => env('THUMBS', null),
    ],
    'access' => [
        'registration' => env('ENABLE_REGISTRATION', true),
        'single_login' => env('SINGLE_LOGIN', true),
    ],
    'login' => [
        'captcha' => env('APP_CAPTCHA', false),
        'recaptcha' => env('APP_RECAPTCHA', false),
        'reset_password' => env('APP_RESET', false),
        'key' => env('APP_LOGIN_KEY', 'email'),
    ],
    'mail' => [
        'enabled' => env('APP_SENDMAIL', false),
    ],
    'timeout' => env('TIMEOUT', 2000),
    'seqcode' => env('SEQ_CODE', ''),
    'analytic' => env('APP_GA', ''),
    'limit' => env('PAGE_LIMIT', 10),
    'default_image' => env('DEFAULT_IMAGE', '')
];
