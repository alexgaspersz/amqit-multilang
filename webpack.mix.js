const mix = require('laravel-mix');
const webpack = require('webpack');

mix.setPublicPath('public')
    .sass('resources/sass/backend/app.scss', 'public/build/css/backend.css')
    .sass('resources/sass/backend/login.scss', 'public/build/css/login.css')
    .sass('resources/sass/error.scss', 'public/build/css/error.css')
    .js([
        'resources/js/backend/before.js',
        'resources/js/backend/app.js',
        'resources/js/backend/after.js'
    ], 'public/build/js/backend.js')
    .js('resources/js/backend/login.js', 'public/build/js/login.js')
    .scripts('resources/js/plugins/options.js', 'public/build/js/options.js')
    .copy([
        'resources/vendors/nestable/jquery.nestable.js',
        'resources/vendors/nestable/nestable.css'
    ], 'public/build/vendors/nestable/')
    .copy('resources/vendors/pdfjs/', 'public/build/vendors/pdfjs/')
    .copy('resources/vendors/froala/', 'public/build/vendors/froala/')
    .copy('resources/vendors/chartjs/Chart.min.js', 'public/build/vendors/chartjs/')
    .copy('resources/vendors/simpleclone/simpleclone.js', 'public/build/vendors/simpleclone/')
    .copy('resources/vendors/popupoverlay/jquery.popupoverlay.min.js', 'public/build/vendors/popupoverlay/')
    .copy('node_modules/dropzone/dist/min/', 'public/build/vendors/dropzone/')
    .copy('node_modules/pace-progress/pace.min.js', 'public/build/vendors/pace-progress/')
    .copy([
        'node_modules/moment/min/locales.min.js',
        'node_modules/moment/min/locales.min.js.map',
        'node_modules/moment/min/moment.min.js',
        'node_modules/moment/min/moment.min.js.map'
    ], 'public/build/vendors/moment/')
    .copy('resources/assets/fonts/', 'public/build/fonts/')
    .copy('resources/assets/icons/', 'public/build/icons/')
    .copy('resources/assets/images/', 'public/build/images/')
    .copy('node_modules/@coreui/icons/fonts', 'public/build/fonts/coreui/')
    .copy('node_modules/@coreui/icons/sprites/', 'public/build/fonts/coreui/sprites')
    .extract([
        'jquery',
        'popper.js',
        'bootstrap',
        'axios',
        'sweetalert2',
        'lodash',
        'ladda',
        'toastr'
    ])
    .options({
        processCssUrls: false,
    })
    .sourceMaps()
    .webpackConfig({
        plugins: [
            new webpack.ProgressPlugin(),
            new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/)
        ]
    })
    .browserSync({
        files: ["public/build/css/*.css", "public/build/js/*.js"]
    });

if (mix.inProduction()) {
    mix.version()
        .options({
            terser: {
                cache: true,
                parallel: true,
                sourceMap: true
            }
        });
} else {
    mix.webpackConfig({
        devtool: 'inline-source-map'
    });
}
